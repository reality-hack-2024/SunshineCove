# Sunshine Wall

Sunshine Wall is a 3 part project consisting of a Unity client, Node.js server, and React web interface.

## Unity Client

The Unity client is located in `/sunshine-unity/`. It has no special setup requirements. 

## Server

The Node.js server is located in `/server/`. It requires Node.js v18+ to run.

To start the server:

```
node index.js
```

## Web Interface 

The React web interface is a standard React app. It is located in `/React-App/` and can be started like any React project.

## License

This project is licensed under the MIT license. See [LICENSE](https://www.mit.edu/~amini/LICENSE.md) for details.
