using System;
using System.Collections;
using UnityEngine;

public class PhotoFrameBehavior : MonoBehaviour
{
    public MeshRenderer meshRenderer; // Assign the MeshRenderer of the object you want to apply the texture to.
    public string base64Image; // Declare base64Image as a public variable

    void Start()
    {
        // StartCoroutine(LoadTextureFromBase64(base64Image));
    }

    public IEnumerator LoadTextureFromBase64(string base64Image)
    {
        // Decode the base64 image
        byte[] imageBytes = Convert.FromBase64String(base64Image);

        // Create a new Texture2D
        Texture2D texture = new Texture2D(2, 2);

        // Load the image bytes into the texture
        if (texture.LoadImage(imageBytes))
        {
            // If the load was successful, apply the texture
            meshRenderer.material.mainTexture = texture;
        }
        else
        {
            Debug.LogError("Failed to load texture from base64 image");
        }

        yield return null;
    }
}