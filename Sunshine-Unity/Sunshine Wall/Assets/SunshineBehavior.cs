using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.XR.ARFoundation;
using UnityEngine.Networking;
using TMPro;

[Serializable]
public class Message
{
    public List<Item> items;
}

[Serializable]
public class Item
{
    public string type;
    public string value;
}

public class SunshineBehavior : MonoBehaviour
{
    public ARCameraManager _cameraManager;
    private Transform _cameraTransform;
    public ARRaycastManager _raycastManager;
    public GameObject SunshineWall;
    public GameObject SunshineFrame;
    public GameObject SunshineText;
    // Start is called before the first frame update

    private string lastImageVal = "";
    void Start()
    {
        _cameraTransform = _cameraManager.transform;
        // parseJSONMessage("[{\"type\":\"image\", \"value\": \"https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png\"}]");
        StartCoroutine(FetchJSONStateRoutine());

        FetchJSONState();
    }

    // Update is called once per frame
    void Update()
    {
        //CastRay();
    }

    IEnumerator FetchJSONStateRoutine()
    {
        while (true)
        {
            FetchJSONState();
            yield return new WaitForSeconds(10);
        }
    }

    void FetchJSONState()
    {
        // string URL = "http://10.29.182.142:8080/home";
        string URL = "https://cat-api.bripi.com/home";
        Debug.Log(URL);
        StartCoroutine(GetRequest(URL));
    }

    IEnumerator GetRequest(string url)
    {
        Newtonsoft.Json.Linq.JObject json = new Newtonsoft.Json.Linq.JObject();
        using (UnityWebRequest webRequest = UnityWebRequest.Get(url))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();
            Debug.Log(webRequest.downloadHandler.text);
            ParseJSONMessage(webRequest.downloadHandler.text);
        }
    }

    // Parse using Unity's JsonUtility

    void ParseJSONMessage(string jsonMessage)
    {
        List<Item> items = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Item>>(jsonMessage);

        // Now you can use the items list
        foreach (var item in items)
        {
            Debug.Log("Type: " + item.type + ", Value: " + item.value);
            if (item.type == "image")
            {

                try
                {
                    string url = item.value;
                    Debug.Log(url);

                    if (url == lastImageVal)
                    {
                        return;
                    }
                    lastImageVal = url;

                    if (url.Contains("http"))
                    {
                        StartCoroutine(LoadImage(url));
                    }
                    else
                    {
                        StartCoroutine(LoadTextureFromBase64(url));
                    }
                }
                catch (System.Exception)
                {

                    throw;
                }
            }
            
            if (item.type == "text" || item.type == "note")
            {
                string text = item.value;
                Debug.Log(text);

                // SunshineText.GetComponent<TextMeshPro>().text = text;
                GameObject.Find("NoteText").GetComponent<TextMeshProUGUI>().text = text;
            }
        }
    }

    public IEnumerator LoadTextureFromBase64(string base64Image)
    {
        // Remove the data URL scheme (if present)
        string base64Data = base64Image.StartsWith("data:")
            ? base64Image.Substring(base64Image.IndexOf(",") + 1)
            : base64Image;

        // Decode the base64 image
        byte[] imageBytes = Convert.FromBase64String(base64Data);

        // Create a new Texture2D
        Texture2D texture = new Texture2D(2, 2);

        // Load the image bytes into the texture
        if (texture.LoadImage(imageBytes))
        {
            // If the load was successful, apply the texture
            SunshineFrame.GetComponent<MeshRenderer>().material.mainTexture = texture;
        }
        else
        {
            Debug.LogError("Failed to load texture from base64 image");
        }

        yield return null;
    }

    IEnumerator LoadImage(string url)
    {
        using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(url))
        {
            // Send the request and wait for a response
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.Success)
            {
                // On success, get the downloaded texture
                Texture2D texture = DownloadHandlerTexture.GetContent(www);

                // Apply the texture to the object's material
                SunshineFrame.GetComponent<MeshRenderer>().material.mainTexture = texture;
            }
            else
            {
                Debug.Log(www.error);
            }
        }
    }

    private void CastRay()
    {
        Ray ray = new Ray(_cameraTransform.position, _cameraTransform.forward);

        List<ARRaycastHit> hitResults = new List<ARRaycastHit>();
        if (_raycastManager.Raycast(ray, hitResults))
        {
            Pose pose = hitResults[0].pose;
            SunshineWall.transform.position = pose.position;
            SunshineWall.transform.rotation = pose.rotation;
        }
        else
        {

        }
    }
}
