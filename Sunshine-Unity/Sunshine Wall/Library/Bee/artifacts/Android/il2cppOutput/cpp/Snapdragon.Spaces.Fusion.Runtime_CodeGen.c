﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





extern void UnitySourceGeneratedAssemblyMonoScriptTypes_v1_Get_m1BFA6340E0E31846BD3F39C8D4A24DFAA4B7F65D (void);
extern void UnitySourceGeneratedAssemblyMonoScriptTypes_v1__ctor_mE7A216AE74E9610C0E5BFA4CFFF3337607494101 (void);
extern void FusionFeature__ctor_mA3F45EC07A296491517D827B4FD7B4A801343A81 (void);
extern void FusionLogic_Awake_m19C03A5EEE37916CCF1BE869224681B7A299DC33 (void);
extern void FusionLogic__ctor_m5DF6AF5D959C99F76F7D63B07671E3134688EC7D (void);
static Il2CppMethodPointer s_methodPointers[5] = 
{
	UnitySourceGeneratedAssemblyMonoScriptTypes_v1_Get_m1BFA6340E0E31846BD3F39C8D4A24DFAA4B7F65D,
	UnitySourceGeneratedAssemblyMonoScriptTypes_v1__ctor_mE7A216AE74E9610C0E5BFA4CFFF3337607494101,
	FusionFeature__ctor_mA3F45EC07A296491517D827B4FD7B4A801343A81,
	FusionLogic_Awake_m19C03A5EEE37916CCF1BE869224681B7A299DC33,
	FusionLogic__ctor_m5DF6AF5D959C99F76F7D63B07671E3134688EC7D,
};
static const int32_t s_InvokerIndices[5] = 
{
	15016,
	8910,
	8910,
	8910,
	8910,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Snapdragon_Spaces_Fusion_Runtime_CodeGenModule;
const Il2CppCodeGenModule g_Snapdragon_Spaces_Fusion_Runtime_CodeGenModule = 
{
	"Snapdragon.Spaces.Fusion.Runtime.dll",
	5,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
