/******************************************************************************
 * File: SpacesQrCodeManager.cs
 * Copyright (c) 2023 Qualcomm Technologies, Inc. and/or its subsidiaries. All rights reserved.
 *
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 ******************************************************************************/

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

namespace Qualcomm.Snapdragon.Spaces
{
    [Serializable]
    [DisallowMultipleComponent]
    [DefaultExecutionOrder(int.MinValue + 1)]
    [RequireComponent(typeof(ARSessionOrigin))]
    public sealed class SpacesQrCodeManager : ARTrackableManager<XRQrCodeTrackingSubsystem, XRQrCodeTrackingSubsystemDescriptor, XRQrCodeTrackingSubsystem.Provider, XRTrackedMarker, SpacesARMarker>
    {
        [SerializeField]
        [Tooltip("If not null, instantiates this prefab for each detected marker.")]
        private GameObject _markerPrefab;

        public GameObject markerPrefab
        {
            get => _markerPrefab;
            set => _markerPrefab = value;
        }

        [SerializeField]
        private bool _markerTracking;

        private byte _minQrVersionSupported = 1;
        private byte _maxQrVersionSupported = 10;

        [Tooltip("The marker tracking mode to be used when tracking markers.")]
        public MarkerTrackingMode markerTrackingMode = MarkerTrackingMode.Dynamic;

        [Tooltip("Defines the width and height (in meters) of the physical markers that will be detected.")]
        public Vector2 markerSize = new(0.1f, 0.1f);

        [Tooltip("Defines the inclusive minimum version. Should be smaller than 'maxQrVersion'.")]
        public byte minQrVersion = 1;

        [Tooltip("Defines the inclusive maximum version. Should be bigger than 'minQrVersion'.")]
        public byte maxQrVersion = 10;

        public bool MarkerTracking {
            get => _markerTracking;
            set {
                subsystem.EnableMarkerTracking(value);
                _markerTracking = value;
            }
        }

        public event Action<SpacesMarkersChangedEventArgs> markersChanged;

        protected override void OnEnable()
        {
            base.OnEnable();

            minQrVersion = Math.Clamp(minQrVersion, _minQrVersionSupported, _maxQrVersionSupported);
            maxQrVersion = Math.Clamp(maxQrVersion, _minQrVersionSupported, _maxQrVersionSupported);
            subsystem.SetMarkerDescriptor(new XRMarkerDescriptor(
                markerTrackingMode, markerSize,
                new Tuple<byte, byte>(
                    minQrVersion,
                    maxQrVersion)));
            subsystem.EnableMarkerTracking(_markerTracking);
        }

        protected override GameObject GetPrefab() => _markerPrefab;

        protected override string gameObjectName => nameof(SpacesARMarker);

        protected override void OnAfterSetSessionRelativeData(
            SpacesARMarker marker,
            XRTrackedMarker sessionRelativeData)
        {
            if (!marker.IsMarkerDataAvailable)
            {
                marker.TryGetMarkerData(subsystem);
            }
        }

        protected override void OnTrackablesChanged(
            List<SpacesARMarker> added,
            List<SpacesARMarker> updated,
            List<SpacesARMarker> removed)
        {
            markersChanged?.Invoke(new SpacesMarkersChangedEventArgs(added, updated, removed));
        }

        private void OnValidate()
        {
            minQrVersion = Math.Clamp(minQrVersion, _minQrVersionSupported, _maxQrVersionSupported);
            maxQrVersion = Math.Clamp(maxQrVersion, _minQrVersionSupported, _maxQrVersionSupported);
        }
    }
}
