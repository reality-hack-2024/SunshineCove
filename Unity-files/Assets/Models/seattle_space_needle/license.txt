Model Information:
* title:	Seattle Space Needle
* source:	https://sketchfab.com/3d-models/seattle-space-needle-977df37b5403494bb83a49c4328f9204
* author:	HarryTrinder (https://sketchfab.com/HarryTrinder)

Model License:
* license type:	CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)
* requirements:	Author must be credited. Commercial use is allowed.

If you use this 3D model in your project be sure to copy paste this credit wherever you share it:
This work is based on "Seattle Space Needle" (https://sketchfab.com/3d-models/seattle-space-needle-977df37b5403494bb83a49c4328f9204) by HarryTrinder (https://sketchfab.com/HarryTrinder) licensed under CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)