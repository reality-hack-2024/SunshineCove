using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

public class SunshineBehavior : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        parseJSONMessage("[{\"type\":\"image\", \"value\": \"https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png\"}]");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    async void parseJSONMessage(string jsonMessage)
    {
        List<Dictionary<string, string>> message = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(jsonMessage);
        foreach (Dictionary<string, string> item in message)
        {
            if (item["type"] == "image")
            {
                string url = item["value"];
                Debug.Log(url);
                // StartCoroutine(LoadImage(url));
            }
        }
    }
}
