﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"


extern const RuntimeMethod* BaseRuntimeFeature_OnFrameStateUpdate_m73151720BB25328366DD7B8665017ED420FC58B2_RuntimeMethod_var;
extern const RuntimeMethod* BaseRuntimeFeature_RunOnRenderThreadWithData_m02CE392AA2B19CCA6467DCA7CE3B166EDC6E1928_RuntimeMethod_var;
extern const RuntimeMethod* BaseRuntimeFeature_RunOnRenderThread_m2E8512A10A77075A41C26E119959C00AF6BD3B58_RuntimeMethod_var;



// 0x00000001 SpacesCpuImageApi SpacesCpuImageApi::get_instance()
extern void SpacesCpuImageApi_get_instance_m52026DD599BF14F810E05FAB14C440C9220A235C (void);
// 0x00000002 System.Void SpacesCpuImageApi::set_instance(SpacesCpuImageApi)
extern void SpacesCpuImageApi_set_instance_mC2CC50C31516CAF05A524AF1D075A1D382993C90 (void);
// 0x00000003 SpacesCpuImageApi SpacesCpuImageApi::CreateInstance()
extern void SpacesCpuImageApi_CreateInstance_mD2D35E2A4323F98549A001BFBEA1B680D57320FE (void);
// 0x00000004 System.Boolean SpacesCpuImageApi::NativeHandleValid(System.Int32)
extern void SpacesCpuImageApi_NativeHandleValid_m6833E3FBA0B12936487CC709E4056677C6A6D284 (void);
// 0x00000005 System.Boolean SpacesCpuImageApi::FormatSupported(UnityEngine.XR.ARSubsystems.XRCpuImage,UnityEngine.TextureFormat)
extern void SpacesCpuImageApi_FormatSupported_mE16E8EA909EBBBB77E0D6CF163E6E7D6A3E930C8 (void);
// 0x00000006 System.Boolean SpacesCpuImageApi::TryGetPlane(System.Int32,System.Int32,UnityEngine.XR.ARSubsystems.XRCpuImage/Plane/Cinfo&)
extern void SpacesCpuImageApi_TryGetPlane_m13804BD415C91C343566F40D745AF215F33D4330 (void);
// 0x00000007 System.Boolean SpacesCpuImageApi::TryGetConvertedDataSize(System.Int32,UnityEngine.Vector2Int,UnityEngine.TextureFormat,System.Int32&)
extern void SpacesCpuImageApi_TryGetConvertedDataSize_mD7CA34E4B2F25CB4DD9B1CB83485061770DC2E31 (void);
// 0x00000008 System.Boolean SpacesCpuImageApi::TryConvert(System.Int32,UnityEngine.XR.ARSubsystems.XRCpuImage/ConversionParams,System.IntPtr,System.Int32)
extern void SpacesCpuImageApi_TryConvert_mC78DE87A75B48DCA0FFD69BE1FEC492939B05DB8 (void);
// 0x00000009 System.Void SpacesCpuImageApi::DisposeImage(System.Int32)
extern void SpacesCpuImageApi_DisposeImage_m5A5CEA4F0DCF2215F832B8E311F431AF7FB6D498 (void);
// 0x0000000A System.Void SpacesCpuImageApi::.ctor()
extern void SpacesCpuImageApi__ctor_mEABA309E40AD779F0F734430AEE138124808A229 (void);
// 0x0000000B System.Void Qualcomm.Snapdragon.Spaces.SpacesObsoleteVersionAttribute::.ctor(System.String)
extern void SpacesObsoleteVersionAttribute__ctor_mBF135C88AB4383CB2F5E0F8032796D1793D4D5B5 (void);
// 0x0000000C System.Void Qualcomm.Snapdragon.Spaces.SpacesObsoleteVersionAttribute::.ctor(System.String,System.Boolean)
extern void SpacesObsoleteVersionAttribute__ctor_m0B592C136EE3EFDB05AC6A04EF40D25A72C1B98F (void);
// 0x0000000D System.Void Qualcomm.Snapdragon.Spaces.SpacesObsoleteVersionAttribute::.ctor(System.String,System.String)
extern void SpacesObsoleteVersionAttribute__ctor_m79DED243DA49BC684ED2C7282543DFD507A6918F (void);
// 0x0000000E System.Void Qualcomm.Snapdragon.Spaces.SpacesObsoleteVersionAttribute::.ctor(System.String,System.String,System.Boolean)
extern void SpacesObsoleteVersionAttribute__ctor_mD432687FAE899722D72BD3CAD0B61C1DE049E876 (void);
// 0x0000000F System.UInt32 Qualcomm.Snapdragon.Spaces.CameraAccessFeature::get_RuntimeMaxVerticalResolution()
extern void CameraAccessFeature_get_RuntimeMaxVerticalResolution_mEB7DC478FB922B4C238821BD6E866238A6495BB9 (void);
// 0x00000010 System.Void Qualcomm.Snapdragon.Spaces.CameraAccessFeature::set_RuntimeMaxVerticalResolution(System.UInt32)
extern void CameraAccessFeature_set_RuntimeMaxVerticalResolution_mC74123DE5B57AC38240BCA7D8F3600B0CC35929D (void);
// 0x00000011 System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.XrCameraInfoQCOM> Qualcomm.Snapdragon.Spaces.CameraAccessFeature::get_CameraInfos()
extern void CameraAccessFeature_get_CameraInfos_mB2D2FA3424DF6A66DBD460F2801D4B55F2DF5FB1 (void);
// 0x00000012 System.UInt32 Qualcomm.Snapdragon.Spaces.CameraAccessFeature::get_DownsamplingFactor()
extern void CameraAccessFeature_get_DownsamplingFactor_m7E6601DAF1289B22356B5941C421CBC8B4D3B5BD (void);
// 0x00000013 Qualcomm.Snapdragon.Spaces.XrCameraSensorPropertiesQCOM[] Qualcomm.Snapdragon.Spaces.CameraAccessFeature::get_SensorProperties()
extern void CameraAccessFeature_get_SensorProperties_mEEF0AB61DB904251A0A1BE5666B0D5D6F794CB97 (void);
// 0x00000014 Qualcomm.Snapdragon.Spaces.XrCameraFrameDataQCOM Qualcomm.Snapdragon.Spaces.CameraAccessFeature::get_CachedFrameData()
extern void CameraAccessFeature_get_CachedFrameData_mEF13E91DA520B822818A70FE38DCB364C84BC7B9 (void);
// 0x00000015 Qualcomm.Snapdragon.Spaces.SpacesYUVFrame Qualcomm.Snapdragon.Spaces.CameraAccessFeature::get_CachedYuvFrame()
extern void CameraAccessFeature_get_CachedYuvFrame_m3AE38BAE1D034B92E65A8A7B007DF3AD232C438E (void);
// 0x00000016 Qualcomm.Snapdragon.Spaces.XrCameraFrameBufferQCOM[] Qualcomm.Snapdragon.Spaces.CameraAccessFeature::get_FrameBuffers()
extern void CameraAccessFeature_get_FrameBuffers_mD9F75CCDCBEBE8A7180B5684694F0AF1F2C68812 (void);
// 0x00000017 UnityEngine.Pose Qualcomm.Snapdragon.Spaces.CameraAccessFeature::get_LastFramePose()
extern void CameraAccessFeature_get_LastFramePose_m4C65EFFDE33A73DFA5BA9F67DB09C37CA74E7EFD (void);
// 0x00000018 System.Boolean Qualcomm.Snapdragon.Spaces.CameraAccessFeature::get_IsRequiringBaseRuntimeFeature()
extern void CameraAccessFeature_get_IsRequiringBaseRuntimeFeature_m38A8A9B037F0E371BEB211241D9480779F54FE98 (void);
// 0x00000019 System.String Qualcomm.Snapdragon.Spaces.CameraAccessFeature::GetXrLayersToLoad()
extern void CameraAccessFeature_GetXrLayersToLoad_mEE9DC616AE93F79F1B9D2BD0149D2292FF0DD4EF (void);
// 0x0000001A System.Boolean Qualcomm.Snapdragon.Spaces.CameraAccessFeature::OnInstanceCreate(System.UInt64)
extern void CameraAccessFeature_OnInstanceCreate_m695E43BD3678CD6F367DC84D67CBA5BDA1B88D77 (void);
// 0x0000001B System.Void Qualcomm.Snapdragon.Spaces.CameraAccessFeature::OnSubsystemCreate()
extern void CameraAccessFeature_OnSubsystemCreate_m58CDD035F80484017B2E00B679C1F3BB6EE21279 (void);
// 0x0000001C System.Void Qualcomm.Snapdragon.Spaces.CameraAccessFeature::OnSubsystemStop()
extern void CameraAccessFeature_OnSubsystemStop_m9263D15D132C5D6365E18244F70DD83AA1EC0F4C (void);
// 0x0000001D System.Void Qualcomm.Snapdragon.Spaces.CameraAccessFeature::OnSubsystemDestroy()
extern void CameraAccessFeature_OnSubsystemDestroy_m055C874107972FAAAFD5950E5B1B47BC3CE60999 (void);
// 0x0000001E System.Void Qualcomm.Snapdragon.Spaces.CameraAccessFeature::OnHookMethods()
extern void CameraAccessFeature_OnHookMethods_m619B53413C22FD4E26305F85D84512A6FD468944 (void);
// 0x0000001F System.Boolean Qualcomm.Snapdragon.Spaces.CameraAccessFeature::TryEnumerateCameras()
extern void CameraAccessFeature_TryEnumerateCameras_m8E03ED6547D20107C69585A13A1245B9F7272927 (void);
// 0x00000020 System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.XrCameraFrameConfigurationQCOM> Qualcomm.Snapdragon.Spaces.CameraAccessFeature::TryGetSupportedFrameConfigurations(System.String)
extern void CameraAccessFeature_TryGetSupportedFrameConfigurations_mC38236D225D15815B89FF2669D29071F6116F3E9 (void);
// 0x00000021 System.Boolean Qualcomm.Snapdragon.Spaces.CameraAccessFeature::TryCreateCameraHandle(System.UInt64&,System.String)
extern void CameraAccessFeature_TryCreateCameraHandle_m204AA2A45E7390120BEB69E96A52AE8B691D8D8A (void);
// 0x00000022 System.Boolean Qualcomm.Snapdragon.Spaces.CameraAccessFeature::TryReleaseCameraHandle(System.UInt64)
extern void CameraAccessFeature_TryReleaseCameraHandle_m5BC9A9F88CFB97ADA191535E2F118678857FEC90 (void);
// 0x00000023 System.Boolean Qualcomm.Snapdragon.Spaces.CameraAccessFeature::TryAccessFrame(System.UInt64,Qualcomm.Snapdragon.Spaces.XrCameraFrameConfigurationQCOM,System.UInt32)
extern void CameraAccessFeature_TryAccessFrame_m7123B2CF6E26981284E1B7A069208C12C4F9D54D (void);
// 0x00000024 System.Boolean Qualcomm.Snapdragon.Spaces.CameraAccessFeature::TryReleaseFrame()
extern void CameraAccessFeature_TryReleaseFrame_m4F36A24C523B6DB6E6E94B77A0DCB67909D9F6AA (void);
// 0x00000025 System.Boolean Qualcomm.Snapdragon.Spaces.CameraAccessFeature::XrTimeFromSpacesTimespec(Qualcomm.Snapdragon.Spaces.Timespec,System.Int64&)
extern void CameraAccessFeature_XrTimeFromSpacesTimespec_m81156D4491D1CD6F13BE822E4D690D81A3002275 (void);
// 0x00000026 System.Boolean Qualcomm.Snapdragon.Spaces.CameraAccessFeature::SpacesTimespecFromXRTime(System.Int64,Qualcomm.Snapdragon.Spaces.Timespec&)
extern void CameraAccessFeature_SpacesTimespecFromXRTime_m09AE123B935E6D36DD5D9E140DDF4E3D43E57EBC (void);
// 0x00000027 System.Boolean Qualcomm.Snapdragon.Spaces.CameraAccessFeature::TryExtractFrame(Qualcomm.Snapdragon.Spaces.XrCameraFrameConfigurationQCOM,Qualcomm.Snapdragon.Spaces.XrCameraSensorPropertiesQCOM[],Qualcomm.Snapdragon.Spaces.XrCameraFrameDataQCOM,Qualcomm.Snapdragon.Spaces.XrCameraFrameBufferQCOM[],Qualcomm.Snapdragon.Spaces.SpacesYUVFrame&)
extern void CameraAccessFeature_TryExtractFrame_m1645E9140376D6A01B354F0164A2DE2F494F7818 (void);
// 0x00000028 System.Void Qualcomm.Snapdragon.Spaces.CameraAccessFeature::ClearFrameCache()
extern void CameraAccessFeature_ClearFrameCache_mD26961080756CF989A81B67E7F1620CEB4FA967B (void);
// 0x00000029 System.Void Qualcomm.Snapdragon.Spaces.CameraAccessFeature::UpdateDownsamplingFactor(Qualcomm.Snapdragon.Spaces.XrCameraFrameConfigurationQCOM)
extern void CameraAccessFeature_UpdateDownsamplingFactor_m4D408469CCD3A1710EEC06B918EA87F4A005E407 (void);
// 0x0000002A System.Void Qualcomm.Snapdragon.Spaces.CameraAccessFeature::.ctor()
extern void CameraAccessFeature__ctor_mD408BBD7ADD3A6B86EC2F9BE98FD382D39EFD11E (void);
// 0x0000002B System.Void Qualcomm.Snapdragon.Spaces.CameraAccessFeature::.cctor()
extern void CameraAccessFeature__cctor_m53840006C7E771132E56D375082E2608F70961A2 (void);
// 0x0000002C System.Void Qualcomm.Snapdragon.Spaces.CameraAccessFeature/EnumerateCamerasQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void EnumerateCamerasQCOMDelegate__ctor_m37CD16E3E70D6E40FCBD233B065254A759478795 (void);
// 0x0000002D Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.CameraAccessFeature/EnumerateCamerasQCOMDelegate::Invoke(System.UInt64,System.UInt32,System.UInt32&,System.IntPtr)
extern void EnumerateCamerasQCOMDelegate_Invoke_mED2B205E17AEB6156F8404C3C8B494DBA9DE7F25 (void);
// 0x0000002E System.IAsyncResult Qualcomm.Snapdragon.Spaces.CameraAccessFeature/EnumerateCamerasQCOMDelegate::BeginInvoke(System.UInt64,System.UInt32,System.UInt32&,System.IntPtr,System.AsyncCallback,System.Object)
extern void EnumerateCamerasQCOMDelegate_BeginInvoke_m1C593EB5BCA3609C8FB2CA493A5D17AEE4BCF4AA (void);
// 0x0000002F Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.CameraAccessFeature/EnumerateCamerasQCOMDelegate::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void EnumerateCamerasQCOMDelegate_EndInvoke_mD1D20D59F14EE2F5AEF1B0FD6AE78D4E9D266919 (void);
// 0x00000030 System.Void Qualcomm.Snapdragon.Spaces.CameraAccessFeature/GetSupportedFrameConfigurationsQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void GetSupportedFrameConfigurationsQCOMDelegate__ctor_m18D3C4A32F213A088A4804F3227295CFF9E9F28D (void);
// 0x00000031 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.CameraAccessFeature/GetSupportedFrameConfigurationsQCOMDelegate::Invoke(System.UInt64,System.String,System.UInt32,System.UInt32&,System.IntPtr)
extern void GetSupportedFrameConfigurationsQCOMDelegate_Invoke_mE01AB124BFB62A7CDDE0B2D462A1CD7A53359BB8 (void);
// 0x00000032 System.IAsyncResult Qualcomm.Snapdragon.Spaces.CameraAccessFeature/GetSupportedFrameConfigurationsQCOMDelegate::BeginInvoke(System.UInt64,System.String,System.UInt32,System.UInt32&,System.IntPtr,System.AsyncCallback,System.Object)
extern void GetSupportedFrameConfigurationsQCOMDelegate_BeginInvoke_m94576B47CF55542DD0EE8121D42116E1988E0399 (void);
// 0x00000033 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.CameraAccessFeature/GetSupportedFrameConfigurationsQCOMDelegate::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void GetSupportedFrameConfigurationsQCOMDelegate_EndInvoke_mC56C0A4DEFAE133AB514C89337DD12DEFE15698B (void);
// 0x00000034 System.Void Qualcomm.Snapdragon.Spaces.CameraAccessFeature/CreateCameraHandleQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void CreateCameraHandleQCOMDelegate__ctor_m15BA12D33D9A1A02B887DACEE25071D6606538C4 (void);
// 0x00000035 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.CameraAccessFeature/CreateCameraHandleQCOMDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrCameraActivationInfoQCOM&,System.UInt64&)
extern void CreateCameraHandleQCOMDelegate_Invoke_m32EB04874065CC21E70B36C735930AB5DBC11B6C (void);
// 0x00000036 System.IAsyncResult Qualcomm.Snapdragon.Spaces.CameraAccessFeature/CreateCameraHandleQCOMDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrCameraActivationInfoQCOM&,System.UInt64&,System.AsyncCallback,System.Object)
extern void CreateCameraHandleQCOMDelegate_BeginInvoke_mB887E2896F6E7060D3429554D37E20DE9199A60E (void);
// 0x00000037 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.CameraAccessFeature/CreateCameraHandleQCOMDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrCameraActivationInfoQCOM&,System.UInt64&,System.IAsyncResult)
extern void CreateCameraHandleQCOMDelegate_EndInvoke_mEC77230885EFC6CE89661061A86377476A92304C (void);
// 0x00000038 System.Void Qualcomm.Snapdragon.Spaces.CameraAccessFeature/ReleaseCameraHandleQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void ReleaseCameraHandleQCOMDelegate__ctor_m2DB153D228BBE69330514FCC5563062CCA7AB3A1 (void);
// 0x00000039 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.CameraAccessFeature/ReleaseCameraHandleQCOMDelegate::Invoke(System.UInt64)
extern void ReleaseCameraHandleQCOMDelegate_Invoke_m2D86AB36F832F0E414D06CFE868179DC1603D275 (void);
// 0x0000003A System.IAsyncResult Qualcomm.Snapdragon.Spaces.CameraAccessFeature/ReleaseCameraHandleQCOMDelegate::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void ReleaseCameraHandleQCOMDelegate_BeginInvoke_m7E674429AEB1E59C9F58F403F1D7DB1B3C61BCA6 (void);
// 0x0000003B Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.CameraAccessFeature/ReleaseCameraHandleQCOMDelegate::EndInvoke(System.IAsyncResult)
extern void ReleaseCameraHandleQCOMDelegate_EndInvoke_mBD74CDF8D300C7489CF4C29065B545FE201654A6 (void);
// 0x0000003C System.Void Qualcomm.Snapdragon.Spaces.CameraAccessFeature/AccessFrameQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void AccessFrameQCOMDelegate__ctor_mCF62BE8A680D792C1447E433C40C26D7B8147F5C (void);
// 0x0000003D Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.CameraAccessFeature/AccessFrameQCOMDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrCameraFrameDataQCOM&,Qualcomm.Snapdragon.Spaces.XrCameraFrameBuffersQCOM&)
extern void AccessFrameQCOMDelegate_Invoke_m236B9BE1F139F4219ABA76D5F6E2E930E8184B8D (void);
// 0x0000003E System.IAsyncResult Qualcomm.Snapdragon.Spaces.CameraAccessFeature/AccessFrameQCOMDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrCameraFrameDataQCOM&,Qualcomm.Snapdragon.Spaces.XrCameraFrameBuffersQCOM&,System.AsyncCallback,System.Object)
extern void AccessFrameQCOMDelegate_BeginInvoke_m3AD3C18909020FE3909B379B7C5204D16C331B78 (void);
// 0x0000003F Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.CameraAccessFeature/AccessFrameQCOMDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrCameraFrameDataQCOM&,Qualcomm.Snapdragon.Spaces.XrCameraFrameBuffersQCOM&,System.IAsyncResult)
extern void AccessFrameQCOMDelegate_EndInvoke_mFA28EE77ED59137720F00DB467D26DF8C4544F34 (void);
// 0x00000040 System.Void Qualcomm.Snapdragon.Spaces.CameraAccessFeature/ReleaseFrameQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void ReleaseFrameQCOMDelegate__ctor_m3AE4007AF52F03114A6208E0CF827FBF949DC83A (void);
// 0x00000041 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.CameraAccessFeature/ReleaseFrameQCOMDelegate::Invoke(System.UInt64)
extern void ReleaseFrameQCOMDelegate_Invoke_mD453F7B5B005BBAB2C39DF55A2792575836736B9 (void);
// 0x00000042 System.IAsyncResult Qualcomm.Snapdragon.Spaces.CameraAccessFeature/ReleaseFrameQCOMDelegate::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void ReleaseFrameQCOMDelegate_BeginInvoke_mED908AD4A41B61DF879C6DF1371603CC22A22F36 (void);
// 0x00000043 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.CameraAccessFeature/ReleaseFrameQCOMDelegate::EndInvoke(System.IAsyncResult)
extern void ReleaseFrameQCOMDelegate_EndInvoke_m0243213313A0A8789A42A950DFAE5C93E30B5AD1 (void);
// 0x00000044 System.Void Qualcomm.Snapdragon.Spaces.CameraAccessFeature/xrConvertTimespecTimeToTimeKHR::.ctor(System.Object,System.IntPtr)
extern void xrConvertTimespecTimeToTimeKHR__ctor_mC9D1DB707C68D7E368D3B371D73AC8178F87B7E0 (void);
// 0x00000045 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.CameraAccessFeature/xrConvertTimespecTimeToTimeKHR::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.Timespec&,System.Int64&)
extern void xrConvertTimespecTimeToTimeKHR_Invoke_m77ABACB9235F1555E1ADF18F2DB9E18C10A75440 (void);
// 0x00000046 System.IAsyncResult Qualcomm.Snapdragon.Spaces.CameraAccessFeature/xrConvertTimespecTimeToTimeKHR::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.Timespec&,System.Int64&,System.AsyncCallback,System.Object)
extern void xrConvertTimespecTimeToTimeKHR_BeginInvoke_m0315D219DB07FF3DB6663AE9670103863886CC63 (void);
// 0x00000047 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.CameraAccessFeature/xrConvertTimespecTimeToTimeKHR::EndInvoke(Qualcomm.Snapdragon.Spaces.Timespec&,System.Int64&,System.IAsyncResult)
extern void xrConvertTimespecTimeToTimeKHR_EndInvoke_m75500BF399CEF6DDF64A3A5A6D80A7BA9AB65FBC (void);
// 0x00000048 System.Void Qualcomm.Snapdragon.Spaces.CameraAccessFeature/xrConvertTimeToTimespecTimeKHR::.ctor(System.Object,System.IntPtr)
extern void xrConvertTimeToTimespecTimeKHR__ctor_mD68289EC0A72106E26A652F3181D51B356A37355 (void);
// 0x00000049 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.CameraAccessFeature/xrConvertTimeToTimespecTimeKHR::Invoke(System.UInt64,System.Int64,Qualcomm.Snapdragon.Spaces.Timespec&)
extern void xrConvertTimeToTimespecTimeKHR_Invoke_m4476300A0A4F0444CB09E2490E5E9A2DFC51642D (void);
// 0x0000004A System.IAsyncResult Qualcomm.Snapdragon.Spaces.CameraAccessFeature/xrConvertTimeToTimespecTimeKHR::BeginInvoke(System.UInt64,System.Int64,Qualcomm.Snapdragon.Spaces.Timespec&,System.AsyncCallback,System.Object)
extern void xrConvertTimeToTimespecTimeKHR_BeginInvoke_m767DA85E0E7FC07AC89E5512FF646AE66542EA11 (void);
// 0x0000004B Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.CameraAccessFeature/xrConvertTimeToTimespecTimeKHR::EndInvoke(Qualcomm.Snapdragon.Spaces.Timespec&,System.IAsyncResult)
extern void xrConvertTimeToTimespecTimeKHR_EndInvoke_mB5B58B2E657B60E7497C5BE9610DB253D622D890 (void);
// 0x0000004C System.Void Qualcomm.Snapdragon.Spaces.CameraSubsystem::RegisterDescriptor()
extern void CameraSubsystem_RegisterDescriptor_m248B6032D68D4232138D61CB9FF7466C84D7A654 (void);
// 0x0000004D System.Void Qualcomm.Snapdragon.Spaces.CameraSubsystem::.ctor()
extern void CameraSubsystem__ctor_m8F8E0937C1BAAB102152D1505413C2E2504DA7FD (void);
// 0x0000004E UnityEngine.XR.ARSubsystems.XRCpuImage/Api Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::get_cpuImageApi()
extern void CameraProvider_get_cpuImageApi_m787BE7DC53BC38BEF9D323E92285195E8E91D323 (void);
// 0x0000004F UnityEngine.Material Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::get_cameraMaterial()
extern void CameraProvider_get_cameraMaterial_m4749B8ED8DD5E0A5023AF841720A3DB788A1CE44 (void);
// 0x00000050 System.Boolean Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::get_permissionGranted()
extern void CameraProvider_get_permissionGranted_m066ACAC33143AB2AAADE13F2AF831FC7AD976B84 (void);
// 0x00000051 System.Boolean Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::get_invertCulling()
extern void CameraProvider_get_invertCulling_m18054D2DC109DFFE134C54F7E72D27D1654129FB (void);
// 0x00000052 UnityEngine.XR.ARSubsystems.Feature Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::get_currentCamera()
extern void CameraProvider_get_currentCamera_mAA3EB34AE060CF24C4B1722F24C76B99C7EA1DD4 (void);
// 0x00000053 UnityEngine.XR.ARSubsystems.Feature Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::get_requestedCamera()
extern void CameraProvider_get_requestedCamera_mFB65322F5843AF1DF29308D94CF31D821C472172 (void);
// 0x00000054 System.Void Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::set_requestedCamera(UnityEngine.XR.ARSubsystems.Feature)
extern void CameraProvider_set_requestedCamera_m0D2FE593DDE310383C0B5A3D7F496D5D5A9BBD13 (void);
// 0x00000055 System.Boolean Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::get_autoFocusEnabled()
extern void CameraProvider_get_autoFocusEnabled_m127753A0986432D723CCAFDAFC6FE3259E8B5F01 (void);
// 0x00000056 System.Boolean Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::get_autoFocusRequested()
extern void CameraProvider_get_autoFocusRequested_m923BA5E154B0EA1FF6D9E84CE2AA0EBE7A86DD47 (void);
// 0x00000057 System.Void Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::set_autoFocusRequested(System.Boolean)
extern void CameraProvider_set_autoFocusRequested_m2122BED391E54D2B6E7229D61C20B6B78CB89B14 (void);
// 0x00000058 UnityEngine.XR.ARSubsystems.Feature Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::get_currentLightEstimation()
extern void CameraProvider_get_currentLightEstimation_mB16784DAB070888E8612CF1035B4D6EA3E86EF34 (void);
// 0x00000059 UnityEngine.XR.ARSubsystems.Feature Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::get_requestedLightEstimation()
extern void CameraProvider_get_requestedLightEstimation_m95B818B69C3E356C3709FA42C842F26423FC5ED8 (void);
// 0x0000005A System.Void Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::set_requestedLightEstimation(UnityEngine.XR.ARSubsystems.Feature)
extern void CameraProvider_set_requestedLightEstimation_m45BECB29A4837E63416D0CEF8B50CF54E4679621 (void);
// 0x0000005B System.Nullable`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration> Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::get_currentConfiguration()
extern void CameraProvider_get_currentConfiguration_mFC94ACF7171BB206224DE73B0978D80B109AF94C (void);
// 0x0000005C System.Void Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::set_currentConfiguration(System.Nullable`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration>)
extern void CameraProvider_set_currentConfiguration_m2A05A938A3D2A203368ED4F19E5C28B1FC39F0C0 (void);
// 0x0000005D System.Void Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::Start()
extern void CameraProvider_Start_m345019FC1E9F1CD9F1C5AB85E0CF9F09854EA762 (void);
// 0x0000005E System.Void Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::Stop()
extern void CameraProvider_Stop_m642A6667A5EF8F1343DCBDDECC55CDC7752C6D2C (void);
// 0x0000005F System.Void Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::Destroy()
extern void CameraProvider_Destroy_m73A269A94E9D98CCBD2FD3A92BCFA07793CEEBB5 (void);
// 0x00000060 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRCameraConfiguration> Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::GetConfigurations(UnityEngine.XR.ARSubsystems.XRCameraConfiguration,Unity.Collections.Allocator)
extern void CameraProvider_GetConfigurations_mE6A6CDDDEE84B7A06B8CA4B55B43D20C35B5B6F9 (void);
// 0x00000061 System.Boolean Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::TryGetIntrinsics(UnityEngine.XR.ARSubsystems.XRCameraIntrinsics&)
extern void CameraProvider_TryGetIntrinsics_m1A56AAC35F8867E4FE751DBA1F7EB8F036A0B508 (void);
// 0x00000062 System.Boolean Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::TryAcquireLatestCpuImage(UnityEngine.XR.ARSubsystems.XRCpuImage/Cinfo&)
extern void CameraProvider_TryAcquireLatestCpuImage_mCF734CD72FD92CF1D0D3A2006AE1725BAD3BABC9 (void);
// 0x00000063 System.Boolean Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::TryGetFrame(UnityEngine.XR.ARSubsystems.XRCameraParams,UnityEngine.XR.ARSubsystems.XRCameraFrame&)
extern void CameraProvider_TryGetFrame_mDFFF0C181C75C9C7EC85D4E2B8C49165569AEF75 (void);
// 0x00000064 System.Boolean Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::InitialiseRGBCamera()
extern void CameraProvider_InitialiseRGBCamera_mDB8BFC56CE1CC406070C78A3E654A22EFB194284 (void);
// 0x00000065 System.Double Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::ConvertXrTimeToDouble(System.Int64)
extern void CameraProvider_ConvertXrTimeToDouble_m341BC40F2C23EEE3A21E6723D9DEA024E776DD88 (void);
// 0x00000066 System.Void Qualcomm.Snapdragon.Spaces.CameraSubsystem/CameraProvider::.ctor()
extern void CameraProvider__ctor_m6CC8C5CD3D484945BEE0AA3401F170832633DF3A (void);
// 0x00000067 System.UInt32 Qualcomm.Snapdragon.Spaces.SpacesARCameraManagerConfig::get_MaxVerticalResolution()
extern void SpacesARCameraManagerConfig_get_MaxVerticalResolution_mDEBF4830F0BE8EB20A3A66B355826C4AE3F9DDFC (void);
// 0x00000068 System.Void Qualcomm.Snapdragon.Spaces.SpacesARCameraManagerConfig::set_MaxVerticalResolution(System.UInt32)
extern void SpacesARCameraManagerConfig_set_MaxVerticalResolution_m29ECAE19462C2657A3DB12E246DD3F891DA90B74 (void);
// 0x00000069 System.UInt32 Qualcomm.Snapdragon.Spaces.SpacesARCameraManagerConfig::get_DownsamplingStride()
extern void SpacesARCameraManagerConfig_get_DownsamplingStride_mCE0DF3214B996EDF959E513B77C641E24D5391D6 (void);
// 0x0000006A System.Void Qualcomm.Snapdragon.Spaces.SpacesARCameraManagerConfig::Start()
extern void SpacesARCameraManagerConfig_Start_mE47D7BADB69E692FCA94A0A46507C9381E90226D (void);
// 0x0000006B System.Void Qualcomm.Snapdragon.Spaces.SpacesARCameraManagerConfig::.ctor()
extern void SpacesARCameraManagerConfig__ctor_m11C88F7D9BD49DD174DE7EAEBC6ED6891F278579 (void);
// 0x0000006C System.Void Qualcomm.Snapdragon.Spaces.SpacesCameraPoseProvider::Start()
extern void SpacesCameraPoseProvider_Start_mCC8D9ABC256D1262BF537123D72F276C96C2C405 (void);
// 0x0000006D UnityEngine.SpatialTracking.PoseDataFlags Qualcomm.Snapdragon.Spaces.SpacesCameraPoseProvider::GetPoseFromProvider(UnityEngine.Pose&)
extern void SpacesCameraPoseProvider_GetPoseFromProvider_mDCFF8521FE33B28ECF436CDD1531B765F880B0D6 (void);
// 0x0000006E System.Void Qualcomm.Snapdragon.Spaces.SpacesCameraPoseProvider::.ctor()
extern void SpacesCameraPoseProvider__ctor_m823CDF6F95244AA958D7A6FA6AA7A710C6F9869F (void);
// 0x0000006F System.UInt64 Qualcomm.Snapdragon.Spaces.SpacesYUVFrame::get_Handle()
extern void SpacesYUVFrame_get_Handle_m2C88D88FC6499874ABD21BE3E1337D4EA2F2BE7E (void);
// 0x00000070 System.Int64 Qualcomm.Snapdragon.Spaces.SpacesYUVFrame::get_Timestamp()
extern void SpacesYUVFrame_get_Timestamp_m2A72B1EE42ED9D509FDD6646657B0D1D91702B4A (void);
// 0x00000071 UnityEngine.Vector2Int Qualcomm.Snapdragon.Spaces.SpacesYUVFrame::get_Dimensions()
extern void SpacesYUVFrame_get_Dimensions_m1827DF1A5EC7D99C4827485A0FB821AC58B07EFA (void);
// 0x00000072 Qualcomm.Snapdragon.Spaces.XrCameraFrameFormatQCOM Qualcomm.Snapdragon.Spaces.SpacesYUVFrame::get_Format()
extern void SpacesYUVFrame_get_Format_m65DE75FF20A89356C57B50B5F5302D9996DEFC7F (void);
// 0x00000073 Unity.Collections.NativeArray`1<System.Byte> Qualcomm.Snapdragon.Spaces.SpacesYUVFrame::get_YPlaneData()
extern void SpacesYUVFrame_get_YPlaneData_m3993CB49BA30972EEF8BD2123D89F47453FD126A (void);
// 0x00000074 Unity.Collections.NativeArray`1<System.Byte> Qualcomm.Snapdragon.Spaces.SpacesYUVFrame::get_UVPlaneData()
extern void SpacesYUVFrame_get_UVPlaneData_mF92E937C538B40164858A8A6ED2ACB5EBF71893D (void);
// 0x00000075 System.Boolean Qualcomm.Snapdragon.Spaces.SpacesYUVFrame::get_IsValid()
extern void SpacesYUVFrame_get_IsValid_m6E5056C1B6822DC0E3875AF55FB2CF136BE90B04 (void);
// 0x00000076 System.Void Qualcomm.Snapdragon.Spaces.SpacesYUVFrame::.ctor(System.UInt64,System.Int64,Qualcomm.Snapdragon.Spaces.XrCameraFrameFormatQCOM,UnityEngine.Vector2Int,Unity.Collections.NativeArray`1<System.Byte>&,Unity.Collections.NativeArray`1<System.Byte>&)
extern void SpacesYUVFrame__ctor_m0BF180942ECACF8626D6C0A742192660B4CAF6BB (void);
// 0x00000077 System.Void Qualcomm.Snapdragon.Spaces.SpacesYUVFrame::Dispose()
extern void SpacesYUVFrame_Dispose_mAFC5961E9DAA678B43EAB75E709F74C501847329 (void);
// 0x00000078 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::SetThreadHint(Qualcomm.Snapdragon.Spaces.SpacesThreadType,System.Boolean)
extern void BaseRuntimeFeature_SetThreadHint_m2A4E47BE76DE9AF219A7FDB2A13FD6F1470CD390 (void);
// 0x00000079 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::ConfigureXRAndroidApplicationThreads()
extern void BaseRuntimeFeature_ConfigureXRAndroidApplicationThreads_mF2F5556D4B5E2201F2F5833AA8A0DE80114E2AC2 (void);
// 0x0000007A System.Boolean Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::CheckServicesCameraPermissions()
extern void BaseRuntimeFeature_CheckServicesCameraPermissions_mE66D341E7610ABD6F4B656D52621BF49E74A4A50 (void);
// 0x0000007B System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.ComponentVersion> Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::get_ComponentVersions()
extern void BaseRuntimeFeature_get_ComponentVersions_mD25C18B3A3BBA513072C872B9B562AA74F665A68 (void);
// 0x0000007C System.Boolean Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::TryRetrieveComponentVersions()
extern void BaseRuntimeFeature_TryRetrieveComponentVersions_mA6CF7542BA7CB5282F19096B7194CAA8A28E5B1C (void);
// 0x0000007D System.String Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::GetXrLayersToLoad()
extern void BaseRuntimeFeature_GetXrLayersToLoad_m71055C2F3D9471E554C6E7FEADED9A649B5F524D (void);
// 0x0000007E System.Boolean Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::OnInstanceCreate(System.UInt64)
extern void BaseRuntimeFeature_OnInstanceCreate_m2E7A018DF8F6FB09F1315733583D4B9C542A8924 (void);
// 0x0000007F System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::OnSessionCreate(System.UInt64)
extern void BaseRuntimeFeature_OnSessionCreate_mE246653F490B2387F8046CF90CA3BC7612BA19B3 (void);
// 0x00000080 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::OnSystemChange(System.UInt64)
extern void BaseRuntimeFeature_OnSystemChange_m18702C4A41F1BEC646FF161B6D80799E2E37A967 (void);
// 0x00000081 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::OnAppSpaceChange(System.UInt64)
extern void BaseRuntimeFeature_OnAppSpaceChange_mC4140669CBB6F47975D0BBDC7BA455D852CAF9B0 (void);
// 0x00000082 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::SetSleepMode()
extern void BaseRuntimeFeature_SetSleepMode_mE1286B7DBE696D7D30D91486FE52E697743C7A19 (void);
// 0x00000083 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::OnHookMethods()
extern void BaseRuntimeFeature_OnHookMethods_mBBA04EB551F88CB122811D172CF77A08C366E285 (void);
// 0x00000084 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::OnSubsystemCreate()
extern void BaseRuntimeFeature_OnSubsystemCreate_m75F4B3815D7FF70AEF7178F663A02F5AF114FB15 (void);
// 0x00000085 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::OnSubsystemStart()
extern void BaseRuntimeFeature_OnSubsystemStart_mE9B2341CB336C5441DCEEEEC31C59DC813CF2F03 (void);
// 0x00000086 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::OnSessionBegin(System.UInt64)
extern void BaseRuntimeFeature_OnSessionBegin_m044260AD7D57EA548B767C7EAA9EE69356D51480 (void);
// 0x00000087 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::OnSubsystemStop()
extern void BaseRuntimeFeature_OnSubsystemStop_m723B4D27F92254E03EBA8F5DFB527234A3A85850 (void);
// 0x00000088 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::OnSubsystemDestroy()
extern void BaseRuntimeFeature_OnSubsystemDestroy_m7E564C461C4E23827A95330B78324B8CE5FED111 (void);
// 0x00000089 System.Single[] Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::get_SupportedDisplayRefreshRates()
extern void BaseRuntimeFeature_get_SupportedDisplayRefreshRates_m0D4B82F3E55C7C699E905030BEA48EE88A826BCB (void);
// 0x0000008A System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::set_SupportedDisplayRefreshRates(System.Single[])
extern void BaseRuntimeFeature_set_SupportedDisplayRefreshRates_mB404D572047A1A649194084734ACAD532F88F90A (void);
// 0x0000008B System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::EnumerateDisplayRefreshRates()
extern void BaseRuntimeFeature_EnumerateDisplayRefreshRates_m9777967216E3539ACC70CEEF41416731EBF5DBC6 (void);
// 0x0000008C System.Boolean Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::TrySetDisplayRefreshRate(System.Single)
extern void BaseRuntimeFeature_TrySetDisplayRefreshRate_m0A8C3F6B9E88802F1482931723AADA1ED43C8055 (void);
// 0x0000008D System.Boolean Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::TryGetDisplayRefreshRate(System.Single&)
extern void BaseRuntimeFeature_TryGetDisplayRefreshRate_mC824B321795D32EB90399E3E713209BB533431F3 (void);
// 0x0000008E System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::SetPassthroughEnabled(System.Boolean)
extern void BaseRuntimeFeature_SetPassthroughEnabled_m3783CB8020C7F575EFFE2B04BD83E4F590B7647A (void);
// 0x0000008F System.Boolean Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::GetPassthroughEnabled()
extern void BaseRuntimeFeature_GetPassthroughEnabled_m7C34EE10FB0682415320631CEDF414BA4E2866AC (void);
// 0x00000090 System.Boolean Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::IsPassthroughSupported()
extern void BaseRuntimeFeature_IsPassthroughSupported_mB8D6AE6B16824CFE581287D020A6057CBB46BBBA (void);
// 0x00000091 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::SetPassthroughEnabled_Native(System.Boolean)
extern void BaseRuntimeFeature_SetPassthroughEnabled_Native_m855EDAEF625BB3D220FB0907C44D5E0C5E31C306 (void);
// 0x00000092 System.Boolean Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::GetPassthroughEnabled_Native()
extern void BaseRuntimeFeature_GetPassthroughEnabled_Native_m6C08514C132579230F8A5EA0F9AE6EED320BB905 (void);
// 0x00000093 System.Boolean Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::IsPassthroughSupported_Native()
extern void BaseRuntimeFeature_IsPassthroughSupported_Native_m8012B77828BEFB8FA716F78C65444FF173C75099 (void);
// 0x00000094 System.IntPtr Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::GetInterceptedInstanceProcAddr(System.IntPtr)
extern void BaseRuntimeFeature_GetInterceptedInstanceProcAddr_m098961F6EC9A7CADD28C2652EB0602152507D06A (void);
// 0x00000095 System.IntPtr Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::HookGetInstanceProcAddr(System.IntPtr)
extern void BaseRuntimeFeature_HookGetInstanceProcAddr_m268A646EEEE3DB7070678C7440756F05FC18DDD8 (void);
// 0x00000096 System.UInt32 Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::Internal_CreateCompositionLayer(System.UInt64,System.UInt64,System.UInt32,System.UInt32,System.UInt32)
extern void BaseRuntimeFeature_Internal_CreateCompositionLayer_mFF6C2BC5F96F051C66AC9289BD3834FABB3BC790 (void);
// 0x00000097 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::Internal_DestroyCompositionLayer(System.UInt32)
extern void BaseRuntimeFeature_Internal_DestroyCompositionLayer_m09B34A7656869309C64549BB5609DC8DA64D2330 (void);
// 0x00000098 System.IntPtr Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::Internal_AcquireSwapchainImageForLayer(System.UInt32)
extern void BaseRuntimeFeature_Internal_AcquireSwapchainImageForLayer_mD4B64C324182FCF1229C4CDE44C5DB9374842357 (void);
// 0x00000099 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::Internal_ReleaseSwapchainImageForLayer(System.UInt32)
extern void BaseRuntimeFeature_Internal_ReleaseSwapchainImageForLayer_mFD9291CBC606E5670D3720AA68B455BFF6AB6BE7 (void);
// 0x0000009A System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::Internal_SetExtentsForQuadLayer(System.UInt32,System.Single,System.Single)
extern void BaseRuntimeFeature_Internal_SetExtentsForQuadLayer_m46DA74199B98C36C8E0A83540BD9E4178D2F25BF (void);
// 0x0000009B System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::Internal_SetOrientationForQuadLayer(System.UInt32,System.Single,System.Single,System.Single,System.Single)
extern void BaseRuntimeFeature_Internal_SetOrientationForQuadLayer_m3F38E9CD402E4696DAECEDF1497E40386268A829 (void);
// 0x0000009C System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::Internal_SetPositionForQuadLayer(System.UInt32,System.Single,System.Single,System.Single)
extern void BaseRuntimeFeature_Internal_SetPositionForQuadLayer_mD1EA21F5110C46AC3421BD1E271E896D07421491 (void);
// 0x0000009D System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::Internal_SetSortingOrderForQuadLayer(System.UInt32,System.UInt32)
extern void BaseRuntimeFeature_Internal_SetSortingOrderForQuadLayer_mDF0BD0866453873DC7B87061A4E8B58B2BC67A86 (void);
// 0x0000009E System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::Internal_SetQuadLayerVisible(System.UInt32,System.Boolean)
extern void BaseRuntimeFeature_Internal_SetQuadLayerVisible_m411FC2BD13D8C64454F7B3B0B6B1D07F7AEAF63E (void);
// 0x0000009F System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::Internal_SetMaxCompositionLayerCount(System.UInt32)
extern void BaseRuntimeFeature_Internal_SetMaxCompositionLayerCount_m176EB7FB380013882B7C8687C9F81A102C147AC7 (void);
// 0x000000A0 System.Boolean Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::Internal_IsXrFrameInProgress()
extern void BaseRuntimeFeature_Internal_IsXrFrameInProgress_m40717B9CF9CE4C14DFFAF1FAB6920556F06C46B7 (void);
// 0x000000A1 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::Internal_SendToSwapchain(System.UInt32,System.IntPtr,System.Int32,System.Int32)
extern void BaseRuntimeFeature_Internal_SendToSwapchain_mE3EC340746E263919B83DBF42AE69C2868134E5B (void);
// 0x000000A2 System.Boolean Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::Internal_UseNativeTexture()
extern void BaseRuntimeFeature_Internal_UseNativeTexture_mF8E516712AF68A97C39115A82E2DD872249CEE70 (void);
// 0x000000A3 System.UInt32 Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::CreateCompositionLayer(System.Int32,System.Int32,System.UInt32)
extern void BaseRuntimeFeature_CreateCompositionLayer_m767C0565759728F8FC9D9029097438AA9DCB78C5 (void);
// 0x000000A4 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::DestroyCompositionLayer(System.UInt32)
extern void BaseRuntimeFeature_DestroyCompositionLayer_mA9DA86C6DC7962CEE2BB8BDB4A6ED23D2BFD417A (void);
// 0x000000A5 System.IntPtr Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::AcquireSwapchainImageForLayer(System.UInt32)
extern void BaseRuntimeFeature_AcquireSwapchainImageForLayer_mDED03C3B1F833FE93951BF0B327130176EF976BC (void);
// 0x000000A6 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::ReleaseSwapchainImageForLayer(System.UInt32)
extern void BaseRuntimeFeature_ReleaseSwapchainImageForLayer_m7F2E14A66EE9CE2DB059998B5894D7EC3F8E4F7F (void);
// 0x000000A7 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::SetExtentsForQuadLayer(System.UInt32,UnityEngine.Vector2)
extern void BaseRuntimeFeature_SetExtentsForQuadLayer_m680F2074E1EAF0BE461B2E54C0F8175610E66905 (void);
// 0x000000A8 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::SetOrientationForQuadLayer(System.UInt32,UnityEngine.Quaternion)
extern void BaseRuntimeFeature_SetOrientationForQuadLayer_mE550DC8EFBFE46B0115D2B487872534AFB58B63D (void);
// 0x000000A9 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::SetPositionForQuadLayer(System.UInt32,UnityEngine.Vector3)
extern void BaseRuntimeFeature_SetPositionForQuadLayer_m1E50F0DF52E0AFFF9C253638818B5AB1380AF35E (void);
// 0x000000AA System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::SetSortingOrderForQuadLayer(System.UInt32,System.UInt32)
extern void BaseRuntimeFeature_SetSortingOrderForQuadLayer_mB667FAF600835ADCDCF74F00B00F0764ED32B4F6 (void);
// 0x000000AB System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::SetQuadLayerVisible(System.UInt32,System.Boolean)
extern void BaseRuntimeFeature_SetQuadLayerVisible_mF676A1F2482218314B1F78D322A43CCEFCCCC476 (void);
// 0x000000AC System.Boolean Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::IsXrFrameInProgress()
extern void BaseRuntimeFeature_IsXrFrameInProgress_mF1FDBFAA81A04238720B03701CC12DAA2117D678 (void);
// 0x000000AD System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::SendToSwapchain(System.UInt32,System.IntPtr,System.Int32,System.Int32)
extern void BaseRuntimeFeature_SendToSwapchain_mD9B1907F3CA116CA60FF2A5CC27525FA156E771C (void);
// 0x000000AE System.Boolean Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::UseNativeTexture()
extern void BaseRuntimeFeature_UseNativeTexture_m7C09386B4C58AA2E69AE23F59044A17A89B6EC6E (void);
// 0x000000AF System.Int64 Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::get_PredictedDisplayTime()
extern void BaseRuntimeFeature_get_PredictedDisplayTime_m432B805A627B7F73C14298F21CE791AA90999DAC (void);
// 0x000000B0 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::SetFrameStateCallback(Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/ReceiveFrameStateCallback)
extern void BaseRuntimeFeature_SetFrameStateCallback_mF8BB16A7F56755DBDCD00CC062D0F9793FF8BC55 (void);
// 0x000000B1 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::OnFrameStateUpdate(Qualcomm.Snapdragon.Spaces.XrFrameState)
extern void BaseRuntimeFeature_OnFrameStateUpdate_m73151720BB25328366DD7B8665017ED420FC58B2 (void);
// 0x000000B2 System.Int32 Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::ReserveEventIndices(System.Int32)
extern void BaseRuntimeFeature_ReserveEventIndices_m7E128F18E461729C919F588BBFF1FB821F568346 (void);
// 0x000000B3 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::ConfigureSpacesRenderEvents()
extern void BaseRuntimeFeature_ConfigureSpacesRenderEvents_m9F49135821080BD9CB522FB2F6EFEA8755E7F7C1 (void);
// 0x000000B4 System.Int32 Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::SpacesRenderEventToEventId(Qualcomm.Snapdragon.Spaces.SpacesRenderEvent)
extern void BaseRuntimeFeature_SpacesRenderEventToEventId_m68A80F9C32366947EB122BAE061B7E5D14877BA1 (void);
// 0x000000B5 Qualcomm.Snapdragon.Spaces.SpacesRenderEvent Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::EventIdToSpacesRenderEvent(System.Int32)
extern void BaseRuntimeFeature_EventIdToSpacesRenderEvent_m2293E714627BD5D0E55F7030B53039C13F0C3D48 (void);
// 0x000000B6 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::RunOnRenderThread(System.Int32)
extern void BaseRuntimeFeature_RunOnRenderThread_m2E8512A10A77075A41C26E119959C00AF6BD3B58 (void);
// 0x000000B7 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::RunOnRenderThreadWithData(System.Int32,System.IntPtr)
extern void BaseRuntimeFeature_RunOnRenderThreadWithData_m02CE392AA2B19CCA6467DCA7CE3B166EDC6E1928 (void);
// 0x000000B8 System.Boolean Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::TryResetPose()
extern void BaseRuntimeFeature_TryResetPose_mFE6D23F3563ADB4C5842F2A490F58A78F128D8CB (void);
// 0x000000B9 System.Boolean Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::TryStartControllerOnHandset()
extern void BaseRuntimeFeature_TryStartControllerOnHandset_m381E7B1FA5B6D0B9D287458D523A5B4B286BAEC8 (void);
// 0x000000BA Qualcomm.Snapdragon.Spaces.XrSystemProperties Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::get_SystemProperties()
extern void BaseRuntimeFeature_get_SystemProperties_m652A5F6215A0A98CFBF7ED3D0E0D9F1451365003 (void);
// 0x000000BB System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::OnGetSystemProperties()
extern void BaseRuntimeFeature_OnGetSystemProperties_m0008C660B41FEB9D6D7DB31BE4B904E7FD7DA7DA (void);
// 0x000000BC Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::Internal_GetSystemProperties()
extern void BaseRuntimeFeature_Internal_GetSystemProperties_m75DD4468BD8C24F0CC18272E45A08C40373F2B23 (void);
// 0x000000BD System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::.ctor()
extern void BaseRuntimeFeature__ctor_m5862243FBD9D83BD16F42CBAB40F144F741441EC (void);
// 0x000000BE System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature::.cctor()
extern void BaseRuntimeFeature__cctor_m192D0679A110C08B9D5AF38368883AC5C734B164 (void);
// 0x000000BF System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/xrSetAndroidApplicationThreadKHRDelegate::.ctor(System.Object,System.IntPtr)
extern void xrSetAndroidApplicationThreadKHRDelegate__ctor_mB3E28FB7C1D36CA3749359DAC92D2EDB8EA7EAD9 (void);
// 0x000000C0 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/xrSetAndroidApplicationThreadKHRDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrAndroidThreadTypeKHR,System.UInt32)
extern void xrSetAndroidApplicationThreadKHRDelegate_Invoke_m01237F2CB271BF28AF4840F19BFD179A3C5C8FE8 (void);
// 0x000000C1 System.IAsyncResult Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/xrSetAndroidApplicationThreadKHRDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrAndroidThreadTypeKHR,System.UInt32,System.AsyncCallback,System.Object)
extern void xrSetAndroidApplicationThreadKHRDelegate_BeginInvoke_m026432DE5CBBA7789DBCF9753F5AFEBF3F480A7C (void);
// 0x000000C2 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/xrSetAndroidApplicationThreadKHRDelegate::EndInvoke(System.IAsyncResult)
extern void xrSetAndroidApplicationThreadKHRDelegate_EndInvoke_m5E97AAC1E0FCE2CDD64D9762F2ADA4EE972CCA6E (void);
// 0x000000C3 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/xrGetComponentVersionsQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void xrGetComponentVersionsQCOMDelegate__ctor_m7F1F188B65DC367C76018B9D38FBBC09F02505C4 (void);
// 0x000000C4 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/xrGetComponentVersionsQCOMDelegate::Invoke(System.UInt64,System.UInt32,System.UInt32&,System.IntPtr)
extern void xrGetComponentVersionsQCOMDelegate_Invoke_mA04F1C043BE1932409F9A0CE774502E7FB1A607B (void);
// 0x000000C5 System.IAsyncResult Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/xrGetComponentVersionsQCOMDelegate::BeginInvoke(System.UInt64,System.UInt32,System.UInt32&,System.IntPtr,System.AsyncCallback,System.Object)
extern void xrGetComponentVersionsQCOMDelegate_BeginInvoke_mD71FD0B1327B0CA788EF2D9C126A24804DB9E907 (void);
// 0x000000C6 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/xrGetComponentVersionsQCOMDelegate::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void xrGetComponentVersionsQCOMDelegate_EndInvoke_m07847DD2472736948DA80FBB9B49E1C256533C78 (void);
// 0x000000C7 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/OnSpacesAppSpaceChangeDelegate::.ctor(System.Object,System.IntPtr)
extern void OnSpacesAppSpaceChangeDelegate__ctor_mC6D65079960A9CE4C02ECD4E23BCB57DEEA0FFDA (void);
// 0x000000C8 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/OnSpacesAppSpaceChangeDelegate::Invoke(System.UInt64)
extern void OnSpacesAppSpaceChangeDelegate_Invoke_m78055D97180AE1ABF747CE02DB02EAECF4A8AAEF (void);
// 0x000000C9 System.IAsyncResult Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/OnSpacesAppSpaceChangeDelegate::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void OnSpacesAppSpaceChangeDelegate_BeginInvoke_m9094CD843CF3732F9C21DF0C4D9781E9C4CE046D (void);
// 0x000000CA System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/OnSpacesAppSpaceChangeDelegate::EndInvoke(System.IAsyncResult)
extern void OnSpacesAppSpaceChangeDelegate_EndInvoke_m796A614189EA0FEC407A0A3AF54AAA11C2765DA2 (void);
// 0x000000CB System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/xrEnumerateDisplayRefreshRatesFBDelegate::.ctor(System.Object,System.IntPtr)
extern void xrEnumerateDisplayRefreshRatesFBDelegate__ctor_m9073646BB90FAF5E6E4B4C59A95F0190C9E7079A (void);
// 0x000000CC Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/xrEnumerateDisplayRefreshRatesFBDelegate::Invoke(System.UInt64,System.UInt32,System.UInt32&,System.IntPtr)
extern void xrEnumerateDisplayRefreshRatesFBDelegate_Invoke_m5404BDD227B1DE720E53599D02935EFC334C7990 (void);
// 0x000000CD System.IAsyncResult Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/xrEnumerateDisplayRefreshRatesFBDelegate::BeginInvoke(System.UInt64,System.UInt32,System.UInt32&,System.IntPtr,System.AsyncCallback,System.Object)
extern void xrEnumerateDisplayRefreshRatesFBDelegate_BeginInvoke_mB583E027D48BCACF82585BA75F808945B69C48A1 (void);
// 0x000000CE Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/xrEnumerateDisplayRefreshRatesFBDelegate::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void xrEnumerateDisplayRefreshRatesFBDelegate_EndInvoke_m534DCB4C7945B5BBE55F45B04F4B49FED2EAC650 (void);
// 0x000000CF System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/xrGetDisplayRefreshRateFBDelegate::.ctor(System.Object,System.IntPtr)
extern void xrGetDisplayRefreshRateFBDelegate__ctor_m2230D5188D791BB01A37BD33E3D8CA450D97F51C (void);
// 0x000000D0 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/xrGetDisplayRefreshRateFBDelegate::Invoke(System.UInt64,System.Single&)
extern void xrGetDisplayRefreshRateFBDelegate_Invoke_m09588150A6D745E8369B0ECBA251E047A49D37C5 (void);
// 0x000000D1 System.IAsyncResult Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/xrGetDisplayRefreshRateFBDelegate::BeginInvoke(System.UInt64,System.Single&,System.AsyncCallback,System.Object)
extern void xrGetDisplayRefreshRateFBDelegate_BeginInvoke_m3C7B0C8F59A9C1021060D05AB0EE5A9DFD82B9D5 (void);
// 0x000000D2 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/xrGetDisplayRefreshRateFBDelegate::EndInvoke(System.Single&,System.IAsyncResult)
extern void xrGetDisplayRefreshRateFBDelegate_EndInvoke_m5CC93D5145B2BBC291D29A1C38B62C45C2DB3696 (void);
// 0x000000D3 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/xrRequestDisplayRefreshRateFBDelegate::.ctor(System.Object,System.IntPtr)
extern void xrRequestDisplayRefreshRateFBDelegate__ctor_m4C8051C233EA8A7AFC31C9A924176F2870A7053E (void);
// 0x000000D4 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/xrRequestDisplayRefreshRateFBDelegate::Invoke(System.UInt64,System.Single)
extern void xrRequestDisplayRefreshRateFBDelegate_Invoke_m5DC48E855D90D6DBD325BCC0096AB2D7F0F1764A (void);
// 0x000000D5 System.IAsyncResult Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/xrRequestDisplayRefreshRateFBDelegate::BeginInvoke(System.UInt64,System.Single,System.AsyncCallback,System.Object)
extern void xrRequestDisplayRefreshRateFBDelegate_BeginInvoke_m733A9F289AEECBEE4B66D2B64E80C311060066C3 (void);
// 0x000000D6 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/xrRequestDisplayRefreshRateFBDelegate::EndInvoke(System.IAsyncResult)
extern void xrRequestDisplayRefreshRateFBDelegate_EndInvoke_mE4F6CF38A0E873F04737252A5F3D8A53FBDA073F (void);
// 0x000000D7 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/ReceiveFrameStateCallback::.ctor(System.Object,System.IntPtr)
extern void ReceiveFrameStateCallback__ctor_mF6FAAA6AF2ADD39E8529E383FBD4DD2BD504B2A3 (void);
// 0x000000D8 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/ReceiveFrameStateCallback::Invoke(Qualcomm.Snapdragon.Spaces.XrFrameState)
extern void ReceiveFrameStateCallback_Invoke_m1747202E0B1D6CDEA24E32FA5965696230B85203 (void);
// 0x000000D9 System.IAsyncResult Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/ReceiveFrameStateCallback::BeginInvoke(Qualcomm.Snapdragon.Spaces.XrFrameState,System.AsyncCallback,System.Object)
extern void ReceiveFrameStateCallback_BeginInvoke_m95FAFE536FD1EA1E74194BFEC8C130EF17479843 (void);
// 0x000000DA System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/ReceiveFrameStateCallback::EndInvoke(System.IAsyncResult)
extern void ReceiveFrameStateCallback_EndInvoke_mB9C316BA40064076BCBDDFCCDD9A854806C4A7FC (void);
// 0x000000DB System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/RenderEventDelegate::.ctor(System.Object,System.IntPtr)
extern void RenderEventDelegate__ctor_mD49A79E2D77C1A3028C3A8342979149E8B2089EB (void);
// 0x000000DC System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/RenderEventDelegate::Invoke(System.Int32)
extern void RenderEventDelegate_Invoke_m3FE5ACE5C3FEFAF10767FFCD14D08327CC158071 (void);
// 0x000000DD System.IAsyncResult Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/RenderEventDelegate::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void RenderEventDelegate_BeginInvoke_m88233EA08F6BD5B9016D87D4042F21BFAF71704D (void);
// 0x000000DE System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/RenderEventDelegate::EndInvoke(System.IAsyncResult)
extern void RenderEventDelegate_EndInvoke_m55DB2894F2FC7C65D8A7BCCC9010E5667C972141 (void);
// 0x000000DF System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/RenderEventWithDataDelegate::.ctor(System.Object,System.IntPtr)
extern void RenderEventWithDataDelegate__ctor_m4C4FB74199703D8BD5289B6B81322D06625B7A52 (void);
// 0x000000E0 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/RenderEventWithDataDelegate::Invoke(System.Int32,System.IntPtr)
extern void RenderEventWithDataDelegate_Invoke_mCA0AAE70F2BB5C82EA2AB2A8F1EC6EB98BE9FFE6 (void);
// 0x000000E1 System.IAsyncResult Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/RenderEventWithDataDelegate::BeginInvoke(System.Int32,System.IntPtr,System.AsyncCallback,System.Object)
extern void RenderEventWithDataDelegate_BeginInvoke_m24EEDBA3A98BF8C006C41FAB2339E164A340BB60 (void);
// 0x000000E2 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/RenderEventWithDataDelegate::EndInvoke(System.IAsyncResult)
extern void RenderEventWithDataDelegate_EndInvoke_mA16A1D76867B803E424A2DB7180753515368D166 (void);
// 0x000000E3 System.Void Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/xrGetSystemPropertiesDelegate::.ctor(System.Object,System.IntPtr)
extern void xrGetSystemPropertiesDelegate__ctor_m47C6B0848979D84E86DB54E0EDD1D12C506730FE (void);
// 0x000000E4 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/xrGetSystemPropertiesDelegate::Invoke(System.UInt64,System.UInt64,System.IntPtr)
extern void xrGetSystemPropertiesDelegate_Invoke_mB16AF1B083AF580C53C9DB4279E6CAA4E1826CCD (void);
// 0x000000E5 System.IAsyncResult Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/xrGetSystemPropertiesDelegate::BeginInvoke(System.UInt64,System.UInt64,System.IntPtr,System.AsyncCallback,System.Object)
extern void xrGetSystemPropertiesDelegate_BeginInvoke_m4DEF169081703F16BB569616767922537A29A6D2 (void);
// 0x000000E6 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature/xrGetSystemPropertiesDelegate::EndInvoke(System.IAsyncResult)
extern void xrGetSystemPropertiesDelegate_EndInvoke_m215FEEA00DFE12CE059F932D4D8A74AEE9F1B9EB (void);
// 0x000000E7 System.Void Qualcomm.Snapdragon.Spaces.ComponentVersion::.ctor(System.String,System.String,System.String,System.String,System.String)
extern void ComponentVersion__ctor_m1398FA8526AA2FC1AA0C9C002AED93F555B3BC8B (void);
// 0x000000E8 System.Void Qualcomm.Snapdragon.Spaces.XrCameraActivationFrameConfigurationQCOM::.ctor(Qualcomm.Snapdragon.Spaces.XrCameraFrameFormatQCOM,System.String,System.UInt32,System.Boolean)
extern void XrCameraActivationFrameConfigurationQCOM__ctor_mC01CC056B56188B07DCAF655B305D78BE7575D9E (void);
// 0x000000E9 System.String Qualcomm.Snapdragon.Spaces.XrCameraActivationFrameConfigurationQCOM::ToString()
extern void XrCameraActivationFrameConfigurationQCOM_ToString_mA33EBA223A3F907709277F3029DB734BAB05D069 (void);
// 0x000000EA System.Void Qualcomm.Snapdragon.Spaces.XrCameraActivationInfoQCOM::.ctor(System.String)
extern void XrCameraActivationInfoQCOM__ctor_m95201ED05481B95C234EFFA791DB372BDA561B85 (void);
// 0x000000EB System.String Qualcomm.Snapdragon.Spaces.XrCameraActivationInfoQCOM::ToString()
extern void XrCameraActivationInfoQCOM_ToString_m336B7CD08F1239F2F96F7A195AFE12D2FF72D101 (void);
// 0x000000EC System.Void Qualcomm.Snapdragon.Spaces.XrCameraFrameBufferQCOM::.ctor(System.UInt32,System.IntPtr,Qualcomm.Snapdragon.Spaces.XrOffset2Di,System.UInt32,System.Byte[])
extern void XrCameraFrameBufferQCOM__ctor_mB4B112F9286FE81D0A56B047D932AF79CC5D10F2 (void);
// 0x000000ED System.Void Qualcomm.Snapdragon.Spaces.XrCameraFrameBufferQCOM::.ctor(Qualcomm.Snapdragon.Spaces.XrCameraFrameBufferQCOM,System.IntPtr)
extern void XrCameraFrameBufferQCOM__ctor_mE785229ABB8C9A2FB5A1688229B1AB80B7165879 (void);
// 0x000000EE System.UInt32 Qualcomm.Snapdragon.Spaces.XrCameraFrameBufferQCOM::get_BufferSize()
extern void XrCameraFrameBufferQCOM_get_BufferSize_m950D5D3907A1869EEA881A2317C0AD3D7912B7A2 (void);
// 0x000000EF System.IntPtr Qualcomm.Snapdragon.Spaces.XrCameraFrameBufferQCOM::get_Buffer()
extern void XrCameraFrameBufferQCOM_get_Buffer_mF7F72DFD8B5EC6CA89CAE6CA9A912A93794DFC55 (void);
// 0x000000F0 System.IntPtr Qualcomm.Snapdragon.Spaces.XrCameraFrameBufferQCOM::get_HardwareBuffer()
extern void XrCameraFrameBufferQCOM_get_HardwareBuffer_m0AF3D153BED58C85694CE0952F9291D393882A88 (void);
// 0x000000F1 Qualcomm.Snapdragon.Spaces.XrOffset2Di Qualcomm.Snapdragon.Spaces.XrCameraFrameBufferQCOM::get_Offset()
extern void XrCameraFrameBufferQCOM_get_Offset_m2A9616A87A8D114D5C6F5B877614803B56612757 (void);
// 0x000000F2 System.UInt32 Qualcomm.Snapdragon.Spaces.XrCameraFrameBufferQCOM::get_PlaneCount()
extern void XrCameraFrameBufferQCOM_get_PlaneCount_m068F55256645F2AFB1FF6D402ABDF7DF8B178A6B (void);
// 0x000000F3 System.Byte[] Qualcomm.Snapdragon.Spaces.XrCameraFrameBufferQCOM::get_Planes()
extern void XrCameraFrameBufferQCOM_get_Planes_m30C5177D7A5E5D9CA49C2DDC3F43A5593D4A6E29 (void);
// 0x000000F4 System.String Qualcomm.Snapdragon.Spaces.XrCameraFrameBufferQCOM::ToString()
extern void XrCameraFrameBufferQCOM_ToString_m1640A05E0ED21CB8C11B2B3F53551D9A20C1505C (void);
// 0x000000F5 Qualcomm.Snapdragon.Spaces.XrCameraFramePlaneQCOM[] Qualcomm.Snapdragon.Spaces.XrCameraFrameBufferQCOM::get_PlanesArray()
extern void XrCameraFrameBufferQCOM_get_PlanesArray_m781AA15B1A8B108136DC68546FBB869BAB0A69C0 (void);
// 0x000000F6 System.Void Qualcomm.Snapdragon.Spaces.XrCameraFrameBuffersQCOM::.ctor(System.IntPtr,System.UInt32,System.IntPtr)
extern void XrCameraFrameBuffersQCOM__ctor_mFAF9A5A88C5AF80F029B5F89BDF3E3536AA4BB9E (void);
// 0x000000F7 System.UInt32 Qualcomm.Snapdragon.Spaces.XrCameraFrameBuffersQCOM::get_FrameBufferCount()
extern void XrCameraFrameBuffersQCOM_get_FrameBufferCount_mBB3219FC8C8263519C8D9C03EB5960AD0A7625FE (void);
// 0x000000F8 System.IntPtr Qualcomm.Snapdragon.Spaces.XrCameraFrameBuffersQCOM::get_FrameBuffers()
extern void XrCameraFrameBuffersQCOM_get_FrameBuffers_mEF5A67FA8F90E32A6D8FB70D97582CDA20B9BF81 (void);
// 0x000000F9 System.String Qualcomm.Snapdragon.Spaces.XrCameraFrameBuffersQCOM::ToString()
extern void XrCameraFrameBuffersQCOM_ToString_m35FC790BEB29947A3C585A1910AEF0CC10DD6D16 (void);
// 0x000000FA System.Void Qualcomm.Snapdragon.Spaces.XrCameraFrameConfigurationQCOM::.ctor(Qualcomm.Snapdragon.Spaces.XrCameraFrameFormatQCOM,System.String,Qualcomm.Snapdragon.Spaces.XrExtent2Di,System.UInt32,System.UInt32,System.UInt32,System.UInt32)
extern void XrCameraFrameConfigurationQCOM__ctor_m3942FC71C58D9E6031DEFDB639F394D4932A86E0 (void);
// 0x000000FB Qualcomm.Snapdragon.Spaces.XrCameraFrameFormatQCOM Qualcomm.Snapdragon.Spaces.XrCameraFrameConfigurationQCOM::get_Format()
extern void XrCameraFrameConfigurationQCOM_get_Format_m6BAA09753953C3355C176B7D28BA0264C58A9F55 (void);
// 0x000000FC System.String Qualcomm.Snapdragon.Spaces.XrCameraFrameConfigurationQCOM::get_ResolutionName()
extern void XrCameraFrameConfigurationQCOM_get_ResolutionName_mC9FAE99F606993BC7CB1DAC117B8787CD8B7E7DF (void);
// 0x000000FD Qualcomm.Snapdragon.Spaces.XrExtent2Di Qualcomm.Snapdragon.Spaces.XrCameraFrameConfigurationQCOM::get_Dimensions()
extern void XrCameraFrameConfigurationQCOM_get_Dimensions_mA4CCECC8E2328C0B5CF51C328C9E49B8CA29C389 (void);
// 0x000000FE System.UInt32 Qualcomm.Snapdragon.Spaces.XrCameraFrameConfigurationQCOM::get_MinFPS()
extern void XrCameraFrameConfigurationQCOM_get_MinFPS_m94122334221AE771D207F98FE47F1FA1B1B92686 (void);
// 0x000000FF System.UInt32 Qualcomm.Snapdragon.Spaces.XrCameraFrameConfigurationQCOM::get_MaxFPS()
extern void XrCameraFrameConfigurationQCOM_get_MaxFPS_m98188152C09B0C2283C8ABB89BDF52AE641BA34F (void);
// 0x00000100 System.UInt32 Qualcomm.Snapdragon.Spaces.XrCameraFrameConfigurationQCOM::get_FrameBufferCount()
extern void XrCameraFrameConfigurationQCOM_get_FrameBufferCount_mAEC0C4D48348E78839296BA0F17D16D1EDADCAD4 (void);
// 0x00000101 System.UInt32 Qualcomm.Snapdragon.Spaces.XrCameraFrameConfigurationQCOM::get_FrameHardwareBufferCount()
extern void XrCameraFrameConfigurationQCOM_get_FrameHardwareBufferCount_mAC6A3CE4A06E7BFA5F5DDCE5E1D7F1AE22F4A882 (void);
// 0x00000102 System.String Qualcomm.Snapdragon.Spaces.XrCameraFrameConfigurationQCOM::ToString()
extern void XrCameraFrameConfigurationQCOM_ToString_m20BB1EE9D69A31994E54F082BF32C02DD73E8F33 (void);
// 0x00000103 System.Boolean Qualcomm.Snapdragon.Spaces.XrCameraFrameConfigurationQCOM::Equals(Qualcomm.Snapdragon.Spaces.XrCameraFrameConfigurationQCOM)
extern void XrCameraFrameConfigurationQCOM_Equals_m79BAF59F2489C2FC04698EEA69660B3720E94C16 (void);
// 0x00000104 System.Boolean Qualcomm.Snapdragon.Spaces.XrCameraFrameConfigurationQCOM::Equals(System.Object)
extern void XrCameraFrameConfigurationQCOM_Equals_m6C81D6EF68F4406E509277EDAC64592DF817FD32 (void);
// 0x00000105 System.Int32 Qualcomm.Snapdragon.Spaces.XrCameraFrameConfigurationQCOM::GetHashCode()
extern void XrCameraFrameConfigurationQCOM_GetHashCode_m88D0A1632ECF24BE403724F6BE31D41C9BAF5D96 (void);
// 0x00000106 System.Void Qualcomm.Snapdragon.Spaces.XrCameraFrameDataQCOM::.ctor(System.IntPtr,System.UInt64,Qualcomm.Snapdragon.Spaces.XrCameraFrameFormatQCOM,System.UInt32,System.Int64)
extern void XrCameraFrameDataQCOM__ctor_m21472D27A6AD1B4266106153194A8F2F1C95E11C (void);
// 0x00000107 Qualcomm.Snapdragon.Spaces.XrStructureType Qualcomm.Snapdragon.Spaces.XrCameraFrameDataQCOM::get_Type()
extern void XrCameraFrameDataQCOM_get_Type_mE5BBBBD0E4DFDE0D12419924BB91C99CFB730F58 (void);
// 0x00000108 System.IntPtr Qualcomm.Snapdragon.Spaces.XrCameraFrameDataQCOM::get_Next()
extern void XrCameraFrameDataQCOM_get_Next_mCE6D17EB550A4FD2EE8A6EF399B18BD1600A0ACD (void);
// 0x00000109 System.UInt64 Qualcomm.Snapdragon.Spaces.XrCameraFrameDataQCOM::get_Handle()
extern void XrCameraFrameDataQCOM_get_Handle_m26A480ABB370791A1258A3E102BD9FB3CC74DD93 (void);
// 0x0000010A Qualcomm.Snapdragon.Spaces.XrCameraFrameFormatQCOM Qualcomm.Snapdragon.Spaces.XrCameraFrameDataQCOM::get_Format()
extern void XrCameraFrameDataQCOM_get_Format_m5D209DAFD825E8967B0DE6001ADDCBC71762AA0A (void);
// 0x0000010B System.UInt32 Qualcomm.Snapdragon.Spaces.XrCameraFrameDataQCOM::get_FrameNumber()
extern void XrCameraFrameDataQCOM_get_FrameNumber_m8B4ED328D6E36ED8B61A2919C4C82A6C03F61B10 (void);
// 0x0000010C System.Int64 Qualcomm.Snapdragon.Spaces.XrCameraFrameDataQCOM::get_Timestamp()
extern void XrCameraFrameDataQCOM_get_Timestamp_mBF90CF8A92D202ECE328A6C19955215D983C509C (void);
// 0x0000010D System.String Qualcomm.Snapdragon.Spaces.XrCameraFrameDataQCOM::ToString()
extern void XrCameraFrameDataQCOM_ToString_mCA5D1225589871DC98C2959C1F2B4B7BF01895D8 (void);
// 0x0000010E System.Void Qualcomm.Snapdragon.Spaces.XrCameraFrameHardwareBufferQCOM::.ctor(System.IntPtr)
extern void XrCameraFrameHardwareBufferQCOM__ctor_mB52C3509013DAC2D369350950A83731A07C429CB (void);
// 0x0000010F System.String Qualcomm.Snapdragon.Spaces.XrCameraFrameHardwareBufferQCOM::ToString()
extern void XrCameraFrameHardwareBufferQCOM_ToString_m6ED5F07B3836FC15534D01364A1CC9B226EA36D4 (void);
// 0x00000110 System.Void Qualcomm.Snapdragon.Spaces.XrCameraFramePlaneQCOM::.ctor(Qualcomm.Snapdragon.Spaces.XrCameraFramePlaneTypeQCOM,System.UInt32,System.UInt32)
extern void XrCameraFramePlaneQCOM__ctor_m284891F8B4782C344844E1E3B769F8DC14A3F61D (void);
// 0x00000111 Qualcomm.Snapdragon.Spaces.XrCameraFramePlaneTypeQCOM Qualcomm.Snapdragon.Spaces.XrCameraFramePlaneQCOM::get_PlaneType()
extern void XrCameraFramePlaneQCOM_get_PlaneType_m056E8B6DCF863AF69C363C8C9BBC6A17AFAC0550 (void);
// 0x00000112 System.UInt32 Qualcomm.Snapdragon.Spaces.XrCameraFramePlaneQCOM::get_Offset()
extern void XrCameraFramePlaneQCOM_get_Offset_m8AF8D2AF8562907454F27993C9C9853483FA114A (void);
// 0x00000113 System.UInt32 Qualcomm.Snapdragon.Spaces.XrCameraFramePlaneQCOM::get_Stride()
extern void XrCameraFramePlaneQCOM_get_Stride_m090D94F7651FE1AB5559A37739AB90178422290B (void);
// 0x00000114 System.String Qualcomm.Snapdragon.Spaces.XrCameraFramePlaneQCOM::ToString()
extern void XrCameraFramePlaneQCOM_ToString_m7F4DC8BA7B9EBEC86627F9DCF8AFD809E17A9D09 (void);
// 0x00000115 System.Void Qualcomm.Snapdragon.Spaces.XrCameraInfoQCOM::.ctor(System.String,Qualcomm.Snapdragon.Spaces.XrCameraTypeQCOM,System.UInt32)
extern void XrCameraInfoQCOM__ctor_mAE436B7F665A8313BC9A5A24A7159C437775207A (void);
// 0x00000116 System.String Qualcomm.Snapdragon.Spaces.XrCameraInfoQCOM::get_CameraSet()
extern void XrCameraInfoQCOM_get_CameraSet_mE444C2C329BA9C5F9A14A16310005465DCF958C1 (void);
// 0x00000117 Qualcomm.Snapdragon.Spaces.XrCameraTypeQCOM Qualcomm.Snapdragon.Spaces.XrCameraInfoQCOM::get_CameraType()
extern void XrCameraInfoQCOM_get_CameraType_m4F5E8A91874162DD8331097F58D02F32296FB66D (void);
// 0x00000118 System.UInt32 Qualcomm.Snapdragon.Spaces.XrCameraInfoQCOM::get_SensorCount()
extern void XrCameraInfoQCOM_get_SensorCount_m6AE604B7A97AE6C883978221F11CA564562E407D (void);
// 0x00000119 System.String Qualcomm.Snapdragon.Spaces.XrCameraInfoQCOM::ToString()
extern void XrCameraInfoQCOM_ToString_mE8AB2E88F245A4DE1C185D918273C782647F29B2 (void);
// 0x0000011A System.Void Qualcomm.Snapdragon.Spaces.XrCameraSensorInfosQCOM::.ctor(System.UInt64,System.UInt32,System.IntPtr)
extern void XrCameraSensorInfosQCOM__ctor_m5051C95DC0D6173C0045176AA82A987E2BD79CBE (void);
// 0x0000011B System.String Qualcomm.Snapdragon.Spaces.XrCameraSensorInfosQCOM::ToString()
extern void XrCameraSensorInfosQCOM_ToString_m4887306CA657989C0FD510390E019AAF3F383F45 (void);
// 0x0000011C System.Void Qualcomm.Snapdragon.Spaces.XrCameraSensorIntrinsicsQCOM::.ctor(Qualcomm.Snapdragon.Spaces.XrVector2f,Qualcomm.Snapdragon.Spaces.XrVector2f,System.Single[],System.Single[],Qualcomm.Snapdragon.Spaces.XrCameraDistortionModelQCOM)
extern void XrCameraSensorIntrinsicsQCOM__ctor_mF823C9405A4F3ADE635106439C22625FD54D39BE (void);
// 0x0000011D Qualcomm.Snapdragon.Spaces.XrVector2f Qualcomm.Snapdragon.Spaces.XrCameraSensorIntrinsicsQCOM::get_PrincipalPoint()
extern void XrCameraSensorIntrinsicsQCOM_get_PrincipalPoint_mF4C53A48DF15DA4539C81ECB8ABC8331749D2E90 (void);
// 0x0000011E Qualcomm.Snapdragon.Spaces.XrVector2f Qualcomm.Snapdragon.Spaces.XrCameraSensorIntrinsicsQCOM::get_FocalLength()
extern void XrCameraSensorIntrinsicsQCOM_get_FocalLength_m61659D7731F9C69E4D10B948F15C9F337F64C2D6 (void);
// 0x0000011F System.Single[] Qualcomm.Snapdragon.Spaces.XrCameraSensorIntrinsicsQCOM::get_RadialDistortion()
extern void XrCameraSensorIntrinsicsQCOM_get_RadialDistortion_mEDD0949CEB1ED6CB742D5DD4A5686FA40D38B42B (void);
// 0x00000120 System.Single[] Qualcomm.Snapdragon.Spaces.XrCameraSensorIntrinsicsQCOM::get_TangentialDistortion()
extern void XrCameraSensorIntrinsicsQCOM_get_TangentialDistortion_m649714F8CA4CCF1E0FDCDAFA70392E187E2D5992 (void);
// 0x00000121 Qualcomm.Snapdragon.Spaces.XrCameraDistortionModelQCOM Qualcomm.Snapdragon.Spaces.XrCameraSensorIntrinsicsQCOM::get_DistortionModel()
extern void XrCameraSensorIntrinsicsQCOM_get_DistortionModel_m8E397728AFDA69997EADE3EFD106E384425C5938 (void);
// 0x00000122 System.String Qualcomm.Snapdragon.Spaces.XrCameraSensorIntrinsicsQCOM::ToString()
extern void XrCameraSensorIntrinsicsQCOM_ToString_m1D9A21D03AE7EC2F530FAA9C1D7C80493B130733 (void);
// 0x00000123 System.Void Qualcomm.Snapdragon.Spaces.XrCameraSensorPropertiesQCOM::.ctor(Qualcomm.Snapdragon.Spaces.XrCameraSensorIntrinsicsQCOM,Qualcomm.Snapdragon.Spaces.XrPosef,Qualcomm.Snapdragon.Spaces.XrOffset2Di,Qualcomm.Snapdragon.Spaces.XrExtent2Di,Qualcomm.Snapdragon.Spaces.XrCameraSensorFacingFlagsQCOM,System.UInt64)
extern void XrCameraSensorPropertiesQCOM__ctor_mEAFD9FC1E283431CA018AC2F7255C8B57C057AA6 (void);
// 0x00000124 Qualcomm.Snapdragon.Spaces.XrCameraSensorIntrinsicsQCOM Qualcomm.Snapdragon.Spaces.XrCameraSensorPropertiesQCOM::get_Intrinsics()
extern void XrCameraSensorPropertiesQCOM_get_Intrinsics_m108ABACDFE8D9C6659ED98B336D80F337CBD376C (void);
// 0x00000125 Qualcomm.Snapdragon.Spaces.XrPosef Qualcomm.Snapdragon.Spaces.XrCameraSensorPropertiesQCOM::get_Extrinsic()
extern void XrCameraSensorPropertiesQCOM_get_Extrinsic_m87075C50E31AA53E33661B75F5347765FFD7AA19 (void);
// 0x00000126 Qualcomm.Snapdragon.Spaces.XrOffset2Di Qualcomm.Snapdragon.Spaces.XrCameraSensorPropertiesQCOM::get_ImageOffset()
extern void XrCameraSensorPropertiesQCOM_get_ImageOffset_mB40F948CF9DB970B4E7FA114ACBC1A90501CE9D4 (void);
// 0x00000127 Qualcomm.Snapdragon.Spaces.XrExtent2Di Qualcomm.Snapdragon.Spaces.XrCameraSensorPropertiesQCOM::get_ImageDimensions()
extern void XrCameraSensorPropertiesQCOM_get_ImageDimensions_mA4FE4D7DC903DCE7F160EE064509A3B14209414F (void);
// 0x00000128 Qualcomm.Snapdragon.Spaces.XrCameraSensorFacingFlagsQCOM Qualcomm.Snapdragon.Spaces.XrCameraSensorPropertiesQCOM::get_Facing()
extern void XrCameraSensorPropertiesQCOM_get_Facing_mF9019A5010372316F6EB0940630BA144310D778E (void);
// 0x00000129 System.UInt64 Qualcomm.Snapdragon.Spaces.XrCameraSensorPropertiesQCOM::get_RollingShutterLineTime()
extern void XrCameraSensorPropertiesQCOM_get_RollingShutterLineTime_mA9A434624C43BC2B0D916E001267235101D41EAC (void);
// 0x0000012A System.String Qualcomm.Snapdragon.Spaces.XrCameraSensorPropertiesQCOM::ToString()
extern void XrCameraSensorPropertiesQCOM_ToString_m0DF439C02486E2353F88E5CE3F0074ADD0A28D91 (void);
// 0x0000012B System.Int64 Qualcomm.Snapdragon.Spaces.Timespec::get_Seconds()
extern void Timespec_get_Seconds_m2E1C0153CD3BEC4805B547A871DBD327ACA911B2 (void);
// 0x0000012C System.Int64 Qualcomm.Snapdragon.Spaces.Timespec::get_Nanoseconds()
extern void Timespec_get_Nanoseconds_mF4FDF78B2ED03D58C3D591FCC414F0DAFDDEB594 (void);
// 0x0000012D System.Void Qualcomm.Snapdragon.Spaces.XrComponentVersionQCOM::.ctor(System.String,System.String,System.String,System.String,System.String)
extern void XrComponentVersionQCOM__ctor_mC929B10383F3D44EFB2300FF17EACE1B6B88C51C (void);
// 0x0000012E System.Single Qualcomm.Snapdragon.Spaces.XrExtent2Df::get_Width()
extern void XrExtent2Df_get_Width_mA8D6682438E1FDE0E5BBB25F3786D19E79B2CE70 (void);
// 0x0000012F System.Single Qualcomm.Snapdragon.Spaces.XrExtent2Df::get_Height()
extern void XrExtent2Df_get_Height_m8F6D94CCA19BEDE6F61051882F0A6CC9A4CA981C (void);
// 0x00000130 System.Void Qualcomm.Snapdragon.Spaces.XrExtent2Df::.ctor(System.Single,System.Single)
extern void XrExtent2Df__ctor_mA538CCEAF45364438C110B220C205691B6A953E6 (void);
// 0x00000131 System.Single Qualcomm.Snapdragon.Spaces.XrExtent2DfQCOM::get_Min()
extern void XrExtent2DfQCOM_get_Min_m94AECAF0A8B83DDDF1D0633F00601745139DAF8A (void);
// 0x00000132 System.Single Qualcomm.Snapdragon.Spaces.XrExtent2DfQCOM::get_Max()
extern void XrExtent2DfQCOM_get_Max_m0A320B33A9969FF1F3CA9A3C5F4FDD1FE0B86B9C (void);
// 0x00000133 System.Void Qualcomm.Snapdragon.Spaces.XrExtent2Di::.ctor(UnityEngine.Vector2Int)
extern void XrExtent2Di__ctor_mB29746B125742CB6AD747BA5FE9356AC685D73A0 (void);
// 0x00000134 System.Void Qualcomm.Snapdragon.Spaces.XrExtent2Di::.ctor(System.Int32,System.Int32)
extern void XrExtent2Di__ctor_m1D66F0C237CBCC874FE6E8A9523111C8FDBE6246 (void);
// 0x00000135 UnityEngine.Vector2Int Qualcomm.Snapdragon.Spaces.XrExtent2Di::ToVector2Int()
extern void XrExtent2Di_ToVector2Int_m16AB6984BE425191D978319A34B7A6605F7FF495 (void);
// 0x00000136 System.UInt32 Qualcomm.Snapdragon.Spaces.XrExtent2Di::get_Width()
extern void XrExtent2Di_get_Width_m572E28B2072225975B15815FBD4C751999567319 (void);
// 0x00000137 System.UInt32 Qualcomm.Snapdragon.Spaces.XrExtent2Di::get_Height()
extern void XrExtent2Di_get_Height_mA7B78D20A9F26DDDA1F0A9270350C5752193C5A5 (void);
// 0x00000138 System.String Qualcomm.Snapdragon.Spaces.XrExtent2Di::ToString()
extern void XrExtent2Di_ToString_m8BF3362EDF0DFBC8FCDF7F961F5A592EECA5C59D (void);
// 0x00000139 System.Int64 Qualcomm.Snapdragon.Spaces.XrFrameState::get_PredictedDisplayTime()
extern void XrFrameState_get_PredictedDisplayTime_m6FBB46748AABE30FFE537C362C2D396B70CDC824 (void);
// 0x0000013A System.Void Qualcomm.Snapdragon.Spaces.XrOffset2Di::.ctor(UnityEngine.Vector2Int)
extern void XrOffset2Di__ctor_m526ED1D1AE4472C41EC084A8652E4E81CAAC3B75 (void);
// 0x0000013B System.Void Qualcomm.Snapdragon.Spaces.XrOffset2Di::.ctor(System.Int32,System.Int32)
extern void XrOffset2Di__ctor_m6F07B24B95F5D1A6FE46CAA42920C5009112F4AD (void);
// 0x0000013C UnityEngine.Vector2Int Qualcomm.Snapdragon.Spaces.XrOffset2Di::ToVector2Int()
extern void XrOffset2Di_ToVector2Int_mB7CFEA8B38030163FFDA08995B60F4DF67CC1E63 (void);
// 0x0000013D System.Void Qualcomm.Snapdragon.Spaces.XrPosef::.ctor(UnityEngine.Pose)
extern void XrPosef__ctor_m3D4E4162D410903083F144466FE9B2DEBB0C21BD (void);
// 0x0000013E System.Void Qualcomm.Snapdragon.Spaces.XrPosef::.ctor(Qualcomm.Snapdragon.Spaces.XrQuaternionf,Qualcomm.Snapdragon.Spaces.XrVector3f)
extern void XrPosef__ctor_mE7394EBD4A40A09890877E8DECD26C8B6569209B (void);
// 0x0000013F UnityEngine.Pose Qualcomm.Snapdragon.Spaces.XrPosef::ToPose()
extern void XrPosef_ToPose_mDBAF38DC00B0EE86768DD9CA3031FF569FCB2BD6 (void);
// 0x00000140 System.String Qualcomm.Snapdragon.Spaces.XrPosef::ToString()
extern void XrPosef_ToString_mC16B2E1F2C83C928C73B09A09F14CC2B371DCF27 (void);
// 0x00000141 System.Void Qualcomm.Snapdragon.Spaces.XrQuaternionf::.ctor(UnityEngine.Quaternion)
extern void XrQuaternionf__ctor_m0D486D3121CBEE2FE399F14876D4FEF9031DBA94 (void);
// 0x00000142 System.Void Qualcomm.Snapdragon.Spaces.XrQuaternionf::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void XrQuaternionf__ctor_mDA964313A2BBA72BED3617B247255478040C0E87 (void);
// 0x00000143 Qualcomm.Snapdragon.Spaces.XrQuaternionf Qualcomm.Snapdragon.Spaces.XrQuaternionf::get_identity()
extern void XrQuaternionf_get_identity_mDCB638E9174D71A34DDD4F077CA5D96A51E56499 (void);
// 0x00000144 UnityEngine.Quaternion Qualcomm.Snapdragon.Spaces.XrQuaternionf::ToQuaternion()
extern void XrQuaternionf_ToQuaternion_m62B17D280617BFDAC4F5F2AB88061F00CBCD5522 (void);
// 0x00000145 System.String Qualcomm.Snapdragon.Spaces.XrQuaternionf::ToString()
extern void XrQuaternionf_ToString_m870370BDABD005D8499212D09FA41F153D150061 (void);
// 0x00000146 System.Void Qualcomm.Snapdragon.Spaces.XrSpaceLocation::InitStructureType()
extern void XrSpaceLocation_InitStructureType_m42DB29E751707B06F9BD6F9D572DF0F5B9EC5466 (void);
// 0x00000147 UnityEngine.Pose Qualcomm.Snapdragon.Spaces.XrSpaceLocation::GetPose()
extern void XrSpaceLocation_GetPose_m45AE7E0E2ADECD3E177598478CDAD84EC9A6E2FA (void);
// 0x00000148 UnityEngine.XR.ARSubsystems.TrackingState Qualcomm.Snapdragon.Spaces.XrSpaceLocation::GetTrackingState()
extern void XrSpaceLocation_GetTrackingState_mC12F5FB6F53F113B8717D98F01ED775F6AA4D536 (void);
// 0x00000149 System.String Qualcomm.Snapdragon.Spaces.XrSpaceLocation::ToString()
extern void XrSpaceLocation_ToString_mE8131ED4AC6143052739BB3ACAE76EBCA9A80734 (void);
// 0x0000014A System.UInt32 Qualcomm.Snapdragon.Spaces.XrSystemGraphicsProperties::get_MaxSwapchainImageHeight()
extern void XrSystemGraphicsProperties_get_MaxSwapchainImageHeight_m043CAFF02E9975B3F693CCAE13D22CE7BEBC9FCD (void);
// 0x0000014B System.UInt32 Qualcomm.Snapdragon.Spaces.XrSystemGraphicsProperties::get_MaxSwapchainImageWidth()
extern void XrSystemGraphicsProperties_get_MaxSwapchainImageWidth_m4F8D3DEF2B72DDF06E14D4C0B17C57EAC01E2000 (void);
// 0x0000014C System.UInt32 Qualcomm.Snapdragon.Spaces.XrSystemGraphicsProperties::get_MaxLayerCount()
extern void XrSystemGraphicsProperties_get_MaxLayerCount_mE0298316C9D0D531C20B60C83803E03F624EB542 (void);
// 0x0000014D System.Void Qualcomm.Snapdragon.Spaces.XrSystemProperties::.ctor(System.UInt64)
extern void XrSystemProperties__ctor_m9D2AC269F6F0BE29D1A4523057F1DC9DE9B9C7A9 (void);
// 0x0000014E Qualcomm.Snapdragon.Spaces.XrSystemGraphicsProperties Qualcomm.Snapdragon.Spaces.XrSystemProperties::GetGraphicsProperties()
extern void XrSystemProperties_GetGraphicsProperties_m9AD557E8D5670538BB5050A62B7A0F4210C4CC2C (void);
// 0x0000014F Qualcomm.Snapdragon.Spaces.XrSystemTrackingProperties Qualcomm.Snapdragon.Spaces.XrSystemProperties::GetTrackingProperties()
extern void XrSystemProperties_GetTrackingProperties_m285EA2F0A78F1BC576B74AF66054F3EE3DF1C295 (void);
// 0x00000150 System.Boolean Qualcomm.Snapdragon.Spaces.XrSystemTrackingProperties::get_OrientationTracking()
extern void XrSystemTrackingProperties_get_OrientationTracking_mE950098329F529540C759AD9F44456F48748141C (void);
// 0x00000151 System.Boolean Qualcomm.Snapdragon.Spaces.XrSystemTrackingProperties::get_PositionTracking()
extern void XrSystemTrackingProperties_get_PositionTracking_m507982F3CE3D9ACA6597F6A4CD01F6C99D06466B (void);
// 0x00000152 System.Void Qualcomm.Snapdragon.Spaces.XrVector2f::.ctor(UnityEngine.Vector2)
extern void XrVector2f__ctor_m64BBC3D64D3404C4B8292BB00FB2C9B1AF4B7DDB (void);
// 0x00000153 System.Void Qualcomm.Snapdragon.Spaces.XrVector2f::.ctor(System.Single,System.Single)
extern void XrVector2f__ctor_m5FE06D12022775A9D701AAEB7572BAE5B66B8796 (void);
// 0x00000154 UnityEngine.Vector2 Qualcomm.Snapdragon.Spaces.XrVector2f::ToVector2()
extern void XrVector2f_ToVector2_m5A4EC5EB1E0D1F204CEFA8AA9FB4BEB42BDD44E5 (void);
// 0x00000155 System.Void Qualcomm.Snapdragon.Spaces.XrVector3f::.ctor(UnityEngine.Vector3)
extern void XrVector3f__ctor_mD92D4AEDDCD261C251C659F451A7849DB6FF021B (void);
// 0x00000156 System.Void Qualcomm.Snapdragon.Spaces.XrVector3f::.ctor(System.Single,System.Single,System.Single)
extern void XrVector3f__ctor_mE6DAD65E298C88872C330C0A5EE282384FAE2D0C (void);
// 0x00000157 Qualcomm.Snapdragon.Spaces.XrVector3f Qualcomm.Snapdragon.Spaces.XrVector3f::get_zero()
extern void XrVector3f_get_zero_m9F43EF0518BFF78FB4C210CEF0B3E615987BB640 (void);
// 0x00000158 UnityEngine.Vector3 Qualcomm.Snapdragon.Spaces.XrVector3f::ToVector3()
extern void XrVector3f_ToVector3_m67309D501E9415F28F58B064E274AD69ED1B2455 (void);
// 0x00000159 System.String Qualcomm.Snapdragon.Spaces.XrVector3f::ToString()
extern void XrVector3f_ToString_m6D9D628E0DAD096FD0D3723CFDD7D9E6901A83F1 (void);
// 0x0000015A System.Void Qualcomm.Snapdragon.Spaces.XrHandGestureQCOM::.ctor(System.Int32,System.Single,System.Single)
extern void XrHandGestureQCOM__ctor_m914EC3452AF5071A62AD3345CDE434285CF851B1 (void);
// 0x0000015B System.Int32 Qualcomm.Snapdragon.Spaces.XrHandGestureQCOM::get_Gesture()
extern void XrHandGestureQCOM_get_Gesture_m4E999DAE36C6797A5493147F9F29E21C2BF9F0A5 (void);
// 0x0000015C System.Single Qualcomm.Snapdragon.Spaces.XrHandGestureQCOM::get_GestureRatio()
extern void XrHandGestureQCOM_get_GestureRatio_m5E04D2CCC6BAC25F2E8560EDA3D471B7AE014556 (void);
// 0x0000015D System.Single Qualcomm.Snapdragon.Spaces.XrHandGestureQCOM::get_FlipRatio()
extern void XrHandGestureQCOM_get_FlipRatio_m13546D6E39A468FBD1734435D08495951B4F2785 (void);
// 0x0000015E Qualcomm.Snapdragon.Spaces.XrPosef Qualcomm.Snapdragon.Spaces.XrHandJointLocationEXT::get_pose()
extern void XrHandJointLocationEXT_get_pose_mFA78A4F7BBD37CA6280AE3C951BD7879EE89DB41 (void);
// 0x0000015F System.Void Qualcomm.Snapdragon.Spaces.XrHandJointLocationsEXT::.ctor(System.Int32,System.IntPtr)
extern void XrHandJointLocationsEXT__ctor_m0D12A0DCC179FDF9DBA8F067C8072E7B98AEC3F6 (void);
// 0x00000160 System.Boolean Qualcomm.Snapdragon.Spaces.XrHandJointLocationsEXT::get_IsActive()
extern void XrHandJointLocationsEXT_get_IsActive_m0A68DDF8900B4CC714AC019BC540E77B6565A5D5 (void);
// 0x00000161 System.Int32 Qualcomm.Snapdragon.Spaces.XrHandJointLocationsEXT::get_JointCount()
extern void XrHandJointLocationsEXT_get_JointCount_mC75E77B048DB6148FB577355FA1DFAF83D2A9267 (void);
// 0x00000162 System.IntPtr Qualcomm.Snapdragon.Spaces.XrHandJointLocationsEXT::get_JointLocations()
extern void XrHandJointLocationsEXT_get_JointLocations_m56BA8627F5B7CA2BA9ADCDF72EFBC05C5897C3E1 (void);
// 0x00000163 System.Void Qualcomm.Snapdragon.Spaces.XrHandJointsLocateInfoEXT::.ctor(System.UInt64,System.Int64)
extern void XrHandJointsLocateInfoEXT__ctor_mA49C108B1597D5FD8D6995A58B405FA704C2B46F (void);
// 0x00000164 System.Void Qualcomm.Snapdragon.Spaces.XrHandTrackerCreateInfoEXT::.ctor(Qualcomm.Snapdragon.Spaces.XrHandEXT)
extern void XrHandTrackerCreateInfoEXT__ctor_m6F9563A07C20E6885F51F5A428A3BFFC87BEC97B (void);
// 0x00000165 System.Void Qualcomm.Snapdragon.Spaces.XrRayCastCreateInfoQCOM::.ctor(Qualcomm.Snapdragon.Spaces.XrRayTypeQCOM,System.UInt32,Qualcomm.Snapdragon.Spaces.XrRayCastTargetTypeQCOM[])
extern void XrRayCastCreateInfoQCOM__ctor_m68DE7FD4A34534AD378DF65AF8C3C41877EAEB2C (void);
// 0x00000166 System.UInt64 Qualcomm.Snapdragon.Spaces.XrRayCollisionQCOM::get_TargetId()
extern void XrRayCollisionQCOM_get_TargetId_mD6F97A47FD13AD014DFD78A060A76E54F1B3417E (void);
// 0x00000167 UnityEngine.Vector3 Qualcomm.Snapdragon.Spaces.XrRayCollisionQCOM::get_Position()
extern void XrRayCollisionQCOM_get_Position_m3F35F66CE759A2D0DEA424B0D3110BCA73E4EADC (void);
// 0x00000168 UnityEngine.Vector3 Qualcomm.Snapdragon.Spaces.XrRayCollisionQCOM::get_SurfaceNormal()
extern void XrRayCollisionQCOM_get_SurfaceNormal_m20D4DD727EF71A63BAA4D31361A656A5F3EC0CBA (void);
// 0x00000169 System.Void Qualcomm.Snapdragon.Spaces.XrRayCollisionQCOM::.ctor(System.UInt64,Qualcomm.Snapdragon.Spaces.XrSceneObjectTypeMSFT,Qualcomm.Snapdragon.Spaces.XrRayCastTargetTypeQCOM,Qualcomm.Snapdragon.Spaces.XrVector3f,Qualcomm.Snapdragon.Spaces.XrVector3f,System.Int64)
extern void XrRayCollisionQCOM__ctor_m4F8A352EAED2EE7176296C9FD3757502B86353D7 (void);
// 0x0000016A System.String Qualcomm.Snapdragon.Spaces.XrRayCollisionQCOM::ToString()
extern void XrRayCollisionQCOM_ToString_m4488BA0A99E1DC859541D33B4B81114E538855D7 (void);
// 0x0000016B System.Void Qualcomm.Snapdragon.Spaces.XrRayCollisionsGetInfoQCOM::.ctor(System.UInt64)
extern void XrRayCollisionsGetInfoQCOM__ctor_mE1F7A4E426D9DF4CA741BEFDF35259D0D6C37149 (void);
// 0x0000016C System.IntPtr Qualcomm.Snapdragon.Spaces.XrRayCollisionsQCOM::get_Collisions()
extern void XrRayCollisionsQCOM_get_Collisions_mE9E4205A7F22B2E28A60FB3BE42A0ACDE6FBAA7F (void);
// 0x0000016D System.Void Qualcomm.Snapdragon.Spaces.XrRayCollisionsQCOM::.ctor(System.IntPtr)
extern void XrRayCollisionsQCOM__ctor_m3F89E3FFDD2E276BCE00252958C795AB079AB960 (void);
// 0x0000016E System.Void Qualcomm.Snapdragon.Spaces.XrRayCollisionsQCOM::.ctor(System.UInt32,System.IntPtr,System.IntPtr)
extern void XrRayCollisionsQCOM__ctor_mCD6DC48F6634CA1C8ED3A63508573D2C0EA877E4 (void);
// 0x0000016F System.UInt64 Qualcomm.Snapdragon.Spaces.XrImageTargetLocationQCOM::get_LocationFlags()
extern void XrImageTargetLocationQCOM_get_LocationFlags_m480BE04234F57D42B53BEFCDC02BD0C788D2854C (void);
// 0x00000170 System.UInt64 Qualcomm.Snapdragon.Spaces.XrImageTargetLocationQCOM::get_ImageTargetHandle()
extern void XrImageTargetLocationQCOM_get_ImageTargetHandle_mF4D42C080A4D7CF533223F40CA8341BB67E6A07F (void);
// 0x00000171 Qualcomm.Snapdragon.Spaces.XrPosef Qualcomm.Snapdragon.Spaces.XrImageTargetLocationQCOM::get_XrPose()
extern void XrImageTargetLocationQCOM_get_XrPose_mCF7B716AD6CEF043AFBAB4CB953EDFB4DF1BDD20 (void);
// 0x00000172 System.Void Qualcomm.Snapdragon.Spaces.XrImageTargetLocationsQCOM::.ctor(System.UInt32,System.IntPtr)
extern void XrImageTargetLocationsQCOM__ctor_mAD7AAE03A06E2ED5A1C422DD04FE6B27A0676C8D (void);
// 0x00000173 System.Int32 Qualcomm.Snapdragon.Spaces.XrImageTargetLocationsQCOM::get_Count()
extern void XrImageTargetLocationsQCOM_get_Count_m845436280D51DE2910F71B75077223B3D4655A8A (void);
// 0x00000174 System.IntPtr Qualcomm.Snapdragon.Spaces.XrImageTargetLocationsQCOM::get_ImageTargetLocationsPtr()
extern void XrImageTargetLocationsQCOM_get_ImageTargetLocationsPtr_mED494AF493F7E1A5965D0145B54F93C2819C5C78 (void);
// 0x00000175 System.Void Qualcomm.Snapdragon.Spaces.XrImageTargetsLocateInfoQCOM::.ctor(System.UInt64,System.Int64)
extern void XrImageTargetsLocateInfoQCOM__ctor_mE60D4F7B12C7CEDD3BF35171287E3B5137D5B44E (void);
// 0x00000176 System.Void Qualcomm.Snapdragon.Spaces.XrImageTargetsTrackingModeInfoQCOM::.ctor(System.Int32,System.IntPtr,System.IntPtr)
extern void XrImageTargetsTrackingModeInfoQCOM__ctor_mF21A1217D975F220BAD61ED0454064ACDAE49768 (void);
// 0x00000177 System.Void Qualcomm.Snapdragon.Spaces.XrImageTrackerCreateInfoQCOM::.ctor(System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern void XrImageTrackerCreateInfoQCOM__ctor_m4EEBF84E77A1E1E4FFE770E46FF42B6CA1378295 (void);
// 0x00000178 System.Void Qualcomm.Snapdragon.Spaces.XrImageTrackerDataSetImageQCOM::.ctor(System.String,System.Single,Qualcomm.Snapdragon.Spaces.XrVector2f,System.IntPtr,System.UInt32)
extern void XrImageTrackerDataSetImageQCOM__ctor_m13420B9F18019E201E47AFEA44CC0C4E0680CD1E (void);
// 0x00000179 System.UInt32 Qualcomm.Snapdragon.Spaces.SceneUnderstandingMSFTPlane::get_VertexCount()
extern void SceneUnderstandingMSFTPlane_get_VertexCount_mD2485F6B795486C4566D9AAC565ED9A3AB1E254A (void);
// 0x0000017A System.UInt32 Qualcomm.Snapdragon.Spaces.SceneUnderstandingMSFTPlane::get_IndexCount()
extern void SceneUnderstandingMSFTPlane_get_IndexCount_mF0C611B76CADD2CCF2DADCB2330D9A2F14E1D2ED (void);
// 0x0000017B UnityEngine.Pose Qualcomm.Snapdragon.Spaces.SceneUnderstandingMSFTPlane::get_Pose()
extern void SceneUnderstandingMSFTPlane_get_Pose_m589F165643A3D65CB002F5395D1922CA863D2F0D (void);
// 0x0000017C UnityEngine.XR.ARSubsystems.BoundedPlane Qualcomm.Snapdragon.Spaces.SceneUnderstandingMSFTPlane::GetBoundedPlane(UnityEngine.Pose)
extern void SceneUnderstandingMSFTPlane_GetBoundedPlane_mEAC3E52128A7CFBE6A532673FC2E1E57BFFFCE8D (void);
// 0x0000017D UnityEngine.XR.ARSubsystems.PlaneAlignment Qualcomm.Snapdragon.Spaces.SceneUnderstandingMSFTPlane::XrScenePlaneAlignmentTypeToPlaneAlignment(Qualcomm.Snapdragon.Spaces.XrScenePlaneAlignmentTypeMSFT)
extern void SceneUnderstandingMSFTPlane_XrScenePlaneAlignmentTypeToPlaneAlignment_m180136E8B349E8D6BC1875951E36D52D9AED277E (void);
// 0x0000017E UnityEngine.XR.ARSubsystems.TrackingState Qualcomm.Snapdragon.Spaces.SceneUnderstandingMSFTPlane::GetTrackingState()
extern void SceneUnderstandingMSFTPlane_GetTrackingState_mBF8451F916683716463C2EA604514E6659DC600C (void);
// 0x0000017F System.Void Qualcomm.Snapdragon.Spaces.XrPlaneConvexHullBufferInfoQCOM::.ctor(System.UInt64)
extern void XrPlaneConvexHullBufferInfoQCOM__ctor_m6216FC2AD864CF71BFB322AD153B49D0BE188AE7 (void);
// 0x00000180 System.Void Qualcomm.Snapdragon.Spaces.XrPlaneConvexHullVertexBufferQCOM::.ctor(System.IntPtr)
extern void XrPlaneConvexHullVertexBufferQCOM__ctor_m06D87B8A0B035C3D199FEC800009BE173F6CDB97 (void);
// 0x00000181 System.Void Qualcomm.Snapdragon.Spaces.XrPlaneConvexHullVertexBufferQCOM::.ctor(System.UInt32,System.IntPtr,System.IntPtr)
extern void XrPlaneConvexHullVertexBufferQCOM__ctor_m8F17950F6A9F35AFA3A8C8D5EECA6F682F99C212 (void);
// 0x00000182 System.Void Qualcomm.Snapdragon.Spaces.XrPlaneDetectionCreateInfoQCOM::.ctor(Qualcomm.Snapdragon.Spaces.XrPlaneFilterQCOM,System.Boolean)
extern void XrPlaneDetectionCreateInfoQCOM__ctor_m09D4C9EF97EE2904489A20E7E5150DA4972315EE (void);
// 0x00000183 Qualcomm.Snapdragon.Spaces.XrExtent2DfQCOM Qualcomm.Snapdragon.Spaces.XrPlaneExtentQCOM::get_ExtentX()
extern void XrPlaneExtentQCOM_get_ExtentX_mA43B232DF1EB85716DB7915E867FF9EEF779FF14 (void);
// 0x00000184 Qualcomm.Snapdragon.Spaces.XrExtent2DfQCOM Qualcomm.Snapdragon.Spaces.XrPlaneExtentQCOM::get_ExtentY()
extern void XrPlaneExtentQCOM_get_ExtentY_m40E8D4402F3F490F0ECEC4A753F2AF888F1B352A (void);
// 0x00000185 System.UInt64 Qualcomm.Snapdragon.Spaces.XrPlaneLocationQCOM::get_ConvexHullId()
extern void XrPlaneLocationQCOM_get_ConvexHullId_mECBD3FEFFB2D85508052401E65354E90B98862A3 (void);
// 0x00000186 UnityEngine.XR.ARSubsystems.BoundedPlane Qualcomm.Snapdragon.Spaces.XrPlaneLocationQCOM::GetBoundedPlane(System.UInt64)
extern void XrPlaneLocationQCOM_GetBoundedPlane_mBFBE48AE22AF36F0CCAEBE6ACA62AC355CCB490B (void);
// 0x00000187 UnityEngine.XR.ARSubsystems.PlaneAlignment Qualcomm.Snapdragon.Spaces.XrPlaneLocationQCOM::XrPlaneTypeToPlaneAlignment(Qualcomm.Snapdragon.Spaces.XrPlaneTypeQCOM)
extern void XrPlaneLocationQCOM_XrPlaneTypeToPlaneAlignment_mC084EB647B85BCD37B18C319F795F52243480165 (void);
// 0x00000188 System.Void Qualcomm.Snapdragon.Spaces.XrPlaneLocationsQCOM::.ctor(System.IntPtr)
extern void XrPlaneLocationsQCOM__ctor_m0732504CABB07CE7FB531E5DE2F3A0E1B6702BA9 (void);
// 0x00000189 System.Void Qualcomm.Snapdragon.Spaces.XrPlaneLocationsQCOM::.ctor(System.UInt32,System.IntPtr,System.IntPtr)
extern void XrPlaneLocationsQCOM__ctor_mF62762AC161282C374C6990DC8677B01CC2D978C (void);
// 0x0000018A System.IntPtr Qualcomm.Snapdragon.Spaces.XrPlaneLocationsQCOM::get_PlaneCountOutput()
extern void XrPlaneLocationsQCOM_get_PlaneCountOutput_mB291268D3ABA521FB809141A8D6E534397FC25B3 (void);
// 0x0000018B System.IntPtr Qualcomm.Snapdragon.Spaces.XrPlaneLocationsQCOM::get_PlaneLocations()
extern void XrPlaneLocationsQCOM_get_PlaneLocations_m9F42475FF18EF1BBA4B7F9D64C0ED19621C6E40E (void);
// 0x0000018C System.Void Qualcomm.Snapdragon.Spaces.XrPlanesLocateInfoQCOM::.ctor(System.UInt64,System.Int64)
extern void XrPlanesLocateInfoQCOM__ctor_m4F811F5724DA7A6A1E903B5C1B0981C0D4311FBF (void);
// 0x0000018D System.Void Qualcomm.Snapdragon.Spaces.XrMarkerDetectionStartInfoQCOM::.ctor(System.UInt32,System.IntPtr,System.IntPtr)
extern void XrMarkerDetectionStartInfoQCOM__ctor_m4BE31B1DB8305A22473EBBB39C37C540DB57BBCF (void);
// 0x0000018E System.Void Qualcomm.Snapdragon.Spaces.XrMarkerDetectionStartInfoQCOM::.ctor(System.UInt32,System.IntPtr)
extern void XrMarkerDetectionStartInfoQCOM__ctor_mCA884E7DE29D3C9B6369B613FD8822B9283F7C76 (void);
// 0x0000018F System.Void Qualcomm.Snapdragon.Spaces.XrMarkerSpaceCreateInfoQCOM::.ctor(System.IntPtr,Qualcomm.Snapdragon.Spaces.XrPosef)
extern void XrMarkerSpaceCreateInfoQCOM__ctor_m50AB7485BB052A1E3266D4BEC03E50F774D1F935 (void);
// 0x00000190 System.Void Qualcomm.Snapdragon.Spaces.XrMarkerTrackerCreateInfoQCOM::.ctor()
extern void XrMarkerTrackerCreateInfoQCOM__ctor_mD2466D57219B40F3A51FAFE73F2D05140B47F604 (void);
// 0x00000191 System.Void Qualcomm.Snapdragon.Spaces.XrMarkerTrackingModeInfoQCOM::.ctor(Qualcomm.Snapdragon.Spaces.XrMarkerTrackingModeQCOM)
extern void XrMarkerTrackingModeInfoQCOM__ctor_m2D680306B638728344DEB235C70995F87617D623 (void);
// 0x00000192 System.UInt64 Qualcomm.Snapdragon.Spaces.XrMarkerUpdateInfoQCOM::get_Marker()
extern void XrMarkerUpdateInfoQCOM_get_Marker_mA80BD87CFDB08692508EA0594A5CC1B895139E6C (void);
// 0x00000193 System.Boolean Qualcomm.Snapdragon.Spaces.XrMarkerUpdateInfoQCOM::get_IsDetected()
extern void XrMarkerUpdateInfoQCOM_get_IsDetected_mA1F9B8849E6EC7B8CF992E59EE8B71ACF45AAD11 (void);
// 0x00000194 System.Boolean Qualcomm.Snapdragon.Spaces.XrMarkerUpdateInfoQCOM::get_IsMarkerDataAvailable()
extern void XrMarkerUpdateInfoQCOM_get_IsMarkerDataAvailable_m2F97E271C4DE68664F3438431623232E0245A48F (void);
// 0x00000195 System.String Qualcomm.Snapdragon.Spaces.XrMarkerUpdateInfoQCOM::ToString()
extern void XrMarkerUpdateInfoQCOM_ToString_m44B04704D107F400CF1C8CB036786C33628FF00C (void);
// 0x00000196 System.Void Qualcomm.Snapdragon.Spaces.XrQrCodeVersionFilterQCOM::.ctor(System.UInt32,System.IntPtr)
extern void XrQrCodeVersionFilterQCOM__ctor_m6114340545DFA05ED9E671F6CDB0E7E9863E71EF (void);
// 0x00000197 System.Void Qualcomm.Snapdragon.Spaces.XrQrCodeVersionRangeQCOM::.ctor(Qualcomm.Snapdragon.Spaces.XrQrCodeSymbolTypeQCOM,System.Byte,System.Byte)
extern void XrQrCodeVersionRangeQCOM__ctor_mEE8968742EDBB3354417F01840242F4B8989C9DD (void);
// 0x00000198 System.Void Qualcomm.Snapdragon.Spaces.XrUserDefinedMarkerSizeQCOM::.ctor(Qualcomm.Snapdragon.Spaces.XrExtent2Df)
extern void XrUserDefinedMarkerSizeQCOM__ctor_m8CFE8D7BAB694A5A0068761AE3D3276216E63573 (void);
// 0x00000199 System.Void Qualcomm.Snapdragon.Spaces.XrSceneCreateInfoMSFT::.ctor()
extern void XrSceneCreateInfoMSFT__ctor_mC36C9038DD64C3A1DA6F45FAC007F5EBA27D0582 (void);
// 0x0000019A System.Void Qualcomm.Snapdragon.Spaces.XrSceneObserverCreateInfoMSFT::.ctor()
extern void XrSceneObserverCreateInfoMSFT__ctor_m0AFC5A1189B4C5E151A642B0AACBD5E235E5815F (void);
// 0x0000019B System.Void Qualcomm.Snapdragon.Spaces.XrSpatialAnchorCreateInfoMSFT::.ctor(UnityEngine.Pose,System.UInt64,System.Int64)
extern void XrSpatialAnchorCreateInfoMSFT__ctor_m30BEA3DC5CBAF77172B5CF78783DC2A0A4441DC6 (void);
// 0x0000019C System.Void Qualcomm.Snapdragon.Spaces.XrSpatialAnchorFromPersistedAnchorCreateInfoMSFT::.ctor(System.UInt64,System.String)
extern void XrSpatialAnchorFromPersistedAnchorCreateInfoMSFT__ctor_m51AE46AEA9B4290FBC9288833FC4BE71C8C52B68 (void);
// 0x0000019D System.Void Qualcomm.Snapdragon.Spaces.XrSpatialAnchorPersistenceInfoMSFT::.ctor(System.String,System.UInt64)
extern void XrSpatialAnchorPersistenceInfoMSFT__ctor_m597F42F50F6786918CF94BA946517678E9E2557B (void);
// 0x0000019E System.Void Qualcomm.Snapdragon.Spaces.XrSpatialAnchorPersistenceNameMSFT::.ctor(System.String)
extern void XrSpatialAnchorPersistenceNameMSFT__ctor_m79DF56CBF47872031AD00EFF1B94980060D679C8 (void);
// 0x0000019F System.Void Qualcomm.Snapdragon.Spaces.XrSpatialAnchorSpaceCreateInfoMSFT::.ctor(System.UInt64)
extern void XrSpatialAnchorSpaceCreateInfoMSFT__ctor_m0E7262C30F18E0E1FE14CBC5E9F2DEDBE55EC70E (void);
// 0x000001A0 UnityEngine.XR.ARFoundation.ARSessionOrigin Qualcomm.Snapdragon.Spaces.OriginLocationUtility::FindARSessionOrigin(System.Boolean)
extern void OriginLocationUtility_FindARSessionOrigin_m3AF4E6E957AC451789BF220EF690736EC9530BFC (void);
// 0x000001A1 UnityEngine.Camera Qualcomm.Snapdragon.Spaces.OriginLocationUtility::GetOriginCamera(System.Boolean)
extern void OriginLocationUtility_GetOriginCamera_m566BBE932D4C000B9B4506F8BB27BA2A405C3BB7 (void);
// 0x000001A2 UnityEngine.Transform Qualcomm.Snapdragon.Spaces.OriginLocationUtility::GetOriginTransform(System.Boolean)
extern void OriginLocationUtility_GetOriginTransform_mBB5C1CF2C4E110BC79E404697A0FA421E5E89D1E (void);
// 0x000001A3 UnityEngine.Vector2 Qualcomm.Snapdragon.Spaces.SpacesCompositionLayer::get_Extents()
extern void SpacesCompositionLayer_get_Extents_m7D22498E3E1A67A21473BEA9E8A52767C64F6AFA (void);
// 0x000001A4 System.Void Qualcomm.Snapdragon.Spaces.SpacesCompositionLayer::set_Extents(UnityEngine.Vector2)
extern void SpacesCompositionLayer_set_Extents_m5FF4E7B0000A7105D29C13E636F251EF629C0732 (void);
// 0x000001A5 UnityEngine.Quaternion Qualcomm.Snapdragon.Spaces.SpacesCompositionLayer::get_Orientation()
extern void SpacesCompositionLayer_get_Orientation_m893DF5F09722685C070784634183FB04CC5C10E0 (void);
// 0x000001A6 System.Void Qualcomm.Snapdragon.Spaces.SpacesCompositionLayer::set_Orientation(UnityEngine.Quaternion)
extern void SpacesCompositionLayer_set_Orientation_mA127A24AF6702E73875906A13F709C954AA23EA6 (void);
// 0x000001A7 UnityEngine.Vector3 Qualcomm.Snapdragon.Spaces.SpacesCompositionLayer::get_Position()
extern void SpacesCompositionLayer_get_Position_m229352ED364ED8F36072ED1498920C04B6C00BF6 (void);
// 0x000001A8 System.Void Qualcomm.Snapdragon.Spaces.SpacesCompositionLayer::set_Position(UnityEngine.Vector3)
extern void SpacesCompositionLayer_set_Position_m0C69A4C88D209DA1427927A635756045A5DC1D7B (void);
// 0x000001A9 System.UInt32 Qualcomm.Snapdragon.Spaces.SpacesCompositionLayer::get_SortingOrder()
extern void SpacesCompositionLayer_get_SortingOrder_mCEE7BF222A1D7CB097F9221B2FF80D41DA9B9800 (void);
// 0x000001AA System.Void Qualcomm.Snapdragon.Spaces.SpacesCompositionLayer::set_SortingOrder(System.UInt32)
extern void SpacesCompositionLayer_set_SortingOrder_mAD2E85E5BABC8A02099F740B6CDC8C44360E507E (void);
// 0x000001AB System.Void Qualcomm.Snapdragon.Spaces.SpacesCompositionLayer::Start()
extern void SpacesCompositionLayer_Start_m1A41283AA0032D1E08E1A0E61C369047CE45208B (void);
// 0x000001AC System.Void Qualcomm.Snapdragon.Spaces.SpacesCompositionLayer::Update()
extern void SpacesCompositionLayer_Update_m949315224C41DEC945FB3ADB68476E099E1D3D30 (void);
// 0x000001AD System.Void Qualcomm.Snapdragon.Spaces.SpacesCompositionLayer::OnDestroy()
extern void SpacesCompositionLayer_OnDestroy_mBF85D8B1091AF5FA38BF39059BAF167A3CCEBB20 (void);
// 0x000001AE System.Void Qualcomm.Snapdragon.Spaces.SpacesCompositionLayer::OnEnable()
extern void SpacesCompositionLayer_OnEnable_m41EECA50376A34A014CE1665CD2497690C0AB988 (void);
// 0x000001AF System.Void Qualcomm.Snapdragon.Spaces.SpacesCompositionLayer::OnDisable()
extern void SpacesCompositionLayer_OnDisable_m0ED2D57D401CC0D1C32AACCF903003350279AD90 (void);
// 0x000001B0 System.Void Qualcomm.Snapdragon.Spaces.SpacesCompositionLayer::UpdateSwapchainImage()
extern void SpacesCompositionLayer_UpdateSwapchainImage_m41E4DA32588A6EBA476FD38CFFD21015C104C351 (void);
// 0x000001B1 System.Void Qualcomm.Snapdragon.Spaces.SpacesCompositionLayer::.ctor()
extern void SpacesCompositionLayer__ctor_m6D0F713F753770453F14E01E1A650DD757C46826 (void);
// 0x000001B2 System.Void Qualcomm.Snapdragon.Spaces.SessionSubsystem::RegisterDescriptor()
extern void SessionSubsystem_RegisterDescriptor_m21D41FBC86C2542A21A99DADA960CE2CFD0DEC00 (void);
// 0x000001B3 System.Void Qualcomm.Snapdragon.Spaces.SessionSubsystem::.ctor()
extern void SessionSubsystem__ctor_m56974216D0C275EF099478A6F6CD0C75D218E8C7 (void);
// 0x000001B4 Qualcomm.Snapdragon.Spaces.BaseRuntimeFeature Qualcomm.Snapdragon.Spaces.SessionSubsystem/SessionProvider::get_UnderlyingFeature()
extern void SessionProvider_get_UnderlyingFeature_m8BFCA562F555F14AB3F40729E76510FFF6A9D9B0 (void);
// 0x000001B5 UnityEngine.XR.ARSubsystems.Feature Qualcomm.Snapdragon.Spaces.SessionSubsystem/SessionProvider::get_requestedFeatures()
extern void SessionProvider_get_requestedFeatures_mC84B1F22FA2355F292CF94E09F929ABEC641EFBF (void);
// 0x000001B6 UnityEngine.XR.ARSubsystems.Feature Qualcomm.Snapdragon.Spaces.SessionSubsystem/SessionProvider::get_currentTrackingMode()
extern void SessionProvider_get_currentTrackingMode_mFF13C8CD14E3B49C2152600C9A92B7DF4A4DDCB5 (void);
// 0x000001B7 UnityEngine.XR.ARSubsystems.TrackingState Qualcomm.Snapdragon.Spaces.SessionSubsystem/SessionProvider::get_trackingState()
extern void SessionProvider_get_trackingState_mDEF8C84DE1249D10DAC9D2EE96DE9A4BAB689C7C (void);
// 0x000001B8 UnityEngine.XR.ARSubsystems.NotTrackingReason Qualcomm.Snapdragon.Spaces.SessionSubsystem/SessionProvider::get_notTrackingReason()
extern void SessionProvider_get_notTrackingReason_m156956A942B544501D1E7E5633F57481EA85E022 (void);
// 0x000001B9 UnityEngine.XR.ARSubsystems.Feature Qualcomm.Snapdragon.Spaces.SessionSubsystem/SessionProvider::get_requestedTrackingMode()
extern void SessionProvider_get_requestedTrackingMode_m2C746BECD2B8BF5B176848180B6A9D8F4B0A7CEC (void);
// 0x000001BA System.Void Qualcomm.Snapdragon.Spaces.SessionSubsystem/SessionProvider::set_requestedTrackingMode(UnityEngine.XR.ARSubsystems.Feature)
extern void SessionProvider_set_requestedTrackingMode_m3A41CD10877FA32752CF02E863E55958F401665A (void);
// 0x000001BB System.Int32 Qualcomm.Snapdragon.Spaces.SessionSubsystem/SessionProvider::get_frameRate()
extern void SessionProvider_get_frameRate_m8D9AE50F5BA715087477FCD704E079265A56E836 (void);
// 0x000001BC System.Boolean Qualcomm.Snapdragon.Spaces.SessionSubsystem/SessionProvider::get_matchFrameRateEnabled()
extern void SessionProvider_get_matchFrameRateEnabled_m6EC1EBD8390BE410055BA13DCD57862ADA6C0D44 (void);
// 0x000001BD System.Boolean Qualcomm.Snapdragon.Spaces.SessionSubsystem/SessionProvider::get_matchFrameRateRequested()
extern void SessionProvider_get_matchFrameRateRequested_m635B7F938F8EB9EE50964BEB225FE7CB6152CC0F (void);
// 0x000001BE System.IntPtr Qualcomm.Snapdragon.Spaces.SessionSubsystem/SessionProvider::get_nativePtr()
extern void SessionProvider_get_nativePtr_m3ADDC71B66003C1E65A5F3A2538AE9271E2EB404 (void);
// 0x000001BF System.Guid Qualcomm.Snapdragon.Spaces.SessionSubsystem/SessionProvider::get_sessionId()
extern void SessionProvider_get_sessionId_m3A87F6A02956795034B78324C747F81664979F66 (void);
// 0x000001C0 System.Void Qualcomm.Snapdragon.Spaces.SessionSubsystem/SessionProvider::Start()
extern void SessionProvider_Start_mB43616AF993473E91CF477050149C93B59788795 (void);
// 0x000001C1 System.Void Qualcomm.Snapdragon.Spaces.SessionSubsystem/SessionProvider::Stop()
extern void SessionProvider_Stop_m689DC7870976C02C63F4C744887987281C77D1A7 (void);
// 0x000001C2 System.Void Qualcomm.Snapdragon.Spaces.SessionSubsystem/SessionProvider::Destroy()
extern void SessionProvider_Destroy_m7C7B2C3F30972D1E784137714AABC925586444A2 (void);
// 0x000001C3 UnityEngine.XR.ARSubsystems.Promise`1<UnityEngine.XR.ARSubsystems.SessionAvailability> Qualcomm.Snapdragon.Spaces.SessionSubsystem/SessionProvider::GetAvailabilityAsync()
extern void SessionProvider_GetAvailabilityAsync_m04A788ACC06E5E1189CB4DB543068FE70C28B891 (void);
// 0x000001C4 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.ConfigurationDescriptor> Qualcomm.Snapdragon.Spaces.SessionSubsystem/SessionProvider::GetConfigurationDescriptors(Unity.Collections.Allocator)
extern void SessionProvider_GetConfigurationDescriptors_m4760DE44B4549C0A22A654F9D2706F5F7D07648E (void);
// 0x000001C5 System.Void Qualcomm.Snapdragon.Spaces.SessionSubsystem/SessionProvider::OnApplicationPause()
extern void SessionProvider_OnApplicationPause_m3C449DA54CCC29E8D31681837CB0F411B7D78432 (void);
// 0x000001C6 System.Void Qualcomm.Snapdragon.Spaces.SessionSubsystem/SessionProvider::OnApplicationResume()
extern void SessionProvider_OnApplicationResume_m23950128290C2986ECC4671E3A1C8158AE24B395 (void);
// 0x000001C7 System.Void Qualcomm.Snapdragon.Spaces.SessionSubsystem/SessionProvider::Update(UnityEngine.XR.ARSubsystems.XRSessionUpdateParams,UnityEngine.XR.ARSubsystems.Configuration)
extern void SessionProvider_Update_m1E28C28457FFB01DB725A2CC8601794DEF0E02F7 (void);
// 0x000001C8 System.Void Qualcomm.Snapdragon.Spaces.SessionSubsystem/SessionProvider::Update(UnityEngine.XR.ARSubsystems.XRSessionUpdateParams)
extern void SessionProvider_Update_mD5021ACDFC52F2111A180487E64A1C36E039C32E (void);
// 0x000001C9 System.Void Qualcomm.Snapdragon.Spaces.SessionSubsystem/SessionProvider::.ctor()
extern void SessionProvider__ctor_mCE4EC57FC85B9CDCF128AC96254FFE8A4ACA7CED (void);
// 0x000001CA System.Void Qualcomm.Snapdragon.Spaces.SpacesMicrosoftMixedRealityMotionControllerProfile::.ctor()
extern void SpacesMicrosoftMixedRealityMotionControllerProfile__ctor_mBAA2E7F31C033C6BC4FFB3E5652051FEBAADDAA1 (void);
// 0x000001CB System.UInt64 Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::get_SessionHandle()
extern void SpacesOpenXRFeature_get_SessionHandle_mC1D5AE58DB4A5326847DA40475D9459B6AF58F16 (void);
// 0x000001CC System.Void Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::set_SessionHandle(System.UInt64)
extern void SpacesOpenXRFeature_set_SessionHandle_m2547BA850C8F2FA59F494FB70DC6206863E53611 (void);
// 0x000001CD System.UInt64 Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::get_SystemIDHandle()
extern void SpacesOpenXRFeature_get_SystemIDHandle_m833A27758B6E2A903DF370165AC24FB22063CBE3 (void);
// 0x000001CE System.Void Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::set_SystemIDHandle(System.UInt64)
extern void SpacesOpenXRFeature_set_SystemIDHandle_m853677837093C9A032AC2F8D3DFE946B239142F2 (void);
// 0x000001CF System.UInt64 Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::get_InstanceHandle()
extern void SpacesOpenXRFeature_get_InstanceHandle_m98BC1FEA822EF6AD1CD03957C407B03BFD18DB72 (void);
// 0x000001D0 System.Void Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::set_InstanceHandle(System.UInt64)
extern void SpacesOpenXRFeature_set_InstanceHandle_m7D88CD0D833F481C64B699A98415A6E1A98B8BD4 (void);
// 0x000001D1 System.UInt64 Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::get_SpaceHandle()
extern void SpacesOpenXRFeature_get_SpaceHandle_m3A8C3EA515461E2D805C79E15857736C89CF87D0 (void);
// 0x000001D2 System.Void Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::set_SpaceHandle(System.UInt64)
extern void SpacesOpenXRFeature_set_SpaceHandle_m5A396CC5EE54CF9B5307E88314EFBDA1F7E54402 (void);
// 0x000001D3 System.Boolean Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::get_IsSessionRunning()
extern void SpacesOpenXRFeature_get_IsSessionRunning_m3E3825F16B149E2CD593D0F41FAA4BA9B27BB602 (void);
// 0x000001D4 System.Void Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::set_IsSessionRunning(System.Boolean)
extern void SpacesOpenXRFeature_set_IsSessionRunning_mC98A5F7BD3450123D352A22A9C8519313B7AE7A7 (void);
// 0x000001D5 System.Int32 Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::get_SessionState()
extern void SpacesOpenXRFeature_get_SessionState_mCAAD8017A68C7AD0C34E965264CB9B2205260221 (void);
// 0x000001D6 System.Void Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::set_SessionState(System.Int32)
extern void SpacesOpenXRFeature_set_SessionState_m51D6547022685508F73BC705203549EB720A9481 (void);
// 0x000001D7 System.Boolean Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::get_IsRequiringBaseRuntimeFeature()
extern void SpacesOpenXRFeature_get_IsRequiringBaseRuntimeFeature_mB400EDEA312EC59600570A7BEC183E8EBF41F25A (void);
// 0x000001D8 System.Boolean Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::OnInstanceCreate(System.UInt64)
extern void SpacesOpenXRFeature_OnInstanceCreate_m0324D4194E2B85DE1F841E6F07FD59E585DAB336 (void);
// 0x000001D9 System.Void Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::OnHookMethods()
extern void SpacesOpenXRFeature_OnHookMethods_mADAD2B6D5635468C58B238E4BDB2BB04435DDA14 (void);
// 0x000001DA System.Void Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::HookMethod(System.String,TDelegate&)
// 0x000001DB System.Void Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::OnInstanceDestroy(System.UInt64)
extern void SpacesOpenXRFeature_OnInstanceDestroy_m8DA872D6621843E73068E413321DFC5D3FAA7C0C (void);
// 0x000001DC System.Void Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::OnSystemChange(System.UInt64)
extern void SpacesOpenXRFeature_OnSystemChange_mE4534AD73928172B278C4075AEBE7E317A287B42 (void);
// 0x000001DD System.Void Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::OnSessionCreate(System.UInt64)
extern void SpacesOpenXRFeature_OnSessionCreate_m36B81205F47352B84F2039C94CE58DBBB66A1804 (void);
// 0x000001DE System.Void Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::OnSessionBegin(System.UInt64)
extern void SpacesOpenXRFeature_OnSessionBegin_m411839095F3AE8D29887EDC4C5699A11CE49DD22 (void);
// 0x000001DF System.Void Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::OnSessionStateChange(System.Int32,System.Int32)
extern void SpacesOpenXRFeature_OnSessionStateChange_mAA634FEFFA96C3ADE35A80EE2CC13BCE241D6751 (void);
// 0x000001E0 System.Void Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::OnSessionEnd(System.UInt64)
extern void SpacesOpenXRFeature_OnSessionEnd_m64496EF1F09BB07A63E450C4A937B311ECE320DD (void);
// 0x000001E1 System.Void Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::OnSessionDestroy(System.UInt64)
extern void SpacesOpenXRFeature_OnSessionDestroy_m8491E3AFBA1497289FD669B13C583C0F33BC9A59 (void);
// 0x000001E2 System.Void Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::OnAppSpaceChange(System.UInt64)
extern void SpacesOpenXRFeature_OnAppSpaceChange_mB3B954691F79C6A6CCD5C2E900EB445696E7AC87 (void);
// 0x000001E3 System.Collections.Generic.IEnumerable`1<System.String> Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::GetMissingExtensions(System.String)
extern void SpacesOpenXRFeature_GetMissingExtensions_mC574B76CDC275762A5368CF9214ACA2DF6722361 (void);
// 0x000001E4 System.String Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::GetXrLayersToLoad()
extern void SpacesOpenXRFeature_GetXrLayersToLoad_m630E60FE96BDE7EBEBA65D88D62018F22183DC07 (void);
// 0x000001E5 System.UInt32 Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::RequestLayers(System.String)
extern void SpacesOpenXRFeature_RequestLayers_m14C99CB5811B2F000D19FD2AE2BCDD2866FF5E88 (void);
// 0x000001E6 System.IntPtr Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::HookGetInstanceProcAddr(System.IntPtr)
extern void SpacesOpenXRFeature_HookGetInstanceProcAddr_mE588877A52024E62D2CD74CA17CF53ECE83C33B6 (void);
// 0x000001E7 System.Void Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature::.ctor()
extern void SpacesOpenXRFeature__ctor_mFC1AC4D00ABFBCF3DC87141BF5ECEB49DCF5EE16 (void);
// 0x000001E8 System.Void Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature/GetInstanceProcAddrDelegate::.ctor(System.Object,System.IntPtr)
extern void GetInstanceProcAddrDelegate__ctor_m25E509979A33A25930863C6370A9666AA73A01B8 (void);
// 0x000001E9 System.Int32 Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature/GetInstanceProcAddrDelegate::Invoke(System.IntPtr,System.String,System.IntPtr&)
extern void GetInstanceProcAddrDelegate_Invoke_m668AB68A61FA880E785ED4F7530758B41EC07EE5 (void);
// 0x000001EA System.IAsyncResult Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature/GetInstanceProcAddrDelegate::BeginInvoke(System.IntPtr,System.String,System.IntPtr&,System.AsyncCallback,System.Object)
extern void GetInstanceProcAddrDelegate_BeginInvoke_m95F0D72B853C1066279BF14847406FD09DA383B4 (void);
// 0x000001EB System.Int32 Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature/GetInstanceProcAddrDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void GetInstanceProcAddrDelegate_EndInvoke_m5089FB47BF5023C18040B1BCC505A6B4C02A3BFC (void);
// 0x000001EC System.Void Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature/<>c::.cctor()
extern void U3CU3Ec__cctor_mF07EDA2CB995EB25E08A9935950461F11124ADBF (void);
// 0x000001ED System.Void Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature/<>c::.ctor()
extern void U3CU3Ec__ctor_m04A2E59D537938A5B110707BFCC60699BA1A3F6D (void);
// 0x000001EE System.Boolean Qualcomm.Snapdragon.Spaces.SpacesOpenXRFeature/<>c::<GetMissingExtensions>b__40_0(System.String)
extern void U3CU3Ec_U3CGetMissingExtensionsU3Eb__40_0_m31B4109FBB1D9F176565640DC73E1A3AB72B9205 (void);
// 0x000001EF System.Void Qualcomm.Snapdragon.Spaces.SpacesRuntimeControllerStartController::StartControllerOnLoadIfNeeded()
extern void SpacesRuntimeControllerStartController_StartControllerOnLoadIfNeeded_m159D565E88598EA914BDC4C26449D0F44727FE3B (void);
// 0x000001F0 System.Void Qualcomm.Snapdragon.Spaces.SpacesRuntimeControllerStartController::.ctor()
extern void SpacesRuntimeControllerStartController__ctor_mB1DC08674CA6FA4063912094695A641F7DE2C018 (void);
// 0x000001F1 System.Void Qualcomm.Snapdragon.Spaces.SpacesThreadUtility::SetThreadHint(Qualcomm.Snapdragon.Spaces.SpacesThreadType)
extern void SpacesThreadUtility_SetThreadHint_m107730E58D99683DE911A325E742D3344A38C5BF (void);
// 0x000001F2 System.Void Qualcomm.Snapdragon.Spaces.SpacesThreadUtility::.cctor()
extern void SpacesThreadUtility__cctor_m5764964D0E2EB7A82ED47F428E7286E8940454DB (void);
// 0x000001F3 System.Int32 Qualcomm.Snapdragon.Spaces.ScopeArrayPtr`1::get_ElementCount()
// 0x000001F4 System.IntPtr Qualcomm.Snapdragon.Spaces.ScopeArrayPtr`1::get_Raw()
// 0x000001F5 System.Void Qualcomm.Snapdragon.Spaces.ScopeArrayPtr`1::.ctor(System.Int32,System.String)
// 0x000001F6 System.Void Qualcomm.Snapdragon.Spaces.ScopeArrayPtr`1::.ctor(System.IntPtr&,System.Int32,System.String)
// 0x000001F7 System.Void Qualcomm.Snapdragon.Spaces.ScopeArrayPtr`1::Finalize()
// 0x000001F8 System.Void Qualcomm.Snapdragon.Spaces.ScopeArrayPtr`1::Dispose()
// 0x000001F9 System.Void Qualcomm.Snapdragon.Spaces.ScopeArrayPtr`1::DisposeResources()
// 0x000001FA System.Void Qualcomm.Snapdragon.Spaces.ScopeArrayPtr`1::Copy(T,System.Int32)
// 0x000001FB T Qualcomm.Snapdragon.Spaces.ScopeArrayPtr`1::AtIndex(System.Int32)
// 0x000001FC System.IntPtr Qualcomm.Snapdragon.Spaces.ScopeArrayPtr`1::AtIndexRaw(System.Int32)
// 0x000001FD System.IntPtr Qualcomm.Snapdragon.Spaces.ScopePtr`1::get_Raw()
// 0x000001FE System.Void Qualcomm.Snapdragon.Spaces.ScopePtr`1::.ctor(System.String)
// 0x000001FF System.Void Qualcomm.Snapdragon.Spaces.ScopePtr`1::.ctor(System.IntPtr&,System.String)
// 0x00000200 System.Void Qualcomm.Snapdragon.Spaces.ScopePtr`1::.ctor(T,System.String)
// 0x00000201 System.Void Qualcomm.Snapdragon.Spaces.ScopePtr`1::.ctor(System.Int32,System.String)
// 0x00000202 System.Void Qualcomm.Snapdragon.Spaces.ScopePtr`1::Finalize()
// 0x00000203 System.Void Qualcomm.Snapdragon.Spaces.ScopePtr`1::Dispose()
// 0x00000204 System.Void Qualcomm.Snapdragon.Spaces.ScopePtr`1::DisposeResources()
// 0x00000205 System.Void Qualcomm.Snapdragon.Spaces.ScopePtr`1::Copy(T)
// 0x00000206 System.String Qualcomm.Snapdragon.Spaces.ScopePtr`1::AsString()
// 0x00000207 T Qualcomm.Snapdragon.Spaces.ScopePtr`1::AsStruct()
// 0x00000208 UnityEngine.Pose[] Qualcomm.Snapdragon.Spaces.SpacesHand::get_RawJoints()
extern void SpacesHand_get_RawJoints_m544EB999467A240FA9277DCF7A2E220EB1A4C88E (void);
// 0x00000209 System.Int32 Qualcomm.Snapdragon.Spaces.SpacesHand::get_RawGesture()
extern void SpacesHand_get_RawGesture_mCE3F3F78C6EDCB9A94E8489D1851835760285C8C (void);
// 0x0000020A System.Single Qualcomm.Snapdragon.Spaces.SpacesHand::get_RawGestureRatio()
extern void SpacesHand_get_RawGestureRatio_mEE8BAC489DCC60E59920ED7270B0120341406EB3 (void);
// 0x0000020B System.Single Qualcomm.Snapdragon.Spaces.SpacesHand::get_RawFlipRatio()
extern void SpacesHand_get_RawFlipRatio_mD1A89190D4DC1A04E18BD6ED30F099E39162DAFC (void);
// 0x0000020C UnityEngine.Pose Qualcomm.Snapdragon.Spaces.SpacesHand::get_Pose()
extern void SpacesHand_get_Pose_m6CCFDDE3CC1ADCD75B96E6603E2B6BA382EC57D4 (void);
// 0x0000020D System.Boolean Qualcomm.Snapdragon.Spaces.SpacesHand::get_IsLeft()
extern void SpacesHand_get_IsLeft_mAB2A45AC1D2AF3F5651C2BE77C0770B7BF83FC9A (void);
// 0x0000020E System.Void Qualcomm.Snapdragon.Spaces.SpacesHand::set_IsLeft(System.Boolean)
extern void SpacesHand_set_IsLeft_m8E935112F7F569DC9BB70FF2AE68FEE3EC6C8D33 (void);
// 0x0000020F Qualcomm.Snapdragon.Spaces.SpacesHand/Joint[] Qualcomm.Snapdragon.Spaces.SpacesHand::get_Joints()
extern void SpacesHand_get_Joints_mD1BD591CE21E448A34ECB7D230AA3E4D9F220A35 (void);
// 0x00000210 Qualcomm.Snapdragon.Spaces.SpacesHand/Gesture Qualcomm.Snapdragon.Spaces.SpacesHand::get_CurrentGesture()
extern void SpacesHand_get_CurrentGesture_mF057BEF3C8F94F6363C8C5EEF3599BB9FBFC94E9 (void);
// 0x00000211 System.Void Qualcomm.Snapdragon.Spaces.SpacesHand::UpdateHandData(Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystem)
extern void SpacesHand_UpdateHandData_mF7C271A6927FED4968F9D71A3AF83089CC06DE94 (void);
// 0x00000212 System.Void Qualcomm.Snapdragon.Spaces.SpacesHand::.ctor()
extern void SpacesHand__ctor_m62DF4B810F8C76315B4CD19C2005BDD6F21E3C72 (void);
// 0x00000213 Qualcomm.Snapdragon.Spaces.SpacesHand/Joint Qualcomm.Snapdragon.Spaces.SpacesHand::<get_Joints>b__22_0(UnityEngine.Pose,System.Int32)
extern void SpacesHand_U3Cget_JointsU3Eb__22_0_mCF099BDBA6F8E8F148AF47F943652829950F48ED (void);
// 0x00000214 System.Void Qualcomm.Snapdragon.Spaces.SpacesHand/Gesture::.ctor(Qualcomm.Snapdragon.Spaces.SpacesHand/GestureType,System.Single,System.Single)
extern void Gesture__ctor_m9084C4B3F4AFEA3CF5B0AE49703302FB5BFB06EC (void);
// 0x00000215 System.Void Qualcomm.Snapdragon.Spaces.SpacesHand/Joint::.ctor(Qualcomm.Snapdragon.Spaces.SpacesHand/JointType,UnityEngine.Pose)
extern void Joint__ctor_mBE6F7B02F1E804F90B610CF06FD3FF690A19BF2E (void);
// 0x00000216 System.Void Qualcomm.Snapdragon.Spaces.SpacesHandJointVisualizer::Start()
extern void SpacesHandJointVisualizer_Start_m5AC47EC132E9375C30B6EB547DC6B064FDA1A884 (void);
// 0x00000217 System.Void Qualcomm.Snapdragon.Spaces.SpacesHandJointVisualizer::Update()
extern void SpacesHandJointVisualizer_Update_mA4518625708E39A034764C1EDC629D329F1810EB (void);
// 0x00000218 System.Void Qualcomm.Snapdragon.Spaces.SpacesHandJointVisualizer::UpdatePointMatrices(Qualcomm.Snapdragon.Spaces.SpacesHand)
extern void SpacesHandJointVisualizer_UpdatePointMatrices_m90827F2ECA108610E91E16528708A8AD88B68BF5 (void);
// 0x00000219 System.Void Qualcomm.Snapdragon.Spaces.SpacesHandJointVisualizer::GenerateJointMaterials()
extern void SpacesHandJointVisualizer_GenerateJointMaterials_mEAC404E760A3CC1B909EE51045E9DF4C1F09F12D (void);
// 0x0000021A System.Void Qualcomm.Snapdragon.Spaces.SpacesHandJointVisualizer::.ctor()
extern void SpacesHandJointVisualizer__ctor_mF84F73B099771C771F6B8837CE063200573E557A (void);
// 0x0000021B Qualcomm.Snapdragon.Spaces.SpacesHand Qualcomm.Snapdragon.Spaces.SpacesHandManager::get_LeftHand()
extern void SpacesHandManager_get_LeftHand_m7BA685E844D5EBD46B7361C5D866FB2AB69A2E1C (void);
// 0x0000021C Qualcomm.Snapdragon.Spaces.SpacesHand Qualcomm.Snapdragon.Spaces.SpacesHandManager::get_RightHand()
extern void SpacesHandManager_get_RightHand_m21B9409CA29D5849141275176A9AE8E39130D224 (void);
// 0x0000021D System.Void Qualcomm.Snapdragon.Spaces.SpacesHandManager::OnEnable()
extern void SpacesHandManager_OnEnable_m1FF12441396846E6E1EB0DB5F1E554B393E7C4CA (void);
// 0x0000021E System.Void Qualcomm.Snapdragon.Spaces.SpacesHandManager::OnDisable()
extern void SpacesHandManager_OnDisable_m2C83507516D5C38C0BDDEC49E1210EA3EBC4A4D3 (void);
// 0x0000021F System.Void Qualcomm.Snapdragon.Spaces.SpacesHandManager::add_handsChanged(System.Action`1<Qualcomm.Snapdragon.Spaces.SpacesHandsChangedEventArgs>)
extern void SpacesHandManager_add_handsChanged_mD9284C78D38987BBD1BC026B22DFC14DD3D7EE18 (void);
// 0x00000220 System.Void Qualcomm.Snapdragon.Spaces.SpacesHandManager::remove_handsChanged(System.Action`1<Qualcomm.Snapdragon.Spaces.SpacesHandsChangedEventArgs>)
extern void SpacesHandManager_remove_handsChanged_mCF9B04EC0C73A9D83B92CA7195CBD424B0E95C49 (void);
// 0x00000221 UnityEngine.GameObject Qualcomm.Snapdragon.Spaces.SpacesHandManager::GetPrefab()
extern void SpacesHandManager_GetPrefab_mA5C972A5DA04CCC0FB3B0ADFA2F8FD308195847E (void);
// 0x00000222 UnityEngine.GameObject Qualcomm.Snapdragon.Spaces.SpacesHandManager::get_HandPrefab()
extern void SpacesHandManager_get_HandPrefab_mE659206C2FD1C9BE01BBD74F250DECD5B5B305C0 (void);
// 0x00000223 System.Void Qualcomm.Snapdragon.Spaces.SpacesHandManager::set_HandPrefab(UnityEngine.GameObject)
extern void SpacesHandManager_set_HandPrefab_m73A1B2174B941A3F206615DDA82396EB4AE2F709 (void);
// 0x00000224 System.String Qualcomm.Snapdragon.Spaces.SpacesHandManager::get_gameObjectName()
extern void SpacesHandManager_get_gameObjectName_mF86D809FD29F9B0A8CF64FD9650C29CE7AB8725A (void);
// 0x00000225 System.Void Qualcomm.Snapdragon.Spaces.SpacesHandManager::OnAfterSetSessionRelativeData(Qualcomm.Snapdragon.Spaces.SpacesHand,Qualcomm.Snapdragon.Spaces.XRTrackedHand)
extern void SpacesHandManager_OnAfterSetSessionRelativeData_m916BC689D6A1AC3D1CBC3623B7B1C5A274A143C3 (void);
// 0x00000226 System.Void Qualcomm.Snapdragon.Spaces.SpacesHandManager::OnTrackablesChanged(System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.SpacesHand>,System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.SpacesHand>,System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.SpacesHand>)
extern void SpacesHandManager_OnTrackablesChanged_m47703D9B3489D9919F2F798B80F2DC70E1981C22 (void);
// 0x00000227 Qualcomm.Snapdragon.Spaces.SpacesHand Qualcomm.Snapdragon.Spaces.SpacesHandManager::GetSpacesHandByTrackableID(UnityEngine.XR.ARSubsystems.TrackableId)
extern void SpacesHandManager_GetSpacesHandByTrackableID_m2D52CFA509B044E461D6201A7CB31C24325B580D (void);
// 0x00000228 System.Void Qualcomm.Snapdragon.Spaces.SpacesHandManager::.ctor()
extern void SpacesHandManager__ctor_mFFD875DAE979195409DE2C5D179EFA9C69845592 (void);
// 0x00000229 System.Void Qualcomm.Snapdragon.Spaces.SpacesHandsChangedEventArgs::.ctor(System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.SpacesHand>,System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.SpacesHand>,System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.SpacesHand>)
extern void SpacesHandsChangedEventArgs__ctor_m6DB86EF5C7C21BB5071A61BDB3472D1FAAF79D81 (void);
// 0x0000022A System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.SpacesHand> Qualcomm.Snapdragon.Spaces.SpacesHandsChangedEventArgs::get_added()
extern void SpacesHandsChangedEventArgs_get_added_m5CB059B37808F89770348F83E634B8357734B23C (void);
// 0x0000022B System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.SpacesHand> Qualcomm.Snapdragon.Spaces.SpacesHandsChangedEventArgs::get_updated()
extern void SpacesHandsChangedEventArgs_get_updated_m8285564FB82F9C9BD7CB0520D8FA6A9058104939 (void);
// 0x0000022C System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.SpacesHand> Qualcomm.Snapdragon.Spaces.SpacesHandsChangedEventArgs::get_removed()
extern void SpacesHandsChangedEventArgs_get_removed_m027BB7F2A0FB42472DAFC362C3749ACFC0D72E07 (void);
// 0x0000022D System.Void Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystemDescriptor::.ctor(Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystemDescriptor/Cinfo)
extern void XRHandTrackingSubsystemDescriptor__ctor_m6A7417B3EC99DF9A6AC20FB1E6C43F04D5A30C33 (void);
// 0x0000022E System.Void Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystemDescriptor::Create(Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystemDescriptor/Cinfo)
extern void XRHandTrackingSubsystemDescriptor_Create_mA81A0552179791150D5D28E6DFD0AE696EDEFEEA (void);
// 0x0000022F System.String Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystemDescriptor/Cinfo::get_id()
extern void Cinfo_get_id_mF3EBDAF7A344E71CBB5E82700C5B72F97FB69D5B (void);
// 0x00000230 System.Void Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystemDescriptor/Cinfo::set_id(System.String)
extern void Cinfo_set_id_mDC05FE6B201A56946C2C5EA912F8216DF9A09C92 (void);
// 0x00000231 System.Type Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystemDescriptor/Cinfo::get_providerType()
extern void Cinfo_get_providerType_mA77251D3E258944822919C88EFD9B644664CF25A (void);
// 0x00000232 System.Void Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystemDescriptor/Cinfo::set_providerType(System.Type)
extern void Cinfo_set_providerType_m6B9926F0E4230B88C381C91D2F5F42EFED493D6E (void);
// 0x00000233 System.Type Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystemDescriptor/Cinfo::get_subsystemTypeOverride()
extern void Cinfo_get_subsystemTypeOverride_m46BA874D2F7FE887BC89D8FC6E5E63687BD0934D (void);
// 0x00000234 System.Void Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystemDescriptor/Cinfo::set_subsystemTypeOverride(System.Type)
extern void Cinfo_set_subsystemTypeOverride_mCF7CF4A13208D788BAAE060D96FAEF41723FA9B0 (void);
// 0x00000235 System.Int32 Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystemDescriptor/Cinfo::GetHashCode()
extern void Cinfo_GetHashCode_mD14AB2A03A219CED499DA7FF91AD90C29C13B117 (void);
// 0x00000236 System.Boolean Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystemDescriptor/Cinfo::Equals(Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystemDescriptor/Cinfo)
extern void Cinfo_Equals_m374DC4526F1EC61A7F2A14FF9F7FF6051CA32FFD (void);
// 0x00000237 System.Boolean Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystemDescriptor/Cinfo::Equals(System.Object)
extern void Cinfo_Equals_m7965F4DB417B97CB3972F630D2196E57284A4EA1 (void);
// 0x00000238 System.Boolean Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystemDescriptor/Cinfo::op_Equality(Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystemDescriptor/Cinfo,Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystemDescriptor/Cinfo)
extern void Cinfo_op_Equality_mF468220505403D83DD69F4F81B517C0C4B71E7CD (void);
// 0x00000239 System.Boolean Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystemDescriptor/Cinfo::op_Inequality(Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystemDescriptor/Cinfo,Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystemDescriptor/Cinfo)
extern void Cinfo_op_Inequality_mD6C46DCB653C29AE0AF754322D7C5C6C592F49F6 (void);
// 0x0000023A Qualcomm.Snapdragon.Spaces.XRTrackedHand Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystem::get_LeftHand()
extern void XRHandTrackingSubsystem_get_LeftHand_mF272692C87894D3C83F768FC4DAD8E16D8ACFEBE (void);
// 0x0000023B Qualcomm.Snapdragon.Spaces.XRTrackedHand Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystem::get_RightHand()
extern void XRHandTrackingSubsystem_get_RightHand_mC5BBA0D2603AAC0EEB14B07B7592035D9BD51CC0 (void);
// 0x0000023C UnityEngine.XR.ARSubsystems.TrackableChanges`1<Qualcomm.Snapdragon.Spaces.XRTrackedHand> Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XRHandTrackingSubsystem_GetChanges_m55263EFE2FE55F8D95E45BB5844EDE502AC3F0D7 (void);
// 0x0000023D System.Void Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystem::GetJoints(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose[]&)
extern void XRHandTrackingSubsystem_GetJoints_mE66AEC15FB670E17B41C294396760CA50365E886 (void);
// 0x0000023E System.Void Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystem::GetGestureData(UnityEngine.XR.ARSubsystems.TrackableId,System.Int32&,System.Single&,System.Single&)
extern void XRHandTrackingSubsystem_GetGestureData_m6803D1AE165A48AE94E9046797CE8A27D80E342C (void);
// 0x0000023F System.Void Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystem::.ctor()
extern void XRHandTrackingSubsystem__ctor_m1F7AACF6AEED9D559DF1BF0FB38271FB96526CBF (void);
// 0x00000240 Qualcomm.Snapdragon.Spaces.XRTrackedHand Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystem/Provider::GetLeftHand()
// 0x00000241 Qualcomm.Snapdragon.Spaces.XRTrackedHand Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystem/Provider::GetRightHand()
// 0x00000242 UnityEngine.XR.ARSubsystems.TrackableChanges`1<Qualcomm.Snapdragon.Spaces.XRTrackedHand> Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystem/Provider::GetChanges(Qualcomm.Snapdragon.Spaces.XRTrackedHand,Unity.Collections.Allocator)
// 0x00000243 System.Void Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystem/Provider::GetJoints(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose[]&)
// 0x00000244 System.Void Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystem/Provider::GetGestureData(UnityEngine.XR.ARSubsystems.TrackableId,System.Int32&,System.Single&,System.Single&)
// 0x00000245 System.Void Qualcomm.Snapdragon.Spaces.XRHandTrackingSubsystem/Provider::.ctor()
extern void Provider__ctor_m476D4EFE62E17401E1719C334C3E62D7C5614673 (void);
// 0x00000246 UnityEngine.XR.ARSubsystems.TrackableId Qualcomm.Snapdragon.Spaces.XRTrackedHand::get_trackableId()
extern void XRTrackedHand_get_trackableId_m405042E716232C8EECA276E8639D53F5466E195D (void);
// 0x00000247 System.IntPtr Qualcomm.Snapdragon.Spaces.XRTrackedHand::get_nativePtr()
extern void XRTrackedHand_get_nativePtr_mA695B53C1EE10B8B922A4E44F0FA37CFCE54E59F (void);
// 0x00000248 UnityEngine.Pose Qualcomm.Snapdragon.Spaces.XRTrackedHand::get_pose()
extern void XRTrackedHand_get_pose_m45569784BDCB04B40CE717EF6043C8DCBF268BB9 (void);
// 0x00000249 System.Void Qualcomm.Snapdragon.Spaces.XRTrackedHand::set_pose(UnityEngine.Pose)
extern void XRTrackedHand_set_pose_m4413C8A35AE5F773C992984623AEB3A6439C431E (void);
// 0x0000024A UnityEngine.XR.ARSubsystems.TrackingState Qualcomm.Snapdragon.Spaces.XRTrackedHand::get_trackingState()
extern void XRTrackedHand_get_trackingState_mE860352EC4FFF755F28F51A797A474E9088C20E9 (void);
// 0x0000024B System.Void Qualcomm.Snapdragon.Spaces.XRTrackedHand::set_trackingState(UnityEngine.XR.ARSubsystems.TrackingState)
extern void XRTrackedHand_set_trackingState_mB9D9B72BF2CA0EDD55B61B57A359D0088B653946 (void);
// 0x0000024C Qualcomm.Snapdragon.Spaces.XRTrackedHand Qualcomm.Snapdragon.Spaces.XRTrackedHand::get_defaultValue()
extern void XRTrackedHand_get_defaultValue_m90F48B65C04253A63533D38D24D555CDEB9F2626 (void);
// 0x0000024D System.Void Qualcomm.Snapdragon.Spaces.XRTrackedHand::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.XR.ARSubsystems.TrackingState)
extern void XRTrackedHand__ctor_mB934D2C6BCF136E9648DCB30C91207C8A19BAF86 (void);
// 0x0000024E System.Void Qualcomm.Snapdragon.Spaces.XRTrackedHand::UpdatePoseAndTrackedState(UnityEngine.Pose,UnityEngine.XR.ARSubsystems.TrackingState)
extern void XRTrackedHand_UpdatePoseAndTrackedState_m8D1B046B1613AF64B1688BCC8EBCC1501CF02E7D (void);
// 0x0000024F System.Int32 Qualcomm.Snapdragon.Spaces.XRTrackedHand::GetHashCode()
extern void XRTrackedHand_GetHashCode_mD23C58F566A3F049FFA8A5185042B3D85BD6486E (void);
// 0x00000250 System.Boolean Qualcomm.Snapdragon.Spaces.XRTrackedHand::Equals(Qualcomm.Snapdragon.Spaces.XRTrackedHand)
extern void XRTrackedHand_Equals_mDB882CA9828980C6256982565930FAF1D4D0FC29 (void);
// 0x00000251 System.Boolean Qualcomm.Snapdragon.Spaces.XRTrackedHand::Equals(System.Object)
extern void XRTrackedHand_Equals_m2C2C8454E4B205407EE5AEB340B8BDE90F8D9CD2 (void);
// 0x00000252 System.Boolean Qualcomm.Snapdragon.Spaces.XRTrackedHand::op_Equality(Qualcomm.Snapdragon.Spaces.XRTrackedHand,Qualcomm.Snapdragon.Spaces.XRTrackedHand)
extern void XRTrackedHand_op_Equality_mD758154746E37ED7E21A319BDD7B107C090B0461 (void);
// 0x00000253 System.Boolean Qualcomm.Snapdragon.Spaces.XRTrackedHand::op_Inequality(Qualcomm.Snapdragon.Spaces.XRTrackedHand,Qualcomm.Snapdragon.Spaces.XRTrackedHand)
extern void XRTrackedHand_op_Inequality_mFD2AC8F6E1AC49D8B2DDAEA1B135EE2CB11ED2BE (void);
// 0x00000254 System.Void Qualcomm.Snapdragon.Spaces.XRTrackedHand::.cctor()
extern void XRTrackedHand__cctor_m8BE1E52FE86DD5F79BC0A9A439EB86D82C16D2C5 (void);
// 0x00000255 System.Void Qualcomm.Snapdragon.Spaces.Hand::.ctor(Qualcomm.Snapdragon.Spaces.XRTrackedHand,System.Boolean)
extern void Hand__ctor_m886B03765B8042D9AC645817A840261A434AB863 (void);
// 0x00000256 Qualcomm.Snapdragon.Spaces.XRTrackedHand Qualcomm.Snapdragon.Spaces.Hand::get_TrackedHand()
extern void Hand_get_TrackedHand_m04A54DCF4C6E348BAB8A6CD28838E9802C2DC79F (void);
// 0x00000257 System.Boolean Qualcomm.Snapdragon.Spaces.Hand::get_IsLeft()
extern void Hand_get_IsLeft_m14715A87F11DC8A4F0DE69811CD281989FC2AAD5 (void);
// 0x00000258 System.Void Qualcomm.Snapdragon.Spaces.Hand::UpdateTrackedHandPoseAndTrackingState(UnityEngine.Pose,UnityEngine.XR.ARSubsystems.TrackingState)
extern void Hand_UpdateTrackedHandPoseAndTrackingState_mD85E0376157A8E24FA59145034E092D2A4D407AE (void);
// 0x00000259 System.UInt64 Qualcomm.Snapdragon.Spaces.HandTrackingFeature::get_LeftHandTrackerHandle()
extern void HandTrackingFeature_get_LeftHandTrackerHandle_m27933C675B381FA36E3616CBFC1000B507011412 (void);
// 0x0000025A System.UInt64 Qualcomm.Snapdragon.Spaces.HandTrackingFeature::get_RightHandTrackerHandle()
extern void HandTrackingFeature_get_RightHandTrackerHandle_m315EB6F090E153A69CCC519F0D685E68D50C7439 (void);
// 0x0000025B System.Boolean Qualcomm.Snapdragon.Spaces.HandTrackingFeature::get_IsRequiringBaseRuntimeFeature()
extern void HandTrackingFeature_get_IsRequiringBaseRuntimeFeature_mB326A6D81048B512B216AE537E8AED9BF6DC427C (void);
// 0x0000025C System.Boolean Qualcomm.Snapdragon.Spaces.HandTrackingFeature::TryCreateHandTracking()
extern void HandTrackingFeature_TryCreateHandTracking_m777594DD56B93690AC1AA1FA81934FA78B3DB886 (void);
// 0x0000025D System.Boolean Qualcomm.Snapdragon.Spaces.HandTrackingFeature::TryDestroyHandTracking()
extern void HandTrackingFeature_TryDestroyHandTracking_m9839DB0C9B1405AF21FAE72C7C707BC8104AE040 (void);
// 0x0000025E System.Tuple`2<System.Collections.Generic.List`1<UnityEngine.Pose>,UnityEngine.XR.ARSubsystems.TrackingState> Qualcomm.Snapdragon.Spaces.HandTrackingFeature::TryGetHandTrackingJointsAndTrackingState(System.Boolean)
extern void HandTrackingFeature_TryGetHandTrackingJointsAndTrackingState_mAEBD552052028C4AA788848BA07D0CAF03EE2A2F (void);
// 0x0000025F System.Tuple`3<System.Int32,System.Single,System.Single> Qualcomm.Snapdragon.Spaces.HandTrackingFeature::TryGetHandGestureData(System.Boolean)
extern void HandTrackingFeature_TryGetHandGestureData_m26531FF64B238A49E12977EAA53309D777FDF088 (void);
// 0x00000260 System.String Qualcomm.Snapdragon.Spaces.HandTrackingFeature::GetXrLayersToLoad()
extern void HandTrackingFeature_GetXrLayersToLoad_m939F79631D7B42DD039C5ED45AAFE79575CF0335 (void);
// 0x00000261 System.Boolean Qualcomm.Snapdragon.Spaces.HandTrackingFeature::OnInstanceCreate(System.UInt64)
extern void HandTrackingFeature_OnInstanceCreate_mA6997DFAADB739A01676749FBDFFF56E31EBE55F (void);
// 0x00000262 System.Void Qualcomm.Snapdragon.Spaces.HandTrackingFeature::OnSubsystemCreate()
extern void HandTrackingFeature_OnSubsystemCreate_mD12D1203D61AADCC28226A5DC53F61A888E52551 (void);
// 0x00000263 System.Void Qualcomm.Snapdragon.Spaces.HandTrackingFeature::OnSubsystemStop()
extern void HandTrackingFeature_OnSubsystemStop_mD9A492686555BD030D3FC9C8A9EA8CDCBE729E52 (void);
// 0x00000264 System.Void Qualcomm.Snapdragon.Spaces.HandTrackingFeature::OnSubsystemDestroy()
extern void HandTrackingFeature_OnSubsystemDestroy_m019FE6C89072E39EAF803C16B85A8C3EA3CEF10B (void);
// 0x00000265 System.Void Qualcomm.Snapdragon.Spaces.HandTrackingFeature::OnHookMethods()
extern void HandTrackingFeature_OnHookMethods_m083B3F17DFB51A1662DEE9A87C969FFF31289402 (void);
// 0x00000266 System.Void Qualcomm.Snapdragon.Spaces.HandTrackingFeature::.ctor()
extern void HandTrackingFeature__ctor_m1921FE7112D46A931BE0B6396CAEA0D0920A8D09 (void);
// 0x00000267 System.Void Qualcomm.Snapdragon.Spaces.HandTrackingFeature::.cctor()
extern void HandTrackingFeature__cctor_mD7F4DD3ADA5F4663246E3CBCC355C82B11B4F8D8 (void);
// 0x00000268 System.Void Qualcomm.Snapdragon.Spaces.HandTrackingFeature/CreateHandTrackerEXTDelegate::.ctor(System.Object,System.IntPtr)
extern void CreateHandTrackerEXTDelegate__ctor_mBDED34AF2C5E6B0159D92AA58265CAE533270C28 (void);
// 0x00000269 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.HandTrackingFeature/CreateHandTrackerEXTDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrHandTrackerCreateInfoEXT&,System.UInt64&)
extern void CreateHandTrackerEXTDelegate_Invoke_m0166EB069A0C522178FE85BB2EFB8CFE543E62BA (void);
// 0x0000026A System.IAsyncResult Qualcomm.Snapdragon.Spaces.HandTrackingFeature/CreateHandTrackerEXTDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrHandTrackerCreateInfoEXT&,System.UInt64&,System.AsyncCallback,System.Object)
extern void CreateHandTrackerEXTDelegate_BeginInvoke_m0E3F209332A9348C7CAB8BA6F4604535F437CF4D (void);
// 0x0000026B Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.HandTrackingFeature/CreateHandTrackerEXTDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrHandTrackerCreateInfoEXT&,System.UInt64&,System.IAsyncResult)
extern void CreateHandTrackerEXTDelegate_EndInvoke_mC73C93020F7CFC2BEF105983D92E33D0799A9D57 (void);
// 0x0000026C System.Void Qualcomm.Snapdragon.Spaces.HandTrackingFeature/DestroyHandTrackerEXTDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyHandTrackerEXTDelegate__ctor_m70EF03DF1097600A3287F9F61EC60C646747FB1D (void);
// 0x0000026D Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.HandTrackingFeature/DestroyHandTrackerEXTDelegate::Invoke(System.UInt64)
extern void DestroyHandTrackerEXTDelegate_Invoke_m7D6BFEC4D471B367C795F3BBAF654E53A3636A0B (void);
// 0x0000026E System.IAsyncResult Qualcomm.Snapdragon.Spaces.HandTrackingFeature/DestroyHandTrackerEXTDelegate::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void DestroyHandTrackerEXTDelegate_BeginInvoke_m9E203DED6B0C0612130AEFC9E8DA2E91622033B1 (void);
// 0x0000026F Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.HandTrackingFeature/DestroyHandTrackerEXTDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyHandTrackerEXTDelegate_EndInvoke_m478EF12CD8D73B78D5426A4E203D51D00764E554 (void);
// 0x00000270 System.Void Qualcomm.Snapdragon.Spaces.HandTrackingFeature/LocateHandJointsEXTDelegate::.ctor(System.Object,System.IntPtr)
extern void LocateHandJointsEXTDelegate__ctor_m426D8A6EBB6A4ADCEDC50860DAF1E11988245081 (void);
// 0x00000271 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.HandTrackingFeature/LocateHandJointsEXTDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrHandJointsLocateInfoEXT&,Qualcomm.Snapdragon.Spaces.XrHandJointLocationsEXT&)
extern void LocateHandJointsEXTDelegate_Invoke_mB665BBC2725F498FC707D05B237144C7032D1765 (void);
// 0x00000272 System.IAsyncResult Qualcomm.Snapdragon.Spaces.HandTrackingFeature/LocateHandJointsEXTDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrHandJointsLocateInfoEXT&,Qualcomm.Snapdragon.Spaces.XrHandJointLocationsEXT&,System.AsyncCallback,System.Object)
extern void LocateHandJointsEXTDelegate_BeginInvoke_mF230988C68505AD980F01C4E6B48671AC28D01D4 (void);
// 0x00000273 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.HandTrackingFeature/LocateHandJointsEXTDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrHandJointsLocateInfoEXT&,Qualcomm.Snapdragon.Spaces.XrHandJointLocationsEXT&,System.IAsyncResult)
extern void LocateHandJointsEXTDelegate_EndInvoke_mAC90C1861DA3C7DD96870BCEA836C63BD87224BD (void);
// 0x00000274 System.Void Qualcomm.Snapdragon.Spaces.HandTrackingFeature/GetHandGestureQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void GetHandGestureQCOMDelegate__ctor_mE3B70F480F217F0FB1F4DBDB1FC158121E07A62D (void);
// 0x00000275 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.HandTrackingFeature/GetHandGestureQCOMDelegate::Invoke(System.UInt64,System.Int64,Qualcomm.Snapdragon.Spaces.XrHandGestureQCOM&)
extern void GetHandGestureQCOMDelegate_Invoke_mB10DF6E2E81D137300AEC6637098A879AFA93468 (void);
// 0x00000276 System.IAsyncResult Qualcomm.Snapdragon.Spaces.HandTrackingFeature/GetHandGestureQCOMDelegate::BeginInvoke(System.UInt64,System.Int64,Qualcomm.Snapdragon.Spaces.XrHandGestureQCOM&,System.AsyncCallback,System.Object)
extern void GetHandGestureQCOMDelegate_BeginInvoke_m8787295EC47D8462E24AA0B92ED614DB4087A2BA (void);
// 0x00000277 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.HandTrackingFeature/GetHandGestureQCOMDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrHandGestureQCOM&,System.IAsyncResult)
extern void GetHandGestureQCOMDelegate_EndInvoke_m02319277D64E8008B42F31CD642C0EC7C60A0EC9 (void);
// 0x00000278 System.Void Qualcomm.Snapdragon.Spaces.HandTrackingSubsystem::RegisterDescriptor()
extern void HandTrackingSubsystem_RegisterDescriptor_m8B11B6873B30AB0D9C40638048E2CC2D5F3A0642 (void);
// 0x00000279 System.Void Qualcomm.Snapdragon.Spaces.HandTrackingSubsystem::.ctor()
extern void HandTrackingSubsystem__ctor_m7CCA2433969577B6CA40E2B0ACAEE1E495C3F4F7 (void);
// 0x0000027A System.Void Qualcomm.Snapdragon.Spaces.HandTrackingSubsystem/HandTrackingProvider::Start()
extern void HandTrackingProvider_Start_mAAA202FA527096DA2197C0835272406721641A09 (void);
// 0x0000027B System.Void Qualcomm.Snapdragon.Spaces.HandTrackingSubsystem/HandTrackingProvider::Stop()
extern void HandTrackingProvider_Stop_mFE8404A6AF9190D08F0B8C2D779F2C5EF3CF2EB9 (void);
// 0x0000027C System.Void Qualcomm.Snapdragon.Spaces.HandTrackingSubsystem/HandTrackingProvider::Destroy()
extern void HandTrackingProvider_Destroy_m44257B7CFDD86DDB87DDCCBD6B859603CC77C083 (void);
// 0x0000027D Qualcomm.Snapdragon.Spaces.XRTrackedHand Qualcomm.Snapdragon.Spaces.HandTrackingSubsystem/HandTrackingProvider::GetLeftHand()
extern void HandTrackingProvider_GetLeftHand_m83EF37A390EF7332E2A3CC48E2498302ED1607D6 (void);
// 0x0000027E Qualcomm.Snapdragon.Spaces.XRTrackedHand Qualcomm.Snapdragon.Spaces.HandTrackingSubsystem/HandTrackingProvider::GetRightHand()
extern void HandTrackingProvider_GetRightHand_m11C5E2581EF168131E557F3369AD3EE552110281 (void);
// 0x0000027F UnityEngine.XR.ARSubsystems.TrackableChanges`1<Qualcomm.Snapdragon.Spaces.XRTrackedHand> Qualcomm.Snapdragon.Spaces.HandTrackingSubsystem/HandTrackingProvider::GetChanges(Qualcomm.Snapdragon.Spaces.XRTrackedHand,Unity.Collections.Allocator)
extern void HandTrackingProvider_GetChanges_m0380DD4CCA681365935D3AB9260FA77E4A7EF356 (void);
// 0x00000280 System.Void Qualcomm.Snapdragon.Spaces.HandTrackingSubsystem/HandTrackingProvider::GetJoints(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose[]&)
extern void HandTrackingProvider_GetJoints_m351BF63ACDBE727F2798524CA8F612D05153E134 (void);
// 0x00000281 System.Void Qualcomm.Snapdragon.Spaces.HandTrackingSubsystem/HandTrackingProvider::GetGestureData(UnityEngine.XR.ARSubsystems.TrackableId,System.Int32&,System.Single&,System.Single&)
extern void HandTrackingProvider_GetGestureData_m95BA08035CB1EB4B2E4A013819FDC65BC33C7841 (void);
// 0x00000282 System.Void Qualcomm.Snapdragon.Spaces.HandTrackingSubsystem/HandTrackingProvider::UpdateTrackableChangesLists(Qualcomm.Snapdragon.Spaces.Hand&,System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.XRTrackedHand>&,System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.XRTrackedHand>&,System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.TrackableId>&)
extern void HandTrackingProvider_UpdateTrackableChangesLists_mB8E61058FBB680B684DE822C8FDFCB423BBBE622 (void);
// 0x00000283 Qualcomm.Snapdragon.Spaces.Hand Qualcomm.Snapdragon.Spaces.HandTrackingSubsystem/HandTrackingProvider::GetHandByTrackableID(UnityEngine.XR.ARSubsystems.TrackableId)
extern void HandTrackingProvider_GetHandByTrackableID_m7E5D026A6F6AF9074C08AF78852CCFD615E4A070 (void);
// 0x00000284 System.Void Qualcomm.Snapdragon.Spaces.HandTrackingSubsystem/HandTrackingProvider::.ctor()
extern void HandTrackingProvider__ctor_mD09760F7CAD17CA9CACDDB02572D367962CD7F4C (void);
// 0x00000285 System.Boolean Qualcomm.Snapdragon.Spaces.HitTestingFeature::TryCreateRaycast(System.UInt64&)
extern void HitTestingFeature_TryCreateRaycast_m4CC5CD495CC4A6D7B2F38B8989D050856F922498 (void);
// 0x00000286 System.Boolean Qualcomm.Snapdragon.Spaces.HitTestingFeature::TryDestroyRayCast(System.UInt64)
extern void HitTestingFeature_TryDestroyRayCast_mF0ECFCCAAD5754076AA26461F3118545139529C3 (void);
// 0x00000287 System.Boolean Qualcomm.Snapdragon.Spaces.HitTestingFeature::TryCastRay(System.UInt64,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void HitTestingFeature_TryCastRay_m927963921B88DAFFD9209A34380032E200CD5B3D (void);
// 0x00000288 System.Boolean Qualcomm.Snapdragon.Spaces.HitTestingFeature::TryGetRaycastState(System.UInt64)
extern void HitTestingFeature_TryGetRaycastState_mDBD0A24867D3FA1DD8B9AFBD41A3217E1746A73B (void);
// 0x00000289 System.Boolean Qualcomm.Snapdragon.Spaces.HitTestingFeature::TryGetRayCastCollisions(System.UInt64,UnityEngine.Vector3,System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRRaycastHit>&)
extern void HitTestingFeature_TryGetRayCastCollisions_m16245933E0BB8E187AEC92EEDDEFD8B999E5A0E3 (void);
// 0x0000028A System.String Qualcomm.Snapdragon.Spaces.HitTestingFeature::GetXrLayersToLoad()
extern void HitTestingFeature_GetXrLayersToLoad_m5D5973DFAD5039C64F7943787662E676E3CA0228 (void);
// 0x0000028B System.Boolean Qualcomm.Snapdragon.Spaces.HitTestingFeature::OnInstanceCreate(System.UInt64)
extern void HitTestingFeature_OnInstanceCreate_m66BF0A2872E658881CB2DC46759B3B2DB66EC9C8 (void);
// 0x0000028C System.Void Qualcomm.Snapdragon.Spaces.HitTestingFeature::OnSubsystemCreate()
extern void HitTestingFeature_OnSubsystemCreate_mD6A76C01006981E20D0DBC9A39D58EA3B7AB6214 (void);
// 0x0000028D System.Void Qualcomm.Snapdragon.Spaces.HitTestingFeature::OnSubsystemStop()
extern void HitTestingFeature_OnSubsystemStop_mA83AB75209B2A2EFBA4BD773DFCFFE7939F59A6D (void);
// 0x0000028E System.Void Qualcomm.Snapdragon.Spaces.HitTestingFeature::OnSubsystemDestroy()
extern void HitTestingFeature_OnSubsystemDestroy_m4F5DCB1BAD5A1378F9A73489D289F51E21DF3972 (void);
// 0x0000028F System.Void Qualcomm.Snapdragon.Spaces.HitTestingFeature::OnHookMethods()
extern void HitTestingFeature_OnHookMethods_mCD5A94ABD5C04EF74FEB55C3E7334FD087857330 (void);
// 0x00000290 System.Void Qualcomm.Snapdragon.Spaces.HitTestingFeature::.ctor()
extern void HitTestingFeature__ctor_mFC95DE1B03F9810F1A0F667B2C8C087A1F4FFD5F (void);
// 0x00000291 System.Void Qualcomm.Snapdragon.Spaces.HitTestingFeature::.cctor()
extern void HitTestingFeature__cctor_m0A4EB4B31DEBD1CF0893E12153829B9B5635E293 (void);
// 0x00000292 System.Void Qualcomm.Snapdragon.Spaces.HitTestingFeature/CreateRayCastQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void CreateRayCastQCOMDelegate__ctor_mBADC967FEF8927310FDC404718D802066A68E07C (void);
// 0x00000293 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.HitTestingFeature/CreateRayCastQCOMDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrRayCastCreateInfoQCOM&,System.IntPtr)
extern void CreateRayCastQCOMDelegate_Invoke_m56D851C5C155033E96E25EB49587F35261B758CF (void);
// 0x00000294 System.IAsyncResult Qualcomm.Snapdragon.Spaces.HitTestingFeature/CreateRayCastQCOMDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrRayCastCreateInfoQCOM&,System.IntPtr,System.AsyncCallback,System.Object)
extern void CreateRayCastQCOMDelegate_BeginInvoke_m2F276F588CDAA3EB72442281A8189BF5CCBF98CC (void);
// 0x00000295 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.HitTestingFeature/CreateRayCastQCOMDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrRayCastCreateInfoQCOM&,System.IAsyncResult)
extern void CreateRayCastQCOMDelegate_EndInvoke_m30D0B0E85BF2AAA744CF9B9B2149A0F607BEB24E (void);
// 0x00000296 System.Void Qualcomm.Snapdragon.Spaces.HitTestingFeature/DestroyRayCastQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyRayCastQCOMDelegate__ctor_m7779C706A348F0AE1622014A4AD60D096953421E (void);
// 0x00000297 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.HitTestingFeature/DestroyRayCastQCOMDelegate::Invoke(System.UInt64)
extern void DestroyRayCastQCOMDelegate_Invoke_m5143AA4ED07E7AA518138643889EF80726142FF3 (void);
// 0x00000298 System.IAsyncResult Qualcomm.Snapdragon.Spaces.HitTestingFeature/DestroyRayCastQCOMDelegate::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void DestroyRayCastQCOMDelegate_BeginInvoke_m416E5FDB9A9E56FA9D472CFF0732580AF9199A87 (void);
// 0x00000299 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.HitTestingFeature/DestroyRayCastQCOMDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyRayCastQCOMDelegate_EndInvoke_m5C5B2AAC3EAB8BBC3D731D975901AEAED39A5070 (void);
// 0x0000029A System.Void Qualcomm.Snapdragon.Spaces.HitTestingFeature/GetRayCastStateQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void GetRayCastStateQCOMDelegate__ctor_m890ADCC3DEC6E734A2400193C3CDC5821C758C6B (void);
// 0x0000029B Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.HitTestingFeature/GetRayCastStateQCOMDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrRayCastStateQCOM&)
extern void GetRayCastStateQCOMDelegate_Invoke_mDF5D67B33BDD7473D3A55BD6FEB5CF75A87DB4B9 (void);
// 0x0000029C System.IAsyncResult Qualcomm.Snapdragon.Spaces.HitTestingFeature/GetRayCastStateQCOMDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrRayCastStateQCOM&,System.AsyncCallback,System.Object)
extern void GetRayCastStateQCOMDelegate_BeginInvoke_mA0870309116A2B5F403C6B4DD76F9F5B936CB67F (void);
// 0x0000029D Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.HitTestingFeature/GetRayCastStateQCOMDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrRayCastStateQCOM&,System.IAsyncResult)
extern void GetRayCastStateQCOMDelegate_EndInvoke_m846E6E83880C0026AF13FF085EBD471D66D2D018 (void);
// 0x0000029E System.Void Qualcomm.Snapdragon.Spaces.HitTestingFeature/CastRayQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void CastRayQCOMDelegate__ctor_m3603222D3D26A5F8DC8B901F97D0B979BA06FFC6 (void);
// 0x0000029F Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.HitTestingFeature/CastRayQCOMDelegate::Invoke(System.UInt64,System.UInt64,Qualcomm.Snapdragon.Spaces.XrVector3f,Qualcomm.Snapdragon.Spaces.XrVector3f,System.Single)
extern void CastRayQCOMDelegate_Invoke_mEF7C630D95133A322B633308F19EC9123484B236 (void);
// 0x000002A0 System.IAsyncResult Qualcomm.Snapdragon.Spaces.HitTestingFeature/CastRayQCOMDelegate::BeginInvoke(System.UInt64,System.UInt64,Qualcomm.Snapdragon.Spaces.XrVector3f,Qualcomm.Snapdragon.Spaces.XrVector3f,System.Single,System.AsyncCallback,System.Object)
extern void CastRayQCOMDelegate_BeginInvoke_m63C38219870FE8A2BE1009F2E3547BC93D5FC749 (void);
// 0x000002A1 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.HitTestingFeature/CastRayQCOMDelegate::EndInvoke(System.IAsyncResult)
extern void CastRayQCOMDelegate_EndInvoke_mF1F7CF1B4851B4B485A036D7B76F7FFBAD20EFF5 (void);
// 0x000002A2 System.Void Qualcomm.Snapdragon.Spaces.HitTestingFeature/GetRayCollisionsQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void GetRayCollisionsQCOMDelegate__ctor_m1B84EE2D51AEC28AFB698B63546310F97DB1F1D0 (void);
// 0x000002A3 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.HitTestingFeature/GetRayCollisionsQCOMDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrRayCollisionsGetInfoQCOM&,Qualcomm.Snapdragon.Spaces.XrRayCollisionsQCOM&)
extern void GetRayCollisionsQCOMDelegate_Invoke_mA4E9EF3F5168E19DDE1247E5588C3625ABF0988E (void);
// 0x000002A4 System.IAsyncResult Qualcomm.Snapdragon.Spaces.HitTestingFeature/GetRayCollisionsQCOMDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrRayCollisionsGetInfoQCOM&,Qualcomm.Snapdragon.Spaces.XrRayCollisionsQCOM&,System.AsyncCallback,System.Object)
extern void GetRayCollisionsQCOMDelegate_BeginInvoke_m25EB96755E83E2258E8F687DF23F77A6604FF059 (void);
// 0x000002A5 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.HitTestingFeature/GetRayCollisionsQCOMDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrRayCollisionsGetInfoQCOM&,Qualcomm.Snapdragon.Spaces.XrRayCollisionsQCOM&,System.IAsyncResult)
extern void GetRayCollisionsQCOMDelegate_EndInvoke_m77EECA673FC942BB118DB217CCC0E787AEDCEAC0 (void);
// 0x000002A6 System.Void Qualcomm.Snapdragon.Spaces.Raycast::.ctor(System.UInt64,UnityEngine.Ray)
extern void Raycast__ctor_m85C28C9BB9A6D0452CEFFA28F93BE95A5C01C723 (void);
// 0x000002A7 System.UInt64 Qualcomm.Snapdragon.Spaces.Raycast::get_RaycastHandle()
extern void Raycast_get_RaycastHandle_m150062E9E9408387F402F293D9E0DF26D33681E2 (void);
// 0x000002A8 System.Void Qualcomm.Snapdragon.Spaces.Raycast::UpdateSubsystemRaycastHitAndTrackingState(UnityEngine.XR.ARSubsystems.XRRaycastHit,UnityEngine.XR.ARSubsystems.TrackingState)
extern void Raycast_UpdateSubsystemRaycastHitAndTrackingState_m0209D6C600876E81B397344716E38F8CBE4BC281 (void);
// 0x000002A9 System.Void Qualcomm.Snapdragon.Spaces.RaycastSubsystem::RegisterDescriptor()
extern void RaycastSubsystem_RegisterDescriptor_mB1115F523113341648C73B5227FF565093EC0D29 (void);
// 0x000002AA System.Void Qualcomm.Snapdragon.Spaces.RaycastSubsystem::.ctor()
extern void RaycastSubsystem__ctor_m71E7DA59900B4124BD552A575FE467A546D21136 (void);
// 0x000002AB System.Void Qualcomm.Snapdragon.Spaces.RaycastSubsystem/SpacesProvider::Start()
extern void SpacesProvider_Start_m68BE4A5FBCB109BF5A21FB6D996A4BFB7AB1775A (void);
// 0x000002AC System.Void Qualcomm.Snapdragon.Spaces.RaycastSubsystem/SpacesProvider::Stop()
extern void SpacesProvider_Stop_mA2BB2621D231E2ECDFCA4ADDC77766898FFCA7E8 (void);
// 0x000002AD UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRRaycast> Qualcomm.Snapdragon.Spaces.RaycastSubsystem/SpacesProvider::GetChanges(UnityEngine.XR.ARSubsystems.XRRaycast,Unity.Collections.Allocator)
extern void SpacesProvider_GetChanges_m84994B408C43B06C1ADB8BACE0DD09ECEB0DAC67 (void);
// 0x000002AE System.Boolean Qualcomm.Snapdragon.Spaces.RaycastSubsystem/SpacesProvider::TryAddRaycast(UnityEngine.Vector2,System.Single,UnityEngine.XR.ARSubsystems.XRRaycast&)
extern void SpacesProvider_TryAddRaycast_m65FC2D8EA4AEA837E693FEC6A32A415A4229CE7D (void);
// 0x000002AF System.Boolean Qualcomm.Snapdragon.Spaces.RaycastSubsystem/SpacesProvider::TryAddRaycast(UnityEngine.Ray,System.Single,UnityEngine.XR.ARSubsystems.XRRaycast&)
extern void SpacesProvider_TryAddRaycast_m300F37AB683C9456199106EE58211CB931D79617 (void);
// 0x000002B0 System.Void Qualcomm.Snapdragon.Spaces.RaycastSubsystem/SpacesProvider::RemoveRaycast(UnityEngine.XR.ARSubsystems.TrackableId)
extern void SpacesProvider_RemoveRaycast_mC5EB4903DD128C2A88DE17CC7C669F73C491F898 (void);
// 0x000002B1 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> Qualcomm.Snapdragon.Spaces.RaycastSubsystem/SpacesProvider::Raycast(UnityEngine.XR.ARSubsystems.XRRaycastHit,UnityEngine.Ray,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void SpacesProvider_Raycast_m8DABE12051EE74AEA79E74B850001D7CC9F3BFDF (void);
// 0x000002B2 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> Qualcomm.Snapdragon.Spaces.RaycastSubsystem/SpacesProvider::Raycast(UnityEngine.XR.ARSubsystems.XRRaycastHit,UnityEngine.Vector2,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void SpacesProvider_Raycast_mD5E5EC09EB8B2D26E9CA30DE3C5E8DC2DFBAF70E (void);
// 0x000002B3 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> Qualcomm.Snapdragon.Spaces.RaycastSubsystem/SpacesProvider::Raycast(UnityEngine.XR.ARSubsystems.XRRaycastHit,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void SpacesProvider_Raycast_m7D8C27EE5C48DB56611D2508A1BA6BDD209DBAF9 (void);
// 0x000002B4 System.Void Qualcomm.Snapdragon.Spaces.RaycastSubsystem/SpacesProvider::.ctor()
extern void SpacesProvider__ctor_m2C561B6817C303928C9DDC541432E57991FF0479 (void);
// 0x000002B5 System.Boolean Qualcomm.Snapdragon.Spaces.RaycastSubsystem/SpacesProvider::<GetChanges>b__7_0(Qualcomm.Snapdragon.Spaces.Raycast)
extern void SpacesProvider_U3CGetChangesU3Eb__7_0_mE9611E02B3F12C541C6E369B4E2847728C2A8C5D (void);
// 0x000002B6 System.Void Qualcomm.Snapdragon.Spaces.RaycastSubsystem/SpacesProvider/<>c::.cctor()
extern void U3CU3Ec__cctor_m4061F1D8FFC187F8F3272DB895BE505F4553B949 (void);
// 0x000002B7 System.Void Qualcomm.Snapdragon.Spaces.RaycastSubsystem/SpacesProvider/<>c::.ctor()
extern void U3CU3Ec__ctor_m75E81460C97997805AD6998DECBD7F7E490F92B0 (void);
// 0x000002B8 System.Int32 Qualcomm.Snapdragon.Spaces.RaycastSubsystem/SpacesProvider/<>c::<GetChanges>b__7_1(UnityEngine.XR.ARSubsystems.XRRaycastHit,UnityEngine.XR.ARSubsystems.XRRaycastHit)
extern void U3CU3Ec_U3CGetChangesU3Eb__7_1_m323ED3342AF28E61260B21D453EF73CDAA3976A5 (void);
// 0x000002B9 System.Int32 Qualcomm.Snapdragon.Spaces.RaycastSubsystem/SpacesProvider/<>c::<Raycast>b__13_0(UnityEngine.XR.ARSubsystems.XRRaycastHit,UnityEngine.XR.ARSubsystems.XRRaycastHit)
extern void U3CU3Ec_U3CRaycastU3Eb__13_0_mDEF1F86ECBD8925633C304AE2C8D638DFCC422BB (void);
// 0x000002BA System.Void Qualcomm.Snapdragon.Spaces.RaycastSubsystem/SpacesProvider/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m715AD5A6B6A2FF9A8BA9B6876612FC544E7A73B1 (void);
// 0x000002BB System.Boolean Qualcomm.Snapdragon.Spaces.RaycastSubsystem/SpacesProvider/<>c__DisplayClass10_0::<RemoveRaycast>b__0(Qualcomm.Snapdragon.Spaces.Raycast)
extern void U3CU3Ec__DisplayClass10_0_U3CRemoveRaycastU3Eb__0_m0904BF865D137B932A04DA8303C594256128E613 (void);
// 0x000002BC System.Boolean Qualcomm.Snapdragon.Spaces.ImageTrackingFeature::get_IsRequiringBaseRuntimeFeature()
extern void ImageTrackingFeature_get_IsRequiringBaseRuntimeFeature_m3C2FD478747B8CA287E76D90C28B627AA424AD71 (void);
// 0x000002BD System.Boolean Qualcomm.Snapdragon.Spaces.ImageTrackingFeature::TryCreateImageTracker(System.UInt64&,UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary,System.Int32)
extern void ImageTrackingFeature_TryCreateImageTracker_m2789D5E699FCF96C32CB35D78DF440B7B5B14D17 (void);
// 0x000002BE System.Boolean Qualcomm.Snapdragon.Spaces.ImageTrackingFeature::TryDestroyImageTracker(System.UInt64)
extern void ImageTrackingFeature_TryDestroyImageTracker_mA1FBFE7603566186D79A87A246EA7B77EFE32F33 (void);
// 0x000002BF System.Boolean Qualcomm.Snapdragon.Spaces.ImageTrackingFeature::TryLocateImageTargets(System.UInt64,System.Int32,System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>&)
extern void ImageTrackingFeature_TryLocateImageTargets_m9B421381A531F5394343B033A88F210F53D34BD5 (void);
// 0x000002C0 System.Boolean Qualcomm.Snapdragon.Spaces.ImageTrackingFeature::TrySetTrackingModes(System.UInt64,System.Collections.Generic.List`1<System.String>,System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.SpacesImageTrackingMode>)
extern void ImageTrackingFeature_TrySetTrackingModes_mCD227C964BBCA9BF7DD22A097FC5824E48A10B89 (void);
// 0x000002C1 System.Boolean Qualcomm.Snapdragon.Spaces.ImageTrackingFeature::TryStopTrackingImageInstance(System.String,System.UInt32)
extern void ImageTrackingFeature_TryStopTrackingImageInstance_mE2E27200BCF44D7581729E05D4BBA1D470AD4BDB (void);
// 0x000002C2 System.String Qualcomm.Snapdragon.Spaces.ImageTrackingFeature::GetXrLayersToLoad()
extern void ImageTrackingFeature_GetXrLayersToLoad_mA314D0DB21D26234C9A070F30FB27BDD7AE56B8E (void);
// 0x000002C3 System.Boolean Qualcomm.Snapdragon.Spaces.ImageTrackingFeature::OnInstanceCreate(System.UInt64)
extern void ImageTrackingFeature_OnInstanceCreate_mFF667D56FC59EEB9B7B103E08479912E6064BCEF (void);
// 0x000002C4 System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingFeature::OnSubsystemCreate()
extern void ImageTrackingFeature_OnSubsystemCreate_m3524696660A957CF0747875B9333F2BFB27B7C6D (void);
// 0x000002C5 System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingFeature::OnSubsystemStop()
extern void ImageTrackingFeature_OnSubsystemStop_m0F9DAA8C5BDE6584A8CEE99DC139A8DCB7E9F8C7 (void);
// 0x000002C6 System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingFeature::OnSubsystemDestroy()
extern void ImageTrackingFeature_OnSubsystemDestroy_m292AAE8349E8FB65D6F23182D2D1CF7AE981EA15 (void);
// 0x000002C7 System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingFeature::OnHookMethods()
extern void ImageTrackingFeature_OnHookMethods_m2B6DC4E8363299A85F3A884F1DF23E06017561C9 (void);
// 0x000002C8 System.Boolean Qualcomm.Snapdragon.Spaces.ImageTrackingFeature::TryGetImageTargetNameAndId(System.UInt64,System.String&,System.UInt32&)
extern void ImageTrackingFeature_TryGetImageTargetNameAndId_m273E7F5CFF2F6B5279344A123ED14CBF5470F38E (void);
// 0x000002C9 System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingFeature::.ctor()
extern void ImageTrackingFeature__ctor_m62C31C25F537753FACBA13205F8D7392FF4E2FFD (void);
// 0x000002CA System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingFeature::.cctor()
extern void ImageTrackingFeature__cctor_m3F40BCEE3B529CF5E7D3E22AB85036A7190AC680 (void);
// 0x000002CB System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/CreateImageTrackerQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void CreateImageTrackerQCOMDelegate__ctor_mAFF1E22C6A9DAEF53A028BFA208DC61C14C4F9FB (void);
// 0x000002CC Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/CreateImageTrackerQCOMDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrImageTrackerCreateInfoQCOM&,System.UInt64&)
extern void CreateImageTrackerQCOMDelegate_Invoke_m35EBD8EACB34F7C4429F097275D8D0C50BA475EA (void);
// 0x000002CD System.IAsyncResult Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/CreateImageTrackerQCOMDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrImageTrackerCreateInfoQCOM&,System.UInt64&,System.AsyncCallback,System.Object)
extern void CreateImageTrackerQCOMDelegate_BeginInvoke_m2145D7073DDBE366F34EE1F002D16E94DB943AFF (void);
// 0x000002CE Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/CreateImageTrackerQCOMDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrImageTrackerCreateInfoQCOM&,System.UInt64&,System.IAsyncResult)
extern void CreateImageTrackerQCOMDelegate_EndInvoke_m7EABF8D41C4445762A65ABC52A2EA1ADFCD0FBF0 (void);
// 0x000002CF System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/DestroyImageTrackerQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyImageTrackerQCOMDelegate__ctor_m79828BB34A1A60886A6DCDF6961853DB09833CA1 (void);
// 0x000002D0 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/DestroyImageTrackerQCOMDelegate::Invoke(System.UInt64)
extern void DestroyImageTrackerQCOMDelegate_Invoke_m613D4AD60B1F7AA10218E48FFE2287995B47D400 (void);
// 0x000002D1 System.IAsyncResult Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/DestroyImageTrackerQCOMDelegate::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void DestroyImageTrackerQCOMDelegate_BeginInvoke_m4607FD3F4A2C7C81C6265F8B1FEAB7600EEBBB73 (void);
// 0x000002D2 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/DestroyImageTrackerQCOMDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyImageTrackerQCOMDelegate_EndInvoke_m43A6715735EC80E3ECBD60B11B339AAD1AF34865 (void);
// 0x000002D3 System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/LocateImageTargetsQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void LocateImageTargetsQCOMDelegate__ctor_mB5EE196D99031FD1DE058163A9F614B5EA246BF8 (void);
// 0x000002D4 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/LocateImageTargetsQCOMDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrImageTargetsLocateInfoQCOM&,Qualcomm.Snapdragon.Spaces.XrImageTargetLocationsQCOM&)
extern void LocateImageTargetsQCOMDelegate_Invoke_mBB8EDC45B99E7719BDBF7458078240E6E47ED247 (void);
// 0x000002D5 System.IAsyncResult Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/LocateImageTargetsQCOMDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrImageTargetsLocateInfoQCOM&,Qualcomm.Snapdragon.Spaces.XrImageTargetLocationsQCOM&,System.AsyncCallback,System.Object)
extern void LocateImageTargetsQCOMDelegate_BeginInvoke_mD31F484459B806438CF7AFC038EFD5F8456E522F (void);
// 0x000002D6 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/LocateImageTargetsQCOMDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrImageTargetsLocateInfoQCOM&,Qualcomm.Snapdragon.Spaces.XrImageTargetLocationsQCOM&,System.IAsyncResult)
extern void LocateImageTargetsQCOMDelegate_EndInvoke_mE90605EC915012D4659CFAA6048E633CB8199990 (void);
// 0x000002D7 System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/ImageTargetToNameAndIdQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void ImageTargetToNameAndIdQCOMDelegate__ctor_mE7C5B28B1EF524CB45978E3A119AC9CA39774972 (void);
// 0x000002D8 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/ImageTargetToNameAndIdQCOMDelegate::Invoke(System.UInt64,System.UInt32,System.UInt32&,System.IntPtr,System.UInt32&)
extern void ImageTargetToNameAndIdQCOMDelegate_Invoke_mF51A1C27ED55239BB7136EDEEAFC537EDEF4D8A8 (void);
// 0x000002D9 System.IAsyncResult Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/ImageTargetToNameAndIdQCOMDelegate::BeginInvoke(System.UInt64,System.UInt32,System.UInt32&,System.IntPtr,System.UInt32&,System.AsyncCallback,System.Object)
extern void ImageTargetToNameAndIdQCOMDelegate_BeginInvoke_mE1E80CEB249799543646C771EC3CEE026C190E17 (void);
// 0x000002DA Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/ImageTargetToNameAndIdQCOMDelegate::EndInvoke(System.UInt32&,System.UInt32&,System.IAsyncResult)
extern void ImageTargetToNameAndIdQCOMDelegate_EndInvoke_mB7D54587BFC4F8B4AA352F54C93A5452D3FC4E9D (void);
// 0x000002DB System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/SetImageTargetsTrackingModeQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void SetImageTargetsTrackingModeQCOMDelegate__ctor_mCFD2F582080B591BDBABBD9C34BAA602CBA11A33 (void);
// 0x000002DC Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/SetImageTargetsTrackingModeQCOMDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrImageTargetsTrackingModeInfoQCOM)
extern void SetImageTargetsTrackingModeQCOMDelegate_Invoke_m0DA827C7E2C065D2898C1E0AEDEC7263C3EE2B90 (void);
// 0x000002DD System.IAsyncResult Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/SetImageTargetsTrackingModeQCOMDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrImageTargetsTrackingModeInfoQCOM,System.AsyncCallback,System.Object)
extern void SetImageTargetsTrackingModeQCOMDelegate_BeginInvoke_m9121B2B5669C4B2A4A904E8F89193B40ECC35D01 (void);
// 0x000002DE Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/SetImageTargetsTrackingModeQCOMDelegate::EndInvoke(System.IAsyncResult)
extern void SetImageTargetsTrackingModeQCOMDelegate_EndInvoke_m202708EA2BCBC0B0ACC6AAC2B92B2E1EA5DFBE38 (void);
// 0x000002DF System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/StopImageTargetTrackingQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void StopImageTargetTrackingQCOMDelegate__ctor_m3E87DC2AF1AC6A725E1846F70178D4FD338016A7 (void);
// 0x000002E0 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/StopImageTargetTrackingQCOMDelegate::Invoke(System.UInt64)
extern void StopImageTargetTrackingQCOMDelegate_Invoke_mC8D344E629A2E7877B74CF9F7DD7893288F04903 (void);
// 0x000002E1 System.IAsyncResult Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/StopImageTargetTrackingQCOMDelegate::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void StopImageTargetTrackingQCOMDelegate_BeginInvoke_m5FDEBEDE33F7062412C471C4895B2E950BB57CCD (void);
// 0x000002E2 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/StopImageTargetTrackingQCOMDelegate::EndInvoke(System.IAsyncResult)
extern void StopImageTargetTrackingQCOMDelegate_EndInvoke_m6C84212EFBA892C405AAECFD86BBEDA4E3F1FD94 (void);
// 0x000002E3 System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/xrSetTrackingOptimizationSettingsHintQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void xrSetTrackingOptimizationSettingsHintQCOMDelegate__ctor_mD82909EB40A2089625217F1DE9004F74532FDF61 (void);
// 0x000002E4 System.Int32 Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/xrSetTrackingOptimizationSettingsHintQCOMDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrTrackingOptimizationSettingsDomainQCOM,Qualcomm.Snapdragon.Spaces.XrTrackingOptimizationSettingsHintQCOM)
extern void xrSetTrackingOptimizationSettingsHintQCOMDelegate_Invoke_m30946D76A1F17070E365542C63107628A71E80D0 (void);
// 0x000002E5 System.IAsyncResult Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/xrSetTrackingOptimizationSettingsHintQCOMDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrTrackingOptimizationSettingsDomainQCOM,Qualcomm.Snapdragon.Spaces.XrTrackingOptimizationSettingsHintQCOM,System.AsyncCallback,System.Object)
extern void xrSetTrackingOptimizationSettingsHintQCOMDelegate_BeginInvoke_m326E1C78F195EF075D1E00C582C25BC14B459FAA (void);
// 0x000002E6 System.Int32 Qualcomm.Snapdragon.Spaces.ImageTrackingFeature/xrSetTrackingOptimizationSettingsHintQCOMDelegate::EndInvoke(System.IAsyncResult)
extern void xrSetTrackingOptimizationSettingsHintQCOMDelegate_EndInvoke_m581B788B52ED69BA9E2B0302F7E53322793D33EB (void);
// 0x000002E7 System.Void Qualcomm.Snapdragon.Spaces.ImageTarget::.ctor(System.UInt64,UnityEngine.XR.ARSubsystems.XRTrackedImage)
extern void ImageTarget__ctor_mF8F8356081A3924D9A612BAC4E69DD6407125344 (void);
// 0x000002E8 System.UInt64 Qualcomm.Snapdragon.Spaces.ImageTarget::get_ImageTargetHandle()
extern void ImageTarget_get_ImageTargetHandle_m8B024478A9FEC94D96370C30C5C4A8E16CCD2526 (void);
// 0x000002E9 System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingSubsystem::RegisterDescriptor()
extern void ImageTrackingSubsystem_RegisterDescriptor_m25BF3A48E0FA55CF91D1A32C14915FD164BC67BC (void);
// 0x000002EA System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingSubsystem::.ctor()
extern void ImageTrackingSubsystem__ctor_mB992192934593491E9718496208397B83E1E7BF7 (void);
// 0x000002EB System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingSubsystem/ImageTrackingProvider::set_imageLibrary(UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary)
extern void ImageTrackingProvider_set_imageLibrary_m8FCBA3A261C767A5809A4CA69397B5027CD760AC (void);
// 0x000002EC System.Int32 Qualcomm.Snapdragon.Spaces.ImageTrackingSubsystem/ImageTrackingProvider::get_requestedMaxNumberOfMovingImages()
extern void ImageTrackingProvider_get_requestedMaxNumberOfMovingImages_mC6101B30AD239CE69C630E3DD438725C75F301E1 (void);
// 0x000002ED System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingSubsystem/ImageTrackingProvider::set_requestedMaxNumberOfMovingImages(System.Int32)
extern void ImageTrackingProvider_set_requestedMaxNumberOfMovingImages_mE6B06CDC17E4C5C87BEC566F1BDCA6E2B533AA93 (void);
// 0x000002EE System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingSubsystem/ImageTrackingProvider::SetInitialTrackingModesDelegate(Qualcomm.Snapdragon.Spaces.ImageTrackingSubsystem/ImageTrackingProvider/SpacesInitialTrackingModesDelegate)
extern void ImageTrackingProvider_SetInitialTrackingModesDelegate_m3EA517120C03F8CD7859B883A90BD037601212D6 (void);
// 0x000002EF System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingSubsystem/ImageTrackingProvider::Start()
extern void ImageTrackingProvider_Start_m6EB3BE6261E546DC76B69D5C70FE2F75713DDDFD (void);
// 0x000002F0 System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingSubsystem/ImageTrackingProvider::Stop()
extern void ImageTrackingProvider_Stop_m59A70BFEF66EBB008F8FAE672CB5F38F0A8C5673 (void);
// 0x000002F1 System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingSubsystem/ImageTrackingProvider::Destroy()
extern void ImageTrackingProvider_Destroy_m68D5BDE28623DAECF7DDC1D137D97E3108C44DEB (void);
// 0x000002F2 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage> Qualcomm.Snapdragon.Spaces.ImageTrackingSubsystem/ImageTrackingProvider::GetChanges(UnityEngine.XR.ARSubsystems.XRTrackedImage,Unity.Collections.Allocator)
extern void ImageTrackingProvider_GetChanges_m3CD1045C43478CF0635FD4B268DA855A48688DD0 (void);
// 0x000002F3 UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary Qualcomm.Snapdragon.Spaces.ImageTrackingSubsystem/ImageTrackingProvider::CreateRuntimeLibrary(UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary)
extern void ImageTrackingProvider_CreateRuntimeLibrary_m0F0F3FBD372190BC093D236C41A88B7471B06B7A (void);
// 0x000002F4 System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingSubsystem/ImageTrackingProvider::SetTrackingModes(Qualcomm.Snapdragon.Spaces.SpacesReferenceImageTrackingModes)
extern void ImageTrackingProvider_SetTrackingModes_mDCB8C03418F44CF85D4CEF95F7920A238E3C68F5 (void);
// 0x000002F5 System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingSubsystem/ImageTrackingProvider::StopTrackingImageInstance(System.String,System.UInt32)
extern void ImageTrackingProvider_StopTrackingImageInstance_m40DB0200AA7BB39F38292C3E4AF3F9F40BFC95B5 (void);
// 0x000002F6 System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingSubsystem/ImageTrackingProvider::DestroyImageTracker()
extern void ImageTrackingProvider_DestroyImageTracker_m81CB3B1EB22896ED0966110A0146CAA39CCFBA5B (void);
// 0x000002F7 System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingSubsystem/ImageTrackingProvider::.ctor()
extern void ImageTrackingProvider__ctor_m4E216C6F6FE05DC958C54FA47118184A8D6185C6 (void);
// 0x000002F8 System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingSubsystem/ImageTrackingProvider/SpacesInitialTrackingModesDelegate::.ctor(System.Object,System.IntPtr)
extern void SpacesInitialTrackingModesDelegate__ctor_mE18A4E0DB16EFCC00C324F3C601E284AC07AE6D9 (void);
// 0x000002F9 Qualcomm.Snapdragon.Spaces.SpacesReferenceImageTrackingModes Qualcomm.Snapdragon.Spaces.ImageTrackingSubsystem/ImageTrackingProvider/SpacesInitialTrackingModesDelegate::Invoke()
extern void SpacesInitialTrackingModesDelegate_Invoke_m303EA9CF25A27315CB9581B38067C80FAD02CF95 (void);
// 0x000002FA System.IAsyncResult Qualcomm.Snapdragon.Spaces.ImageTrackingSubsystem/ImageTrackingProvider/SpacesInitialTrackingModesDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void SpacesInitialTrackingModesDelegate_BeginInvoke_m92312DDD06AA3DA834AF882BF502FDE4FFC32D3D (void);
// 0x000002FB Qualcomm.Snapdragon.Spaces.SpacesReferenceImageTrackingModes Qualcomm.Snapdragon.Spaces.ImageTrackingSubsystem/ImageTrackingProvider/SpacesInitialTrackingModesDelegate::EndInvoke(System.IAsyncResult)
extern void SpacesInitialTrackingModesDelegate_EndInvoke_m8B7A37EACB4ACE1396555C3C5B7F370DD60706D7 (void);
// 0x000002FC System.Void Qualcomm.Snapdragon.Spaces.ImageTrackingSubsystem/ImageTrackingProvider/<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_m52EB6D29C868C4EDC8016116ED55A7495CEF93C2 (void);
// 0x000002FD System.Boolean Qualcomm.Snapdragon.Spaces.ImageTrackingSubsystem/ImageTrackingProvider/<>c__DisplayClass17_0::<GetChanges>b__0(UnityEngine.XR.ARSubsystems.XRTrackedImage)
extern void U3CU3Ec__DisplayClass17_0_U3CGetChangesU3Eb__0_m0264AC86208267E995C2A7D15D0520A7385F1E60 (void);
// 0x000002FE System.Void Qualcomm.Snapdragon.Spaces.SpacesReferenceImageConfigurator::.ctor()
extern void SpacesReferenceImageConfigurator__ctor_m4410BBC07CAC93F74D79B060BAAB1F63554A444F (void);
// 0x000002FF System.Void Qualcomm.Snapdragon.Spaces.SpacesReferenceImageConfigurator::SetTrackingModeForReferenceImage(System.String,Qualcomm.Snapdragon.Spaces.SpacesImageTrackingMode)
extern void SpacesReferenceImageConfigurator_SetTrackingModeForReferenceImage_m0242A3EFDEC316DCA1BB15C8DAC371B57875A054 (void);
// 0x00000300 Qualcomm.Snapdragon.Spaces.SpacesImageTrackingMode Qualcomm.Snapdragon.Spaces.SpacesReferenceImageConfigurator::GetTrackingModeForReferenceImage(System.String)
extern void SpacesReferenceImageConfigurator_GetTrackingModeForReferenceImage_m09197189143FBD6F6D6F64EA0EDE68EDAB76F4FD (void);
// 0x00000301 System.Boolean Qualcomm.Snapdragon.Spaces.SpacesReferenceImageConfigurator::HasReferenceImageTrackingMode(System.String)
extern void SpacesReferenceImageConfigurator_HasReferenceImageTrackingMode_mB5246242B4A9F24755FE0443AAB93FD1ACAE4C23 (void);
// 0x00000302 System.Void Qualcomm.Snapdragon.Spaces.SpacesReferenceImageConfigurator::StopTrackingImageInstance(System.String,UnityEngine.XR.ARSubsystems.TrackableId)
extern void SpacesReferenceImageConfigurator_StopTrackingImageInstance_mB0DB1D3E1AD229CFABBE6CC3A24240C4E554F093 (void);
// 0x00000303 System.Void Qualcomm.Snapdragon.Spaces.SpacesReferenceImageConfigurator::Awake()
extern void SpacesReferenceImageConfigurator_Awake_m89E72694CAD6060567AC93E58A6B416412810352 (void);
// 0x00000304 System.Void Qualcomm.Snapdragon.Spaces.SpacesReferenceImageConfigurator::OnEnable()
extern void SpacesReferenceImageConfigurator_OnEnable_m3155BC669BA2169F8914E4F8A67E01AD61B6740F (void);
// 0x00000305 System.Void Qualcomm.Snapdragon.Spaces.SpacesReferenceImageConfigurator::OnDisable()
extern void SpacesReferenceImageConfigurator_OnDisable_m5EBBCBD998A1A0C102E7A22BE0A313B846C7EC5C (void);
// 0x00000306 Qualcomm.Snapdragon.Spaces.SpacesReferenceImageTrackingModes Qualcomm.Snapdragon.Spaces.SpacesReferenceImageConfigurator::GetSpacesReferenceImageTrackingModes()
extern void SpacesReferenceImageConfigurator_GetSpacesReferenceImageTrackingModes_mDA5907A261591A4942A8F48679F88DD31B687A7D (void);
// 0x00000307 System.Collections.Generic.Dictionary`2<System.String,Qualcomm.Snapdragon.Spaces.SpacesImageTrackingMode> Qualcomm.Snapdragon.Spaces.SpacesReferenceImageConfigurator::CreateTrackingModesDictionary()
extern void SpacesReferenceImageConfigurator_CreateTrackingModesDictionary_m5F201E1517DB9397E71C41DFB0BE3B6876E8672A (void);
// 0x00000308 System.Void Qualcomm.Snapdragon.Spaces.SpacesReferenceImageConfigurator::SyncTrackingModes(System.Collections.Generic.Dictionary`2<System.String,Qualcomm.Snapdragon.Spaces.SpacesImageTrackingMode>)
extern void SpacesReferenceImageConfigurator_SyncTrackingModes_m04DC7A3BC1B2DDCCFB47EBC8F340DA4AB0A1D297 (void);
// 0x00000309 System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.SpacesImageTrackingMode> Qualcomm.Snapdragon.Spaces.SpacesReferenceImageTrackingModes::get_TrackingModes()
extern void SpacesReferenceImageTrackingModes_get_TrackingModes_mA83E8E306AB35C0378BD98ADE7C32F1CDFE78407 (void);
// 0x0000030A System.Collections.Generic.List`1<System.String> Qualcomm.Snapdragon.Spaces.SpacesReferenceImageTrackingModes::get_ReferenceImageNames()
extern void SpacesReferenceImageTrackingModes_get_ReferenceImageNames_m5318079BDB7282EAD230DB70495BE91E69E8ACA7 (void);
// 0x0000030B System.Int32 Qualcomm.Snapdragon.Spaces.SpacesReferenceImageTrackingModes::get_Count()
extern void SpacesReferenceImageTrackingModes_get_Count_m28B919AAFED09118FE1700F8F8BCD8B32BB7BB7C (void);
// 0x0000030C System.Void Qualcomm.Snapdragon.Spaces.SpacesReferenceImageTrackingModes::SetTrackingModeForReferenceImage(System.String,Qualcomm.Snapdragon.Spaces.XrImageTargetTrackingModeQCOM)
extern void SpacesReferenceImageTrackingModes_SetTrackingModeForReferenceImage_m845EBD79551814F0EB100FDDEA8B4656C9E932B3 (void);
// 0x0000030D Qualcomm.Snapdragon.Spaces.XrImageTargetTrackingModeQCOM Qualcomm.Snapdragon.Spaces.SpacesReferenceImageTrackingModes::GetTrackingModeForReferenceImage(System.String)
extern void SpacesReferenceImageTrackingModes_GetTrackingModeForReferenceImage_mACBFB49F5F673B4F8315EB676A70377D60AEDCEF (void);
// 0x0000030E System.Void Qualcomm.Snapdragon.Spaces.SpacesReferenceImageTrackingModes::AddTrackingModeForReferenceImage(System.String,Qualcomm.Snapdragon.Spaces.XrImageTargetTrackingModeQCOM)
extern void SpacesReferenceImageTrackingModes_AddTrackingModeForReferenceImage_mBF822E91F35AB0B900F9BD86461EDA310F527D65 (void);
// 0x0000030F System.Void Qualcomm.Snapdragon.Spaces.SpacesReferenceImageTrackingModes::Clear()
extern void SpacesReferenceImageTrackingModes_Clear_mE6FF623097421F07C73FD1CA2B73F00C559A8981 (void);
// 0x00000310 System.Void Qualcomm.Snapdragon.Spaces.SpacesReferenceImageTrackingModes::.ctor()
extern void SpacesReferenceImageTrackingModes__ctor_m8BFFB3446D29F552720458D147BA66C471431E5F (void);
// 0x00000311 System.Void Qualcomm.Snapdragon.Spaces.SpacesReferenceImageTrackingModes/<>c::.cctor()
extern void U3CU3Ec__cctor_m79BAB9ADCE65159C4E100C21B7A31F1BCEBC6331 (void);
// 0x00000312 System.Void Qualcomm.Snapdragon.Spaces.SpacesReferenceImageTrackingModes/<>c::.ctor()
extern void U3CU3Ec__ctor_m3E47C3801566370F30181FAA10C8E8EB72F9E39E (void);
// 0x00000313 Qualcomm.Snapdragon.Spaces.SpacesImageTrackingMode Qualcomm.Snapdragon.Spaces.SpacesReferenceImageTrackingModes/<>c::<get_TrackingModes>b__3_0(Qualcomm.Snapdragon.Spaces.XrImageTargetTrackingModeQCOM)
extern void U3CU3Ec_U3Cget_TrackingModesU3Eb__3_0_m491E8D8DB5485FAD1B20129927CA39A9C052D27B (void);
// 0x00000314 System.Void Qualcomm.Snapdragon.Spaces.SpacesRuntimeReferenceImageLibrary::.ctor(UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary,Qualcomm.Snapdragon.Spaces.SpacesReferenceImageTrackingModes)
extern void SpacesRuntimeReferenceImageLibrary__ctor_m3F89591FF0533B5D4FBF9D438FCAF4C1C6E4795F (void);
// 0x00000315 System.Int32 Qualcomm.Snapdragon.Spaces.SpacesRuntimeReferenceImageLibrary::get_count()
extern void SpacesRuntimeReferenceImageLibrary_get_count_mF5AFFF5C071D471A88FAD153D5858FCE2ED836DA (void);
// 0x00000316 Qualcomm.Snapdragon.Spaces.SpacesReferenceImageTrackingModes Qualcomm.Snapdragon.Spaces.SpacesRuntimeReferenceImageLibrary::get_TrackingModes()
extern void SpacesRuntimeReferenceImageLibrary_get_TrackingModes_m32718E8E8319EC8EF29251E7F15AEF1CA022E287 (void);
// 0x00000317 UnityEngine.XR.ARSubsystems.XRReferenceImage Qualcomm.Snapdragon.Spaces.SpacesRuntimeReferenceImageLibrary::GetReferenceImageAt(System.Int32)
extern void SpacesRuntimeReferenceImageLibrary_GetReferenceImageAt_m541CDF5911CF59DD7A754C763E80C62362A32CC5 (void);
// 0x00000318 System.Void Qualcomm.Snapdragon.Spaces.Plane::.ctor(UnityEngine.XR.ARSubsystems.BoundedPlane,System.UInt64)
extern void Plane__ctor_m624025168CC3E02B83D8D86E49A7378A7E1A5B13 (void);
// 0x00000319 System.Boolean Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::get_IsRunning()
extern void PlaneDetectionFeature_get_IsRunning_m724E2F5E2BDEAF699D7D66D0E3DAD009C0AFB83D (void);
// 0x0000031A System.UInt64 Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::get_ActiveHandle()
extern void PlaneDetectionFeature_get_ActiveHandle_m3C8D4DCC417FDE787CF69D9176E0D5B49AB64DAF (void);
// 0x0000031B System.Boolean Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::get_IsRequiringBaseRuntimeFeature()
extern void PlaneDetectionFeature_get_IsRequiringBaseRuntimeFeature_m7BED5FE720939C6946BEDE930E46318441CCF2BA (void);
// 0x0000031C System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::RegisterProviderWithSceneObserver(System.String)
extern void PlaneDetectionFeature_RegisterProviderWithSceneObserver_m206F16A9A221592872C85BA14916D84822936EF5 (void);
// 0x0000031D System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::UnregisterProviderWithSceneObserver(System.String)
extern void PlaneDetectionFeature_UnregisterProviderWithSceneObserver_m45BAE9E976C1B9C6B133EC528534C99F5C6300DA (void);
// 0x0000031E System.UInt64 Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::TryCreatePlaneDetection(System.String,UnityEngine.XR.ARSubsystems.PlaneDetectionMode,System.Boolean)
extern void PlaneDetectionFeature_TryCreatePlaneDetection_mF96FC4CBCFA40145A759F0F9C3DCD7129B6A397B (void);
// 0x0000031F System.Boolean Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::TryDestroyPlaneDetection(System.String)
extern void PlaneDetectionFeature_TryDestroyPlaneDetection_mA338E0C7A385EBD6F1434C6D1FC0464EFAFBEB8B (void);
// 0x00000320 System.Boolean Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::TryGetPlaneDetectionState()
extern void PlaneDetectionFeature_TryGetPlaneDetectionState_mB8FC960D8225DC7BD7A434A03C70D2C8E898BF75 (void);
// 0x00000321 System.Boolean Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::TryLocatePlanes(System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.Plane>&)
extern void PlaneDetectionFeature_TryLocatePlanes_mDAF0F8B9CBA2E34290B7032A735B22E1EC3A15E9 (void);
// 0x00000322 System.Boolean Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::TryGetPlaneConvexHullVertexBuffer(System.UInt64,System.Collections.Generic.List`1<UnityEngine.Vector2>&)
extern void PlaneDetectionFeature_TryGetPlaneConvexHullVertexBuffer_mD791EC812BF9F3EF28A749046AA666E571585654 (void);
// 0x00000323 System.String Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::GetXrLayersToLoad()
extern void PlaneDetectionFeature_GetXrLayersToLoad_mDF778FE60B2C87140B6E1F4E48177E82D0FBD2B2 (void);
// 0x00000324 System.IntPtr Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::HookGetInstanceProcAddr(System.IntPtr)
extern void PlaneDetectionFeature_HookGetInstanceProcAddr_m8A395F34C39AF5354F3AFE5C14BE47F72455F0FB (void);
// 0x00000325 System.Boolean Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::OnInstanceCreate(System.UInt64)
extern void PlaneDetectionFeature_OnInstanceCreate_m948262FB32FF8F8D3B7A0D1748610455B1DD851C (void);
// 0x00000326 System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::OnSubsystemCreate()
extern void PlaneDetectionFeature_OnSubsystemCreate_m2D4D7E73739DBEB7D250C9F1ACE01BD60C69B9FE (void);
// 0x00000327 System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::OnSubsystemStop()
extern void PlaneDetectionFeature_OnSubsystemStop_mFCC743FCE9BBFD309376422C4B2B5346A9A6061F (void);
// 0x00000328 System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::OnSubsystemDestroy()
extern void PlaneDetectionFeature_OnSubsystemDestroy_mC7AF55B6DB7FBC2BC00C2A47378332CE07C9EBEC (void);
// 0x00000329 System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::OnSessionCreate(System.UInt64)
extern void PlaneDetectionFeature_OnSessionCreate_m4677AF75D0C91A2B789CC85337F39437217731E2 (void);
// 0x0000032A System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::OnAppSpaceChange(System.UInt64)
extern void PlaneDetectionFeature_OnAppSpaceChange_mCAAF2ED6C588DDA7E587D171A60B2A416157E8E2 (void);
// 0x0000032B System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::OnHookMethods()
extern void PlaneDetectionFeature_OnHookMethods_mAD7AF3BA4D6653F15047F7D5C1A90EA2C6DBCFAF (void);
// 0x0000032C Qualcomm.Snapdragon.Spaces.XrPlaneFilterQCOM Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::PlaneDetectionModeToXrPlaneFilter(UnityEngine.XR.ARSubsystems.PlaneDetectionMode)
extern void PlaneDetectionFeature_PlaneDetectionModeToXrPlaneFilter_mB9DDCDD121FA3000A601111554EEE291FDC33A5D (void);
// 0x0000032D System.Boolean Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::TryDestroyPlaneDetection()
extern void PlaneDetectionFeature_TryDestroyPlaneDetection_m9E61874FE52F4F9B17ADED68294546F4FEE3D8AC (void);
// 0x0000032E System.Boolean Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::TryLocatePlanes_XR_QCOM_Plane_Detection(System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.Plane>&)
extern void PlaneDetectionFeature_TryLocatePlanes_XR_QCOM_Plane_Detection_mE6F2259760C0F8D8E882C30DC57C08C8CBB42516 (void);
// 0x0000032F System.Boolean Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::TryLocatePlanes_XR_MSFT_Scene_Understanding(System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.Plane>&)
extern void PlaneDetectionFeature_TryLocatePlanes_XR_MSFT_Scene_Understanding_m2A042287592C33F4667713350A68237DA6220D5C (void);
// 0x00000330 System.Boolean Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::TryGetPlaneConvexHullVertexBuffer_XR_QCOM_Plane_Detection(System.UInt64,System.Collections.Generic.List`1<UnityEngine.Vector2>&)
extern void PlaneDetectionFeature_TryGetPlaneConvexHullVertexBuffer_XR_QCOM_Plane_Detection_m431C85F8BCA2CA8DA5F26BE8AFE2B020EB190A4C (void);
// 0x00000331 System.Boolean Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::TryGetPlaneConvexHullVertexBuffer_XR_MSFT_Scene_Understanding(System.UInt64,System.Collections.Generic.List`1<UnityEngine.Vector2>&)
extern void PlaneDetectionFeature_TryGetPlaneConvexHullVertexBuffer_XR_MSFT_Scene_Understanding_m37F2F5E8DE2C35F073FF0AACEF38689441005E59 (void);
// 0x00000332 System.IntPtr Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::Internal_GetInterceptedInstanceProcAddr(System.IntPtr)
extern void PlaneDetectionFeature_Internal_GetInterceptedInstanceProcAddr_m3F8DCC1D22C5E2B1191BA4F1B2831F52EA906D55 (void);
// 0x00000333 System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::Internal_RegisterProviderWithSceneObserver(System.String,System.Int32)
extern void PlaneDetectionFeature_Internal_RegisterProviderWithSceneObserver_m6295EF004F4A9A63F25EAD750E181AAEACDDBB2D (void);
// 0x00000334 System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::Internal_UnregisterProviderWithSceneObserver(System.String)
extern void PlaneDetectionFeature_Internal_UnregisterProviderWithSceneObserver_m4232C00DC01AE74A3954ADE5AAE2E64392E6385D (void);
// 0x00000335 System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::Internal_SetInstanceHandle(System.UInt64)
extern void PlaneDetectionFeature_Internal_SetInstanceHandle_m97D11A910168074A27C3993C2AED177207DD081E (void);
// 0x00000336 System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::Internal_SetSessionHandle(System.UInt64)
extern void PlaneDetectionFeature_Internal_SetSessionHandle_m2E973742A3557E57E1EF6A243B983BC36870CAC2 (void);
// 0x00000337 System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::Internal_SetSpaceHandle(System.UInt64)
extern void PlaneDetectionFeature_Internal_SetSpaceHandle_m667C24EC6F3249ED612E59D59C289B75572D2F12 (void);
// 0x00000338 System.Boolean Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::Internal_UpdateObservedScene(System.String)
extern void PlaneDetectionFeature_Internal_UpdateObservedScene_m60BDF83AAC662AFA5CE294A868414C71732B4B1A (void);
// 0x00000339 System.Boolean Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::Internal_UpdatePlanes()
extern void PlaneDetectionFeature_Internal_UpdatePlanes_mB5C6E31305AF9C52679CDC39DF73E8854951F7C4 (void);
// 0x0000033A System.Boolean Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::Internal_CountScenePlanes(System.UInt32&)
extern void PlaneDetectionFeature_Internal_CountScenePlanes_mC44344257A838ACB452F18D4324DB056E4D5B0AE (void);
// 0x0000033B System.Boolean Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::Internal_FetchScenePlanes(System.UInt32,System.IntPtr)
extern void PlaneDetectionFeature_Internal_FetchScenePlanes_mA1172900D4C945B844D5BB14A7BB04492728604C (void);
// 0x0000033C System.Boolean Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::Internal_FetchPlaneVertices(System.IntPtr,System.IntPtr,System.IntPtr)
extern void PlaneDetectionFeature_Internal_FetchPlaneVertices_mC83F40A3A02E086B1965A8EAD7604549A49981D6 (void);
// 0x0000033D System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::.ctor()
extern void PlaneDetectionFeature__ctor_m00401DAC5A416FF44A5E3019D559F01E80DE5F8E (void);
// 0x0000033E System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature::.cctor()
extern void PlaneDetectionFeature__cctor_mED7C4202C607EE6A945FD53BEEC478D857FCE8F0 (void);
// 0x0000033F System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature/CreatePlaneDetectionQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void CreatePlaneDetectionQCOMDelegate__ctor_mB2C7CBE05B5A45FA9840027FF86C3487436083FB (void);
// 0x00000340 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature/CreatePlaneDetectionQCOMDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrPlaneDetectionCreateInfoQCOM&,System.UInt64&)
extern void CreatePlaneDetectionQCOMDelegate_Invoke_m3CA30D087B044DE49E4041F22AFD5A446645B99E (void);
// 0x00000341 System.IAsyncResult Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature/CreatePlaneDetectionQCOMDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrPlaneDetectionCreateInfoQCOM&,System.UInt64&,System.AsyncCallback,System.Object)
extern void CreatePlaneDetectionQCOMDelegate_BeginInvoke_mA5DC65DD79DC01113AF6B1DA02F026F70D7B3F29 (void);
// 0x00000342 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature/CreatePlaneDetectionQCOMDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrPlaneDetectionCreateInfoQCOM&,System.UInt64&,System.IAsyncResult)
extern void CreatePlaneDetectionQCOMDelegate_EndInvoke_m246081FFD89A2E855944A07513AFBE7358DC3E71 (void);
// 0x00000343 System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature/DestroyPlaneDetectionQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyPlaneDetectionQCOMDelegate__ctor_m7D6585803C599BAB923DB82A1B8E3C8DEA2FF876 (void);
// 0x00000344 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature/DestroyPlaneDetectionQCOMDelegate::Invoke(System.UInt64)
extern void DestroyPlaneDetectionQCOMDelegate_Invoke_mB618169C5F7111BEC19C6EC3FEFF6638E1808BD4 (void);
// 0x00000345 System.IAsyncResult Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature/DestroyPlaneDetectionQCOMDelegate::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void DestroyPlaneDetectionQCOMDelegate_BeginInvoke_mE8E73E91281D2532EE13B9C4BBA831D94E241396 (void);
// 0x00000346 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature/DestroyPlaneDetectionQCOMDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyPlaneDetectionQCOMDelegate_EndInvoke_mBDD5BD6E0DEEF3FD6F932366FC4DA9A4C5200E90 (void);
// 0x00000347 System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature/LocatePlanesQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void LocatePlanesQCOMDelegate__ctor_m0CB02B6450B41A7DA7A33935229B19E7C2183E58 (void);
// 0x00000348 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature/LocatePlanesQCOMDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrPlanesLocateInfoQCOM&,Qualcomm.Snapdragon.Spaces.XrPlaneLocationsQCOM&)
extern void LocatePlanesQCOMDelegate_Invoke_m81A54466376ED434AF05996625217A08531C9DE4 (void);
// 0x00000349 System.IAsyncResult Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature/LocatePlanesQCOMDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrPlanesLocateInfoQCOM&,Qualcomm.Snapdragon.Spaces.XrPlaneLocationsQCOM&,System.AsyncCallback,System.Object)
extern void LocatePlanesQCOMDelegate_BeginInvoke_m25818A863263158E92218FFF702DCFF395C4786F (void);
// 0x0000034A Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature/LocatePlanesQCOMDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrPlanesLocateInfoQCOM&,Qualcomm.Snapdragon.Spaces.XrPlaneLocationsQCOM&,System.IAsyncResult)
extern void LocatePlanesQCOMDelegate_EndInvoke_m833A09822CA03181FDFB6A6169EC691EEADD6E69 (void);
// 0x0000034B System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature/GetPlaneDetectioNStateQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void GetPlaneDetectioNStateQCOMDelegate__ctor_mE10FFF5FF68CAE39038AD7AFAD2F66D73852E89B (void);
// 0x0000034C Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature/GetPlaneDetectioNStateQCOMDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrPlaneDetectionState&)
extern void GetPlaneDetectioNStateQCOMDelegate_Invoke_mA0B7C2C21F6DE7FFCCC41FE02A2BC77E21D260AA (void);
// 0x0000034D System.IAsyncResult Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature/GetPlaneDetectioNStateQCOMDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrPlaneDetectionState&,System.AsyncCallback,System.Object)
extern void GetPlaneDetectioNStateQCOMDelegate_BeginInvoke_mD13188ACA6F54A5BFD1F2D49EA1E78E1ACF83C56 (void);
// 0x0000034E Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature/GetPlaneDetectioNStateQCOMDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrPlaneDetectionState&,System.IAsyncResult)
extern void GetPlaneDetectioNStateQCOMDelegate_EndInvoke_mB7CFBD07DB5F43F02E6AB8F89E08A8105F0CB7B6 (void);
// 0x0000034F System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature/xrGetPlaneConvexHullVertexBufferQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void xrGetPlaneConvexHullVertexBufferQCOMDelegate__ctor_mFCB35CFFB01211D1C5A21E814A0244BBE16968BF (void);
// 0x00000350 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature/xrGetPlaneConvexHullVertexBufferQCOMDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrPlaneConvexHullBufferInfoQCOM&,Qualcomm.Snapdragon.Spaces.XrPlaneConvexHullVertexBufferQCOM&)
extern void xrGetPlaneConvexHullVertexBufferQCOMDelegate_Invoke_mC1B520F874517FFBF2D8B6B34D2D399A3CE7405C (void);
// 0x00000351 System.IAsyncResult Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature/xrGetPlaneConvexHullVertexBufferQCOMDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrPlaneConvexHullBufferInfoQCOM&,Qualcomm.Snapdragon.Spaces.XrPlaneConvexHullVertexBufferQCOM&,System.AsyncCallback,System.Object)
extern void xrGetPlaneConvexHullVertexBufferQCOMDelegate_BeginInvoke_mF9D93AFA0E9F825000A7CA718DA1FC7B3EBF0383 (void);
// 0x00000352 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.PlaneDetectionFeature/xrGetPlaneConvexHullVertexBufferQCOMDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrPlaneConvexHullBufferInfoQCOM&,Qualcomm.Snapdragon.Spaces.XrPlaneConvexHullVertexBufferQCOM&,System.IAsyncResult)
extern void xrGetPlaneConvexHullVertexBufferQCOMDelegate_EndInvoke_m6AB3C5D03C80B3E56BB2655F108A317A0FBA9EBA (void);
// 0x00000353 System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionSubsystem::RegisterDescriptor()
extern void PlaneDetectionSubsystem_RegisterDescriptor_mA1E0DD69226518C08B6762A45DDB1CD6D58FCFCF (void);
// 0x00000354 System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionSubsystem::.ctor()
extern void PlaneDetectionSubsystem__ctor_m525B3A4D3BF7D9A3B5686F90EEA0418434461D2E (void);
// 0x00000355 UnityEngine.XR.ARSubsystems.PlaneDetectionMode Qualcomm.Snapdragon.Spaces.PlaneDetectionSubsystem/PlaneDetectionProvider::get_currentPlaneDetectionMode()
extern void PlaneDetectionProvider_get_currentPlaneDetectionMode_mC9977178B505A22EDEC4A5E42543935A08473081 (void);
// 0x00000356 UnityEngine.XR.ARSubsystems.PlaneDetectionMode Qualcomm.Snapdragon.Spaces.PlaneDetectionSubsystem/PlaneDetectionProvider::get_requestedPlaneDetectionMode()
extern void PlaneDetectionProvider_get_requestedPlaneDetectionMode_mCE8F55A534E4CD840D229F210E3EBF33A78CFA0F (void);
// 0x00000357 System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionSubsystem/PlaneDetectionProvider::set_requestedPlaneDetectionMode(UnityEngine.XR.ARSubsystems.PlaneDetectionMode)
extern void PlaneDetectionProvider_set_requestedPlaneDetectionMode_m3F00ECE73CD3D09EEC2C77BA2E8DF3E8A42EB576 (void);
// 0x00000358 System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionSubsystem/PlaneDetectionProvider::Start()
extern void PlaneDetectionProvider_Start_mCF97C528FC1A89C30470F56EC59EFC5EA2444A90 (void);
// 0x00000359 System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionSubsystem/PlaneDetectionProvider::Stop()
extern void PlaneDetectionProvider_Stop_m4F40AC8864F92F715415AFD8D42387E8A0350315 (void);
// 0x0000035A System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionSubsystem/PlaneDetectionProvider::Destroy()
extern void PlaneDetectionProvider_Destroy_m3026F6D36807A10EA73450044E5858DB8496370E (void);
// 0x0000035B System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionSubsystem/PlaneDetectionProvider::GetBoundary(UnityEngine.XR.ARSubsystems.TrackableId,Unity.Collections.Allocator,Unity.Collections.NativeArray`1<UnityEngine.Vector2>&)
extern void PlaneDetectionProvider_GetBoundary_m8A8A50994C9B1FAF3E27FCC69B7F695FEBD3EA3D (void);
// 0x0000035C UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.BoundedPlane> Qualcomm.Snapdragon.Spaces.PlaneDetectionSubsystem/PlaneDetectionProvider::GetChanges(UnityEngine.XR.ARSubsystems.BoundedPlane,Unity.Collections.Allocator)
extern void PlaneDetectionProvider_GetChanges_m2724C838BD41ABC0C5BF94C17D54CE20A53EA89C (void);
// 0x0000035D System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionSubsystem/PlaneDetectionProvider::DestroyPlaneDetection()
extern void PlaneDetectionProvider_DestroyPlaneDetection_m82BA92DDC4F1855B63F078F6C29C8014FA70124C (void);
// 0x0000035E System.Void Qualcomm.Snapdragon.Spaces.PlaneDetectionSubsystem/PlaneDetectionProvider::.ctor()
extern void PlaneDetectionProvider__ctor_m6FA2FADCC4342D9D432E3732D3E46B4C2322C2E0 (void);
// 0x0000035F System.Boolean Qualcomm.Snapdragon.Spaces.SpacesARPlaneManagerConfig::get_UseSceneUnderstandingPlaneDetection()
extern void SpacesARPlaneManagerConfig_get_UseSceneUnderstandingPlaneDetection_m4302156616E8CBE64F67FC9E802CB7D088270CDF (void);
// 0x00000360 System.Boolean Qualcomm.Snapdragon.Spaces.SpacesARPlaneManagerConfig::get_ConvexHullEnabled()
extern void SpacesARPlaneManagerConfig_get_ConvexHullEnabled_m662D27769D6D6457F53B7CEA70FD9FA662D530EC (void);
// 0x00000361 System.Void Qualcomm.Snapdragon.Spaces.SpacesARPlaneManagerConfig::set_ConvexHullEnabled(System.Boolean)
extern void SpacesARPlaneManagerConfig_set_ConvexHullEnabled_mD127A565545FF2027412718AC96E5182E2075BDE (void);
// 0x00000362 System.Void Qualcomm.Snapdragon.Spaces.SpacesARPlaneManagerConfig::Start()
extern void SpacesARPlaneManagerConfig_Start_m110CCEDE46F8AEF65BEC892A61B609AF7AEFB8CF (void);
// 0x00000363 System.Void Qualcomm.Snapdragon.Spaces.SpacesARPlaneManagerConfig::.ctor()
extern void SpacesARPlaneManagerConfig__ctor_m1EB2A9A28D086D1F980D6BD31AEFEE85ACBB2F33 (void);
// 0x00000364 System.Boolean Qualcomm.Snapdragon.Spaces.SpacesARMarker::get_IsMarkerDataAvailable()
extern void SpacesARMarker_get_IsMarkerDataAvailable_m4C3AF7319044E1043EA3D3FF1571ACC5A258D858 (void);
// 0x00000365 System.Void Qualcomm.Snapdragon.Spaces.SpacesARMarker::set_IsMarkerDataAvailable(System.Boolean)
extern void SpacesARMarker_set_IsMarkerDataAvailable_m857846F9A845C23939CF7052863B80A5DE36200D (void);
// 0x00000366 System.String Qualcomm.Snapdragon.Spaces.SpacesARMarker::get_Data()
extern void SpacesARMarker_get_Data_mA936C9D42FC586DB7E05EB163D21ECB3C0860375 (void);
// 0x00000367 System.Void Qualcomm.Snapdragon.Spaces.SpacesARMarker::TryGetMarkerData(Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystem)
extern void SpacesARMarker_TryGetMarkerData_mE93EE7CB056B099083196D1B0706828570EB4928 (void);
// 0x00000368 System.Void Qualcomm.Snapdragon.Spaces.SpacesARMarker::.ctor()
extern void SpacesARMarker__ctor_m2EAA402583058633B8E7B05ED2324643A47EFD2B (void);
// 0x00000369 System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.SpacesARMarker> Qualcomm.Snapdragon.Spaces.SpacesMarkersChangedEventArgs::get_added()
extern void SpacesMarkersChangedEventArgs_get_added_mF8667E8CA2C87E2FE20F475940ADBC0722D97C9D (void);
// 0x0000036A System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.SpacesARMarker> Qualcomm.Snapdragon.Spaces.SpacesMarkersChangedEventArgs::get_updated()
extern void SpacesMarkersChangedEventArgs_get_updated_mD1F9474086262DB5B1CED69A4F5AE73031151FF9 (void);
// 0x0000036B System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.SpacesARMarker> Qualcomm.Snapdragon.Spaces.SpacesMarkersChangedEventArgs::get_removed()
extern void SpacesMarkersChangedEventArgs_get_removed_m243A7EDEEFB7C0F520132232D090A991D5221A07 (void);
// 0x0000036C System.Void Qualcomm.Snapdragon.Spaces.SpacesMarkersChangedEventArgs::.ctor(System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.SpacesARMarker>,System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.SpacesARMarker>,System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.SpacesARMarker>)
extern void SpacesMarkersChangedEventArgs__ctor_m2381BC40792947230BFA24DF519ACB5F0AE230BE (void);
// 0x0000036D System.Int32 Qualcomm.Snapdragon.Spaces.SpacesMarkersChangedEventArgs::GetHashCode()
extern void SpacesMarkersChangedEventArgs_GetHashCode_m1D18494D3716BE3E5832F5FDDFDA0A336FF9FE63 (void);
// 0x0000036E System.Boolean Qualcomm.Snapdragon.Spaces.SpacesMarkersChangedEventArgs::Equals(System.Object)
extern void SpacesMarkersChangedEventArgs_Equals_mC9647B04CB0822A03BFDED271897C3CA02C04F58 (void);
// 0x0000036F System.Boolean Qualcomm.Snapdragon.Spaces.SpacesMarkersChangedEventArgs::Equals(Qualcomm.Snapdragon.Spaces.SpacesMarkersChangedEventArgs)
extern void SpacesMarkersChangedEventArgs_Equals_mF42762FD75B2E3C660788392BB1A734F1EEEE5AA (void);
// 0x00000370 UnityEngine.GameObject Qualcomm.Snapdragon.Spaces.SpacesQrCodeManager::get_markerPrefab()
extern void SpacesQrCodeManager_get_markerPrefab_m2D616CA4634DA3492B7A5B91BB07D100E8B9702D (void);
// 0x00000371 System.Void Qualcomm.Snapdragon.Spaces.SpacesQrCodeManager::set_markerPrefab(UnityEngine.GameObject)
extern void SpacesQrCodeManager_set_markerPrefab_m50141CDA7439521D7704917DA44C53BCAE0934B9 (void);
// 0x00000372 System.Boolean Qualcomm.Snapdragon.Spaces.SpacesQrCodeManager::get_MarkerTracking()
extern void SpacesQrCodeManager_get_MarkerTracking_mCAFCCAD60F723C05731EAC9661265F283C8900DC (void);
// 0x00000373 System.Void Qualcomm.Snapdragon.Spaces.SpacesQrCodeManager::set_MarkerTracking(System.Boolean)
extern void SpacesQrCodeManager_set_MarkerTracking_mAD411B7EBF0590D82C31667BC45E865CB47DD21A (void);
// 0x00000374 System.Void Qualcomm.Snapdragon.Spaces.SpacesQrCodeManager::add_markersChanged(System.Action`1<Qualcomm.Snapdragon.Spaces.SpacesMarkersChangedEventArgs>)
extern void SpacesQrCodeManager_add_markersChanged_m4AE1F8A5A1455D25C06EC91078266C670264B604 (void);
// 0x00000375 System.Void Qualcomm.Snapdragon.Spaces.SpacesQrCodeManager::remove_markersChanged(System.Action`1<Qualcomm.Snapdragon.Spaces.SpacesMarkersChangedEventArgs>)
extern void SpacesQrCodeManager_remove_markersChanged_m04ABDBE2277EAE175DF4DCFD07EE28031919DF50 (void);
// 0x00000376 System.Void Qualcomm.Snapdragon.Spaces.SpacesQrCodeManager::OnEnable()
extern void SpacesQrCodeManager_OnEnable_mEBF0C6DD3CBD2BBFE2F96B5E6FE04C22A7E2D7BE (void);
// 0x00000377 UnityEngine.GameObject Qualcomm.Snapdragon.Spaces.SpacesQrCodeManager::GetPrefab()
extern void SpacesQrCodeManager_GetPrefab_m9E5B3A91354FB4B97AC04057909BDA809984D375 (void);
// 0x00000378 System.String Qualcomm.Snapdragon.Spaces.SpacesQrCodeManager::get_gameObjectName()
extern void SpacesQrCodeManager_get_gameObjectName_mD10FAADC2C2357BB8EC4AEC75467683C8E87096F (void);
// 0x00000379 System.Void Qualcomm.Snapdragon.Spaces.SpacesQrCodeManager::OnAfterSetSessionRelativeData(Qualcomm.Snapdragon.Spaces.SpacesARMarker,Qualcomm.Snapdragon.Spaces.XRTrackedMarker)
extern void SpacesQrCodeManager_OnAfterSetSessionRelativeData_m7DBFFD00549BE6F7D4A915682753D546543CC528 (void);
// 0x0000037A System.Void Qualcomm.Snapdragon.Spaces.SpacesQrCodeManager::OnTrackablesChanged(System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.SpacesARMarker>,System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.SpacesARMarker>,System.Collections.Generic.List`1<Qualcomm.Snapdragon.Spaces.SpacesARMarker>)
extern void SpacesQrCodeManager_OnTrackablesChanged_m5B62FC19C590D1A0FBA9E1F8FB761A0512269DC4 (void);
// 0x0000037B System.Void Qualcomm.Snapdragon.Spaces.SpacesQrCodeManager::OnValidate()
extern void SpacesQrCodeManager_OnValidate_m81E2B01FE0F6FD0A8213F681F21CC7596BA0476E (void);
// 0x0000037C System.Void Qualcomm.Snapdragon.Spaces.SpacesQrCodeManager::.ctor()
extern void SpacesQrCodeManager__ctor_mE3DF2C046967AED54FB649925FBF26356241982F (void);
// 0x0000037D System.Boolean Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature::get_IsRequiringBaseRuntimeFeature()
extern void QrCodeTrackingFeature_get_IsRequiringBaseRuntimeFeature_mD8FFDF7A1A2D0F1D2F12D2EEFCF868E43DC3BDA5 (void);
// 0x0000037E System.Boolean Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature::OnInstanceCreate(System.UInt64)
extern void QrCodeTrackingFeature_OnInstanceCreate_m82AC67109269A34395C5D57215DD37DD11B2239D (void);
// 0x0000037F System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature::OnSubsystemCreate()
extern void QrCodeTrackingFeature_OnSubsystemCreate_m485903D4BAAF20A25A726EA134CA91EDFB7B79EB (void);
// 0x00000380 System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature::OnSubsystemStop()
extern void QrCodeTrackingFeature_OnSubsystemStop_mA069F4A8B109F578997CEB76A63CC76BB01A2EE3 (void);
// 0x00000381 System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature::OnSubsystemDestroy()
extern void QrCodeTrackingFeature_OnSubsystemDestroy_m193E2C7CC911AD3F5232BFFA9ABB29A949EC4742 (void);
// 0x00000382 System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature::OnHookMethods()
extern void QrCodeTrackingFeature_OnHookMethods_mF1A8DCDBD8FB3C2792A2C0C98FC7006D80A0AFDB (void);
// 0x00000383 System.Boolean Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature::TryCreateMarkerTracker(System.UInt64&)
extern void QrCodeTrackingFeature_TryCreateMarkerTracker_mC3BCC1B1A96813C52C0776E6237F10CDD213FE12 (void);
// 0x00000384 System.Boolean Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature::TryDestroyMarkerTracker(System.UInt64)
extern void QrCodeTrackingFeature_TryDestroyMarkerTracker_mA1E075863E3F92D14A80ECEEFE436F1FC2994FA5 (void);
// 0x00000385 System.Boolean Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature::TryStartMarkerDetection(System.UInt64,System.Tuple`2<System.Byte,System.Byte>)
extern void QrCodeTrackingFeature_TryStartMarkerDetection_mF831B44E708F62644DF1A399E9C5CBE15A7634B2 (void);
// 0x00000386 System.Boolean Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature::TryStopMarkerDetection(System.UInt64)
extern void QrCodeTrackingFeature_TryStopMarkerDetection_mCFCF3B16CABDDEE2433EC54718855B2DFB97AAD0 (void);
// 0x00000387 System.Boolean Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature::TryAcquireMarkerUpdate(System.UInt64,System.UInt64&)
extern void QrCodeTrackingFeature_TryAcquireMarkerUpdate_mF22F98C63A3232E4DAE8F9B35B9E73C2549E4A8B (void);
// 0x00000388 System.Boolean Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature::TryGetMarkerUpdateInfo(System.UInt64,System.UInt32&,System.IntPtr&)
extern void QrCodeTrackingFeature_TryGetMarkerUpdateInfo_m8EE25E3BBCC69B900DB002C84821B4A63276C0C1 (void);
// 0x00000389 System.Boolean Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature::TryReleaseMarkerUpdate(System.UInt64)
extern void QrCodeTrackingFeature_TryReleaseMarkerUpdate_m9B51441F524584A01A14A117A4C493DC4F42EB26 (void);
// 0x0000038A System.Boolean Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature::TryCreateMarkerSpace(System.UInt64,Qualcomm.Snapdragon.Spaces.MarkerTrackingMode,UnityEngine.Vector2,System.UInt64&)
extern void QrCodeTrackingFeature_TryCreateMarkerSpace_mDAE482BF8D6013D93445B9CD49A6EA014A150CCC (void);
// 0x0000038B Qualcomm.Snapdragon.Spaces.XrMarkerTrackingModeQCOM Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature::GetXrMarkerTrackingMode(Qualcomm.Snapdragon.Spaces.MarkerTrackingMode)
extern void QrCodeTrackingFeature_GetXrMarkerTrackingMode_m5B1B74EAC4D5CD4DF5D21172ED8878D94AAEA25F (void);
// 0x0000038C System.Boolean Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature::TryDestroyMarkerSpace(System.UInt64)
extern void QrCodeTrackingFeature_TryDestroyMarkerSpace_m3E67C327002397308932F4EE885B739423970C09 (void);
// 0x0000038D System.Boolean Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature::TryGetQrCodeStringData(System.UInt64,System.String&)
extern void QrCodeTrackingFeature_TryGetQrCodeStringData_m9112F2AE93302EDCA86A75616FF1AF50148FBF72 (void);
// 0x0000038E System.Boolean Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature::TryGetMarkerPoseAndTrackingState(System.UInt64,System.Tuple`2<UnityEngine.Pose,UnityEngine.XR.ARSubsystems.TrackingState>&)
extern void QrCodeTrackingFeature_TryGetMarkerPoseAndTrackingState_m8469D93D4E485C2A8E1F737EA8E45998F8927E92 (void);
// 0x0000038F System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature::.ctor()
extern void QrCodeTrackingFeature__ctor_m5F055F4AE3B2AE2E4E214841AE6F2CF766280D5B (void);
// 0x00000390 System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature::.cctor()
extern void QrCodeTrackingFeature__cctor_mEB7DD28C887D524234AE54D7E884EF858BA43362 (void);
// 0x00000391 System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/EnumerateMarkerTypesQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void EnumerateMarkerTypesQCOMDelegate__ctor_m12989DC6D923B9FC5175B3F6FB43A2E650F15576 (void);
// 0x00000392 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/EnumerateMarkerTypesQCOMDelegate::Invoke(System.UInt64,System.UInt32,System.UInt32&,System.IntPtr)
extern void EnumerateMarkerTypesQCOMDelegate_Invoke_m782A87B094DB02E53BBBFAAD0A1484468F695160 (void);
// 0x00000393 System.IAsyncResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/EnumerateMarkerTypesQCOMDelegate::BeginInvoke(System.UInt64,System.UInt32,System.UInt32&,System.IntPtr,System.AsyncCallback,System.Object)
extern void EnumerateMarkerTypesQCOMDelegate_BeginInvoke_m5CEF67EA85E30235BBCF64D1C3BD71403C4D9403 (void);
// 0x00000394 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/EnumerateMarkerTypesQCOMDelegate::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void EnumerateMarkerTypesQCOMDelegate_EndInvoke_mDC2152D1FC31EC7CB4792AAD79B00F04EED7212F (void);
// 0x00000395 System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/EnumerateMarkerTrackingModesQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void EnumerateMarkerTrackingModesQCOMDelegate__ctor_mE62BD4FF2EF254D4419F767070A8472D6E88ABCF (void);
// 0x00000396 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/EnumerateMarkerTrackingModesQCOMDelegate::Invoke(System.UInt64,System.UInt32,System.UInt32&,System.IntPtr)
extern void EnumerateMarkerTrackingModesQCOMDelegate_Invoke_m9B340CF09E30174136E6B92A298C12D2108FED37 (void);
// 0x00000397 System.IAsyncResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/EnumerateMarkerTrackingModesQCOMDelegate::BeginInvoke(System.UInt64,System.UInt32,System.UInt32&,System.IntPtr,System.AsyncCallback,System.Object)
extern void EnumerateMarkerTrackingModesQCOMDelegate_BeginInvoke_mD829652DC9171245E346038A8588276271170DB2 (void);
// 0x00000398 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/EnumerateMarkerTrackingModesQCOMDelegate::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void EnumerateMarkerTrackingModesQCOMDelegate_EndInvoke_m8CEDB0ADCB8DF63F3D5047BC06DAD1203E56BEA6 (void);
// 0x00000399 System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/CreateMarkerTrackerQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void CreateMarkerTrackerQCOMDelegate__ctor_mD7A7614BB00F55789A7AC9111109946E36D50E03 (void);
// 0x0000039A Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/CreateMarkerTrackerQCOMDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrMarkerTrackerCreateInfoQCOM&,System.UInt64&)
extern void CreateMarkerTrackerQCOMDelegate_Invoke_m1B07B6C5A5A655BDEA59F9C704FF4C35B1FD967E (void);
// 0x0000039B System.IAsyncResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/CreateMarkerTrackerQCOMDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrMarkerTrackerCreateInfoQCOM&,System.UInt64&,System.AsyncCallback,System.Object)
extern void CreateMarkerTrackerQCOMDelegate_BeginInvoke_m2C8BEC07D9D2F3D6434DDA76B38F09363EBBE69A (void);
// 0x0000039C Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/CreateMarkerTrackerQCOMDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrMarkerTrackerCreateInfoQCOM&,System.UInt64&,System.IAsyncResult)
extern void CreateMarkerTrackerQCOMDelegate_EndInvoke_mA99684C12B112EDD3ED0B79BC6CCB05B452162A5 (void);
// 0x0000039D System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/DestroyMarkerTrackerQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyMarkerTrackerQCOMDelegate__ctor_m0C1929CA659511A815962DEF0AA9C022E43BFEC6 (void);
// 0x0000039E Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/DestroyMarkerTrackerQCOMDelegate::Invoke(System.UInt64)
extern void DestroyMarkerTrackerQCOMDelegate_Invoke_m5CB65FDE20D78DA5B9AD116A1F2A1673FE26CAF5 (void);
// 0x0000039F System.IAsyncResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/DestroyMarkerTrackerQCOMDelegate::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void DestroyMarkerTrackerQCOMDelegate_BeginInvoke_m98708B37CBB891ED08C84DB231F1E0E84EA04446 (void);
// 0x000003A0 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/DestroyMarkerTrackerQCOMDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyMarkerTrackerQCOMDelegate_EndInvoke_m694864D826CE5EB061D324E3DDBC8D7B722F6BC9 (void);
// 0x000003A1 System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/StartMarkerDetectionQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void StartMarkerDetectionQCOMDelegate__ctor_m7FEEC075A11404A5E62D412281011EB5851590A8 (void);
// 0x000003A2 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/StartMarkerDetectionQCOMDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrMarkerDetectionStartInfoQCOM&)
extern void StartMarkerDetectionQCOMDelegate_Invoke_m8DDAF05CBFA9D0B33A6B5704E67D047D585A74C7 (void);
// 0x000003A3 System.IAsyncResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/StartMarkerDetectionQCOMDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrMarkerDetectionStartInfoQCOM&,System.AsyncCallback,System.Object)
extern void StartMarkerDetectionQCOMDelegate_BeginInvoke_m4352A5856D43CE27B5C9142AE85773757D0A5F77 (void);
// 0x000003A4 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/StartMarkerDetectionQCOMDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrMarkerDetectionStartInfoQCOM&,System.IAsyncResult)
extern void StartMarkerDetectionQCOMDelegate_EndInvoke_m67331F6AE17F744B22A6268E408328A62A3E41CB (void);
// 0x000003A5 System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/StopMarkerDetectionQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void StopMarkerDetectionQCOMDelegate__ctor_m048FEADB626E747B330023E9378D1E510AF4EE6B (void);
// 0x000003A6 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/StopMarkerDetectionQCOMDelegate::Invoke(System.UInt64)
extern void StopMarkerDetectionQCOMDelegate_Invoke_mF85020950E3A7D379C5EF29450E5ED4A682CBA3E (void);
// 0x000003A7 System.IAsyncResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/StopMarkerDetectionQCOMDelegate::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void StopMarkerDetectionQCOMDelegate_BeginInvoke_mB29B5B90465CE482AB619FD786E4A7F05384DCDB (void);
// 0x000003A8 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/StopMarkerDetectionQCOMDelegate::EndInvoke(System.IAsyncResult)
extern void StopMarkerDetectionQCOMDelegate_EndInvoke_mF65CA723F1F94FBD6F606EAF53EF7C1F65597AC2 (void);
// 0x000003A9 System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/GetMarkerSizeQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void GetMarkerSizeQCOMDelegate__ctor_m0B7CD5E7547341722193159F649E83E59F4F13DF (void);
// 0x000003AA Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/GetMarkerSizeQCOMDelegate::Invoke(System.UInt64,System.IntPtr)
extern void GetMarkerSizeQCOMDelegate_Invoke_m7D4A70A7159A4A720867EE54CAC8CFE63D9B46A6 (void);
// 0x000003AB System.IAsyncResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/GetMarkerSizeQCOMDelegate::BeginInvoke(System.UInt64,System.IntPtr,System.AsyncCallback,System.Object)
extern void GetMarkerSizeQCOMDelegate_BeginInvoke_m8B9465ACC90C001855923C0FD100042B23087D0E (void);
// 0x000003AC Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/GetMarkerSizeQCOMDelegate::EndInvoke(System.IAsyncResult)
extern void GetMarkerSizeQCOMDelegate_EndInvoke_m79ED2D8B2C69A0CE6AF26C292A4473CDF646D819 (void);
// 0x000003AD System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/GetMarkerTypeQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void GetMarkerTypeQCOMDelegate__ctor_m76930CF86F7CCAEE2851DBC3DCB1C6DAA4E9E8C5 (void);
// 0x000003AE Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/GetMarkerTypeQCOMDelegate::Invoke(System.UInt64,System.IntPtr)
extern void GetMarkerTypeQCOMDelegate_Invoke_m67CA41ED6F3BC80E84C70DDDC2B0AAD4970905C5 (void);
// 0x000003AF System.IAsyncResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/GetMarkerTypeQCOMDelegate::BeginInvoke(System.UInt64,System.IntPtr,System.AsyncCallback,System.Object)
extern void GetMarkerTypeQCOMDelegate_BeginInvoke_m3D05CFD7238BC84D4C3C7D8E8C279349ECAE9A4E (void);
// 0x000003B0 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/GetMarkerTypeQCOMDelegate::EndInvoke(System.IAsyncResult)
extern void GetMarkerTypeQCOMDelegate_EndInvoke_m897CB810820B4BFEDBB0B7D10839A6D027E835CD (void);
// 0x000003B1 System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/GetQrCodeVersionQCOM::.ctor(System.Object,System.IntPtr)
extern void GetQrCodeVersionQCOM__ctor_mA9E96F700CC6B35D99A48B45F19DF467D0A4D2F4 (void);
// 0x000003B2 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/GetQrCodeVersionQCOM::Invoke(System.UInt64,System.IntPtr)
extern void GetQrCodeVersionQCOM_Invoke_m7D4BFA32E0F8FA5EE9C3D3E6C664CFDC5CA929AA (void);
// 0x000003B3 System.IAsyncResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/GetQrCodeVersionQCOM::BeginInvoke(System.UInt64,System.IntPtr,System.AsyncCallback,System.Object)
extern void GetQrCodeVersionQCOM_BeginInvoke_m205AD3FA51B877D078554FD2F3BC23658B3E2D8E (void);
// 0x000003B4 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/GetQrCodeVersionQCOM::EndInvoke(System.IAsyncResult)
extern void GetQrCodeVersionQCOM_EndInvoke_mB9F81D7217A7E7951D1FB14C5915C33ECEDD4986 (void);
// 0x000003B5 System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/GetQrCodeStringDataQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void GetQrCodeStringDataQCOMDelegate__ctor_mF6CA1F40518F99C5AF6242A2EF128656E004B14A (void);
// 0x000003B6 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/GetQrCodeStringDataQCOMDelegate::Invoke(System.UInt64,System.UInt32,System.UInt32&,System.IntPtr)
extern void GetQrCodeStringDataQCOMDelegate_Invoke_m52BA55522558196E6073AD17E1B5E749395E9152 (void);
// 0x000003B7 System.IAsyncResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/GetQrCodeStringDataQCOMDelegate::BeginInvoke(System.UInt64,System.UInt32,System.UInt32&,System.IntPtr,System.AsyncCallback,System.Object)
extern void GetQrCodeStringDataQCOMDelegate_BeginInvoke_m803E0F10B045C3EB6A591A1A1F2892CE4EF252FF (void);
// 0x000003B8 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/GetQrCodeStringDataQCOMDelegate::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void GetQrCodeStringDataQCOMDelegate_EndInvoke_mFA5085A8E9CC07F581E7174232CC81A4F1591F3B (void);
// 0x000003B9 System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/GetQrCodeRawDataQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void GetQrCodeRawDataQCOMDelegate__ctor_mC426F0C9FD22F4D6BC88E5F575EB99BD0C81A38C (void);
// 0x000003BA Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/GetQrCodeRawDataQCOMDelegate::Invoke(System.UInt64,System.UInt32,System.UInt32&,System.IntPtr)
extern void GetQrCodeRawDataQCOMDelegate_Invoke_m62A24F75EC46108430BE2C4B6F7C605E1138BC04 (void);
// 0x000003BB System.IAsyncResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/GetQrCodeRawDataQCOMDelegate::BeginInvoke(System.UInt64,System.UInt32,System.UInt32&,System.IntPtr,System.AsyncCallback,System.Object)
extern void GetQrCodeRawDataQCOMDelegate_BeginInvoke_m3CFFE3617542AF17E31085D10251F80C99398E07 (void);
// 0x000003BC Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/GetQrCodeRawDataQCOMDelegate::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void GetQrCodeRawDataQCOMDelegate_EndInvoke_mD7558EB76EC21D5C34053527ECDA1A4C2675BAB6 (void);
// 0x000003BD System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/CreateMarkerSpaceQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void CreateMarkerSpaceQCOMDelegate__ctor_mFE9F38698C20CC329D46738E21B03D9C1491CE31 (void);
// 0x000003BE Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/CreateMarkerSpaceQCOMDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrMarkerSpaceCreateInfoQCOM&,System.UInt64&)
extern void CreateMarkerSpaceQCOMDelegate_Invoke_m437DFC04574805B22011129C1D4E5AA62FF34792 (void);
// 0x000003BF System.IAsyncResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/CreateMarkerSpaceQCOMDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrMarkerSpaceCreateInfoQCOM&,System.UInt64&,System.AsyncCallback,System.Object)
extern void CreateMarkerSpaceQCOMDelegate_BeginInvoke_m5D2AFECE5A692000F51F7670D5FC88B3ADC54DDC (void);
// 0x000003C0 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/CreateMarkerSpaceQCOMDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrMarkerSpaceCreateInfoQCOM&,System.UInt64&,System.IAsyncResult)
extern void CreateMarkerSpaceQCOMDelegate_EndInvoke_m1979F5E2D945ABCFA17B40019FDF0896D3E31FDE (void);
// 0x000003C1 System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/SetMarkerTrackingModeQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void SetMarkerTrackingModeQCOMDelegate__ctor_m7F08952442DBED4310D412AB50C5AC3FBECDD63E (void);
// 0x000003C2 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/SetMarkerTrackingModeQCOMDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrMarkerTrackingModeInfoQCOM&)
extern void SetMarkerTrackingModeQCOMDelegate_Invoke_m81EFBED5E89CB0B1F54476256E6052B4D88B6E44 (void);
// 0x000003C3 System.IAsyncResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/SetMarkerTrackingModeQCOMDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrMarkerTrackingModeInfoQCOM&,System.AsyncCallback,System.Object)
extern void SetMarkerTrackingModeQCOMDelegate_BeginInvoke_m5679AA5A9CB3A57EB900E835EF197E5736EC037D (void);
// 0x000003C4 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/SetMarkerTrackingModeQCOMDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrMarkerTrackingModeInfoQCOM&,System.IAsyncResult)
extern void SetMarkerTrackingModeQCOMDelegate_EndInvoke_mC368D22CD5777E463BD5FFC675A890DFC68E064D (void);
// 0x000003C5 System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/PollMarkerUpdateQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void PollMarkerUpdateQCOMDelegate__ctor_m438325B413BB239C41A23E65799B1CAFFB2B4342 (void);
// 0x000003C6 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/PollMarkerUpdateQCOMDelegate::Invoke(System.UInt64,System.UInt64&)
extern void PollMarkerUpdateQCOMDelegate_Invoke_mD65FBF9DE98B81FF16BDE1612CFB836FB4CB9822 (void);
// 0x000003C7 System.IAsyncResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/PollMarkerUpdateQCOMDelegate::BeginInvoke(System.UInt64,System.UInt64&,System.AsyncCallback,System.Object)
extern void PollMarkerUpdateQCOMDelegate_BeginInvoke_m5794B16DE24D8957C74052857ADD2544477E51B2 (void);
// 0x000003C8 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/PollMarkerUpdateQCOMDelegate::EndInvoke(System.UInt64&,System.IAsyncResult)
extern void PollMarkerUpdateQCOMDelegate_EndInvoke_mEB471989367D06F090C793FE02829B6CE3A7FEA8 (void);
// 0x000003C9 System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/GetMarkerUpdateInfoQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void GetMarkerUpdateInfoQCOMDelegate__ctor_mF996B4699C3095DF833696F6A20D5FADB713B829 (void);
// 0x000003CA Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/GetMarkerUpdateInfoQCOMDelegate::Invoke(System.UInt64,System.UInt32&,System.IntPtr&)
extern void GetMarkerUpdateInfoQCOMDelegate_Invoke_m28EFB1B76950E5F3837697D1BD5DEF5E426E5997 (void);
// 0x000003CB System.IAsyncResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/GetMarkerUpdateInfoQCOMDelegate::BeginInvoke(System.UInt64,System.UInt32&,System.IntPtr&,System.AsyncCallback,System.Object)
extern void GetMarkerUpdateInfoQCOMDelegate_BeginInvoke_m578F7BBC4ABF7409227CB3B3828131115B862BD1 (void);
// 0x000003CC Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/GetMarkerUpdateInfoQCOMDelegate::EndInvoke(System.UInt32&,System.IntPtr&,System.IAsyncResult)
extern void GetMarkerUpdateInfoQCOMDelegate_EndInvoke_mA73AA785E6CD06559C98D48925C997216D0F9AF1 (void);
// 0x000003CD System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/ReleaseMarkerUpdateQCOMDelegate::.ctor(System.Object,System.IntPtr)
extern void ReleaseMarkerUpdateQCOMDelegate__ctor_m9AB1E51E6B8CAACD5B93B358C04CDA8017522CC1 (void);
// 0x000003CE Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/ReleaseMarkerUpdateQCOMDelegate::Invoke(System.UInt64)
extern void ReleaseMarkerUpdateQCOMDelegate_Invoke_m81BE71F966856B4B14F6FF6216E135D8A2B830AF (void);
// 0x000003CF System.IAsyncResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/ReleaseMarkerUpdateQCOMDelegate::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void ReleaseMarkerUpdateQCOMDelegate_BeginInvoke_mF0873A656BCDFA60C1C3BBE616E3C3E86C65DC2F (void);
// 0x000003D0 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/ReleaseMarkerUpdateQCOMDelegate::EndInvoke(System.IAsyncResult)
extern void ReleaseMarkerUpdateQCOMDelegate_EndInvoke_m2BBE2345BBFE7093133D0097626A9D5CDA45292D (void);
// 0x000003D1 System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/LocateSpaceDelegate::.ctor(System.Object,System.IntPtr)
extern void LocateSpaceDelegate__ctor_m1651232A134609E119DB1FABE5BC77BEA5A8EFFC (void);
// 0x000003D2 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/LocateSpaceDelegate::Invoke(System.UInt64,System.UInt64,System.Int64,Qualcomm.Snapdragon.Spaces.XrSpaceLocation&)
extern void LocateSpaceDelegate_Invoke_mD1B6F693B1A3B87774CA7291F7E9860FF79F7885 (void);
// 0x000003D3 System.IAsyncResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/LocateSpaceDelegate::BeginInvoke(System.UInt64,System.UInt64,System.Int64,Qualcomm.Snapdragon.Spaces.XrSpaceLocation&,System.AsyncCallback,System.Object)
extern void LocateSpaceDelegate_BeginInvoke_m6298CE38560351F1E02B621A9304F33ACB5BD8DA (void);
// 0x000003D4 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/LocateSpaceDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrSpaceLocation&,System.IAsyncResult)
extern void LocateSpaceDelegate_EndInvoke_mE990D62EC52CFD9F5A0010833F3B480008D2A260 (void);
// 0x000003D5 System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/DestroySpaceDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroySpaceDelegate__ctor_mF59D96B6E0832B8FD26F19707727FA043F83295D (void);
// 0x000003D6 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/DestroySpaceDelegate::Invoke(System.UInt64)
extern void DestroySpaceDelegate_Invoke_m10F1B60B1DFEC5820A2B232D516A000771C9DCCB (void);
// 0x000003D7 System.IAsyncResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/DestroySpaceDelegate::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void DestroySpaceDelegate_BeginInvoke_mCB3A02EA52C5CE8B1AADFE819C2C898490BF8044 (void);
// 0x000003D8 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.QrCodeTrackingFeature/DestroySpaceDelegate::EndInvoke(System.IAsyncResult)
extern void DestroySpaceDelegate_EndInvoke_m5A679D1B62605290F85FD9267EAFD5CD96B43F77 (void);
// 0x000003D9 System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingSubsystem::RegisterDescriptor()
extern void QrCodeTrackingSubsystem_RegisterDescriptor_mB3AF76447008213FAA576D874DDDE982B5938880 (void);
// 0x000003DA System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingSubsystem::.ctor()
extern void QrCodeTrackingSubsystem__ctor_mB4DCE13EEBF1555933AF83C24E93FA02AFA6486D (void);
// 0x000003DB System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingSubsystem/QrCodeTrackingProvider::Start()
extern void QrCodeTrackingProvider_Start_m75B1DD9BA4BEFA0E374EE25426664B7D3E0406D6 (void);
// 0x000003DC System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingSubsystem/QrCodeTrackingProvider::Stop()
extern void QrCodeTrackingProvider_Stop_m6CE044E36B15903A7670B8E88A657CE732ECF048 (void);
// 0x000003DD System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingSubsystem/QrCodeTrackingProvider::Destroy()
extern void QrCodeTrackingProvider_Destroy_mD3C7B824C7473A5B0F12ECF5BBBCC6A108404E07 (void);
// 0x000003DE UnityEngine.XR.ARSubsystems.TrackableChanges`1<Qualcomm.Snapdragon.Spaces.XRTrackedMarker> Qualcomm.Snapdragon.Spaces.QrCodeTrackingSubsystem/QrCodeTrackingProvider::GetChanges(Qualcomm.Snapdragon.Spaces.XRTrackedMarker,Unity.Collections.Allocator)
extern void QrCodeTrackingProvider_GetChanges_mDCEDD7EBBA9CD342DD1AC58293E60C02653FEEE4 (void);
// 0x000003DF System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingSubsystem/QrCodeTrackingProvider::SetMarkerDescriptor(Qualcomm.Snapdragon.Spaces.XRMarkerDescriptor)
extern void QrCodeTrackingProvider_SetMarkerDescriptor_m4649778F980424CD91609E2DC6D1FD4CD98B77B6 (void);
// 0x000003E0 System.Boolean Qualcomm.Snapdragon.Spaces.QrCodeTrackingSubsystem/QrCodeTrackingProvider::TryGetMarkerData(UnityEngine.XR.ARSubsystems.TrackableId,System.String&)
extern void QrCodeTrackingProvider_TryGetMarkerData_m923DF41D1D6D5349B2240FC5D61DC674D72FEE44 (void);
// 0x000003E1 System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingSubsystem/QrCodeTrackingProvider::EnableMarkerTracking(System.Boolean)
extern void QrCodeTrackingProvider_EnableMarkerTracking_m525A50BEC6F3369A725B32E3554A6C76AB0AEAE0 (void);
// 0x000003E2 System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingSubsystem/QrCodeTrackingProvider::.ctor()
extern void QrCodeTrackingProvider__ctor_m5F432B96102EFC12B58C374C3C900D466662E495 (void);
// 0x000003E3 System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingSubsystem/QrCodeTrackingProvider/SpacesMarker::.ctor(System.UInt64,System.UInt64)
extern void SpacesMarker__ctor_mC89B5CE6EB7669D170C5F200845D56A477AF2810 (void);
// 0x000003E4 System.Void Qualcomm.Snapdragon.Spaces.QrCodeTrackingSubsystem/QrCodeTrackingProvider/SpacesMarker::UpdatePoseAndTrackingState(System.Tuple`2<UnityEngine.Pose,UnityEngine.XR.ARSubsystems.TrackingState>)
extern void SpacesMarker_UpdatePoseAndTrackingState_m0811C38B01B8D1324AAC1677912A6584D86BC730 (void);
// 0x000003E5 Qualcomm.Snapdragon.Spaces.MarkerTrackingMode Qualcomm.Snapdragon.Spaces.XRMarkerDescriptor::get_TrackingMode()
extern void XRMarkerDescriptor_get_TrackingMode_mD80E7A2DFE0BB1FAAE9FDDDF022F9B811208C941 (void);
// 0x000003E6 UnityEngine.Vector2 Qualcomm.Snapdragon.Spaces.XRMarkerDescriptor::get_Size()
extern void XRMarkerDescriptor_get_Size_m8413EA768805484DAFA6ABC04F8AA37B9F858DC5 (void);
// 0x000003E7 System.Tuple`2<System.Byte,System.Byte> Qualcomm.Snapdragon.Spaces.XRMarkerDescriptor::get_QrCodeVersionRange()
extern void XRMarkerDescriptor_get_QrCodeVersionRange_m95B6C333F887917DE81679CF8AA95BB76FE9C0F7 (void);
// 0x000003E8 System.Void Qualcomm.Snapdragon.Spaces.XRMarkerDescriptor::.ctor(Qualcomm.Snapdragon.Spaces.MarkerTrackingMode,UnityEngine.Vector2,System.Tuple`2<System.Byte,System.Byte>)
extern void XRMarkerDescriptor__ctor_m5527F572D3A297CB5FA443715835F6C37DB54D41 (void);
// 0x000003E9 Qualcomm.Snapdragon.Spaces.XRMarkerDescriptor Qualcomm.Snapdragon.Spaces.XRMarkerDescriptor::get_DefaultDescriptor()
extern void XRMarkerDescriptor_get_DefaultDescriptor_m18D15CDA431C993F18A405BEB650DFAFF48B7822 (void);
// 0x000003EA System.Boolean Qualcomm.Snapdragon.Spaces.XRMarkerDescriptor::Equals(Qualcomm.Snapdragon.Spaces.XRMarkerDescriptor)
extern void XRMarkerDescriptor_Equals_m49C49CB847600ED9EAA595C48D099EF60EB1A43B (void);
// 0x000003EB System.Int32 Qualcomm.Snapdragon.Spaces.XRMarkerDescriptor::GetHashCode()
extern void XRMarkerDescriptor_GetHashCode_m89653348B8764C2FC89570E8871EC6D325948A48 (void);
// 0x000003EC System.Void Qualcomm.Snapdragon.Spaces.XRMarkerDescriptor::.cctor()
extern void XRMarkerDescriptor__cctor_m91CA206B9B9B2833C1B0D00B578A24DE4D3EE12A (void);
// 0x000003ED UnityEngine.XR.ARSubsystems.TrackableChanges`1<Qualcomm.Snapdragon.Spaces.XRTrackedMarker> Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystem::GetChanges(Unity.Collections.Allocator)
extern void XRQrCodeTrackingSubsystem_GetChanges_mD60AC55BF7EC64500D9C6872965C5BEC3140E8C3 (void);
// 0x000003EE System.Void Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystem::SetMarkerDescriptor(Qualcomm.Snapdragon.Spaces.XRMarkerDescriptor)
extern void XRQrCodeTrackingSubsystem_SetMarkerDescriptor_m69A403D5FF7284A70C06A66656EFD46AB034B1B6 (void);
// 0x000003EF System.Boolean Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystem::TryGetMarkerData(UnityEngine.XR.ARSubsystems.TrackableId,System.String&)
extern void XRQrCodeTrackingSubsystem_TryGetMarkerData_m9D2D6E2A1A75F29F14679F49151CD0BE9006FD01 (void);
// 0x000003F0 System.Void Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystem::EnableMarkerTracking(System.Boolean)
extern void XRQrCodeTrackingSubsystem_EnableMarkerTracking_mEC7DC7B9177D936D2EC9F57C4257295CFCDFF702 (void);
// 0x000003F1 System.Void Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystem::.ctor()
extern void XRQrCodeTrackingSubsystem__ctor_m37A83C006A7240E1F42CA493C288CA153A914A5B (void);
// 0x000003F2 UnityEngine.XR.ARSubsystems.TrackableChanges`1<Qualcomm.Snapdragon.Spaces.XRTrackedMarker> Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystem/Provider::GetChanges(Qualcomm.Snapdragon.Spaces.XRTrackedMarker,Unity.Collections.Allocator)
// 0x000003F3 System.Void Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystem/Provider::SetMarkerDescriptor(Qualcomm.Snapdragon.Spaces.XRMarkerDescriptor)
// 0x000003F4 System.Boolean Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystem/Provider::TryGetMarkerData(UnityEngine.XR.ARSubsystems.TrackableId,System.String&)
// 0x000003F5 System.Void Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystem/Provider::EnableMarkerTracking(System.Boolean)
// 0x000003F6 System.Void Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystem/Provider::.ctor()
extern void Provider__ctor_mDA8CA8C38F7F9B3617142D533A2147E1B99D967A (void);
// 0x000003F7 System.Void Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystemDescriptor::.ctor(Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystemDescriptor/Cinfo)
extern void XRQrCodeTrackingSubsystemDescriptor__ctor_mF5D0326303F91C06868B6E7A17053CD2C36DA546 (void);
// 0x000003F8 System.Void Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystemDescriptor::Create(Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystemDescriptor/Cinfo)
extern void XRQrCodeTrackingSubsystemDescriptor_Create_m2B1E6A3415956725068AD0C7DFD2427EDE403C84 (void);
// 0x000003F9 System.String Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystemDescriptor/Cinfo::get_id()
extern void Cinfo_get_id_mEB29904AA813FB8BD587DCD994F8AFC20C99B742 (void);
// 0x000003FA System.Void Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystemDescriptor/Cinfo::set_id(System.String)
extern void Cinfo_set_id_mE0C00BE3307B5E2BBE1E140CC20094040EE1B811 (void);
// 0x000003FB System.Type Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystemDescriptor/Cinfo::get_providerType()
extern void Cinfo_get_providerType_m5A559C875D7CD9A293A377E656786D28511F08AF (void);
// 0x000003FC System.Void Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystemDescriptor/Cinfo::set_providerType(System.Type)
extern void Cinfo_set_providerType_m3164B2DB59BAAF4BC8160BD6F4B8A466C5F5F7A6 (void);
// 0x000003FD System.Type Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystemDescriptor/Cinfo::get_subsystemTypeOverride()
extern void Cinfo_get_subsystemTypeOverride_m719F057762950FBB9D4A5A858D7A17E00A5A5EDC (void);
// 0x000003FE System.Void Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystemDescriptor/Cinfo::set_subsystemTypeOverride(System.Type)
extern void Cinfo_set_subsystemTypeOverride_m327467C849A070DE62F102D88CFD204B89B26BEF (void);
// 0x000003FF System.Int32 Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystemDescriptor/Cinfo::GetHashCode()
extern void Cinfo_GetHashCode_mDCB386966FE8B59139CDFA71F7F0FBE3FC8DD2FC (void);
// 0x00000400 System.Boolean Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystemDescriptor/Cinfo::Equals(Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystemDescriptor/Cinfo)
extern void Cinfo_Equals_mE2F04B1A09111E4AA11D421E16C5789D222EC756 (void);
// 0x00000401 System.Boolean Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystemDescriptor/Cinfo::Equals(System.Object)
extern void Cinfo_Equals_m987CEE46F6C2B0E1C63883BB4FA09AFEC38C172D (void);
// 0x00000402 System.Boolean Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystemDescriptor/Cinfo::op_Equality(Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystemDescriptor/Cinfo,Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystemDescriptor/Cinfo)
extern void Cinfo_op_Equality_m41D9D625FD40323F89C8D639C1A4755EF364F81E (void);
// 0x00000403 System.Boolean Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystemDescriptor/Cinfo::op_Inequality(Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystemDescriptor/Cinfo,Qualcomm.Snapdragon.Spaces.XRQrCodeTrackingSubsystemDescriptor/Cinfo)
extern void Cinfo_op_Inequality_m5F0D88D8702E816FBC07176D75A4F03121477B0C (void);
// 0x00000404 UnityEngine.XR.ARSubsystems.TrackableId Qualcomm.Snapdragon.Spaces.XRTrackedMarker::get_trackableId()
extern void XRTrackedMarker_get_trackableId_m2FA500913E2999A7E28AC1AE311EB83A326C4B70 (void);
// 0x00000405 UnityEngine.Pose Qualcomm.Snapdragon.Spaces.XRTrackedMarker::get_pose()
extern void XRTrackedMarker_get_pose_m7F044500815660018AC12237A0F1CC64CD31F88F (void);
// 0x00000406 UnityEngine.XR.ARSubsystems.TrackingState Qualcomm.Snapdragon.Spaces.XRTrackedMarker::get_trackingState()
extern void XRTrackedMarker_get_trackingState_m9A85DE39F1DF84FD53FFD8F0113CF169C56BEFFE (void);
// 0x00000407 System.IntPtr Qualcomm.Snapdragon.Spaces.XRTrackedMarker::get_nativePtr()
extern void XRTrackedMarker_get_nativePtr_m016CB37BEC30269FE8D4D4FE1D7043C8EE195AEF (void);
// 0x00000408 System.Void Qualcomm.Snapdragon.Spaces.XRTrackedMarker::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.TrackingState,System.IntPtr)
extern void XRTrackedMarker__ctor_mB4EC0B012C7E2AA4D83DCBB711778354E51808AE (void);
// 0x00000409 Qualcomm.Snapdragon.Spaces.XRTrackedMarker Qualcomm.Snapdragon.Spaces.XRTrackedMarker::get_defaultValue()
extern void XRTrackedMarker_get_defaultValue_m9E7E54AC81848FF7425A1F0A93A5941679F8744D (void);
// 0x0000040A System.Int32 Qualcomm.Snapdragon.Spaces.XRTrackedMarker::GetHashCode()
extern void XRTrackedMarker_GetHashCode_m7F8CB1B3555F5D10ADD12F61900A75816000D52B (void);
// 0x0000040B System.Boolean Qualcomm.Snapdragon.Spaces.XRTrackedMarker::Equals(Qualcomm.Snapdragon.Spaces.XRTrackedMarker)
extern void XRTrackedMarker_Equals_mBF2E6DE4766E703265DDD427F0AC223D24855AEE (void);
// 0x0000040C System.Boolean Qualcomm.Snapdragon.Spaces.XRTrackedMarker::Equals(System.Object)
extern void XRTrackedMarker_Equals_m4F74FFE82E700A097FDB1C1787B53180BF6422EA (void);
// 0x0000040D System.Boolean Qualcomm.Snapdragon.Spaces.XRTrackedMarker::op_Equality(Qualcomm.Snapdragon.Spaces.XRTrackedMarker,Qualcomm.Snapdragon.Spaces.XRTrackedMarker)
extern void XRTrackedMarker_op_Equality_m854EB73FF3C73AD86107BCC5ACC28127826E7D74 (void);
// 0x0000040E System.Boolean Qualcomm.Snapdragon.Spaces.XRTrackedMarker::op_Inequality(Qualcomm.Snapdragon.Spaces.XRTrackedMarker,Qualcomm.Snapdragon.Spaces.XRTrackedMarker)
extern void XRTrackedMarker_op_Inequality_m75E1E5BF3A9C263407857A19CE69CC9345BF3156 (void);
// 0x0000040F System.Void Qualcomm.Snapdragon.Spaces.XRTrackedMarker::.cctor()
extern void XRTrackedMarker__cctor_m7376CECE950C340551EC3A2E37849051D944BED0 (void);
// 0x00000410 System.Void Qualcomm.Snapdragon.Spaces.SpacesAnchorStore::Awake()
extern void SpacesAnchorStore_Awake_m21E040879F16C1478E6155F154E767DA1DAEBD4F (void);
// 0x00000411 System.Void Qualcomm.Snapdragon.Spaces.SpacesAnchorStore::OnEnable()
extern void SpacesAnchorStore_OnEnable_m595445F25623F97E9D5DC16DD444C5D5908CE57A (void);
// 0x00000412 System.Void Qualcomm.Snapdragon.Spaces.SpacesAnchorStore::OnDisable()
extern void SpacesAnchorStore_OnDisable_mFEF60FBC5569901F1B483E045402207F5E660604 (void);
// 0x00000413 System.Void Qualcomm.Snapdragon.Spaces.SpacesAnchorStore::Update()
extern void SpacesAnchorStore_Update_m01E7D6F42B463722C1FADC24B9BACE93FAB80E96 (void);
// 0x00000414 System.Void Qualcomm.Snapdragon.Spaces.SpacesAnchorStore::SaveAnchorsWorker()
extern void SpacesAnchorStore_SaveAnchorsWorker_mED18C38FFED3140BDD3D847B6A92EBA24DA5C901 (void);
// 0x00000415 System.Void Qualcomm.Snapdragon.Spaces.SpacesAnchorStore::LoadAnchorsWorker()
extern void SpacesAnchorStore_LoadAnchorsWorker_m7964D88933310434F330FD011E3FD1A8BA23AE6F (void);
// 0x00000416 System.Void Qualcomm.Snapdragon.Spaces.SpacesAnchorStore::SaveAnchor(UnityEngine.XR.ARFoundation.ARAnchor,System.String,System.Action`1<System.Boolean>)
extern void SpacesAnchorStore_SaveAnchor_mA318437CDC8B7BF2B010BEB009BEB58DC99F2557 (void);
// 0x00000417 System.Void Qualcomm.Snapdragon.Spaces.SpacesAnchorStore::SaveAnchor(UnityEngine.XR.ARFoundation.ARAnchor,System.Action`1<System.Boolean>)
extern void SpacesAnchorStore_SaveAnchor_mEB5CAF37BDDC441EDB7CC4A8705987174BBD2ACB (void);
// 0x00000418 System.Void Qualcomm.Snapdragon.Spaces.SpacesAnchorStore::SaveAnchorWithResult(UnityEngine.XR.ARFoundation.ARAnchor,System.String,System.Action`1<Qualcomm.Snapdragon.Spaces.SpacesAnchorStore/SaveAnchorResult>)
extern void SpacesAnchorStore_SaveAnchorWithResult_m8D8C1FDC98A6BC8DD4E414EA0775FDE9714696BB (void);
// 0x00000419 System.Void Qualcomm.Snapdragon.Spaces.SpacesAnchorStore::SaveAnchorWithResult(UnityEngine.XR.ARFoundation.ARAnchor,System.Action`1<Qualcomm.Snapdragon.Spaces.SpacesAnchorStore/SaveAnchorResult>)
extern void SpacesAnchorStore_SaveAnchorWithResult_m23A04C2D852B4FC7F5E3A89250306BBB77F7F3BB (void);
// 0x0000041A System.Boolean Qualcomm.Snapdragon.Spaces.SpacesAnchorStore::LoadSavedAnchor(System.String)
extern void SpacesAnchorStore_LoadSavedAnchor_m88B070A69A96D9B9A21EF992BC09C196C6FF4851 (void);
// 0x0000041B System.Void Qualcomm.Snapdragon.Spaces.SpacesAnchorStore::LoadSavedAnchor(System.String,System.Action`1<System.Boolean>)
extern void SpacesAnchorStore_LoadSavedAnchor_m5D981A8880C785E09E57A89CDA747A93849FDE1D (void);
// 0x0000041C System.Void Qualcomm.Snapdragon.Spaces.SpacesAnchorStore::LoadAllSavedAnchors(System.Action`1<System.Boolean>)
extern void SpacesAnchorStore_LoadAllSavedAnchors_m06CACF8FDD130B02A9212FA75BCAA1DF594EC1CC (void);
// 0x0000041D System.Void Qualcomm.Snapdragon.Spaces.SpacesAnchorStore::DeleteSavedAnchor(System.String)
extern void SpacesAnchorStore_DeleteSavedAnchor_m814A7ABD19B50A6B7946AC248C27EE428AC874C0 (void);
// 0x0000041E System.Void Qualcomm.Snapdragon.Spaces.SpacesAnchorStore::ClearStore()
extern void SpacesAnchorStore_ClearStore_m41494FDA4367B14DB783792FD8FA571E987B29BE (void);
// 0x0000041F System.String[] Qualcomm.Snapdragon.Spaces.SpacesAnchorStore::GetSavedAnchorNames()
extern void SpacesAnchorStore_GetSavedAnchorNames_mDF6A8348BC71BE9C4F60B6E3FFB24BDF7CE168F4 (void);
// 0x00000420 System.String Qualcomm.Snapdragon.Spaces.SpacesAnchorStore::GetSavedAnchorNameFromARAnchor(UnityEngine.XR.ARFoundation.ARAnchor)
extern void SpacesAnchorStore_GetSavedAnchorNameFromARAnchor_m004B1635DE15C4253A230E0F22E14A855699A054 (void);
// 0x00000421 System.Void Qualcomm.Snapdragon.Spaces.SpacesAnchorStore::.ctor()
extern void SpacesAnchorStore__ctor_m0BFD7AF87D342C45A0F96FAA6BFF9174EF6C61F6 (void);
// 0x00000422 System.Void Qualcomm.Snapdragon.Spaces.SpacesAnchorStore/SaveAnchorData::.ctor(System.String,UnityEngine.XR.ARFoundation.ARAnchor,System.Action`1<Qualcomm.Snapdragon.Spaces.SpacesAnchorStore/SaveAnchorResult>)
extern void SaveAnchorData__ctor_mF16AF39431809253F45CECDD11FE45166BA80067 (void);
// 0x00000423 System.Void Qualcomm.Snapdragon.Spaces.SpacesAnchorStore/LoadAnchorData::.ctor(System.String,System.Action`1<System.Boolean>)
extern void LoadAnchorData__ctor_mCA956DC558E2139C8D449DA3AE7E09568CA921F1 (void);
// 0x00000424 System.Void Qualcomm.Snapdragon.Spaces.SpacesAnchorStore/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_mE2B8DA570B37672EF6E0A559A7CDAA71AB789DE3 (void);
// 0x00000425 System.Void Qualcomm.Snapdragon.Spaces.SpacesAnchorStore/<>c__DisplayClass21_0::<SaveAnchor>b__0(Qualcomm.Snapdragon.Spaces.SpacesAnchorStore/SaveAnchorResult)
extern void U3CU3Ec__DisplayClass21_0_U3CSaveAnchorU3Eb__0_m8FB292C30096241A68AE0AD2D54DB2D68452EA5E (void);
// 0x00000426 System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchor::.ctor(UnityEngine.XR.ARSubsystems.TrackableId,System.UInt64,System.UInt64,UnityEngine.Pose,System.String)
extern void SpatialAnchor__ctor_m89B5BFA2F7EA61C84F97394772CBE0525C8EF8CB (void);
// 0x00000427 System.Boolean Qualcomm.Snapdragon.Spaces.SpatialAnchor::UpdateSubsystemAnchorPoseAndTrackingState(System.Tuple`2<UnityEngine.Pose,UnityEngine.XR.ARSubsystems.TrackingState>)
extern void SpatialAnchor_UpdateSubsystemAnchorPoseAndTrackingState_mDD00EA979ED3C6DAB1140CAD072BCD22BADA7C29 (void);
// 0x00000428 System.Boolean Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature::get_IsRequiringBaseRuntimeFeature()
extern void SpatialAnchorsFeature_get_IsRequiringBaseRuntimeFeature_mAF0EA26323F10AF6D2AAA0530A5BA1D21F0E21B4 (void);
// 0x00000429 System.UInt64 Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature::TryCreateSpatialAnchorHandle(UnityEngine.Pose)
extern void SpatialAnchorsFeature_TryCreateSpatialAnchorHandle_m4685D97C85E540F32B9B33712A19B3C4543AD359 (void);
// 0x0000042A System.UInt64 Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature::TryCreateSpatialAnchorSpaceHandle(System.UInt64)
extern void SpatialAnchorsFeature_TryCreateSpatialAnchorSpaceHandle_mED98DCE76E5721F42ACE282B5E59F6B190A7B5F6 (void);
// 0x0000042B System.Boolean Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature::TryDestroySpatialAnchor(System.UInt64)
extern void SpatialAnchorsFeature_TryDestroySpatialAnchor_mB19090DFCA8F13FFD7710A751387F959BC0D5C67 (void);
// 0x0000042C System.Tuple`2<UnityEngine.Pose,UnityEngine.XR.ARSubsystems.TrackingState> Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature::TryGetSpatialAnchorSpacePoseAndTrackingState(System.UInt64)
extern void SpatialAnchorsFeature_TryGetSpatialAnchorSpacePoseAndTrackingState_mD664863791E7F1A5813CF9EEA1E9D7868A9118B6 (void);
// 0x0000042D System.Boolean Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature::TryCreateSpatialAnchorStoreConnection(System.UInt64&)
extern void SpatialAnchorsFeature_TryCreateSpatialAnchorStoreConnection_m8B05FD91A3DAF31A4F95A89E125CDA8EC908BF32 (void);
// 0x0000042E System.Boolean Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature::TryDestroySpatialAnchorStoreConnection(System.UInt64)
extern void SpatialAnchorsFeature_TryDestroySpatialAnchorStoreConnection_m4BEC6B257130BC4C2B76859D043C7AA10CE5F8C3 (void);
// 0x0000042F System.Boolean Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature::TryClearSpatialAnchorStoreMSFT(System.UInt64)
extern void SpatialAnchorsFeature_TryClearSpatialAnchorStoreMSFT_mE2CB7E2C660A56E601C93EEBA9E86E3C6C34D656 (void);
// 0x00000430 System.Boolean Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature::TryPersistSpatialAnchor(System.UInt64,System.UInt64,System.String,Qualcomm.Snapdragon.Spaces.SpacesAnchorStore/SaveAnchorResult&)
extern void SpatialAnchorsFeature_TryPersistSpatialAnchor_mC3405D6C3B2A2A8E534E48B70BA846C2A6F4AAC9 (void);
// 0x00000431 System.Boolean Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature::TryUnpersistSpatialAnchor(System.UInt64,System.String)
extern void SpatialAnchorsFeature_TryUnpersistSpatialAnchor_m8D4D1AD7BF28030DC6193DBAC13C5DD5105967C5 (void);
// 0x00000432 System.Boolean Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature::TryCreateSpatialAnchorFromPersistedNameMSFT(System.UInt64,System.String,System.UInt64&)
extern void SpatialAnchorsFeature_TryCreateSpatialAnchorFromPersistedNameMSFT_m564DFAA8B0802B284872BAC1B37B4C7AD060C73B (void);
// 0x00000433 System.Boolean Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature::TryEnumeratePersistedSpatialAnchorNames(System.UInt64,System.String[]&)
extern void SpatialAnchorsFeature_TryEnumeratePersistedSpatialAnchorNames_m65120244410EBC374EAFE68DD7FC0BE692A44DD3 (void);
// 0x00000434 System.String Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature::GetXrLayersToLoad()
extern void SpatialAnchorsFeature_GetXrLayersToLoad_m8604DA06AE0184EBE94C29E871C996E7D85F2C0B (void);
// 0x00000435 System.Boolean Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature::OnInstanceCreate(System.UInt64)
extern void SpatialAnchorsFeature_OnInstanceCreate_m1965A9C370CC7E834B7B470F50115328CA4E072A (void);
// 0x00000436 System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature::OnSubsystemCreate()
extern void SpatialAnchorsFeature_OnSubsystemCreate_m279245F1D49560839F61A53CD97A7C67C220FB7F (void);
// 0x00000437 System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature::OnSubsystemStop()
extern void SpatialAnchorsFeature_OnSubsystemStop_m46BD0E199567738406AF385CC5B00E8259CB6239 (void);
// 0x00000438 System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature::OnSubsystemDestroy()
extern void SpatialAnchorsFeature_OnSubsystemDestroy_m6D0EBDB38708E3C11B5250D5E8FD9147C45B10AF (void);
// 0x00000439 System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature::OnHookMethods()
extern void SpatialAnchorsFeature_OnHookMethods_m9A6036B77D73B75CFD691DD52CD72070CBCC5F45 (void);
// 0x0000043A System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature::.ctor()
extern void SpatialAnchorsFeature__ctor_m5C5B37C43F1CFBCAB96944961B62806DB1478C64 (void);
// 0x0000043B System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature::.cctor()
extern void SpatialAnchorsFeature__cctor_mEA98ED48B34FE6A2289DCDE8E4ADB6D4911DD35E (void);
// 0x0000043C System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/CreateSpatialAnchorMSFTDelegate::.ctor(System.Object,System.IntPtr)
extern void CreateSpatialAnchorMSFTDelegate__ctor_mF691172F5B23677D9490298005F408C139DA9ED7 (void);
// 0x0000043D Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/CreateSpatialAnchorMSFTDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrSpatialAnchorCreateInfoMSFT,System.UInt64&)
extern void CreateSpatialAnchorMSFTDelegate_Invoke_m360D2AD6BC6A9FCD2DBE1323F109BB0A392647B0 (void);
// 0x0000043E System.IAsyncResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/CreateSpatialAnchorMSFTDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrSpatialAnchorCreateInfoMSFT,System.UInt64&,System.AsyncCallback,System.Object)
extern void CreateSpatialAnchorMSFTDelegate_BeginInvoke_m1C7B3461BB6990C29739E57A9AE4692F035A3496 (void);
// 0x0000043F Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/CreateSpatialAnchorMSFTDelegate::EndInvoke(System.UInt64&,System.IAsyncResult)
extern void CreateSpatialAnchorMSFTDelegate_EndInvoke_m1C2A701467D24A8580907B36250536DB370F4670 (void);
// 0x00000440 System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/CreateSpatialAnchorSpaceMSFTDelegate::.ctor(System.Object,System.IntPtr)
extern void CreateSpatialAnchorSpaceMSFTDelegate__ctor_m36F40E5A4543496C5D3481973CDC1878BB75305A (void);
// 0x00000441 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/CreateSpatialAnchorSpaceMSFTDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrSpatialAnchorSpaceCreateInfoMSFT,System.UInt64&)
extern void CreateSpatialAnchorSpaceMSFTDelegate_Invoke_m83C7DD3FD8A7B5167C7C927E20E644E323CCAF0B (void);
// 0x00000442 System.IAsyncResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/CreateSpatialAnchorSpaceMSFTDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrSpatialAnchorSpaceCreateInfoMSFT,System.UInt64&,System.AsyncCallback,System.Object)
extern void CreateSpatialAnchorSpaceMSFTDelegate_BeginInvoke_m0E2845D2F498D296941D6101E20B4F6A89BE65D0 (void);
// 0x00000443 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/CreateSpatialAnchorSpaceMSFTDelegate::EndInvoke(System.UInt64&,System.IAsyncResult)
extern void CreateSpatialAnchorSpaceMSFTDelegate_EndInvoke_m05E5BB2179A08ED8A2454F3571FFB7BED6AD154E (void);
// 0x00000444 System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/DestroySpatialAnchorMSFTDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroySpatialAnchorMSFTDelegate__ctor_m439B1234EC35634A19BE7A7257B679DC73658BD9 (void);
// 0x00000445 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/DestroySpatialAnchorMSFTDelegate::Invoke(System.UInt64)
extern void DestroySpatialAnchorMSFTDelegate_Invoke_mD43C523B6B300A640A89403B0740723BF92F7579 (void);
// 0x00000446 System.IAsyncResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/DestroySpatialAnchorMSFTDelegate::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void DestroySpatialAnchorMSFTDelegate_BeginInvoke_mE6F9BF75F9DD2E9AA5EEA78B26560C5660F444D6 (void);
// 0x00000447 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/DestroySpatialAnchorMSFTDelegate::EndInvoke(System.IAsyncResult)
extern void DestroySpatialAnchorMSFTDelegate_EndInvoke_m0448013075FE053F62D3785B285F7CAAA4C6FE46 (void);
// 0x00000448 System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/CreateSpatialAnchorStoreConnectionMSFTDelegate::.ctor(System.Object,System.IntPtr)
extern void CreateSpatialAnchorStoreConnectionMSFTDelegate__ctor_m9E39505763F871F9AC0AF22C21E715FD84FCB73B (void);
// 0x00000449 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/CreateSpatialAnchorStoreConnectionMSFTDelegate::Invoke(System.UInt64,System.UInt64&)
extern void CreateSpatialAnchorStoreConnectionMSFTDelegate_Invoke_m968E41F34BB96E29AFDA75A7F60C001C8768FB7A (void);
// 0x0000044A System.IAsyncResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/CreateSpatialAnchorStoreConnectionMSFTDelegate::BeginInvoke(System.UInt64,System.UInt64&,System.AsyncCallback,System.Object)
extern void CreateSpatialAnchorStoreConnectionMSFTDelegate_BeginInvoke_mA966E548D9865DA0AF8F745C12AC3A2DD2C1BB50 (void);
// 0x0000044B Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/CreateSpatialAnchorStoreConnectionMSFTDelegate::EndInvoke(System.UInt64&,System.IAsyncResult)
extern void CreateSpatialAnchorStoreConnectionMSFTDelegate_EndInvoke_mAC419F701336F5A413E6A84BAB5A93E9464C70E8 (void);
// 0x0000044C System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/DestroySpatialAnchorStoreConnectionMSFTDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroySpatialAnchorStoreConnectionMSFTDelegate__ctor_mE63BDD395A7C62C3CA5926C796D7F025E8F9313A (void);
// 0x0000044D Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/DestroySpatialAnchorStoreConnectionMSFTDelegate::Invoke(System.UInt64)
extern void DestroySpatialAnchorStoreConnectionMSFTDelegate_Invoke_mEFB181C09A6237E3F36CB3887981360C060A54C0 (void);
// 0x0000044E System.IAsyncResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/DestroySpatialAnchorStoreConnectionMSFTDelegate::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void DestroySpatialAnchorStoreConnectionMSFTDelegate_BeginInvoke_m88C2CFD7A6D411A927319CEF4187D341777AA6FA (void);
// 0x0000044F Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/DestroySpatialAnchorStoreConnectionMSFTDelegate::EndInvoke(System.IAsyncResult)
extern void DestroySpatialAnchorStoreConnectionMSFTDelegate_EndInvoke_m428E95F457E0810EF96800E304549C264483BED2 (void);
// 0x00000450 System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/PersistSpatialAnchorMSFTDelegate::.ctor(System.Object,System.IntPtr)
extern void PersistSpatialAnchorMSFTDelegate__ctor_m76A5D24CCC511B93896DD6A263B61D7C90DD10F1 (void);
// 0x00000451 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/PersistSpatialAnchorMSFTDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrSpatialAnchorPersistenceInfoMSFT&)
extern void PersistSpatialAnchorMSFTDelegate_Invoke_m629801285D0E38414C2A1E7537960F98D5CB887B (void);
// 0x00000452 System.IAsyncResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/PersistSpatialAnchorMSFTDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrSpatialAnchorPersistenceInfoMSFT&,System.AsyncCallback,System.Object)
extern void PersistSpatialAnchorMSFTDelegate_BeginInvoke_m3AB30F69A6807F12BB5891259468C11294298174 (void);
// 0x00000453 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/PersistSpatialAnchorMSFTDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrSpatialAnchorPersistenceInfoMSFT&,System.IAsyncResult)
extern void PersistSpatialAnchorMSFTDelegate_EndInvoke_m8F4DD6950A6C2DB1F52AA391B2522356CDD37C0B (void);
// 0x00000454 System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/UnpersistSpatialAnchorMSFTDelegate::.ctor(System.Object,System.IntPtr)
extern void UnpersistSpatialAnchorMSFTDelegate__ctor_mE32740AEC3B347F059C4C52BB95953FA78A94694 (void);
// 0x00000455 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/UnpersistSpatialAnchorMSFTDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrSpatialAnchorPersistenceNameMSFT&)
extern void UnpersistSpatialAnchorMSFTDelegate_Invoke_m574686A42D506A5CFF44232D1040363433728E28 (void);
// 0x00000456 System.IAsyncResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/UnpersistSpatialAnchorMSFTDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrSpatialAnchorPersistenceNameMSFT&,System.AsyncCallback,System.Object)
extern void UnpersistSpatialAnchorMSFTDelegate_BeginInvoke_m5E68648C9DAEC00E814CA1430405C5220A60FD38 (void);
// 0x00000457 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/UnpersistSpatialAnchorMSFTDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrSpatialAnchorPersistenceNameMSFT&,System.IAsyncResult)
extern void UnpersistSpatialAnchorMSFTDelegate_EndInvoke_m16F28F6EA37DCD0715B79CF7A0C0105BCD48A883 (void);
// 0x00000458 System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/EnumeratePersistedSpatialAnchorNamesMSFTDelegate::.ctor(System.Object,System.IntPtr)
extern void EnumeratePersistedSpatialAnchorNamesMSFTDelegate__ctor_mAFAFD2711DF12FD7E0AFDDC6D67BF601E2861B32 (void);
// 0x00000459 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/EnumeratePersistedSpatialAnchorNamesMSFTDelegate::Invoke(System.UInt64,System.UInt32,System.UInt32&,System.IntPtr)
extern void EnumeratePersistedSpatialAnchorNamesMSFTDelegate_Invoke_m8D01B6BEA1F353BEF629E24877FF8020028F47CE (void);
// 0x0000045A System.IAsyncResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/EnumeratePersistedSpatialAnchorNamesMSFTDelegate::BeginInvoke(System.UInt64,System.UInt32,System.UInt32&,System.IntPtr,System.AsyncCallback,System.Object)
extern void EnumeratePersistedSpatialAnchorNamesMSFTDelegate_BeginInvoke_mF20189A65702D6A04D9C95BA7F58A6D93FB173E3 (void);
// 0x0000045B Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/EnumeratePersistedSpatialAnchorNamesMSFTDelegate::EndInvoke(System.UInt32&,System.IAsyncResult)
extern void EnumeratePersistedSpatialAnchorNamesMSFTDelegate_EndInvoke_m3767A6B38656AD3D3A049B5A956D9502AB0CA418 (void);
// 0x0000045C System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/CreateSpatialAnchorFromPersistedNameMSFTDelegate::.ctor(System.Object,System.IntPtr)
extern void CreateSpatialAnchorFromPersistedNameMSFTDelegate__ctor_mC0C7549B313907B39BF54E087D5BBCE7172C05A8 (void);
// 0x0000045D Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/CreateSpatialAnchorFromPersistedNameMSFTDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrSpatialAnchorFromPersistedAnchorCreateInfoMSFT&,System.UInt64&)
extern void CreateSpatialAnchorFromPersistedNameMSFTDelegate_Invoke_m8EE7A705BDFF61921963C9A9AD603B9A4DCD0F3C (void);
// 0x0000045E System.IAsyncResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/CreateSpatialAnchorFromPersistedNameMSFTDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrSpatialAnchorFromPersistedAnchorCreateInfoMSFT&,System.UInt64&,System.AsyncCallback,System.Object)
extern void CreateSpatialAnchorFromPersistedNameMSFTDelegate_BeginInvoke_m48433F3303F65A228720F2EBC11C52CC39C3D10D (void);
// 0x0000045F Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/CreateSpatialAnchorFromPersistedNameMSFTDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrSpatialAnchorFromPersistedAnchorCreateInfoMSFT&,System.UInt64&,System.IAsyncResult)
extern void CreateSpatialAnchorFromPersistedNameMSFTDelegate_EndInvoke_m7E349F63CB0710A23B99E39FF39B08BA043DF541 (void);
// 0x00000460 System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/ClearSpatialAnchorStoreMSFTDelegate::.ctor(System.Object,System.IntPtr)
extern void ClearSpatialAnchorStoreMSFTDelegate__ctor_m13D0AE7888E8B13D91AE3AB946240621F184719B (void);
// 0x00000461 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/ClearSpatialAnchorStoreMSFTDelegate::Invoke(System.UInt64)
extern void ClearSpatialAnchorStoreMSFTDelegate_Invoke_m1A2CD0184B4A8D1F231BE3F57EF07CBA20C62AA1 (void);
// 0x00000462 System.IAsyncResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/ClearSpatialAnchorStoreMSFTDelegate::BeginInvoke(System.UInt64,System.AsyncCallback,System.Object)
extern void ClearSpatialAnchorStoreMSFTDelegate_BeginInvoke_m6CCF5D13AC2EDDD1A9BDA4B152B869E05134EED0 (void);
// 0x00000463 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/ClearSpatialAnchorStoreMSFTDelegate::EndInvoke(System.IAsyncResult)
extern void ClearSpatialAnchorStoreMSFTDelegate_EndInvoke_mA9F7719B9F48972F2A74970861F77F71DDBB9AAB (void);
// 0x00000464 System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/LocateSpaceDelegate::.ctor(System.Object,System.IntPtr)
extern void LocateSpaceDelegate__ctor_m2BF0040304888E95146FFB3E1E71F82C7C0DBAFB (void);
// 0x00000465 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/LocateSpaceDelegate::Invoke(System.UInt64,System.UInt64,System.Int64,Qualcomm.Snapdragon.Spaces.XrSpaceLocation&)
extern void LocateSpaceDelegate_Invoke_m09F2C746A247677A863DA20FE8FDDE4E5245F8C5 (void);
// 0x00000466 System.IAsyncResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/LocateSpaceDelegate::BeginInvoke(System.UInt64,System.UInt64,System.Int64,Qualcomm.Snapdragon.Spaces.XrSpaceLocation&,System.AsyncCallback,System.Object)
extern void LocateSpaceDelegate_BeginInvoke_m813FD8C8E5C188A33A7F4B974E4EFD6C7243DCF6 (void);
// 0x00000467 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialAnchorsFeature/LocateSpaceDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrSpaceLocation&,System.IAsyncResult)
extern void LocateSpaceDelegate_EndInvoke_mAD80E9503E4D5E516D5F9638F7128059864E35DD (void);
// 0x00000468 System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsSubsystem::RegisterDescriptor()
extern void SpatialAnchorsSubsystem_RegisterDescriptor_mEFE16FC3EB00C57718F03116E4AD3B1543286349 (void);
// 0x00000469 System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsSubsystem::.ctor()
extern void SpatialAnchorsSubsystem__ctor_m3DA4F79A357AF845F3AF8E8B5AA6BE430FCDD82B (void);
// 0x0000046A System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsSubsystem/SpatialAnchorsProvider::Start()
extern void SpatialAnchorsProvider_Start_m315EF3EF6CFEF3FEF626DDFFA49F0F53F2847652 (void);
// 0x0000046B System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsSubsystem/SpatialAnchorsProvider::Stop()
extern void SpatialAnchorsProvider_Stop_m583C80431D1F330E1EEC7BA1C12DDBE5AF2E1D8F (void);
// 0x0000046C System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsSubsystem/SpatialAnchorsProvider::Destroy()
extern void SpatialAnchorsProvider_Destroy_m00F3AAB4418AFE6487E7753D86AA59DB42E04FD8 (void);
// 0x0000046D System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsSubsystem/SpatialAnchorsProvider::AddAnchorsWorker()
extern void SpatialAnchorsProvider_AddAnchorsWorker_mEB4CBADFE8B1039B48FF946E475C59A6CFB63E9A (void);
// 0x0000046E UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRAnchor> Qualcomm.Snapdragon.Spaces.SpatialAnchorsSubsystem/SpatialAnchorsProvider::GetChanges(UnityEngine.XR.ARSubsystems.XRAnchor,Unity.Collections.Allocator)
extern void SpatialAnchorsProvider_GetChanges_m80C33A43A87FFE3EF1A5DBED4A9086DEBA5370C5 (void);
// 0x0000046F System.Boolean Qualcomm.Snapdragon.Spaces.SpatialAnchorsSubsystem/SpatialAnchorsProvider::TryAddAnchor(UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRAnchor&)
extern void SpatialAnchorsProvider_TryAddAnchor_mB860D534301AD7AEDB3A994B044C4A70F74B7A95 (void);
// 0x00000470 System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsSubsystem/SpatialAnchorsProvider::SetPersistentAnchorCandidate(Qualcomm.Snapdragon.Spaces.SpatialAnchor)
extern void SpatialAnchorsProvider_SetPersistentAnchorCandidate_m39F58BBE288998D8DCF8668964036C32CD729E2A (void);
// 0x00000471 System.Boolean Qualcomm.Snapdragon.Spaces.SpatialAnchorsSubsystem/SpatialAnchorsProvider::TryRemoveAnchor(UnityEngine.XR.ARSubsystems.TrackableId)
extern void SpatialAnchorsProvider_TryRemoveAnchor_m57FD042A12277644B5136F9291219DB51323FF67 (void);
// 0x00000472 System.Boolean Qualcomm.Snapdragon.Spaces.SpatialAnchorsSubsystem/SpatialAnchorsProvider::TryAddAnchorFromPersistentName(System.UInt64,System.String)
extern void SpatialAnchorsProvider_TryAddAnchorFromPersistentName_m34B7DD94ACB60DC573DBC04CFE35204D91926F75 (void);
// 0x00000473 System.String Qualcomm.Snapdragon.Spaces.SpatialAnchorsSubsystem/SpatialAnchorsProvider::TryGetSavedNameFromTrackableId(UnityEngine.XR.ARSubsystems.TrackableId)
extern void SpatialAnchorsProvider_TryGetSavedNameFromTrackableId_mF37BF9A04A5E321A4D8A91EF85D86EE5CEDCC875 (void);
// 0x00000474 System.UInt64 Qualcomm.Snapdragon.Spaces.SpatialAnchorsSubsystem/SpatialAnchorsProvider::TryGetSpatialAnchorHandleFromTrackableId(UnityEngine.XR.ARSubsystems.TrackableId)
extern void SpatialAnchorsProvider_TryGetSpatialAnchorHandleFromTrackableId_mB6EE6A6D84FD6C7710F8684AA32EB240EEA05C82 (void);
// 0x00000475 System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsSubsystem/SpatialAnchorsProvider::UpdateAnchorSavedName(UnityEngine.XR.ARSubsystems.TrackableId,System.String)
extern void SpatialAnchorsProvider_UpdateAnchorSavedName_m497685B1A1FC34271A9D1FCCAC011DEAD644A648 (void);
// 0x00000476 System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsSubsystem/SpatialAnchorsProvider::ClearAllAnchorSavedNames()
extern void SpatialAnchorsProvider_ClearAllAnchorSavedNames_mD29F12637E25DE3B8F2A7E81C4DAAE3A3F716DB4 (void);
// 0x00000477 System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsSubsystem/SpatialAnchorsProvider::.ctor()
extern void SpatialAnchorsProvider__ctor_mFD5C14D017F935CBF28DC2A6C5BE4B259DD4DBB5 (void);
// 0x00000478 System.Boolean Qualcomm.Snapdragon.Spaces.SpatialAnchorsSubsystem/SpatialAnchorsProvider::<GetChanges>b__16_0(Qualcomm.Snapdragon.Spaces.SpatialAnchor)
extern void SpatialAnchorsProvider_U3CGetChangesU3Eb__16_0_m3018377E0E8CDCA49628F57328C0B7F4E9E3D82C (void);
// 0x00000479 System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsSubsystem/SpatialAnchorsProvider/AddAnchorData::.ctor(Qualcomm.Snapdragon.Spaces.SpatialAnchor,UnityEngine.Pose)
extern void AddAnchorData__ctor_m41F5A154755BC533FD63364040166BEE62A7CB0A (void);
// 0x0000047A System.Void Qualcomm.Snapdragon.Spaces.SpatialAnchorsSubsystem/SpatialAnchorsProvider/<>c__DisplayClass19_0::.ctor()
extern void U3CU3Ec__DisplayClass19_0__ctor_m84553CC5488F55885316C4CC80820C3AC9A4A3F2 (void);
// 0x0000047B System.Boolean Qualcomm.Snapdragon.Spaces.SpatialAnchorsSubsystem/SpatialAnchorsProvider/<>c__DisplayClass19_0::<TryRemoveAnchor>b__0(Qualcomm.Snapdragon.Spaces.SpatialAnchor)
extern void U3CU3Ec__DisplayClass19_0_U3CTryRemoveAnchorU3Eb__0_m499F84C3EC2E75636BE64CD24DC63AFCDBCFD0C4 (void);
// 0x0000047C System.Boolean Qualcomm.Snapdragon.Spaces.SpacesARMeshManagerConfig::get_CalculateCpuNormals()
extern void SpacesARMeshManagerConfig_get_CalculateCpuNormals_m173C07259CC37DCF8ED8879889459232A020528B (void);
// 0x0000047D System.Void Qualcomm.Snapdragon.Spaces.SpacesARMeshManagerConfig::set_CalculateCpuNormals(System.Boolean)
extern void SpacesARMeshManagerConfig_set_CalculateCpuNormals_mFE26E5FE895B48F186CE2A00CA5E0650AD387E2C (void);
// 0x0000047E System.Boolean Qualcomm.Snapdragon.Spaces.SpacesARMeshManagerConfig::get_UseSmoothedNormals()
extern void SpacesARMeshManagerConfig_get_UseSmoothedNormals_m59EA1784FE4DD817747244A2D47A34AAE8CAB1E1 (void);
// 0x0000047F System.Void Qualcomm.Snapdragon.Spaces.SpacesARMeshManagerConfig::set_UseSmoothedNormals(System.Boolean)
extern void SpacesARMeshManagerConfig_set_UseSmoothedNormals_m23E6E5F870D8B64C15944D560C822723354C3C76 (void);
// 0x00000480 System.Single Qualcomm.Snapdragon.Spaces.SpacesARMeshManagerConfig::get_SmoothingEpsilon()
extern void SpacesARMeshManagerConfig_get_SmoothingEpsilon_m2917B8945305A25069A34332BA817FE7FCB5FF11 (void);
// 0x00000481 System.Void Qualcomm.Snapdragon.Spaces.SpacesARMeshManagerConfig::set_SmoothingEpsilon(System.Single)
extern void SpacesARMeshManagerConfig_set_SmoothingEpsilon_m53D760B5A5E5296FD04BE882530FEAD17FEAA2EB (void);
// 0x00000482 System.Void Qualcomm.Snapdragon.Spaces.SpacesARMeshManagerConfig::Awake()
extern void SpacesARMeshManagerConfig_Awake_m022AD0A961200221E203E0ACB02DEED144676EF6 (void);
// 0x00000483 System.Void Qualcomm.Snapdragon.Spaces.SpacesARMeshManagerConfig::OnEnable()
extern void SpacesARMeshManagerConfig_OnEnable_m3104FDFDCB82C0EE35B1006284EC7AD6CA69DD92 (void);
// 0x00000484 System.Void Qualcomm.Snapdragon.Spaces.SpacesARMeshManagerConfig::OnDisable()
extern void SpacesARMeshManagerConfig_OnDisable_m63C83DD89B3607697973A06086947458EB77687D (void);
// 0x00000485 System.Void Qualcomm.Snapdragon.Spaces.SpacesARMeshManagerConfig::Internal_SetMeshVertexAttributesFlags(System.Int32)
extern void SpacesARMeshManagerConfig_Internal_SetMeshVertexAttributesFlags_mF9DD96D8F1860CB2D092CF215733727B504BD9ED (void);
// 0x00000486 System.Void Qualcomm.Snapdragon.Spaces.SpacesARMeshManagerConfig::Internal_SetUseSmoothedNormals(System.Boolean,System.Single)
extern void SpacesARMeshManagerConfig_Internal_SetUseSmoothedNormals_m33CDBF9FEDE491A16022EDB17ED7CABBD48C8CFC (void);
// 0x00000487 System.Void Qualcomm.Snapdragon.Spaces.SpacesARMeshManagerConfig::UpdateMeshVertexAttributes()
extern void SpacesARMeshManagerConfig_UpdateMeshVertexAttributes_mD63EB54342EC033DD7AC7D9C42D8B0BBA81CB227 (void);
// 0x00000488 System.Boolean Qualcomm.Snapdragon.Spaces.SpacesARMeshManagerConfig::Internal_FetchMeshLocations(System.IntPtr,System.IntPtr)
extern void SpacesARMeshManagerConfig_Internal_FetchMeshLocations_mE4E4F772FC46E6138AA3E0DBA4239DA391B695C9 (void);
// 0x00000489 System.Void Qualcomm.Snapdragon.Spaces.SpacesARMeshManagerConfig::UpdateMeshTransforms(UnityEngine.XR.ARFoundation.ARMeshesChangedEventArgs)
extern void SpacesARMeshManagerConfig_UpdateMeshTransforms_m5760A560DD59692FEB3E762E61F62117624F4DAA (void);
// 0x0000048A System.Void Qualcomm.Snapdragon.Spaces.SpacesARMeshManagerConfig::.ctor()
extern void SpacesARMeshManagerConfig__ctor_mEDF57C58902085F21AAF398A1F556526888393A7 (void);
// 0x0000048B System.Void Qualcomm.Snapdragon.Spaces.SpacesARMeshManagerConfig/<>c::.cctor()
extern void U3CU3Ec__cctor_m41F89D8F04CCDE897FFF048FD454B0AB996C6E8D (void);
// 0x0000048C System.Void Qualcomm.Snapdragon.Spaces.SpacesARMeshManagerConfig/<>c::.ctor()
extern void U3CU3Ec__ctor_m52D7D0C3E5A59D7900D0EE4367785F493338D1CC (void);
// 0x0000048D UnityEngine.Transform Qualcomm.Snapdragon.Spaces.SpacesARMeshManagerConfig/<>c::<UpdateMeshTransforms>b__25_0(UnityEngine.MeshFilter)
extern void U3CU3Ec_U3CUpdateMeshTransformsU3Eb__25_0_m56072C0A26A15F8E3067148ECD14418D9716B731 (void);
// 0x0000048E System.Boolean Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature::get_IsRequiringBaseRuntimeFeature()
extern void SpatialMeshingFeature_get_IsRequiringBaseRuntimeFeature_m35C9AF7CDCA8AA6E15B5EBE5156F577B4312AF24 (void);
// 0x0000048F System.String Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature::GetXrLayersToLoad()
extern void SpatialMeshingFeature_GetXrLayersToLoad_mEA2654D185DB5DEDFF0AFDAA7C6AC5022415692C (void);
// 0x00000490 System.IntPtr Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature::HookGetInstanceProcAddr(System.IntPtr)
extern void SpatialMeshingFeature_HookGetInstanceProcAddr_mBEDA89FB8AE0C4F8436DB229A3A624597C0FEDAE (void);
// 0x00000491 System.Boolean Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature::OnInstanceCreate(System.UInt64)
extern void SpatialMeshingFeature_OnInstanceCreate_mBF2FEC09E00D968EB15B97A36C08EA913E3D19F5 (void);
// 0x00000492 System.Void Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature::OnSubsystemCreate()
extern void SpatialMeshingFeature_OnSubsystemCreate_m27E5A22456F1153140FEF8D6539185088F341153 (void);
// 0x00000493 System.Void Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature::OnSubsystemStop()
extern void SpatialMeshingFeature_OnSubsystemStop_m10CB3845E2D47AC7A55B84BEB0DE02D559684865 (void);
// 0x00000494 System.Void Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature::OnSubsystemDestroy()
extern void SpatialMeshingFeature_OnSubsystemDestroy_m32E3C54AD2C8CC3B0D842E8446FCEECF3E75B26D (void);
// 0x00000495 System.Void Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature::OnSessionCreate(System.UInt64)
extern void SpatialMeshingFeature_OnSessionCreate_m7A38F25F3413ABAA6472EAAFE7D2BD092C4FC1BD (void);
// 0x00000496 System.Void Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature::OnAppSpaceChange(System.UInt64)
extern void SpatialMeshingFeature_OnAppSpaceChange_m52A405DF272B76C5258C297A0B474FD48CAE94BB (void);
// 0x00000497 System.IntPtr Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature::Internal_GetInterceptedInstanceProcAddr(System.IntPtr)
extern void SpatialMeshingFeature_Internal_GetInterceptedInstanceProcAddr_m9688A072EEBA614133E53F5A7FD25E1AD397029F (void);
// 0x00000498 System.Void Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature::Internal_RegisterMeshingLifecycleProvider()
extern void SpatialMeshingFeature_Internal_RegisterMeshingLifecycleProvider_m5BD0977192D0C5323EA3A2BAD41BBE8DE4F18744 (void);
// 0x00000499 System.Void Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature::Internal_SetInstanceHandle(System.UInt64)
extern void SpatialMeshingFeature_Internal_SetInstanceHandle_mAFCFE24FB40E7F7D108619005E2349A9EA80EFC1 (void);
// 0x0000049A System.Void Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature::Internal_SetSessionHandle(System.UInt64)
extern void SpatialMeshingFeature_Internal_SetSessionHandle_mA55D51A96361262F56449C9E27111C60F38F5022 (void);
// 0x0000049B System.Void Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature::Internal_SetSpaceHandle(System.UInt64)
extern void SpatialMeshingFeature_Internal_SetSpaceHandle_mAB5E5F895CE3BBE833A2FBA2F256510DDDC197C0 (void);
// 0x0000049C System.Void Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature::Internal_RegisterProviderWithSceneObserver(System.String,System.Int32)
extern void SpatialMeshingFeature_Internal_RegisterProviderWithSceneObserver_m07FB89EE9FBB7844DA7A960A4C4E5FED8910AF36 (void);
// 0x0000049D System.Void Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature::Internal_UnregisterProviderWithSceneObserver(System.String)
extern void SpatialMeshingFeature_Internal_UnregisterProviderWithSceneObserver_m8C2AE43EBC56404C2F205B999FB64C071A105096 (void);
// 0x0000049E System.Void Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature::.ctor()
extern void SpatialMeshingFeature__ctor_m6715D10B5FDAC89A1B7D8A86107AE43D5436BAE1 (void);
// 0x0000049F System.Void Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature::.cctor()
extern void SpatialMeshingFeature__cctor_mAE8142B52B445BF623DB4DFEDC63D7DC2D4BA5F1 (void);
// 0x000004A0 System.Void Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature/CreateSceneObserverMSFTDelegate::.ctor(System.Object,System.IntPtr)
extern void CreateSceneObserverMSFTDelegate__ctor_m69AB72D4903A3BB6A6349AA1F65403C60D0ADE28 (void);
// 0x000004A1 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature/CreateSceneObserverMSFTDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrSceneObserverCreateInfoMSFT&,System.UInt64&)
extern void CreateSceneObserverMSFTDelegate_Invoke_m05CF0D8891D3F59D1372986C78BEFD46E321F390 (void);
// 0x000004A2 System.IAsyncResult Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature/CreateSceneObserverMSFTDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrSceneObserverCreateInfoMSFT&,System.UInt64&,System.AsyncCallback,System.Object)
extern void CreateSceneObserverMSFTDelegate_BeginInvoke_mF7DBF17FFCE45B8E067A67DBD5983193C2EC4159 (void);
// 0x000004A3 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature/CreateSceneObserverMSFTDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrSceneObserverCreateInfoMSFT&,System.UInt64&,System.IAsyncResult)
extern void CreateSceneObserverMSFTDelegate_EndInvoke_m998EEC02E13DCBF505F51073A7B24698ABFF1A07 (void);
// 0x000004A4 System.Void Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature/ComputeNewSceneMSFTDelegate::.ctor(System.Object,System.IntPtr)
extern void ComputeNewSceneMSFTDelegate__ctor_m436EED3867F77DD71F0BCC9CD4254411DB432FA6 (void);
// 0x000004A5 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature/ComputeNewSceneMSFTDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrNewSceneComputeInfoMSFT&)
extern void ComputeNewSceneMSFTDelegate_Invoke_mF73F994CF781E9AE5E3CBE2D62D386B2FA74E016 (void);
// 0x000004A6 System.IAsyncResult Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature/ComputeNewSceneMSFTDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrNewSceneComputeInfoMSFT&,System.AsyncCallback,System.Object)
extern void ComputeNewSceneMSFTDelegate_BeginInvoke_m4A727DB4AB2F84128CF970C7A6AF847F72EB1F4C (void);
// 0x000004A7 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature/ComputeNewSceneMSFTDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrNewSceneComputeInfoMSFT&,System.IAsyncResult)
extern void ComputeNewSceneMSFTDelegate_EndInvoke_mE1822F7F48CB128A98BC47D83BA4C6CBE846DF85 (void);
// 0x000004A8 System.Void Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature/GetSceneComputeStateMSFTDelegate::.ctor(System.Object,System.IntPtr)
extern void GetSceneComputeStateMSFTDelegate__ctor_m224B9A87DCDF1B4780E88BA923EFC6F2073799AD (void);
// 0x000004A9 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature/GetSceneComputeStateMSFTDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrSceneComputeStateMSFT&)
extern void GetSceneComputeStateMSFTDelegate_Invoke_m54FAFDE8470FC944C05ED1774F967C7579BC36BE (void);
// 0x000004AA System.IAsyncResult Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature/GetSceneComputeStateMSFTDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrSceneComputeStateMSFT&,System.AsyncCallback,System.Object)
extern void GetSceneComputeStateMSFTDelegate_BeginInvoke_m6BCCA0E52260EEA8DCA74AB3F2ADD602A0F7FD71 (void);
// 0x000004AB Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature/GetSceneComputeStateMSFTDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrSceneComputeStateMSFT&,System.IAsyncResult)
extern void GetSceneComputeStateMSFTDelegate_EndInvoke_m78E9DDDDE1AC7CAABCD0C259DDEF35E324C5157A (void);
// 0x000004AC System.Void Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature/CreateSceneMSFTDelegate::.ctor(System.Object,System.IntPtr)
extern void CreateSceneMSFTDelegate__ctor_mA4CB239F551DE61980347EC47E18194043B91E6F (void);
// 0x000004AD Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature/CreateSceneMSFTDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrSceneCreateInfoMSFT&,System.UInt64)
extern void CreateSceneMSFTDelegate_Invoke_m04B0655BEAB01AACC0AF3630D54704D37EA696EF (void);
// 0x000004AE System.IAsyncResult Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature/CreateSceneMSFTDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrSceneCreateInfoMSFT&,System.UInt64,System.AsyncCallback,System.Object)
extern void CreateSceneMSFTDelegate_BeginInvoke_m2D50A745EC2354B2D590C4ED359F546204FE504E (void);
// 0x000004AF Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature/CreateSceneMSFTDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrSceneCreateInfoMSFT&,System.IAsyncResult)
extern void CreateSceneMSFTDelegate_EndInvoke_mE6F23C4F5A0F05AD085351F852FC2CE1D5FE5DFA (void);
// 0x000004B0 System.Void Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature/GetSceneComponentsMSFTDelegate::.ctor(System.Object,System.IntPtr)
extern void GetSceneComponentsMSFTDelegate__ctor_mF87106F1821844CEDA522EC90E404F22EC9AC4A1 (void);
// 0x000004B1 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature/GetSceneComponentsMSFTDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrSceneComponentsGetInfoMSFT&,Qualcomm.Snapdragon.Spaces.XrSceneComponentsMSFT&)
extern void GetSceneComponentsMSFTDelegate_Invoke_m585D6D6F72141455768BA4BA7CBAF310A3817F13 (void);
// 0x000004B2 System.IAsyncResult Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature/GetSceneComponentsMSFTDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrSceneComponentsGetInfoMSFT&,Qualcomm.Snapdragon.Spaces.XrSceneComponentsMSFT&,System.AsyncCallback,System.Object)
extern void GetSceneComponentsMSFTDelegate_BeginInvoke_m9FC916EF9608A9B175E2B834298F8A6A1604F72C (void);
// 0x000004B3 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature/GetSceneComponentsMSFTDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrSceneComponentsGetInfoMSFT&,Qualcomm.Snapdragon.Spaces.XrSceneComponentsMSFT&,System.IAsyncResult)
extern void GetSceneComponentsMSFTDelegate_EndInvoke_m52E57976567C5E40FF3B1AF2085A6894E0EE28EB (void);
// 0x000004B4 System.Void Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature/LocateSceneComponentsMSFTDelegate::.ctor(System.Object,System.IntPtr)
extern void LocateSceneComponentsMSFTDelegate__ctor_m274DA0037673F612D02BA6920400D92261E8405A (void);
// 0x000004B5 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature/LocateSceneComponentsMSFTDelegate::Invoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrSceneComponentsLocateInfoMSFT&,Qualcomm.Snapdragon.Spaces.XrSceneComponentLocationsMSFT&)
extern void LocateSceneComponentsMSFTDelegate_Invoke_mC0FE3A6E7BDE9E6A4B55556AEDC2F5643DB63A1F (void);
// 0x000004B6 System.IAsyncResult Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature/LocateSceneComponentsMSFTDelegate::BeginInvoke(System.UInt64,Qualcomm.Snapdragon.Spaces.XrSceneComponentsLocateInfoMSFT&,Qualcomm.Snapdragon.Spaces.XrSceneComponentLocationsMSFT&,System.AsyncCallback,System.Object)
extern void LocateSceneComponentsMSFTDelegate_BeginInvoke_m563F08D3B1AC9160163E93DBA6284BE4E702D68E (void);
// 0x000004B7 Qualcomm.Snapdragon.Spaces.XrResult Qualcomm.Snapdragon.Spaces.SpatialMeshingFeature/LocateSceneComponentsMSFTDelegate::EndInvoke(Qualcomm.Snapdragon.Spaces.XrSceneComponentsLocateInfoMSFT&,Qualcomm.Snapdragon.Spaces.XrSceneComponentLocationsMSFT&,System.IAsyncResult)
extern void LocateSceneComponentsMSFTDelegate_EndInvoke_m627E2124926722ED99898D35E6641DC2797048F5 (void);
static Il2CppMethodPointer s_methodPointers[1207] = 
{
	SpacesCpuImageApi_get_instance_m52026DD599BF14F810E05FAB14C440C9220A235C,
	SpacesCpuImageApi_set_instance_mC2CC50C31516CAF05A524AF1D075A1D382993C90,
	SpacesCpuImageApi_CreateInstance_mD2D35E2A4323F98549A001BFBEA1B680D57320FE,
	SpacesCpuImageApi_NativeHandleValid_m6833E3FBA0B12936487CC709E4056677C6A6D284,
	SpacesCpuImageApi_FormatSupported_mE16E8EA909EBBBB77E0D6CF163E6E7D6A3E930C8,
	SpacesCpuImageApi_TryGetPlane_m13804BD415C91C343566F40D745AF215F33D4330,
	SpacesCpuImageApi_TryGetConvertedDataSize_mD7CA34E4B2F25CB4DD9B1CB83485061770DC2E31,
	SpacesCpuImageApi_TryConvert_mC78DE87A75B48DCA0FFD69BE1FEC492939B05DB8,
	SpacesCpuImageApi_DisposeImage_m5A5CEA4F0DCF2215F832B8E311F431AF7FB6D498,
	SpacesCpuImageApi__ctor_mEABA309E40AD779F0F734430AEE138124808A229,
	SpacesObsoleteVersionAttribute__ctor_mBF135C88AB4383CB2F5E0F8032796D1793D4D5B5,
	SpacesObsoleteVersionAttribute__ctor_m0B592C136EE3EFDB05AC6A04EF40D25A72C1B98F,
	SpacesObsoleteVersionAttribute__ctor_m79DED243DA49BC684ED2C7282543DFD507A6918F,
	SpacesObsoleteVersionAttribute__ctor_mD432687FAE899722D72BD3CAD0B61C1DE049E876,
	CameraAccessFeature_get_RuntimeMaxVerticalResolution_mEB7DC478FB922B4C238821BD6E866238A6495BB9,
	CameraAccessFeature_set_RuntimeMaxVerticalResolution_mC74123DE5B57AC38240BCA7D8F3600B0CC35929D,
	CameraAccessFeature_get_CameraInfos_mB2D2FA3424DF6A66DBD460F2801D4B55F2DF5FB1,
	CameraAccessFeature_get_DownsamplingFactor_m7E6601DAF1289B22356B5941C421CBC8B4D3B5BD,
	CameraAccessFeature_get_SensorProperties_mEEF0AB61DB904251A0A1BE5666B0D5D6F794CB97,
	CameraAccessFeature_get_CachedFrameData_mEF13E91DA520B822818A70FE38DCB364C84BC7B9,
	CameraAccessFeature_get_CachedYuvFrame_m3AE38BAE1D034B92E65A8A7B007DF3AD232C438E,
	CameraAccessFeature_get_FrameBuffers_mD9F75CCDCBEBE8A7180B5684694F0AF1F2C68812,
	CameraAccessFeature_get_LastFramePose_m4C65EFFDE33A73DFA5BA9F67DB09C37CA74E7EFD,
	CameraAccessFeature_get_IsRequiringBaseRuntimeFeature_m38A8A9B037F0E371BEB211241D9480779F54FE98,
	CameraAccessFeature_GetXrLayersToLoad_mEE9DC616AE93F79F1B9D2BD0149D2292FF0DD4EF,
	CameraAccessFeature_OnInstanceCreate_m695E43BD3678CD6F367DC84D67CBA5BDA1B88D77,
	CameraAccessFeature_OnSubsystemCreate_m58CDD035F80484017B2E00B679C1F3BB6EE21279,
	CameraAccessFeature_OnSubsystemStop_m9263D15D132C5D6365E18244F70DD83AA1EC0F4C,
	CameraAccessFeature_OnSubsystemDestroy_m055C874107972FAAAFD5950E5B1B47BC3CE60999,
	CameraAccessFeature_OnHookMethods_m619B53413C22FD4E26305F85D84512A6FD468944,
	CameraAccessFeature_TryEnumerateCameras_m8E03ED6547D20107C69585A13A1245B9F7272927,
	CameraAccessFeature_TryGetSupportedFrameConfigurations_mC38236D225D15815B89FF2669D29071F6116F3E9,
	CameraAccessFeature_TryCreateCameraHandle_m204AA2A45E7390120BEB69E96A52AE8B691D8D8A,
	CameraAccessFeature_TryReleaseCameraHandle_m5BC9A9F88CFB97ADA191535E2F118678857FEC90,
	CameraAccessFeature_TryAccessFrame_m7123B2CF6E26981284E1B7A069208C12C4F9D54D,
	CameraAccessFeature_TryReleaseFrame_m4F36A24C523B6DB6E6E94B77A0DCB67909D9F6AA,
	CameraAccessFeature_XrTimeFromSpacesTimespec_m81156D4491D1CD6F13BE822E4D690D81A3002275,
	CameraAccessFeature_SpacesTimespecFromXRTime_m09AE123B935E6D36DD5D9E140DDF4E3D43E57EBC,
	CameraAccessFeature_TryExtractFrame_m1645E9140376D6A01B354F0164A2DE2F494F7818,
	CameraAccessFeature_ClearFrameCache_mD26961080756CF989A81B67E7F1620CEB4FA967B,
	CameraAccessFeature_UpdateDownsamplingFactor_m4D408469CCD3A1710EEC06B918EA87F4A005E407,
	CameraAccessFeature__ctor_mD408BBD7ADD3A6B86EC2F9BE98FD382D39EFD11E,
	CameraAccessFeature__cctor_m53840006C7E771132E56D375082E2608F70961A2,
	EnumerateCamerasQCOMDelegate__ctor_m37CD16E3E70D6E40FCBD233B065254A759478795,
	EnumerateCamerasQCOMDelegate_Invoke_mED2B205E17AEB6156F8404C3C8B494DBA9DE7F25,
	EnumerateCamerasQCOMDelegate_BeginInvoke_m1C593EB5BCA3609C8FB2CA493A5D17AEE4BCF4AA,
	EnumerateCamerasQCOMDelegate_EndInvoke_mD1D20D59F14EE2F5AEF1B0FD6AE78D4E9D266919,
	GetSupportedFrameConfigurationsQCOMDelegate__ctor_m18D3C4A32F213A088A4804F3227295CFF9E9F28D,
	GetSupportedFrameConfigurationsQCOMDelegate_Invoke_mE01AB124BFB62A7CDDE0B2D462A1CD7A53359BB8,
	GetSupportedFrameConfigurationsQCOMDelegate_BeginInvoke_m94576B47CF55542DD0EE8121D42116E1988E0399,
	GetSupportedFrameConfigurationsQCOMDelegate_EndInvoke_mC56C0A4DEFAE133AB514C89337DD12DEFE15698B,
	CreateCameraHandleQCOMDelegate__ctor_m15BA12D33D9A1A02B887DACEE25071D6606538C4,
	CreateCameraHandleQCOMDelegate_Invoke_m32EB04874065CC21E70B36C735930AB5DBC11B6C,
	CreateCameraHandleQCOMDelegate_BeginInvoke_mB887E2896F6E7060D3429554D37E20DE9199A60E,
	CreateCameraHandleQCOMDelegate_EndInvoke_mEC77230885EFC6CE89661061A86377476A92304C,
	ReleaseCameraHandleQCOMDelegate__ctor_m2DB153D228BBE69330514FCC5563062CCA7AB3A1,
	ReleaseCameraHandleQCOMDelegate_Invoke_m2D86AB36F832F0E414D06CFE868179DC1603D275,
	ReleaseCameraHandleQCOMDelegate_BeginInvoke_m7E674429AEB1E59C9F58F403F1D7DB1B3C61BCA6,
	ReleaseCameraHandleQCOMDelegate_EndInvoke_mBD74CDF8D300C7489CF4C29065B545FE201654A6,
	AccessFrameQCOMDelegate__ctor_mCF62BE8A680D792C1447E433C40C26D7B8147F5C,
	AccessFrameQCOMDelegate_Invoke_m236B9BE1F139F4219ABA76D5F6E2E930E8184B8D,
	AccessFrameQCOMDelegate_BeginInvoke_m3AD3C18909020FE3909B379B7C5204D16C331B78,
	AccessFrameQCOMDelegate_EndInvoke_mFA28EE77ED59137720F00DB467D26DF8C4544F34,
	ReleaseFrameQCOMDelegate__ctor_m3AE4007AF52F03114A6208E0CF827FBF949DC83A,
	ReleaseFrameQCOMDelegate_Invoke_mD453F7B5B005BBAB2C39DF55A2792575836736B9,
	ReleaseFrameQCOMDelegate_BeginInvoke_mED908AD4A41B61DF879C6DF1371603CC22A22F36,
	ReleaseFrameQCOMDelegate_EndInvoke_m0243213313A0A8789A42A950DFAE5C93E30B5AD1,
	xrConvertTimespecTimeToTimeKHR__ctor_mC9D1DB707C68D7E368D3B371D73AC8178F87B7E0,
	xrConvertTimespecTimeToTimeKHR_Invoke_m77ABACB9235F1555E1ADF18F2DB9E18C10A75440,
	xrConvertTimespecTimeToTimeKHR_BeginInvoke_m0315D219DB07FF3DB6663AE9670103863886CC63,
	xrConvertTimespecTimeToTimeKHR_EndInvoke_m75500BF399CEF6DDF64A3A5A6D80A7BA9AB65FBC,
	xrConvertTimeToTimespecTimeKHR__ctor_mD68289EC0A72106E26A652F3181D51B356A37355,
	xrConvertTimeToTimespecTimeKHR_Invoke_m4476300A0A4F0444CB09E2490E5E9A2DFC51642D,
	xrConvertTimeToTimespecTimeKHR_BeginInvoke_m767DA85E0E7FC07AC89E5512FF646AE66542EA11,
	xrConvertTimeToTimespecTimeKHR_EndInvoke_mB5B58B2E657B60E7497C5BE9610DB253D622D890,
	CameraSubsystem_RegisterDescriptor_m248B6032D68D4232138D61CB9FF7466C84D7A654,
	CameraSubsystem__ctor_m8F8E0937C1BAAB102152D1505413C2E2504DA7FD,
	CameraProvider_get_cpuImageApi_m787BE7DC53BC38BEF9D323E92285195E8E91D323,
	CameraProvider_get_cameraMaterial_m4749B8ED8DD5E0A5023AF841720A3DB788A1CE44,
	CameraProvider_get_permissionGranted_m066ACAC33143AB2AAADE13F2AF831FC7AD976B84,
	CameraProvider_get_invertCulling_m18054D2DC109DFFE134C54F7E72D27D1654129FB,
	CameraProvider_get_currentCamera_mAA3EB34AE060CF24C4B1722F24C76B99C7EA1DD4,
	CameraProvider_get_requestedCamera_mFB65322F5843AF1DF29308D94CF31D821C472172,
	CameraProvider_set_requestedCamera_m0D2FE593DDE310383C0B5A3D7F496D5D5A9BBD13,
	CameraProvider_get_autoFocusEnabled_m127753A0986432D723CCAFDAFC6FE3259E8B5F01,
	CameraProvider_get_autoFocusRequested_m923BA5E154B0EA1FF6D9E84CE2AA0EBE7A86DD47,
	CameraProvider_set_autoFocusRequested_m2122BED391E54D2B6E7229D61C20B6B78CB89B14,
	CameraProvider_get_currentLightEstimation_mB16784DAB070888E8612CF1035B4D6EA3E86EF34,
	CameraProvider_get_requestedLightEstimation_m95B818B69C3E356C3709FA42C842F26423FC5ED8,
	CameraProvider_set_requestedLightEstimation_m45BECB29A4837E63416D0CEF8B50CF54E4679621,
	CameraProvider_get_currentConfiguration_mFC94ACF7171BB206224DE73B0978D80B109AF94C,
	CameraProvider_set_currentConfiguration_m2A05A938A3D2A203368ED4F19E5C28B1FC39F0C0,
	CameraProvider_Start_m345019FC1E9F1CD9F1C5AB85E0CF9F09854EA762,
	CameraProvider_Stop_m642A6667A5EF8F1343DCBDDECC55CDC7752C6D2C,
	CameraProvider_Destroy_m73A269A94E9D98CCBD2FD3A92BCFA07793CEEBB5,
	CameraProvider_GetConfigurations_mE6A6CDDDEE84B7A06B8CA4B55B43D20C35B5B6F9,
	CameraProvider_TryGetIntrinsics_m1A56AAC35F8867E4FE751DBA1F7EB8F036A0B508,
	CameraProvider_TryAcquireLatestCpuImage_mCF734CD72FD92CF1D0D3A2006AE1725BAD3BABC9,
	CameraProvider_TryGetFrame_mDFFF0C181C75C9C7EC85D4E2B8C49165569AEF75,
	CameraProvider_InitialiseRGBCamera_mDB8BFC56CE1CC406070C78A3E654A22EFB194284,
	CameraProvider_ConvertXrTimeToDouble_m341BC40F2C23EEE3A21E6723D9DEA024E776DD88,
	CameraProvider__ctor_m6CC8C5CD3D484945BEE0AA3401F170832633DF3A,
	SpacesARCameraManagerConfig_get_MaxVerticalResolution_mDEBF4830F0BE8EB20A3A66B355826C4AE3F9DDFC,
	SpacesARCameraManagerConfig_set_MaxVerticalResolution_m29ECAE19462C2657A3DB12E246DD3F891DA90B74,
	SpacesARCameraManagerConfig_get_DownsamplingStride_mCE0DF3214B996EDF959E513B77C641E24D5391D6,
	SpacesARCameraManagerConfig_Start_mE47D7BADB69E692FCA94A0A46507C9381E90226D,
	SpacesARCameraManagerConfig__ctor_m11C88F7D9BD49DD174DE7EAEBC6ED6891F278579,
	SpacesCameraPoseProvider_Start_mCC8D9ABC256D1262BF537123D72F276C96C2C405,
	SpacesCameraPoseProvider_GetPoseFromProvider_mDCFF8521FE33B28ECF436CDD1531B765F880B0D6,
	SpacesCameraPoseProvider__ctor_m823CDF6F95244AA958D7A6FA6AA7A710C6F9869F,
	SpacesYUVFrame_get_Handle_m2C88D88FC6499874ABD21BE3E1337D4EA2F2BE7E,
	SpacesYUVFrame_get_Timestamp_m2A72B1EE42ED9D509FDD6646657B0D1D91702B4A,
	SpacesYUVFrame_get_Dimensions_m1827DF1A5EC7D99C4827485A0FB821AC58B07EFA,
	SpacesYUVFrame_get_Format_m65DE75FF20A89356C57B50B5F5302D9996DEFC7F,
	SpacesYUVFrame_get_YPlaneData_m3993CB49BA30972EEF8BD2123D89F47453FD126A,
	SpacesYUVFrame_get_UVPlaneData_mF92E937C538B40164858A8A6ED2ACB5EBF71893D,
	SpacesYUVFrame_get_IsValid_m6E5056C1B6822DC0E3875AF55FB2CF136BE90B04,
	SpacesYUVFrame__ctor_m0BF180942ECACF8626D6C0A742192660B4CAF6BB,
	SpacesYUVFrame_Dispose_mAFC5961E9DAA678B43EAB75E709F74C501847329,
	BaseRuntimeFeature_SetThreadHint_m2A4E47BE76DE9AF219A7FDB2A13FD6F1470CD390,
	BaseRuntimeFeature_ConfigureXRAndroidApplicationThreads_mF2F5556D4B5E2201F2F5833AA8A0DE80114E2AC2,
	BaseRuntimeFeature_CheckServicesCameraPermissions_mE66D341E7610ABD6F4B656D52621BF49E74A4A50,
	BaseRuntimeFeature_get_ComponentVersions_mD25C18B3A3BBA513072C872B9B562AA74F665A68,
	BaseRuntimeFeature_TryRetrieveComponentVersions_mA6CF7542BA7CB5282F19096B7194CAA8A28E5B1C,
	BaseRuntimeFeature_GetXrLayersToLoad_m71055C2F3D9471E554C6E7FEADED9A649B5F524D,
	BaseRuntimeFeature_OnInstanceCreate_m2E7A018DF8F6FB09F1315733583D4B9C542A8924,
	BaseRuntimeFeature_OnSessionCreate_mE246653F490B2387F8046CF90CA3BC7612BA19B3,
	BaseRuntimeFeature_OnSystemChange_m18702C4A41F1BEC646FF161B6D80799E2E37A967,
	BaseRuntimeFeature_OnAppSpaceChange_mC4140669CBB6F47975D0BBDC7BA455D852CAF9B0,
	BaseRuntimeFeature_SetSleepMode_mE1286B7DBE696D7D30D91486FE52E697743C7A19,
	BaseRuntimeFeature_OnHookMethods_mBBA04EB551F88CB122811D172CF77A08C366E285,
	BaseRuntimeFeature_OnSubsystemCreate_m75F4B3815D7FF70AEF7178F663A02F5AF114FB15,
	BaseRuntimeFeature_OnSubsystemStart_mE9B2341CB336C5441DCEEEEC31C59DC813CF2F03,
	BaseRuntimeFeature_OnSessionBegin_m044260AD7D57EA548B767C7EAA9EE69356D51480,
	BaseRuntimeFeature_OnSubsystemStop_m723B4D27F92254E03EBA8F5DFB527234A3A85850,
	BaseRuntimeFeature_OnSubsystemDestroy_m7E564C461C4E23827A95330B78324B8CE5FED111,
	BaseRuntimeFeature_get_SupportedDisplayRefreshRates_m0D4B82F3E55C7C699E905030BEA48EE88A826BCB,
	BaseRuntimeFeature_set_SupportedDisplayRefreshRates_mB404D572047A1A649194084734ACAD532F88F90A,
	BaseRuntimeFeature_EnumerateDisplayRefreshRates_m9777967216E3539ACC70CEEF41416731EBF5DBC6,
	BaseRuntimeFeature_TrySetDisplayRefreshRate_m0A8C3F6B9E88802F1482931723AADA1ED43C8055,
	BaseRuntimeFeature_TryGetDisplayRefreshRate_mC824B321795D32EB90399E3E713209BB533431F3,
	BaseRuntimeFeature_SetPassthroughEnabled_m3783CB8020C7F575EFFE2B04BD83E4F590B7647A,
	BaseRuntimeFeature_GetPassthroughEnabled_m7C34EE10FB0682415320631CEDF414BA4E2866AC,
	BaseRuntimeFeature_IsPassthroughSupported_mB8D6AE6B16824CFE581287D020A6057CBB46BBBA,
	BaseRuntimeFeature_SetPassthroughEnabled_Native_m855EDAEF625BB3D220FB0907C44D5E0C5E31C306,
	BaseRuntimeFeature_GetPassthroughEnabled_Native_m6C08514C132579230F8A5EA0F9AE6EED320BB905,
	BaseRuntimeFeature_IsPassthroughSupported_Native_m8012B77828BEFB8FA716F78C65444FF173C75099,
	BaseRuntimeFeature_GetInterceptedInstanceProcAddr_m098961F6EC9A7CADD28C2652EB0602152507D06A,
	BaseRuntimeFeature_HookGetInstanceProcAddr_m268A646EEEE3DB7070678C7440756F05FC18DDD8,
	BaseRuntimeFeature_Internal_CreateCompositionLayer_mFF6C2BC5F96F051C66AC9289BD3834FABB3BC790,
	BaseRuntimeFeature_Internal_DestroyCompositionLayer_m09B34A7656869309C64549BB5609DC8DA64D2330,
	BaseRuntimeFeature_Internal_AcquireSwapchainImageForLayer_mD4B64C324182FCF1229C4CDE44C5DB9374842357,
	BaseRuntimeFeature_Internal_ReleaseSwapchainImageForLayer_mFD9291CBC606E5670D3720AA68B455BFF6AB6BE7,
	BaseRuntimeFeature_Internal_SetExtentsForQuadLayer_m46DA74199B98C36C8E0A83540BD9E4178D2F25BF,
	BaseRuntimeFeature_Internal_SetOrientationForQuadLayer_m3F38E9CD402E4696DAECEDF1497E40386268A829,
	BaseRuntimeFeature_Internal_SetPositionForQuadLayer_mD1EA21F5110C46AC3421BD1E271E896D07421491,
	BaseRuntimeFeature_Internal_SetSortingOrderForQuadLayer_mDF0BD0866453873DC7B87061A4E8B58B2BC67A86,
	BaseRuntimeFeature_Internal_SetQuadLayerVisible_m411FC2BD13D8C64454F7B3B0B6B1D07F7AEAF63E,
	BaseRuntimeFeature_Internal_SetMaxCompositionLayerCount_m176EB7FB380013882B7C8687C9F81A102C147AC7,
	BaseRuntimeFeature_Internal_IsXrFrameInProgress_m40717B9CF9CE4C14DFFAF1FAB6920556F06C46B7,
	BaseRuntimeFeature_Internal_SendToSwapchain_mE3EC340746E263919B83DBF42AE69C2868134E5B,
	BaseRuntimeFeature_Internal_UseNativeTexture_mF8E516712AF68A97C39115A82E2DD872249CEE70,
	BaseRuntimeFeature_CreateCompositionLayer_m767C0565759728F8FC9D9029097438AA9DCB78C5,
	BaseRuntimeFeature_DestroyCompositionLayer_mA9DA86C6DC7962CEE2BB8BDB4A6ED23D2BFD417A,
	BaseRuntimeFeature_AcquireSwapchainImageForLayer_mDED03C3B1F833FE93951BF0B327130176EF976BC,
	BaseRuntimeFeature_ReleaseSwapchainImageForLayer_m7F2E14A66EE9CE2DB059998B5894D7EC3F8E4F7F,
	BaseRuntimeFeature_SetExtentsForQuadLayer_m680F2074E1EAF0BE461B2E54C0F8175610E66905,
	BaseRuntimeFeature_SetOrientationForQuadLayer_mE550DC8EFBFE46B0115D2B487872534AFB58B63D,
	BaseRuntimeFeature_SetPositionForQuadLayer_m1E50F0DF52E0AFFF9C253638818B5AB1380AF35E,
	BaseRuntimeFeature_SetSortingOrderForQuadLayer_mB667FAF600835ADCDCF74F00B00F0764ED32B4F6,
	BaseRuntimeFeature_SetQuadLayerVisible_mF676A1F2482218314B1F78D322A43CCEFCCCC476,
	BaseRuntimeFeature_IsXrFrameInProgress_mF1FDBFAA81A04238720B03701CC12DAA2117D678,
	BaseRuntimeFeature_SendToSwapchain_mD9B1907F3CA116CA60FF2A5CC27525FA156E771C,
	BaseRuntimeFeature_UseNativeTexture_m7C09386B4C58AA2E69AE23F59044A17A89B6EC6E,
	BaseRuntimeFeature_get_PredictedDisplayTime_m432B805A627B7F73C14298F21CE791AA90999DAC,
	BaseRuntimeFeature_SetFrameStateCallback_mF8BB16A7F56755DBDCD00CC062D0F9793FF8BC55,
	BaseRuntimeFeature_OnFrameStateUpdate_m73151720BB25328366DD7B8665017ED420FC58B2,
	BaseRuntimeFeature_ReserveEventIndices_m7E128F18E461729C919F588BBFF1FB821F568346,
	BaseRuntimeFeature_ConfigureSpacesRenderEvents_m9F49135821080BD9CB522FB2F6EFEA8755E7F7C1,
	BaseRuntimeFeature_SpacesRenderEventToEventId_m68A80F9C32366947EB122BAE061B7E5D14877BA1,
	BaseRuntimeFeature_EventIdToSpacesRenderEvent_m2293E714627BD5D0E55F7030B53039C13F0C3D48,
	BaseRuntimeFeature_RunOnRenderThread_m2E8512A10A77075A41C26E119959C00AF6BD3B58,
	BaseRuntimeFeature_RunOnRenderThreadWithData_m02CE392AA2B19CCA6467DCA7CE3B166EDC6E1928,
	BaseRuntimeFeature_TryResetPose_mFE6D23F3563ADB4C5842F2A490F58A78F128D8CB,
	BaseRuntimeFeature_TryStartControllerOnHandset_m381E7B1FA5B6D0B9D287458D523A5B4B286BAEC8,
	BaseRuntimeFeature_get_SystemProperties_m652A5F6215A0A98CFBF7ED3D0E0D9F1451365003,
	BaseRuntimeFeature_OnGetSystemProperties_m0008C660B41FEB9D6D7DB31BE4B904E7FD7DA7DA,
	BaseRuntimeFeature_Internal_GetSystemProperties_m75DD4468BD8C24F0CC18272E45A08C40373F2B23,
	BaseRuntimeFeature__ctor_m5862243FBD9D83BD16F42CBAB40F144F741441EC,
	BaseRuntimeFeature__cctor_m192D0679A110C08B9D5AF38368883AC5C734B164,
	xrSetAndroidApplicationThreadKHRDelegate__ctor_mB3E28FB7C1D36CA3749359DAC92D2EDB8EA7EAD9,
	xrSetAndroidApplicationThreadKHRDelegate_Invoke_m01237F2CB271BF28AF4840F19BFD179A3C5C8FE8,
	xrSetAndroidApplicationThreadKHRDelegate_BeginInvoke_m026432DE5CBBA7789DBCF9753F5AFEBF3F480A7C,
	xrSetAndroidApplicationThreadKHRDelegate_EndInvoke_m5E97AAC1E0FCE2CDD64D9762F2ADA4EE972CCA6E,
	xrGetComponentVersionsQCOMDelegate__ctor_m7F1F188B65DC367C76018B9D38FBBC09F02505C4,
	xrGetComponentVersionsQCOMDelegate_Invoke_mA04F1C043BE1932409F9A0CE774502E7FB1A607B,
	xrGetComponentVersionsQCOMDelegate_BeginInvoke_mD71FD0B1327B0CA788EF2D9C126A24804DB9E907,
	xrGetComponentVersionsQCOMDelegate_EndInvoke_m07847DD2472736948DA80FBB9B49E1C256533C78,
	OnSpacesAppSpaceChangeDelegate__ctor_mC6D65079960A9CE4C02ECD4E23BCB57DEEA0FFDA,
	OnSpacesAppSpaceChangeDelegate_Invoke_m78055D97180AE1ABF747CE02DB02EAECF4A8AAEF,
	OnSpacesAppSpaceChangeDelegate_BeginInvoke_m9094CD843CF3732F9C21DF0C4D9781E9C4CE046D,
	OnSpacesAppSpaceChangeDelegate_EndInvoke_m796A614189EA0FEC407A0A3AF54AAA11C2765DA2,
	xrEnumerateDisplayRefreshRatesFBDelegate__ctor_m9073646BB90FAF5E6E4B4C59A95F0190C9E7079A,
	xrEnumerateDisplayRefreshRatesFBDelegate_Invoke_m5404BDD227B1DE720E53599D02935EFC334C7990,
	xrEnumerateDisplayRefreshRatesFBDelegate_BeginInvoke_mB583E027D48BCACF82585BA75F808945B69C48A1,
	xrEnumerateDisplayRefreshRatesFBDelegate_EndInvoke_m534DCB4C7945B5BBE55F45B04F4B49FED2EAC650,
	xrGetDisplayRefreshRateFBDelegate__ctor_m2230D5188D791BB01A37BD33E3D8CA450D97F51C,
	xrGetDisplayRefreshRateFBDelegate_Invoke_m09588150A6D745E8369B0ECBA251E047A49D37C5,
	xrGetDisplayRefreshRateFBDelegate_BeginInvoke_m3C7B0C8F59A9C1021060D05AB0EE5A9DFD82B9D5,
	xrGetDisplayRefreshRateFBDelegate_EndInvoke_m5CC93D5145B2BBC291D29A1C38B62C45C2DB3696,
	xrRequestDisplayRefreshRateFBDelegate__ctor_m4C8051C233EA8A7AFC31C9A924176F2870A7053E,
	xrRequestDisplayRefreshRateFBDelegate_Invoke_m5DC48E855D90D6DBD325BCC0096AB2D7F0F1764A,
	xrRequestDisplayRefreshRateFBDelegate_BeginInvoke_m733A9F289AEECBEE4B66D2B64E80C311060066C3,
	xrRequestDisplayRefreshRateFBDelegate_EndInvoke_mE4F6CF38A0E873F04737252A5F3D8A53FBDA073F,
	ReceiveFrameStateCallback__ctor_mF6FAAA6AF2ADD39E8529E383FBD4DD2BD504B2A3,
	ReceiveFrameStateCallback_Invoke_m1747202E0B1D6CDEA24E32FA5965696230B85203,
	ReceiveFrameStateCallback_BeginInvoke_m95FAFE536FD1EA1E74194BFEC8C130EF17479843,
	ReceiveFrameStateCallback_EndInvoke_mB9C316BA40064076BCBDDFCCDD9A854806C4A7FC,
	RenderEventDelegate__ctor_mD49A79E2D77C1A3028C3A8342979149E8B2089EB,
	RenderEventDelegate_Invoke_m3FE5ACE5C3FEFAF10767FFCD14D08327CC158071,
	RenderEventDelegate_BeginInvoke_m88233EA08F6BD5B9016D87D4042F21BFAF71704D,
	RenderEventDelegate_EndInvoke_m55DB2894F2FC7C65D8A7BCCC9010E5667C972141,
	RenderEventWithDataDelegate__ctor_m4C4FB74199703D8BD5289B6B81322D06625B7A52,
	RenderEventWithDataDelegate_Invoke_mCA0AAE70F2BB5C82EA2AB2A8F1EC6EB98BE9FFE6,
	RenderEventWithDataDelegate_BeginInvoke_m24EEDBA3A98BF8C006C41FAB2339E164A340BB60,
	RenderEventWithDataDelegate_EndInvoke_mA16A1D76867B803E424A2DB7180753515368D166,
	xrGetSystemPropertiesDelegate__ctor_m47C6B0848979D84E86DB54E0EDD1D12C506730FE,
	xrGetSystemPropertiesDelegate_Invoke_mB16AF1B083AF580C53C9DB4279E6CAA4E1826CCD,
	xrGetSystemPropertiesDelegate_BeginInvoke_m4DEF169081703F16BB569616767922537A29A6D2,
	xrGetSystemPropertiesDelegate_EndInvoke_m215FEEA00DFE12CE059F932D4D8A74AEE9F1B9EB,
	ComponentVersion__ctor_m1398FA8526AA2FC1AA0C9C002AED93F555B3BC8B,
	XrCameraActivationFrameConfigurationQCOM__ctor_mC01CC056B56188B07DCAF655B305D78BE7575D9E,
	XrCameraActivationFrameConfigurationQCOM_ToString_mA33EBA223A3F907709277F3029DB734BAB05D069,
	XrCameraActivationInfoQCOM__ctor_m95201ED05481B95C234EFFA791DB372BDA561B85,
	XrCameraActivationInfoQCOM_ToString_m336B7CD08F1239F2F96F7A195AFE12D2FF72D101,
	XrCameraFrameBufferQCOM__ctor_mB4B112F9286FE81D0A56B047D932AF79CC5D10F2,
	XrCameraFrameBufferQCOM__ctor_mE785229ABB8C9A2FB5A1688229B1AB80B7165879,
	XrCameraFrameBufferQCOM_get_BufferSize_m950D5D3907A1869EEA881A2317C0AD3D7912B7A2,
	XrCameraFrameBufferQCOM_get_Buffer_mF7F72DFD8B5EC6CA89CAE6CA9A912A93794DFC55,
	XrCameraFrameBufferQCOM_get_HardwareBuffer_m0AF3D153BED58C85694CE0952F9291D393882A88,
	XrCameraFrameBufferQCOM_get_Offset_m2A9616A87A8D114D5C6F5B877614803B56612757,
	XrCameraFrameBufferQCOM_get_PlaneCount_m068F55256645F2AFB1FF6D402ABDF7DF8B178A6B,
	XrCameraFrameBufferQCOM_get_Planes_m30C5177D7A5E5D9CA49C2DDC3F43A5593D4A6E29,
	XrCameraFrameBufferQCOM_ToString_m1640A05E0ED21CB8C11B2B3F53551D9A20C1505C,
	XrCameraFrameBufferQCOM_get_PlanesArray_m781AA15B1A8B108136DC68546FBB869BAB0A69C0,
	XrCameraFrameBuffersQCOM__ctor_mFAF9A5A88C5AF80F029B5F89BDF3E3536AA4BB9E,
	XrCameraFrameBuffersQCOM_get_FrameBufferCount_mBB3219FC8C8263519C8D9C03EB5960AD0A7625FE,
	XrCameraFrameBuffersQCOM_get_FrameBuffers_mEF5A67FA8F90E32A6D8FB70D97582CDA20B9BF81,
	XrCameraFrameBuffersQCOM_ToString_m35FC790BEB29947A3C585A1910AEF0CC10DD6D16,
	XrCameraFrameConfigurationQCOM__ctor_m3942FC71C58D9E6031DEFDB639F394D4932A86E0,
	XrCameraFrameConfigurationQCOM_get_Format_m6BAA09753953C3355C176B7D28BA0264C58A9F55,
	XrCameraFrameConfigurationQCOM_get_ResolutionName_mC9FAE99F606993BC7CB1DAC117B8787CD8B7E7DF,
	XrCameraFrameConfigurationQCOM_get_Dimensions_mA4CCECC8E2328C0B5CF51C328C9E49B8CA29C389,
	XrCameraFrameConfigurationQCOM_get_MinFPS_m94122334221AE771D207F98FE47F1FA1B1B92686,
	XrCameraFrameConfigurationQCOM_get_MaxFPS_m98188152C09B0C2283C8ABB89BDF52AE641BA34F,
	XrCameraFrameConfigurationQCOM_get_FrameBufferCount_mAEC0C4D48348E78839296BA0F17D16D1EDADCAD4,
	XrCameraFrameConfigurationQCOM_get_FrameHardwareBufferCount_mAC6A3CE4A06E7BFA5F5DDCE5E1D7F1AE22F4A882,
	XrCameraFrameConfigurationQCOM_ToString_m20BB1EE9D69A31994E54F082BF32C02DD73E8F33,
	XrCameraFrameConfigurationQCOM_Equals_m79BAF59F2489C2FC04698EEA69660B3720E94C16,
	XrCameraFrameConfigurationQCOM_Equals_m6C81D6EF68F4406E509277EDAC64592DF817FD32,
	XrCameraFrameConfigurationQCOM_GetHashCode_m88D0A1632ECF24BE403724F6BE31D41C9BAF5D96,
	XrCameraFrameDataQCOM__ctor_m21472D27A6AD1B4266106153194A8F2F1C95E11C,
	XrCameraFrameDataQCOM_get_Type_mE5BBBBD0E4DFDE0D12419924BB91C99CFB730F58,
	XrCameraFrameDataQCOM_get_Next_mCE6D17EB550A4FD2EE8A6EF399B18BD1600A0ACD,
	XrCameraFrameDataQCOM_get_Handle_m26A480ABB370791A1258A3E102BD9FB3CC74DD93,
	XrCameraFrameDataQCOM_get_Format_m5D209DAFD825E8967B0DE6001ADDCBC71762AA0A,
	XrCameraFrameDataQCOM_get_FrameNumber_m8B4ED328D6E36ED8B61A2919C4C82A6C03F61B10,
	XrCameraFrameDataQCOM_get_Timestamp_mBF90CF8A92D202ECE328A6C19955215D983C509C,
	XrCameraFrameDataQCOM_ToString_mCA5D1225589871DC98C2959C1F2B4B7BF01895D8,
	XrCameraFrameHardwareBufferQCOM__ctor_mB52C3509013DAC2D369350950A83731A07C429CB,
	XrCameraFrameHardwareBufferQCOM_ToString_m6ED5F07B3836FC15534D01364A1CC9B226EA36D4,
	XrCameraFramePlaneQCOM__ctor_m284891F8B4782C344844E1E3B769F8DC14A3F61D,
	XrCameraFramePlaneQCOM_get_PlaneType_m056E8B6DCF863AF69C363C8C9BBC6A17AFAC0550,
	XrCameraFramePlaneQCOM_get_Offset_m8AF8D2AF8562907454F27993C9C9853483FA114A,
	XrCameraFramePlaneQCOM_get_Stride_m090D94F7651FE1AB5559A37739AB90178422290B,
	XrCameraFramePlaneQCOM_ToString_m7F4DC8BA7B9EBEC86627F9DCF8AFD809E17A9D09,
	XrCameraInfoQCOM__ctor_mAE436B7F665A8313BC9A5A24A7159C437775207A,
	XrCameraInfoQCOM_get_CameraSet_mE444C2C329BA9C5F9A14A16310005465DCF958C1,
	XrCameraInfoQCOM_get_CameraType_m4F5E8A91874162DD8331097F58D02F32296FB66D,
	XrCameraInfoQCOM_get_SensorCount_m6AE604B7A97AE6C883978221F11CA564562E407D,
	XrCameraInfoQCOM_ToString_mE8AB2E88F245A4DE1C185D918273C782647F29B2,
	XrCameraSensorInfosQCOM__ctor_m5051C95DC0D6173C0045176AA82A987E2BD79CBE,
	XrCameraSensorInfosQCOM_ToString_m4887306CA657989C0FD510390E019AAF3F383F45,
	XrCameraSensorIntrinsicsQCOM__ctor_mF823C9405A4F3ADE635106439C22625FD54D39BE,
	XrCameraSensorIntrinsicsQCOM_get_PrincipalPoint_mF4C53A48DF15DA4539C81ECB8ABC8331749D2E90,
	XrCameraSensorIntrinsicsQCOM_get_FocalLength_m61659D7731F9C69E4D10B948F15C9F337F64C2D6,
	XrCameraSensorIntrinsicsQCOM_get_RadialDistortion_mEDD0949CEB1ED6CB742D5DD4A5686FA40D38B42B,
	XrCameraSensorIntrinsicsQCOM_get_TangentialDistortion_m649714F8CA4CCF1E0FDCDAFA70392E187E2D5992,
	XrCameraSensorIntrinsicsQCOM_get_DistortionModel_m8E397728AFDA69997EADE3EFD106E384425C5938,
	XrCameraSensorIntrinsicsQCOM_ToString_m1D9A21D03AE7EC2F530FAA9C1D7C80493B130733,
	XrCameraSensorPropertiesQCOM__ctor_mEAFD9FC1E283431CA018AC2F7255C8B57C057AA6,
	XrCameraSensorPropertiesQCOM_get_Intrinsics_m108ABACDFE8D9C6659ED98B336D80F337CBD376C,
	XrCameraSensorPropertiesQCOM_get_Extrinsic_m87075C50E31AA53E33661B75F5347765FFD7AA19,
	XrCameraSensorPropertiesQCOM_get_ImageOffset_mB40F948CF9DB970B4E7FA114ACBC1A90501CE9D4,
	XrCameraSensorPropertiesQCOM_get_ImageDimensions_mA4FE4D7DC903DCE7F160EE064509A3B14209414F,
	XrCameraSensorPropertiesQCOM_get_Facing_mF9019A5010372316F6EB0940630BA144310D778E,
	XrCameraSensorPropertiesQCOM_get_RollingShutterLineTime_mA9A434624C43BC2B0D916E001267235101D41EAC,
	XrCameraSensorPropertiesQCOM_ToString_m0DF439C02486E2353F88E5CE3F0074ADD0A28D91,
	Timespec_get_Seconds_m2E1C0153CD3BEC4805B547A871DBD327ACA911B2,
	Timespec_get_Nanoseconds_mF4FDF78B2ED03D58C3D591FCC414F0DAFDDEB594,
	XrComponentVersionQCOM__ctor_mC929B10383F3D44EFB2300FF17EACE1B6B88C51C,
	XrExtent2Df_get_Width_mA8D6682438E1FDE0E5BBB25F3786D19E79B2CE70,
	XrExtent2Df_get_Height_m8F6D94CCA19BEDE6F61051882F0A6CC9A4CA981C,
	XrExtent2Df__ctor_mA538CCEAF45364438C110B220C205691B6A953E6,
	XrExtent2DfQCOM_get_Min_m94AECAF0A8B83DDDF1D0633F00601745139DAF8A,
	XrExtent2DfQCOM_get_Max_m0A320B33A9969FF1F3CA9A3C5F4FDD1FE0B86B9C,
	XrExtent2Di__ctor_mB29746B125742CB6AD747BA5FE9356AC685D73A0,
	XrExtent2Di__ctor_m1D66F0C237CBCC874FE6E8A9523111C8FDBE6246,
	XrExtent2Di_ToVector2Int_m16AB6984BE425191D978319A34B7A6605F7FF495,
	XrExtent2Di_get_Width_m572E28B2072225975B15815FBD4C751999567319,
	XrExtent2Di_get_Height_mA7B78D20A9F26DDDA1F0A9270350C5752193C5A5,
	XrExtent2Di_ToString_m8BF3362EDF0DFBC8FCDF7F961F5A592EECA5C59D,
	XrFrameState_get_PredictedDisplayTime_m6FBB46748AABE30FFE537C362C2D396B70CDC824,
	XrOffset2Di__ctor_m526ED1D1AE4472C41EC084A8652E4E81CAAC3B75,
	XrOffset2Di__ctor_m6F07B24B95F5D1A6FE46CAA42920C5009112F4AD,
	XrOffset2Di_ToVector2Int_mB7CFEA8B38030163FFDA08995B60F4DF67CC1E63,
	XrPosef__ctor_m3D4E4162D410903083F144466FE9B2DEBB0C21BD,
	XrPosef__ctor_mE7394EBD4A40A09890877E8DECD26C8B6569209B,
	XrPosef_ToPose_mDBAF38DC00B0EE86768DD9CA3031FF569FCB2BD6,
	XrPosef_ToString_mC16B2E1F2C83C928C73B09A09F14CC2B371DCF27,
	XrQuaternionf__ctor_m0D486D3121CBEE2FE399F14876D4FEF9031DBA94,
	XrQuaternionf__ctor_mDA964313A2BBA72BED3617B247255478040C0E87,
	XrQuaternionf_get_identity_mDCB638E9174D71A34DDD4F077CA5D96A51E56499,
	XrQuaternionf_ToQuaternion_m62B17D280617BFDAC4F5F2AB88061F00CBCD5522,
	XrQuaternionf_ToString_m870370BDABD005D8499212D09FA41F153D150061,
	XrSpaceLocation_InitStructureType_m42DB29E751707B06F9BD6F9D572DF0F5B9EC5466,
	XrSpaceLocation_GetPose_m45AE7E0E2ADECD3E177598478CDAD84EC9A6E2FA,
	XrSpaceLocation_GetTrackingState_mC12F5FB6F53F113B8717D98F01ED775F6AA4D536,
	XrSpaceLocation_ToString_mE8131ED4AC6143052739BB3ACAE76EBCA9A80734,
	XrSystemGraphicsProperties_get_MaxSwapchainImageHeight_m043CAFF02E9975B3F693CCAE13D22CE7BEBC9FCD,
	XrSystemGraphicsProperties_get_MaxSwapchainImageWidth_m4F8D3DEF2B72DDF06E14D4C0B17C57EAC01E2000,
	XrSystemGraphicsProperties_get_MaxLayerCount_mE0298316C9D0D531C20B60C83803E03F624EB542,
	XrSystemProperties__ctor_m9D2AC269F6F0BE29D1A4523057F1DC9DE9B9C7A9,
	XrSystemProperties_GetGraphicsProperties_m9AD557E8D5670538BB5050A62B7A0F4210C4CC2C,
	XrSystemProperties_GetTrackingProperties_m285EA2F0A78F1BC576B74AF66054F3EE3DF1C295,
	XrSystemTrackingProperties_get_OrientationTracking_mE950098329F529540C759AD9F44456F48748141C,
	XrSystemTrackingProperties_get_PositionTracking_m507982F3CE3D9ACA6597F6A4CD01F6C99D06466B,
	XrVector2f__ctor_m64BBC3D64D3404C4B8292BB00FB2C9B1AF4B7DDB,
	XrVector2f__ctor_m5FE06D12022775A9D701AAEB7572BAE5B66B8796,
	XrVector2f_ToVector2_m5A4EC5EB1E0D1F204CEFA8AA9FB4BEB42BDD44E5,
	XrVector3f__ctor_mD92D4AEDDCD261C251C659F451A7849DB6FF021B,
	XrVector3f__ctor_mE6DAD65E298C88872C330C0A5EE282384FAE2D0C,
	XrVector3f_get_zero_m9F43EF0518BFF78FB4C210CEF0B3E615987BB640,
	XrVector3f_ToVector3_m67309D501E9415F28F58B064E274AD69ED1B2455,
	XrVector3f_ToString_m6D9D628E0DAD096FD0D3723CFDD7D9E6901A83F1,
	XrHandGestureQCOM__ctor_m914EC3452AF5071A62AD3345CDE434285CF851B1,
	XrHandGestureQCOM_get_Gesture_m4E999DAE36C6797A5493147F9F29E21C2BF9F0A5,
	XrHandGestureQCOM_get_GestureRatio_m5E04D2CCC6BAC25F2E8560EDA3D471B7AE014556,
	XrHandGestureQCOM_get_FlipRatio_m13546D6E39A468FBD1734435D08495951B4F2785,
	XrHandJointLocationEXT_get_pose_mFA78A4F7BBD37CA6280AE3C951BD7879EE89DB41,
	XrHandJointLocationsEXT__ctor_m0D12A0DCC179FDF9DBA8F067C8072E7B98AEC3F6,
	XrHandJointLocationsEXT_get_IsActive_m0A68DDF8900B4CC714AC019BC540E77B6565A5D5,
	XrHandJointLocationsEXT_get_JointCount_mC75E77B048DB6148FB577355FA1DFAF83D2A9267,
	XrHandJointLocationsEXT_get_JointLocations_m56BA8627F5B7CA2BA9ADCDF72EFBC05C5897C3E1,
	XrHandJointsLocateInfoEXT__ctor_mA49C108B1597D5FD8D6995A58B405FA704C2B46F,
	XrHandTrackerCreateInfoEXT__ctor_m6F9563A07C20E6885F51F5A428A3BFFC87BEC97B,
	XrRayCastCreateInfoQCOM__ctor_m68DE7FD4A34534AD378DF65AF8C3C41877EAEB2C,
	XrRayCollisionQCOM_get_TargetId_mD6F97A47FD13AD014DFD78A060A76E54F1B3417E,
	XrRayCollisionQCOM_get_Position_m3F35F66CE759A2D0DEA424B0D3110BCA73E4EADC,
	XrRayCollisionQCOM_get_SurfaceNormal_m20D4DD727EF71A63BAA4D31361A656A5F3EC0CBA,
	XrRayCollisionQCOM__ctor_m4F8A352EAED2EE7176296C9FD3757502B86353D7,
	XrRayCollisionQCOM_ToString_m4488BA0A99E1DC859541D33B4B81114E538855D7,
	XrRayCollisionsGetInfoQCOM__ctor_mE1F7A4E426D9DF4CA741BEFDF35259D0D6C37149,
	XrRayCollisionsQCOM_get_Collisions_mE9E4205A7F22B2E28A60FB3BE42A0ACDE6FBAA7F,
	XrRayCollisionsQCOM__ctor_m3F89E3FFDD2E276BCE00252958C795AB079AB960,
	XrRayCollisionsQCOM__ctor_mCD6DC48F6634CA1C8ED3A63508573D2C0EA877E4,
	XrImageTargetLocationQCOM_get_LocationFlags_m480BE04234F57D42B53BEFCDC02BD0C788D2854C,
	XrImageTargetLocationQCOM_get_ImageTargetHandle_mF4D42C080A4D7CF533223F40CA8341BB67E6A07F,
	XrImageTargetLocationQCOM_get_XrPose_mCF7B716AD6CEF043AFBAB4CB953EDFB4DF1BDD20,
	XrImageTargetLocationsQCOM__ctor_mAD7AAE03A06E2ED5A1C422DD04FE6B27A0676C8D,
	XrImageTargetLocationsQCOM_get_Count_m845436280D51DE2910F71B75077223B3D4655A8A,
	XrImageTargetLocationsQCOM_get_ImageTargetLocationsPtr_mED494AF493F7E1A5965D0145B54F93C2819C5C78,
	XrImageTargetsLocateInfoQCOM__ctor_mE60D4F7B12C7CEDD3BF35171287E3B5137D5B44E,
	XrImageTargetsTrackingModeInfoQCOM__ctor_mF21A1217D975F220BAD61ED0454064ACDAE49768,
	XrImageTrackerCreateInfoQCOM__ctor_m4EEBF84E77A1E1E4FFE770E46FF42B6CA1378295,
	XrImageTrackerDataSetImageQCOM__ctor_m13420B9F18019E201E47AFEA44CC0C4E0680CD1E,
	SceneUnderstandingMSFTPlane_get_VertexCount_mD2485F6B795486C4566D9AAC565ED9A3AB1E254A,
	SceneUnderstandingMSFTPlane_get_IndexCount_mF0C611B76CADD2CCF2DADCB2330D9A2F14E1D2ED,
	SceneUnderstandingMSFTPlane_get_Pose_m589F165643A3D65CB002F5395D1922CA863D2F0D,
	SceneUnderstandingMSFTPlane_GetBoundedPlane_mEAC3E52128A7CFBE6A532673FC2E1E57BFFFCE8D,
	SceneUnderstandingMSFTPlane_XrScenePlaneAlignmentTypeToPlaneAlignment_m180136E8B349E8D6BC1875951E36D52D9AED277E,
	SceneUnderstandingMSFTPlane_GetTrackingState_mBF8451F916683716463C2EA604514E6659DC600C,
	XrPlaneConvexHullBufferInfoQCOM__ctor_m6216FC2AD864CF71BFB322AD153B49D0BE188AE7,
	XrPlaneConvexHullVertexBufferQCOM__ctor_m06D87B8A0B035C3D199FEC800009BE173F6CDB97,
	XrPlaneConvexHullVertexBufferQCOM__ctor_m8F17950F6A9F35AFA3A8C8D5EECA6F682F99C212,
	XrPlaneDetectionCreateInfoQCOM__ctor_m09D4C9EF97EE2904489A20E7E5150DA4972315EE,
	XrPlaneExtentQCOM_get_ExtentX_mA43B232DF1EB85716DB7915E867FF9EEF779FF14,
	XrPlaneExtentQCOM_get_ExtentY_m40E8D4402F3F490F0ECEC4A753F2AF888F1B352A,
	XrPlaneLocationQCOM_get_ConvexHullId_mECBD3FEFFB2D85508052401E65354E90B98862A3,
	XrPlaneLocationQCOM_GetBoundedPlane_mBFBE48AE22AF36F0CCAEBE6ACA62AC355CCB490B,
	XrPlaneLocationQCOM_XrPlaneTypeToPlaneAlignment_mC084EB647B85BCD37B18C319F795F52243480165,
	XrPlaneLocationsQCOM__ctor_m0732504CABB07CE7FB531E5DE2F3A0E1B6702BA9,
	XrPlaneLocationsQCOM__ctor_mF62762AC161282C374C6990DC8677B01CC2D978C,
	XrPlaneLocationsQCOM_get_PlaneCountOutput_mB291268D3ABA521FB809141A8D6E534397FC25B3,
	XrPlaneLocationsQCOM_get_PlaneLocations_m9F42475FF18EF1BBA4B7F9D64C0ED19621C6E40E,
	XrPlanesLocateInfoQCOM__ctor_m4F811F5724DA7A6A1E903B5C1B0981C0D4311FBF,
	XrMarkerDetectionStartInfoQCOM__ctor_m4BE31B1DB8305A22473EBBB39C37C540DB57BBCF,
	XrMarkerDetectionStartInfoQCOM__ctor_mCA884E7DE29D3C9B6369B613FD8822B9283F7C76,
	XrMarkerSpaceCreateInfoQCOM__ctor_m50AB7485BB052A1E3266D4BEC03E50F774D1F935,
	XrMarkerTrackerCreateInfoQCOM__ctor_mD2466D57219B40F3A51FAFE73F2D05140B47F604,
	XrMarkerTrackingModeInfoQCOM__ctor_m2D680306B638728344DEB235C70995F87617D623,
	XrMarkerUpdateInfoQCOM_get_Marker_mA80BD87CFDB08692508EA0594A5CC1B895139E6C,
	XrMarkerUpdateInfoQCOM_get_IsDetected_mA1F9B8849E6EC7B8CF992E59EE8B71ACF45AAD11,
	XrMarkerUpdateInfoQCOM_get_IsMarkerDataAvailable_m2F97E271C4DE68664F3438431623232E0245A48F,
	XrMarkerUpdateInfoQCOM_ToString_m44B04704D107F400CF1C8CB036786C33628FF00C,
	XrQrCodeVersionFilterQCOM__ctor_m6114340545DFA05ED9E671F6CDB0E7E9863E71EF,
	XrQrCodeVersionRangeQCOM__ctor_mEE8968742EDBB3354417F01840242F4B8989C9DD,
	XrUserDefinedMarkerSizeQCOM__ctor_m8CFE8D7BAB694A5A0068761AE3D3276216E63573,
	XrSceneCreateInfoMSFT__ctor_mC36C9038DD64C3A1DA6F45FAC007F5EBA27D0582,
	XrSceneObserverCreateInfoMSFT__ctor_m0AFC5A1189B4C5E151A642B0AACBD5E235E5815F,
	XrSpatialAnchorCreateInfoMSFT__ctor_m30BEA3DC5CBAF77172B5CF78783DC2A0A4441DC6,
	XrSpatialAnchorFromPersistedAnchorCreateInfoMSFT__ctor_m51AE46AEA9B4290FBC9288833FC4BE71C8C52B68,
	XrSpatialAnchorPersistenceInfoMSFT__ctor_m597F42F50F6786918CF94BA946517678E9E2557B,
	XrSpatialAnchorPersistenceNameMSFT__ctor_m79DF56CBF47872031AD00EFF1B94980060D679C8,
	XrSpatialAnchorSpaceCreateInfoMSFT__ctor_m0E7262C30F18E0E1FE14CBC5E9F2DEDBE55EC70E,
	OriginLocationUtility_FindARSessionOrigin_m3AF4E6E957AC451789BF220EF690736EC9530BFC,
	OriginLocationUtility_GetOriginCamera_m566BBE932D4C000B9B4506F8BB27BA2A405C3BB7,
	OriginLocationUtility_GetOriginTransform_mBB5C1CF2C4E110BC79E404697A0FA421E5E89D1E,
	SpacesCompositionLayer_get_Extents_m7D22498E3E1A67A21473BEA9E8A52767C64F6AFA,
	SpacesCompositionLayer_set_Extents_m5FF4E7B0000A7105D29C13E636F251EF629C0732,
	SpacesCompositionLayer_get_Orientation_m893DF5F09722685C070784634183FB04CC5C10E0,
	SpacesCompositionLayer_set_Orientation_mA127A24AF6702E73875906A13F709C954AA23EA6,
	SpacesCompositionLayer_get_Position_m229352ED364ED8F36072ED1498920C04B6C00BF6,
	SpacesCompositionLayer_set_Position_m0C69A4C88D209DA1427927A635756045A5DC1D7B,
	SpacesCompositionLayer_get_SortingOrder_mCEE7BF222A1D7CB097F9221B2FF80D41DA9B9800,
	SpacesCompositionLayer_set_SortingOrder_mAD2E85E5BABC8A02099F740B6CDC8C44360E507E,
	SpacesCompositionLayer_Start_m1A41283AA0032D1E08E1A0E61C369047CE45208B,
	SpacesCompositionLayer_Update_m949315224C41DEC945FB3ADB68476E099E1D3D30,
	SpacesCompositionLayer_OnDestroy_mBF85D8B1091AF5FA38BF39059BAF167A3CCEBB20,
	SpacesCompositionLayer_OnEnable_m41EECA50376A34A014CE1665CD2497690C0AB988,
	SpacesCompositionLayer_OnDisable_m0ED2D57D401CC0D1C32AACCF903003350279AD90,
	SpacesCompositionLayer_UpdateSwapchainImage_m41E4DA32588A6EBA476FD38CFFD21015C104C351,
	SpacesCompositionLayer__ctor_m6D0F713F753770453F14E01E1A650DD757C46826,
	SessionSubsystem_RegisterDescriptor_m21D41FBC86C2542A21A99DADA960CE2CFD0DEC00,
	SessionSubsystem__ctor_m56974216D0C275EF099478A6F6CD0C75D218E8C7,
	SessionProvider_get_UnderlyingFeature_m8BFCA562F555F14AB3F40729E76510FFF6A9D9B0,
	SessionProvider_get_requestedFeatures_mC84B1F22FA2355F292CF94E09F929ABEC641EFBF,
	SessionProvider_get_currentTrackingMode_mFF13C8CD14E3B49C2152600C9A92B7DF4A4DDCB5,
	SessionProvider_get_trackingState_mDEF8C84DE1249D10DAC9D2EE96DE9A4BAB689C7C,
	SessionProvider_get_notTrackingReason_m156956A942B544501D1E7E5633F57481EA85E022,
	SessionProvider_get_requestedTrackingMode_m2C746BECD2B8BF5B176848180B6A9D8F4B0A7CEC,
	SessionProvider_set_requestedTrackingMode_m3A41CD10877FA32752CF02E863E55958F401665A,
	SessionProvider_get_frameRate_m8D9AE50F5BA715087477FCD704E079265A56E836,
	SessionProvider_get_matchFrameRateEnabled_m6EC1EBD8390BE410055BA13DCD57862ADA6C0D44,
	SessionProvider_get_matchFrameRateRequested_m635B7F938F8EB9EE50964BEB225FE7CB6152CC0F,
	SessionProvider_get_nativePtr_m3ADDC71B66003C1E65A5F3A2538AE9271E2EB404,
	SessionProvider_get_sessionId_m3A87F6A02956795034B78324C747F81664979F66,
	SessionProvider_Start_mB43616AF993473E91CF477050149C93B59788795,
	SessionProvider_Stop_m689DC7870976C02C63F4C744887987281C77D1A7,
	SessionProvider_Destroy_m7C7B2C3F30972D1E784137714AABC925586444A2,
	SessionProvider_GetAvailabilityAsync_m04A788ACC06E5E1189CB4DB543068FE70C28B891,
	SessionProvider_GetConfigurationDescriptors_m4760DE44B4549C0A22A654F9D2706F5F7D07648E,
	SessionProvider_OnApplicationPause_m3C449DA54CCC29E8D31681837CB0F411B7D78432,
	SessionProvider_OnApplicationResume_m23950128290C2986ECC4671E3A1C8158AE24B395,
	SessionProvider_Update_m1E28C28457FFB01DB725A2CC8601794DEF0E02F7,
	SessionProvider_Update_mD5021ACDFC52F2111A180487E64A1C36E039C32E,
	SessionProvider__ctor_mCE4EC57FC85B9CDCF128AC96254FFE8A4ACA7CED,
	SpacesMicrosoftMixedRealityMotionControllerProfile__ctor_mBAA2E7F31C033C6BC4FFB3E5652051FEBAADDAA1,
	SpacesOpenXRFeature_get_SessionHandle_mC1D5AE58DB4A5326847DA40475D9459B6AF58F16,
	SpacesOpenXRFeature_set_SessionHandle_m2547BA850C8F2FA59F494FB70DC6206863E53611,
	SpacesOpenXRFeature_get_SystemIDHandle_m833A27758B6E2A903DF370165AC24FB22063CBE3,
	SpacesOpenXRFeature_set_SystemIDHandle_m853677837093C9A032AC2F8D3DFE946B239142F2,
	SpacesOpenXRFeature_get_InstanceHandle_m98BC1FEA822EF6AD1CD03957C407B03BFD18DB72,
	SpacesOpenXRFeature_set_InstanceHandle_m7D88CD0D833F481C64B699A98415A6E1A98B8BD4,
	SpacesOpenXRFeature_get_SpaceHandle_m3A8C3EA515461E2D805C79E15857736C89CF87D0,
	SpacesOpenXRFeature_set_SpaceHandle_m5A396CC5EE54CF9B5307E88314EFBDA1F7E54402,
	SpacesOpenXRFeature_get_IsSessionRunning_m3E3825F16B149E2CD593D0F41FAA4BA9B27BB602,
	SpacesOpenXRFeature_set_IsSessionRunning_mC98A5F7BD3450123D352A22A9C8519313B7AE7A7,
	SpacesOpenXRFeature_get_SessionState_mCAAD8017A68C7AD0C34E965264CB9B2205260221,
	SpacesOpenXRFeature_set_SessionState_m51D6547022685508F73BC705203549EB720A9481,
	SpacesOpenXRFeature_get_IsRequiringBaseRuntimeFeature_mB400EDEA312EC59600570A7BEC183E8EBF41F25A,
	SpacesOpenXRFeature_OnInstanceCreate_m0324D4194E2B85DE1F841E6F07FD59E585DAB336,
	SpacesOpenXRFeature_OnHookMethods_mADAD2B6D5635468C58B238E4BDB2BB04435DDA14,
	NULL,
	SpacesOpenXRFeature_OnInstanceDestroy_m8DA872D6621843E73068E413321DFC5D3FAA7C0C,
	SpacesOpenXRFeature_OnSystemChange_mE4534AD73928172B278C4075AEBE7E317A287B42,
	SpacesOpenXRFeature_OnSessionCreate_m36B81205F47352B84F2039C94CE58DBBB66A1804,
	SpacesOpenXRFeature_OnSessionBegin_m411839095F3AE8D29887EDC4C5699A11CE49DD22,
	SpacesOpenXRFeature_OnSessionStateChange_mAA634FEFFA96C3ADE35A80EE2CC13BCE241D6751,
	SpacesOpenXRFeature_OnSessionEnd_m64496EF1F09BB07A63E450C4A937B311ECE320DD,
	SpacesOpenXRFeature_OnSessionDestroy_m8491E3AFBA1497289FD669B13C583C0F33BC9A59,
	SpacesOpenXRFeature_OnAppSpaceChange_mB3B954691F79C6A6CCD5C2E900EB445696E7AC87,
	SpacesOpenXRFeature_GetMissingExtensions_mC574B76CDC275762A5368CF9214ACA2DF6722361,
	SpacesOpenXRFeature_GetXrLayersToLoad_m630E60FE96BDE7EBEBA65D88D62018F22183DC07,
	SpacesOpenXRFeature_RequestLayers_m14C99CB5811B2F000D19FD2AE2BCDD2866FF5E88,
	SpacesOpenXRFeature_HookGetInstanceProcAddr_mE588877A52024E62D2CD74CA17CF53ECE83C33B6,
	SpacesOpenXRFeature__ctor_mFC1AC4D00ABFBCF3DC87141BF5ECEB49DCF5EE16,
	GetInstanceProcAddrDelegate__ctor_m25E509979A33A25930863C6370A9666AA73A01B8,
	GetInstanceProcAddrDelegate_Invoke_m668AB68A61FA880E785ED4F7530758B41EC07EE5,
	GetInstanceProcAddrDelegate_BeginInvoke_m95F0D72B853C1066279BF14847406FD09DA383B4,
	GetInstanceProcAddrDelegate_EndInvoke_m5089FB47BF5023C18040B1BCC505A6B4C02A3BFC,
	U3CU3Ec__cctor_mF07EDA2CB995EB25E08A9935950461F11124ADBF,
	U3CU3Ec__ctor_m04A2E59D537938A5B110707BFCC60699BA1A3F6D,
	U3CU3Ec_U3CGetMissingExtensionsU3Eb__40_0_m31B4109FBB1D9F176565640DC73E1A3AB72B9205,
	SpacesRuntimeControllerStartController_StartControllerOnLoadIfNeeded_m159D565E88598EA914BDC4C26449D0F44727FE3B,
	SpacesRuntimeControllerStartController__ctor_mB1DC08674CA6FA4063912094695A641F7DE2C018,
	SpacesThreadUtility_SetThreadHint_m107730E58D99683DE911A325E742D3344A38C5BF,
	SpacesThreadUtility__cctor_m5764964D0E2EB7A82ED47F428E7286E8940454DB,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SpacesHand_get_RawJoints_m544EB999467A240FA9277DCF7A2E220EB1A4C88E,
	SpacesHand_get_RawGesture_mCE3F3F78C6EDCB9A94E8489D1851835760285C8C,
	SpacesHand_get_RawGestureRatio_mEE8BAC489DCC60E59920ED7270B0120341406EB3,
	SpacesHand_get_RawFlipRatio_mD1A89190D4DC1A04E18BD6ED30F099E39162DAFC,
	SpacesHand_get_Pose_m6CCFDDE3CC1ADCD75B96E6603E2B6BA382EC57D4,
	SpacesHand_get_IsLeft_mAB2A45AC1D2AF3F5651C2BE77C0770B7BF83FC9A,
	SpacesHand_set_IsLeft_m8E935112F7F569DC9BB70FF2AE68FEE3EC6C8D33,
	SpacesHand_get_Joints_mD1BD591CE21E448A34ECB7D230AA3E4D9F220A35,
	SpacesHand_get_CurrentGesture_mF057BEF3C8F94F6363C8C5EEF3599BB9FBFC94E9,
	SpacesHand_UpdateHandData_mF7C271A6927FED4968F9D71A3AF83089CC06DE94,
	SpacesHand__ctor_m62DF4B810F8C76315B4CD19C2005BDD6F21E3C72,
	SpacesHand_U3Cget_JointsU3Eb__22_0_mCF099BDBA6F8E8F148AF47F943652829950F48ED,
	Gesture__ctor_m9084C4B3F4AFEA3CF5B0AE49703302FB5BFB06EC,
	Joint__ctor_mBE6F7B02F1E804F90B610CF06FD3FF690A19BF2E,
	SpacesHandJointVisualizer_Start_m5AC47EC132E9375C30B6EB547DC6B064FDA1A884,
	SpacesHandJointVisualizer_Update_mA4518625708E39A034764C1EDC629D329F1810EB,
	SpacesHandJointVisualizer_UpdatePointMatrices_m90827F2ECA108610E91E16528708A8AD88B68BF5,
	SpacesHandJointVisualizer_GenerateJointMaterials_mEAC404E760A3CC1B909EE51045E9DF4C1F09F12D,
	SpacesHandJointVisualizer__ctor_mF84F73B099771C771F6B8837CE063200573E557A,
	SpacesHandManager_get_LeftHand_m7BA685E844D5EBD46B7361C5D866FB2AB69A2E1C,
	SpacesHandManager_get_RightHand_m21B9409CA29D5849141275176A9AE8E39130D224,
	SpacesHandManager_OnEnable_m1FF12441396846E6E1EB0DB5F1E554B393E7C4CA,
	SpacesHandManager_OnDisable_m2C83507516D5C38C0BDDEC49E1210EA3EBC4A4D3,
	SpacesHandManager_add_handsChanged_mD9284C78D38987BBD1BC026B22DFC14DD3D7EE18,
	SpacesHandManager_remove_handsChanged_mCF9B04EC0C73A9D83B92CA7195CBD424B0E95C49,
	SpacesHandManager_GetPrefab_mA5C972A5DA04CCC0FB3B0ADFA2F8FD308195847E,
	SpacesHandManager_get_HandPrefab_mE659206C2FD1C9BE01BBD74F250DECD5B5B305C0,
	SpacesHandManager_set_HandPrefab_m73A1B2174B941A3F206615DDA82396EB4AE2F709,
	SpacesHandManager_get_gameObjectName_mF86D809FD29F9B0A8CF64FD9650C29CE7AB8725A,
	SpacesHandManager_OnAfterSetSessionRelativeData_m916BC689D6A1AC3D1CBC3623B7B1C5A274A143C3,
	SpacesHandManager_OnTrackablesChanged_m47703D9B3489D9919F2F798B80F2DC70E1981C22,
	SpacesHandManager_GetSpacesHandByTrackableID_m2D52CFA509B044E461D6201A7CB31C24325B580D,
	SpacesHandManager__ctor_mFFD875DAE979195409DE2C5D179EFA9C69845592,
	SpacesHandsChangedEventArgs__ctor_m6DB86EF5C7C21BB5071A61BDB3472D1FAAF79D81,
	SpacesHandsChangedEventArgs_get_added_m5CB059B37808F89770348F83E634B8357734B23C,
	SpacesHandsChangedEventArgs_get_updated_m8285564FB82F9C9BD7CB0520D8FA6A9058104939,
	SpacesHandsChangedEventArgs_get_removed_m027BB7F2A0FB42472DAFC362C3749ACFC0D72E07,
	XRHandTrackingSubsystemDescriptor__ctor_m6A7417B3EC99DF9A6AC20FB1E6C43F04D5A30C33,
	XRHandTrackingSubsystemDescriptor_Create_mA81A0552179791150D5D28E6DFD0AE696EDEFEEA,
	Cinfo_get_id_mF3EBDAF7A344E71CBB5E82700C5B72F97FB69D5B,
	Cinfo_set_id_mDC05FE6B201A56946C2C5EA912F8216DF9A09C92,
	Cinfo_get_providerType_mA77251D3E258944822919C88EFD9B644664CF25A,
	Cinfo_set_providerType_m6B9926F0E4230B88C381C91D2F5F42EFED493D6E,
	Cinfo_get_subsystemTypeOverride_m46BA874D2F7FE887BC89D8FC6E5E63687BD0934D,
	Cinfo_set_subsystemTypeOverride_mCF7CF4A13208D788BAAE060D96FAEF41723FA9B0,
	Cinfo_GetHashCode_mD14AB2A03A219CED499DA7FF91AD90C29C13B117,
	Cinfo_Equals_m374DC4526F1EC61A7F2A14FF9F7FF6051CA32FFD,
	Cinfo_Equals_m7965F4DB417B97CB3972F630D2196E57284A4EA1,
	Cinfo_op_Equality_mF468220505403D83DD69F4F81B517C0C4B71E7CD,
	Cinfo_op_Inequality_mD6C46DCB653C29AE0AF754322D7C5C6C592F49F6,
	XRHandTrackingSubsystem_get_LeftHand_mF272692C87894D3C83F768FC4DAD8E16D8ACFEBE,
	XRHandTrackingSubsystem_get_RightHand_mC5BBA0D2603AAC0EEB14B07B7592035D9BD51CC0,
	XRHandTrackingSubsystem_GetChanges_m55263EFE2FE55F8D95E45BB5844EDE502AC3F0D7,
	XRHandTrackingSubsystem_GetJoints_mE66AEC15FB670E17B41C294396760CA50365E886,
	XRHandTrackingSubsystem_GetGestureData_m6803D1AE165A48AE94E9046797CE8A27D80E342C,
	XRHandTrackingSubsystem__ctor_m1F7AACF6AEED9D559DF1BF0FB38271FB96526CBF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Provider__ctor_m476D4EFE62E17401E1719C334C3E62D7C5614673,
	XRTrackedHand_get_trackableId_m405042E716232C8EECA276E8639D53F5466E195D,
	XRTrackedHand_get_nativePtr_mA695B53C1EE10B8B922A4E44F0FA37CFCE54E59F,
	XRTrackedHand_get_pose_m45569784BDCB04B40CE717EF6043C8DCBF268BB9,
	XRTrackedHand_set_pose_m4413C8A35AE5F773C992984623AEB3A6439C431E,
	XRTrackedHand_get_trackingState_mE860352EC4FFF755F28F51A797A474E9088C20E9,
	XRTrackedHand_set_trackingState_mB9D9B72BF2CA0EDD55B61B57A359D0088B653946,
	XRTrackedHand_get_defaultValue_m90F48B65C04253A63533D38D24D555CDEB9F2626,
	XRTrackedHand__ctor_mB934D2C6BCF136E9648DCB30C91207C8A19BAF86,
	XRTrackedHand_UpdatePoseAndTrackedState_m8D1B046B1613AF64B1688BCC8EBCC1501CF02E7D,
	XRTrackedHand_GetHashCode_mD23C58F566A3F049FFA8A5185042B3D85BD6486E,
	XRTrackedHand_Equals_mDB882CA9828980C6256982565930FAF1D4D0FC29,
	XRTrackedHand_Equals_m2C2C8454E4B205407EE5AEB340B8BDE90F8D9CD2,
	XRTrackedHand_op_Equality_mD758154746E37ED7E21A319BDD7B107C090B0461,
	XRTrackedHand_op_Inequality_mFD2AC8F6E1AC49D8B2DDAEA1B135EE2CB11ED2BE,
	XRTrackedHand__cctor_m8BE1E52FE86DD5F79BC0A9A439EB86D82C16D2C5,
	Hand__ctor_m886B03765B8042D9AC645817A840261A434AB863,
	Hand_get_TrackedHand_m04A54DCF4C6E348BAB8A6CD28838E9802C2DC79F,
	Hand_get_IsLeft_m14715A87F11DC8A4F0DE69811CD281989FC2AAD5,
	Hand_UpdateTrackedHandPoseAndTrackingState_mD85E0376157A8E24FA59145034E092D2A4D407AE,
	HandTrackingFeature_get_LeftHandTrackerHandle_m27933C675B381FA36E3616CBFC1000B507011412,
	HandTrackingFeature_get_RightHandTrackerHandle_m315EB6F090E153A69CCC519F0D685E68D50C7439,
	HandTrackingFeature_get_IsRequiringBaseRuntimeFeature_mB326A6D81048B512B216AE537E8AED9BF6DC427C,
	HandTrackingFeature_TryCreateHandTracking_m777594DD56B93690AC1AA1FA81934FA78B3DB886,
	HandTrackingFeature_TryDestroyHandTracking_m9839DB0C9B1405AF21FAE72C7C707BC8104AE040,
	HandTrackingFeature_TryGetHandTrackingJointsAndTrackingState_mAEBD552052028C4AA788848BA07D0CAF03EE2A2F,
	HandTrackingFeature_TryGetHandGestureData_m26531FF64B238A49E12977EAA53309D777FDF088,
	HandTrackingFeature_GetXrLayersToLoad_m939F79631D7B42DD039C5ED45AAFE79575CF0335,
	HandTrackingFeature_OnInstanceCreate_mA6997DFAADB739A01676749FBDFFF56E31EBE55F,
	HandTrackingFeature_OnSubsystemCreate_mD12D1203D61AADCC28226A5DC53F61A888E52551,
	HandTrackingFeature_OnSubsystemStop_mD9A492686555BD030D3FC9C8A9EA8CDCBE729E52,
	HandTrackingFeature_OnSubsystemDestroy_m019FE6C89072E39EAF803C16B85A8C3EA3CEF10B,
	HandTrackingFeature_OnHookMethods_m083B3F17DFB51A1662DEE9A87C969FFF31289402,
	HandTrackingFeature__ctor_m1921FE7112D46A931BE0B6396CAEA0D0920A8D09,
	HandTrackingFeature__cctor_mD7F4DD3ADA5F4663246E3CBCC355C82B11B4F8D8,
	CreateHandTrackerEXTDelegate__ctor_mBDED34AF2C5E6B0159D92AA58265CAE533270C28,
	CreateHandTrackerEXTDelegate_Invoke_m0166EB069A0C522178FE85BB2EFB8CFE543E62BA,
	CreateHandTrackerEXTDelegate_BeginInvoke_m0E3F209332A9348C7CAB8BA6F4604535F437CF4D,
	CreateHandTrackerEXTDelegate_EndInvoke_mC73C93020F7CFC2BEF105983D92E33D0799A9D57,
	DestroyHandTrackerEXTDelegate__ctor_m70EF03DF1097600A3287F9F61EC60C646747FB1D,
	DestroyHandTrackerEXTDelegate_Invoke_m7D6BFEC4D471B367C795F3BBAF654E53A3636A0B,
	DestroyHandTrackerEXTDelegate_BeginInvoke_m9E203DED6B0C0612130AEFC9E8DA2E91622033B1,
	DestroyHandTrackerEXTDelegate_EndInvoke_m478EF12CD8D73B78D5426A4E203D51D00764E554,
	LocateHandJointsEXTDelegate__ctor_m426D8A6EBB6A4ADCEDC50860DAF1E11988245081,
	LocateHandJointsEXTDelegate_Invoke_mB665BBC2725F498FC707D05B237144C7032D1765,
	LocateHandJointsEXTDelegate_BeginInvoke_mF230988C68505AD980F01C4E6B48671AC28D01D4,
	LocateHandJointsEXTDelegate_EndInvoke_mAC90C1861DA3C7DD96870BCEA836C63BD87224BD,
	GetHandGestureQCOMDelegate__ctor_mE3B70F480F217F0FB1F4DBDB1FC158121E07A62D,
	GetHandGestureQCOMDelegate_Invoke_mB10DF6E2E81D137300AEC6637098A879AFA93468,
	GetHandGestureQCOMDelegate_BeginInvoke_m8787295EC47D8462E24AA0B92ED614DB4087A2BA,
	GetHandGestureQCOMDelegate_EndInvoke_m02319277D64E8008B42F31CD642C0EC7C60A0EC9,
	HandTrackingSubsystem_RegisterDescriptor_m8B11B6873B30AB0D9C40638048E2CC2D5F3A0642,
	HandTrackingSubsystem__ctor_m7CCA2433969577B6CA40E2B0ACAEE1E495C3F4F7,
	HandTrackingProvider_Start_mAAA202FA527096DA2197C0835272406721641A09,
	HandTrackingProvider_Stop_mFE8404A6AF9190D08F0B8C2D779F2C5EF3CF2EB9,
	HandTrackingProvider_Destroy_m44257B7CFDD86DDB87DDCCBD6B859603CC77C083,
	HandTrackingProvider_GetLeftHand_m83EF37A390EF7332E2A3CC48E2498302ED1607D6,
	HandTrackingProvider_GetRightHand_m11C5E2581EF168131E557F3369AD3EE552110281,
	HandTrackingProvider_GetChanges_m0380DD4CCA681365935D3AB9260FA77E4A7EF356,
	HandTrackingProvider_GetJoints_m351BF63ACDBE727F2798524CA8F612D05153E134,
	HandTrackingProvider_GetGestureData_m95BA08035CB1EB4B2E4A013819FDC65BC33C7841,
	HandTrackingProvider_UpdateTrackableChangesLists_mB8E61058FBB680B684DE822C8FDFCB423BBBE622,
	HandTrackingProvider_GetHandByTrackableID_m7E5D026A6F6AF9074C08AF78852CCFD615E4A070,
	HandTrackingProvider__ctor_mD09760F7CAD17CA9CACDDB02572D367962CD7F4C,
	HitTestingFeature_TryCreateRaycast_m4CC5CD495CC4A6D7B2F38B8989D050856F922498,
	HitTestingFeature_TryDestroyRayCast_mF0ECFCCAAD5754076AA26461F3118545139529C3,
	HitTestingFeature_TryCastRay_m927963921B88DAFFD9209A34380032E200CD5B3D,
	HitTestingFeature_TryGetRaycastState_mDBD0A24867D3FA1DD8B9AFBD41A3217E1746A73B,
	HitTestingFeature_TryGetRayCastCollisions_m16245933E0BB8E187AEC92EEDDEFD8B999E5A0E3,
	HitTestingFeature_GetXrLayersToLoad_m5D5973DFAD5039C64F7943787662E676E3CA0228,
	HitTestingFeature_OnInstanceCreate_m66BF0A2872E658881CB2DC46759B3B2DB66EC9C8,
	HitTestingFeature_OnSubsystemCreate_mD6A76C01006981E20D0DBC9A39D58EA3B7AB6214,
	HitTestingFeature_OnSubsystemStop_mA83AB75209B2A2EFBA4BD773DFCFFE7939F59A6D,
	HitTestingFeature_OnSubsystemDestroy_m4F5DCB1BAD5A1378F9A73489D289F51E21DF3972,
	HitTestingFeature_OnHookMethods_mCD5A94ABD5C04EF74FEB55C3E7334FD087857330,
	HitTestingFeature__ctor_mFC95DE1B03F9810F1A0F667B2C8C087A1F4FFD5F,
	HitTestingFeature__cctor_m0A4EB4B31DEBD1CF0893E12153829B9B5635E293,
	CreateRayCastQCOMDelegate__ctor_mBADC967FEF8927310FDC404718D802066A68E07C,
	CreateRayCastQCOMDelegate_Invoke_m56D851C5C155033E96E25EB49587F35261B758CF,
	CreateRayCastQCOMDelegate_BeginInvoke_m2F276F588CDAA3EB72442281A8189BF5CCBF98CC,
	CreateRayCastQCOMDelegate_EndInvoke_m30D0B0E85BF2AAA744CF9B9B2149A0F607BEB24E,
	DestroyRayCastQCOMDelegate__ctor_m7779C706A348F0AE1622014A4AD60D096953421E,
	DestroyRayCastQCOMDelegate_Invoke_m5143AA4ED07E7AA518138643889EF80726142FF3,
	DestroyRayCastQCOMDelegate_BeginInvoke_m416E5FDB9A9E56FA9D472CFF0732580AF9199A87,
	DestroyRayCastQCOMDelegate_EndInvoke_m5C5B2AAC3EAB8BBC3D731D975901AEAED39A5070,
	GetRayCastStateQCOMDelegate__ctor_m890ADCC3DEC6E734A2400193C3CDC5821C758C6B,
	GetRayCastStateQCOMDelegate_Invoke_mDF5D67B33BDD7473D3A55BD6FEB5CF75A87DB4B9,
	GetRayCastStateQCOMDelegate_BeginInvoke_mA0870309116A2B5F403C6B4DD76F9F5B936CB67F,
	GetRayCastStateQCOMDelegate_EndInvoke_m846E6E83880C0026AF13FF085EBD471D66D2D018,
	CastRayQCOMDelegate__ctor_m3603222D3D26A5F8DC8B901F97D0B979BA06FFC6,
	CastRayQCOMDelegate_Invoke_mEF7C630D95133A322B633308F19EC9123484B236,
	CastRayQCOMDelegate_BeginInvoke_m63C38219870FE8A2BE1009F2E3547BC93D5FC749,
	CastRayQCOMDelegate_EndInvoke_mF1F7CF1B4851B4B485A036D7B76F7FFBAD20EFF5,
	GetRayCollisionsQCOMDelegate__ctor_m1B84EE2D51AEC28AFB698B63546310F97DB1F1D0,
	GetRayCollisionsQCOMDelegate_Invoke_mA4E9EF3F5168E19DDE1247E5588C3625ABF0988E,
	GetRayCollisionsQCOMDelegate_BeginInvoke_m25EB96755E83E2258E8F687DF23F77A6604FF059,
	GetRayCollisionsQCOMDelegate_EndInvoke_m77EECA673FC942BB118DB217CCC0E787AEDCEAC0,
	Raycast__ctor_m85C28C9BB9A6D0452CEFFA28F93BE95A5C01C723,
	Raycast_get_RaycastHandle_m150062E9E9408387F402F293D9E0DF26D33681E2,
	Raycast_UpdateSubsystemRaycastHitAndTrackingState_m0209D6C600876E81B397344716E38F8CBE4BC281,
	RaycastSubsystem_RegisterDescriptor_mB1115F523113341648C73B5227FF565093EC0D29,
	RaycastSubsystem__ctor_m71E7DA59900B4124BD552A575FE467A546D21136,
	SpacesProvider_Start_m68BE4A5FBCB109BF5A21FB6D996A4BFB7AB1775A,
	SpacesProvider_Stop_mA2BB2621D231E2ECDFCA4ADDC77766898FFCA7E8,
	SpacesProvider_GetChanges_m84994B408C43B06C1ADB8BACE0DD09ECEB0DAC67,
	SpacesProvider_TryAddRaycast_m65FC2D8EA4AEA837E693FEC6A32A415A4229CE7D,
	SpacesProvider_TryAddRaycast_m300F37AB683C9456199106EE58211CB931D79617,
	SpacesProvider_RemoveRaycast_mC5EB4903DD128C2A88DE17CC7C669F73C491F898,
	SpacesProvider_Raycast_m8DABE12051EE74AEA79E74B850001D7CC9F3BFDF,
	SpacesProvider_Raycast_mD5E5EC09EB8B2D26E9CA30DE3C5E8DC2DFBAF70E,
	SpacesProvider_Raycast_m7D8C27EE5C48DB56611D2508A1BA6BDD209DBAF9,
	SpacesProvider__ctor_m2C561B6817C303928C9DDC541432E57991FF0479,
	SpacesProvider_U3CGetChangesU3Eb__7_0_mE9611E02B3F12C541C6E369B4E2847728C2A8C5D,
	U3CU3Ec__cctor_m4061F1D8FFC187F8F3272DB895BE505F4553B949,
	U3CU3Ec__ctor_m75E81460C97997805AD6998DECBD7F7E490F92B0,
	U3CU3Ec_U3CGetChangesU3Eb__7_1_m323ED3342AF28E61260B21D453EF73CDAA3976A5,
	U3CU3Ec_U3CRaycastU3Eb__13_0_mDEF1F86ECBD8925633C304AE2C8D638DFCC422BB,
	U3CU3Ec__DisplayClass10_0__ctor_m715AD5A6B6A2FF9A8BA9B6876612FC544E7A73B1,
	U3CU3Ec__DisplayClass10_0_U3CRemoveRaycastU3Eb__0_m0904BF865D137B932A04DA8303C594256128E613,
	ImageTrackingFeature_get_IsRequiringBaseRuntimeFeature_m3C2FD478747B8CA287E76D90C28B627AA424AD71,
	ImageTrackingFeature_TryCreateImageTracker_m2789D5E699FCF96C32CB35D78DF440B7B5B14D17,
	ImageTrackingFeature_TryDestroyImageTracker_mA1FBFE7603566186D79A87A246EA7B77EFE32F33,
	ImageTrackingFeature_TryLocateImageTargets_m9B421381A531F5394343B033A88F210F53D34BD5,
	ImageTrackingFeature_TrySetTrackingModes_mCD227C964BBCA9BF7DD22A097FC5824E48A10B89,
	ImageTrackingFeature_TryStopTrackingImageInstance_mE2E27200BCF44D7581729E05D4BBA1D470AD4BDB,
	ImageTrackingFeature_GetXrLayersToLoad_mA314D0DB21D26234C9A070F30FB27BDD7AE56B8E,
	ImageTrackingFeature_OnInstanceCreate_mFF667D56FC59EEB9B7B103E08479912E6064BCEF,
	ImageTrackingFeature_OnSubsystemCreate_m3524696660A957CF0747875B9333F2BFB27B7C6D,
	ImageTrackingFeature_OnSubsystemStop_m0F9DAA8C5BDE6584A8CEE99DC139A8DCB7E9F8C7,
	ImageTrackingFeature_OnSubsystemDestroy_m292AAE8349E8FB65D6F23182D2D1CF7AE981EA15,
	ImageTrackingFeature_OnHookMethods_m2B6DC4E8363299A85F3A884F1DF23E06017561C9,
	ImageTrackingFeature_TryGetImageTargetNameAndId_m273E7F5CFF2F6B5279344A123ED14CBF5470F38E,
	ImageTrackingFeature__ctor_m62C31C25F537753FACBA13205F8D7392FF4E2FFD,
	ImageTrackingFeature__cctor_m3F40BCEE3B529CF5E7D3E22AB85036A7190AC680,
	CreateImageTrackerQCOMDelegate__ctor_mAFF1E22C6A9DAEF53A028BFA208DC61C14C4F9FB,
	CreateImageTrackerQCOMDelegate_Invoke_m35EBD8EACB34F7C4429F097275D8D0C50BA475EA,
	CreateImageTrackerQCOMDelegate_BeginInvoke_m2145D7073DDBE366F34EE1F002D16E94DB943AFF,
	CreateImageTrackerQCOMDelegate_EndInvoke_m7EABF8D41C4445762A65ABC52A2EA1ADFCD0FBF0,
	DestroyImageTrackerQCOMDelegate__ctor_m79828BB34A1A60886A6DCDF6961853DB09833CA1,
	DestroyImageTrackerQCOMDelegate_Invoke_m613D4AD60B1F7AA10218E48FFE2287995B47D400,
	DestroyImageTrackerQCOMDelegate_BeginInvoke_m4607FD3F4A2C7C81C6265F8B1FEAB7600EEBBB73,
	DestroyImageTrackerQCOMDelegate_EndInvoke_m43A6715735EC80E3ECBD60B11B339AAD1AF34865,
	LocateImageTargetsQCOMDelegate__ctor_mB5EE196D99031FD1DE058163A9F614B5EA246BF8,
	LocateImageTargetsQCOMDelegate_Invoke_mBB8EDC45B99E7719BDBF7458078240E6E47ED247,
	LocateImageTargetsQCOMDelegate_BeginInvoke_mD31F484459B806438CF7AFC038EFD5F8456E522F,
	LocateImageTargetsQCOMDelegate_EndInvoke_mE90605EC915012D4659CFAA6048E633CB8199990,
	ImageTargetToNameAndIdQCOMDelegate__ctor_mE7C5B28B1EF524CB45978E3A119AC9CA39774972,
	ImageTargetToNameAndIdQCOMDelegate_Invoke_mF51A1C27ED55239BB7136EDEEAFC537EDEF4D8A8,
	ImageTargetToNameAndIdQCOMDelegate_BeginInvoke_mE1E80CEB249799543646C771EC3CEE026C190E17,
	ImageTargetToNameAndIdQCOMDelegate_EndInvoke_mB7D54587BFC4F8B4AA352F54C93A5452D3FC4E9D,
	SetImageTargetsTrackingModeQCOMDelegate__ctor_mCFD2F582080B591BDBABBD9C34BAA602CBA11A33,
	SetImageTargetsTrackingModeQCOMDelegate_Invoke_m0DA827C7E2C065D2898C1E0AEDEC7263C3EE2B90,
	SetImageTargetsTrackingModeQCOMDelegate_BeginInvoke_m9121B2B5669C4B2A4A904E8F89193B40ECC35D01,
	SetImageTargetsTrackingModeQCOMDelegate_EndInvoke_m202708EA2BCBC0B0ACC6AAC2B92B2E1EA5DFBE38,
	StopImageTargetTrackingQCOMDelegate__ctor_m3E87DC2AF1AC6A725E1846F70178D4FD338016A7,
	StopImageTargetTrackingQCOMDelegate_Invoke_mC8D344E629A2E7877B74CF9F7DD7893288F04903,
	StopImageTargetTrackingQCOMDelegate_BeginInvoke_m5FDEBEDE33F7062412C471C4895B2E950BB57CCD,
	StopImageTargetTrackingQCOMDelegate_EndInvoke_m6C84212EFBA892C405AAECFD86BBEDA4E3F1FD94,
	xrSetTrackingOptimizationSettingsHintQCOMDelegate__ctor_mD82909EB40A2089625217F1DE9004F74532FDF61,
	xrSetTrackingOptimizationSettingsHintQCOMDelegate_Invoke_m30946D76A1F17070E365542C63107628A71E80D0,
	xrSetTrackingOptimizationSettingsHintQCOMDelegate_BeginInvoke_m326E1C78F195EF075D1E00C582C25BC14B459FAA,
	xrSetTrackingOptimizationSettingsHintQCOMDelegate_EndInvoke_m581B788B52ED69BA9E2B0302F7E53322793D33EB,
	ImageTarget__ctor_mF8F8356081A3924D9A612BAC4E69DD6407125344,
	ImageTarget_get_ImageTargetHandle_m8B024478A9FEC94D96370C30C5C4A8E16CCD2526,
	ImageTrackingSubsystem_RegisterDescriptor_m25BF3A48E0FA55CF91D1A32C14915FD164BC67BC,
	ImageTrackingSubsystem__ctor_mB992192934593491E9718496208397B83E1E7BF7,
	ImageTrackingProvider_set_imageLibrary_m8FCBA3A261C767A5809A4CA69397B5027CD760AC,
	ImageTrackingProvider_get_requestedMaxNumberOfMovingImages_mC6101B30AD239CE69C630E3DD438725C75F301E1,
	ImageTrackingProvider_set_requestedMaxNumberOfMovingImages_mE6B06CDC17E4C5C87BEC566F1BDCA6E2B533AA93,
	ImageTrackingProvider_SetInitialTrackingModesDelegate_m3EA517120C03F8CD7859B883A90BD037601212D6,
	ImageTrackingProvider_Start_m6EB3BE6261E546DC76B69D5C70FE2F75713DDDFD,
	ImageTrackingProvider_Stop_m59A70BFEF66EBB008F8FAE672CB5F38F0A8C5673,
	ImageTrackingProvider_Destroy_m68D5BDE28623DAECF7DDC1D137D97E3108C44DEB,
	ImageTrackingProvider_GetChanges_m3CD1045C43478CF0635FD4B268DA855A48688DD0,
	ImageTrackingProvider_CreateRuntimeLibrary_m0F0F3FBD372190BC093D236C41A88B7471B06B7A,
	ImageTrackingProvider_SetTrackingModes_mDCB8C03418F44CF85D4CEF95F7920A238E3C68F5,
	ImageTrackingProvider_StopTrackingImageInstance_m40DB0200AA7BB39F38292C3E4AF3F9F40BFC95B5,
	ImageTrackingProvider_DestroyImageTracker_m81CB3B1EB22896ED0966110A0146CAA39CCFBA5B,
	ImageTrackingProvider__ctor_m4E216C6F6FE05DC958C54FA47118184A8D6185C6,
	SpacesInitialTrackingModesDelegate__ctor_mE18A4E0DB16EFCC00C324F3C601E284AC07AE6D9,
	SpacesInitialTrackingModesDelegate_Invoke_m303EA9CF25A27315CB9581B38067C80FAD02CF95,
	SpacesInitialTrackingModesDelegate_BeginInvoke_m92312DDD06AA3DA834AF882BF502FDE4FFC32D3D,
	SpacesInitialTrackingModesDelegate_EndInvoke_m8B7A37EACB4ACE1396555C3C5B7F370DD60706D7,
	U3CU3Ec__DisplayClass17_0__ctor_m52EB6D29C868C4EDC8016116ED55A7495CEF93C2,
	U3CU3Ec__DisplayClass17_0_U3CGetChangesU3Eb__0_m0264AC86208267E995C2A7D15D0520A7385F1E60,
	SpacesReferenceImageConfigurator__ctor_m4410BBC07CAC93F74D79B060BAAB1F63554A444F,
	SpacesReferenceImageConfigurator_SetTrackingModeForReferenceImage_m0242A3EFDEC316DCA1BB15C8DAC371B57875A054,
	SpacesReferenceImageConfigurator_GetTrackingModeForReferenceImage_m09197189143FBD6F6D6F64EA0EDE68EDAB76F4FD,
	SpacesReferenceImageConfigurator_HasReferenceImageTrackingMode_mB5246242B4A9F24755FE0443AAB93FD1ACAE4C23,
	SpacesReferenceImageConfigurator_StopTrackingImageInstance_mB0DB1D3E1AD229CFABBE6CC3A24240C4E554F093,
	SpacesReferenceImageConfigurator_Awake_m89E72694CAD6060567AC93E58A6B416412810352,
	SpacesReferenceImageConfigurator_OnEnable_m3155BC669BA2169F8914E4F8A67E01AD61B6740F,
	SpacesReferenceImageConfigurator_OnDisable_m5EBBCBD998A1A0C102E7A22BE0A313B846C7EC5C,
	SpacesReferenceImageConfigurator_GetSpacesReferenceImageTrackingModes_mDA5907A261591A4942A8F48679F88DD31B687A7D,
	SpacesReferenceImageConfigurator_CreateTrackingModesDictionary_m5F201E1517DB9397E71C41DFB0BE3B6876E8672A,
	SpacesReferenceImageConfigurator_SyncTrackingModes_m04DC7A3BC1B2DDCCFB47EBC8F340DA4AB0A1D297,
	SpacesReferenceImageTrackingModes_get_TrackingModes_mA83E8E306AB35C0378BD98ADE7C32F1CDFE78407,
	SpacesReferenceImageTrackingModes_get_ReferenceImageNames_m5318079BDB7282EAD230DB70495BE91E69E8ACA7,
	SpacesReferenceImageTrackingModes_get_Count_m28B919AAFED09118FE1700F8F8BCD8B32BB7BB7C,
	SpacesReferenceImageTrackingModes_SetTrackingModeForReferenceImage_m845EBD79551814F0EB100FDDEA8B4656C9E932B3,
	SpacesReferenceImageTrackingModes_GetTrackingModeForReferenceImage_mACBFB49F5F673B4F8315EB676A70377D60AEDCEF,
	SpacesReferenceImageTrackingModes_AddTrackingModeForReferenceImage_mBF822E91F35AB0B900F9BD86461EDA310F527D65,
	SpacesReferenceImageTrackingModes_Clear_mE6FF623097421F07C73FD1CA2B73F00C559A8981,
	SpacesReferenceImageTrackingModes__ctor_m8BFFB3446D29F552720458D147BA66C471431E5F,
	U3CU3Ec__cctor_m79BAB9ADCE65159C4E100C21B7A31F1BCEBC6331,
	U3CU3Ec__ctor_m3E47C3801566370F30181FAA10C8E8EB72F9E39E,
	U3CU3Ec_U3Cget_TrackingModesU3Eb__3_0_m491E8D8DB5485FAD1B20129927CA39A9C052D27B,
	SpacesRuntimeReferenceImageLibrary__ctor_m3F89591FF0533B5D4FBF9D438FCAF4C1C6E4795F,
	SpacesRuntimeReferenceImageLibrary_get_count_mF5AFFF5C071D471A88FAD153D5858FCE2ED836DA,
	SpacesRuntimeReferenceImageLibrary_get_TrackingModes_m32718E8E8319EC8EF29251E7F15AEF1CA022E287,
	SpacesRuntimeReferenceImageLibrary_GetReferenceImageAt_m541CDF5911CF59DD7A754C763E80C62362A32CC5,
	Plane__ctor_m624025168CC3E02B83D8D86E49A7378A7E1A5B13,
	PlaneDetectionFeature_get_IsRunning_m724E2F5E2BDEAF699D7D66D0E3DAD009C0AFB83D,
	PlaneDetectionFeature_get_ActiveHandle_m3C8D4DCC417FDE787CF69D9176E0D5B49AB64DAF,
	PlaneDetectionFeature_get_IsRequiringBaseRuntimeFeature_m7BED5FE720939C6946BEDE930E46318441CCF2BA,
	PlaneDetectionFeature_RegisterProviderWithSceneObserver_m206F16A9A221592872C85BA14916D84822936EF5,
	PlaneDetectionFeature_UnregisterProviderWithSceneObserver_m45BAE9E976C1B9C6B133EC528534C99F5C6300DA,
	PlaneDetectionFeature_TryCreatePlaneDetection_mF96FC4CBCFA40145A759F0F9C3DCD7129B6A397B,
	PlaneDetectionFeature_TryDestroyPlaneDetection_mA338E0C7A385EBD6F1434C6D1FC0464EFAFBEB8B,
	PlaneDetectionFeature_TryGetPlaneDetectionState_mB8FC960D8225DC7BD7A434A03C70D2C8E898BF75,
	PlaneDetectionFeature_TryLocatePlanes_mDAF0F8B9CBA2E34290B7032A735B22E1EC3A15E9,
	PlaneDetectionFeature_TryGetPlaneConvexHullVertexBuffer_mD791EC812BF9F3EF28A749046AA666E571585654,
	PlaneDetectionFeature_GetXrLayersToLoad_mDF778FE60B2C87140B6E1F4E48177E82D0FBD2B2,
	PlaneDetectionFeature_HookGetInstanceProcAddr_m8A395F34C39AF5354F3AFE5C14BE47F72455F0FB,
	PlaneDetectionFeature_OnInstanceCreate_m948262FB32FF8F8D3B7A0D1748610455B1DD851C,
	PlaneDetectionFeature_OnSubsystemCreate_m2D4D7E73739DBEB7D250C9F1ACE01BD60C69B9FE,
	PlaneDetectionFeature_OnSubsystemStop_mFCC743FCE9BBFD309376422C4B2B5346A9A6061F,
	PlaneDetectionFeature_OnSubsystemDestroy_mC7AF55B6DB7FBC2BC00C2A47378332CE07C9EBEC,
	PlaneDetectionFeature_OnSessionCreate_m4677AF75D0C91A2B789CC85337F39437217731E2,
	PlaneDetectionFeature_OnAppSpaceChange_mCAAF2ED6C588DDA7E587D171A60B2A416157E8E2,
	PlaneDetectionFeature_OnHookMethods_mAD7AF3BA4D6653F15047F7D5C1A90EA2C6DBCFAF,
	PlaneDetectionFeature_PlaneDetectionModeToXrPlaneFilter_mB9DDCDD121FA3000A601111554EEE291FDC33A5D,
	PlaneDetectionFeature_TryDestroyPlaneDetection_m9E61874FE52F4F9B17ADED68294546F4FEE3D8AC,
	PlaneDetectionFeature_TryLocatePlanes_XR_QCOM_Plane_Detection_mE6F2259760C0F8D8E882C30DC57C08C8CBB42516,
	PlaneDetectionFeature_TryLocatePlanes_XR_MSFT_Scene_Understanding_m2A042287592C33F4667713350A68237DA6220D5C,
	PlaneDetectionFeature_TryGetPlaneConvexHullVertexBuffer_XR_QCOM_Plane_Detection_m431C85F8BCA2CA8DA5F26BE8AFE2B020EB190A4C,
	PlaneDetectionFeature_TryGetPlaneConvexHullVertexBuffer_XR_MSFT_Scene_Understanding_m37F2F5E8DE2C35F073FF0AACEF38689441005E59,
	PlaneDetectionFeature_Internal_GetInterceptedInstanceProcAddr_m3F8DCC1D22C5E2B1191BA4F1B2831F52EA906D55,
	PlaneDetectionFeature_Internal_RegisterProviderWithSceneObserver_m6295EF004F4A9A63F25EAD750E181AAEACDDBB2D,
	PlaneDetectionFeature_Internal_UnregisterProviderWithSceneObserver_m4232C00DC01AE74A3954ADE5AAE2E64392E6385D,
	PlaneDetectionFeature_Internal_SetInstanceHandle_m97D11A910168074A27C3993C2AED177207DD081E,
	PlaneDetectionFeature_Internal_SetSessionHandle_m2E973742A3557E57E1EF6A243B983BC36870CAC2,
	PlaneDetectionFeature_Internal_SetSpaceHandle_m667C24EC6F3249ED612E59D59C289B75572D2F12,
	PlaneDetectionFeature_Internal_UpdateObservedScene_m60BDF83AAC662AFA5CE294A868414C71732B4B1A,
	PlaneDetectionFeature_Internal_UpdatePlanes_mB5C6E31305AF9C52679CDC39DF73E8854951F7C4,
	PlaneDetectionFeature_Internal_CountScenePlanes_mC44344257A838ACB452F18D4324DB056E4D5B0AE,
	PlaneDetectionFeature_Internal_FetchScenePlanes_mA1172900D4C945B844D5BB14A7BB04492728604C,
	PlaneDetectionFeature_Internal_FetchPlaneVertices_mC83F40A3A02E086B1965A8EAD7604549A49981D6,
	PlaneDetectionFeature__ctor_m00401DAC5A416FF44A5E3019D559F01E80DE5F8E,
	PlaneDetectionFeature__cctor_mED7C4202C607EE6A945FD53BEEC478D857FCE8F0,
	CreatePlaneDetectionQCOMDelegate__ctor_mB2C7CBE05B5A45FA9840027FF86C3487436083FB,
	CreatePlaneDetectionQCOMDelegate_Invoke_m3CA30D087B044DE49E4041F22AFD5A446645B99E,
	CreatePlaneDetectionQCOMDelegate_BeginInvoke_mA5DC65DD79DC01113AF6B1DA02F026F70D7B3F29,
	CreatePlaneDetectionQCOMDelegate_EndInvoke_m246081FFD89A2E855944A07513AFBE7358DC3E71,
	DestroyPlaneDetectionQCOMDelegate__ctor_m7D6585803C599BAB923DB82A1B8E3C8DEA2FF876,
	DestroyPlaneDetectionQCOMDelegate_Invoke_mB618169C5F7111BEC19C6EC3FEFF6638E1808BD4,
	DestroyPlaneDetectionQCOMDelegate_BeginInvoke_mE8E73E91281D2532EE13B9C4BBA831D94E241396,
	DestroyPlaneDetectionQCOMDelegate_EndInvoke_mBDD5BD6E0DEEF3FD6F932366FC4DA9A4C5200E90,
	LocatePlanesQCOMDelegate__ctor_m0CB02B6450B41A7DA7A33935229B19E7C2183E58,
	LocatePlanesQCOMDelegate_Invoke_m81A54466376ED434AF05996625217A08531C9DE4,
	LocatePlanesQCOMDelegate_BeginInvoke_m25818A863263158E92218FFF702DCFF395C4786F,
	LocatePlanesQCOMDelegate_EndInvoke_m833A09822CA03181FDFB6A6169EC691EEADD6E69,
	GetPlaneDetectioNStateQCOMDelegate__ctor_mE10FFF5FF68CAE39038AD7AFAD2F66D73852E89B,
	GetPlaneDetectioNStateQCOMDelegate_Invoke_mA0B7C2C21F6DE7FFCCC41FE02A2BC77E21D260AA,
	GetPlaneDetectioNStateQCOMDelegate_BeginInvoke_mD13188ACA6F54A5BFD1F2D49EA1E78E1ACF83C56,
	GetPlaneDetectioNStateQCOMDelegate_EndInvoke_mB7CFBD07DB5F43F02E6AB8F89E08A8105F0CB7B6,
	xrGetPlaneConvexHullVertexBufferQCOMDelegate__ctor_mFCB35CFFB01211D1C5A21E814A0244BBE16968BF,
	xrGetPlaneConvexHullVertexBufferQCOMDelegate_Invoke_mC1B520F874517FFBF2D8B6B34D2D399A3CE7405C,
	xrGetPlaneConvexHullVertexBufferQCOMDelegate_BeginInvoke_mF9D93AFA0E9F825000A7CA718DA1FC7B3EBF0383,
	xrGetPlaneConvexHullVertexBufferQCOMDelegate_EndInvoke_m6AB3C5D03C80B3E56BB2655F108A317A0FBA9EBA,
	PlaneDetectionSubsystem_RegisterDescriptor_mA1E0DD69226518C08B6762A45DDB1CD6D58FCFCF,
	PlaneDetectionSubsystem__ctor_m525B3A4D3BF7D9A3B5686F90EEA0418434461D2E,
	PlaneDetectionProvider_get_currentPlaneDetectionMode_mC9977178B505A22EDEC4A5E42543935A08473081,
	PlaneDetectionProvider_get_requestedPlaneDetectionMode_mCE8F55A534E4CD840D229F210E3EBF33A78CFA0F,
	PlaneDetectionProvider_set_requestedPlaneDetectionMode_m3F00ECE73CD3D09EEC2C77BA2E8DF3E8A42EB576,
	PlaneDetectionProvider_Start_mCF97C528FC1A89C30470F56EC59EFC5EA2444A90,
	PlaneDetectionProvider_Stop_m4F40AC8864F92F715415AFD8D42387E8A0350315,
	PlaneDetectionProvider_Destroy_m3026F6D36807A10EA73450044E5858DB8496370E,
	PlaneDetectionProvider_GetBoundary_m8A8A50994C9B1FAF3E27FCC69B7F695FEBD3EA3D,
	PlaneDetectionProvider_GetChanges_m2724C838BD41ABC0C5BF94C17D54CE20A53EA89C,
	PlaneDetectionProvider_DestroyPlaneDetection_m82BA92DDC4F1855B63F078F6C29C8014FA70124C,
	PlaneDetectionProvider__ctor_m6FA2FADCC4342D9D432E3732D3E46B4C2322C2E0,
	SpacesARPlaneManagerConfig_get_UseSceneUnderstandingPlaneDetection_m4302156616E8CBE64F67FC9E802CB7D088270CDF,
	SpacesARPlaneManagerConfig_get_ConvexHullEnabled_m662D27769D6D6457F53B7CEA70FD9FA662D530EC,
	SpacesARPlaneManagerConfig_set_ConvexHullEnabled_mD127A565545FF2027412718AC96E5182E2075BDE,
	SpacesARPlaneManagerConfig_Start_m110CCEDE46F8AEF65BEC892A61B609AF7AEFB8CF,
	SpacesARPlaneManagerConfig__ctor_m1EB2A9A28D086D1F980D6BD31AEFEE85ACBB2F33,
	SpacesARMarker_get_IsMarkerDataAvailable_m4C3AF7319044E1043EA3D3FF1571ACC5A258D858,
	SpacesARMarker_set_IsMarkerDataAvailable_m857846F9A845C23939CF7052863B80A5DE36200D,
	SpacesARMarker_get_Data_mA936C9D42FC586DB7E05EB163D21ECB3C0860375,
	SpacesARMarker_TryGetMarkerData_mE93EE7CB056B099083196D1B0706828570EB4928,
	SpacesARMarker__ctor_m2EAA402583058633B8E7B05ED2324643A47EFD2B,
	SpacesMarkersChangedEventArgs_get_added_mF8667E8CA2C87E2FE20F475940ADBC0722D97C9D,
	SpacesMarkersChangedEventArgs_get_updated_mD1F9474086262DB5B1CED69A4F5AE73031151FF9,
	SpacesMarkersChangedEventArgs_get_removed_m243A7EDEEFB7C0F520132232D090A991D5221A07,
	SpacesMarkersChangedEventArgs__ctor_m2381BC40792947230BFA24DF519ACB5F0AE230BE,
	SpacesMarkersChangedEventArgs_GetHashCode_m1D18494D3716BE3E5832F5FDDFDA0A336FF9FE63,
	SpacesMarkersChangedEventArgs_Equals_mC9647B04CB0822A03BFDED271897C3CA02C04F58,
	SpacesMarkersChangedEventArgs_Equals_mF42762FD75B2E3C660788392BB1A734F1EEEE5AA,
	SpacesQrCodeManager_get_markerPrefab_m2D616CA4634DA3492B7A5B91BB07D100E8B9702D,
	SpacesQrCodeManager_set_markerPrefab_m50141CDA7439521D7704917DA44C53BCAE0934B9,
	SpacesQrCodeManager_get_MarkerTracking_mCAFCCAD60F723C05731EAC9661265F283C8900DC,
	SpacesQrCodeManager_set_MarkerTracking_mAD411B7EBF0590D82C31667BC45E865CB47DD21A,
	SpacesQrCodeManager_add_markersChanged_m4AE1F8A5A1455D25C06EC91078266C670264B604,
	SpacesQrCodeManager_remove_markersChanged_m04ABDBE2277EAE175DF4DCFD07EE28031919DF50,
	SpacesQrCodeManager_OnEnable_mEBF0C6DD3CBD2BBFE2F96B5E6FE04C22A7E2D7BE,
	SpacesQrCodeManager_GetPrefab_m9E5B3A91354FB4B97AC04057909BDA809984D375,
	SpacesQrCodeManager_get_gameObjectName_mD10FAADC2C2357BB8EC4AEC75467683C8E87096F,
	SpacesQrCodeManager_OnAfterSetSessionRelativeData_m7DBFFD00549BE6F7D4A915682753D546543CC528,
	SpacesQrCodeManager_OnTrackablesChanged_m5B62FC19C590D1A0FBA9E1F8FB761A0512269DC4,
	SpacesQrCodeManager_OnValidate_m81E2B01FE0F6FD0A8213F681F21CC7596BA0476E,
	SpacesQrCodeManager__ctor_mE3DF2C046967AED54FB649925FBF26356241982F,
	QrCodeTrackingFeature_get_IsRequiringBaseRuntimeFeature_mD8FFDF7A1A2D0F1D2F12D2EEFCF868E43DC3BDA5,
	QrCodeTrackingFeature_OnInstanceCreate_m82AC67109269A34395C5D57215DD37DD11B2239D,
	QrCodeTrackingFeature_OnSubsystemCreate_m485903D4BAAF20A25A726EA134CA91EDFB7B79EB,
	QrCodeTrackingFeature_OnSubsystemStop_mA069F4A8B109F578997CEB76A63CC76BB01A2EE3,
	QrCodeTrackingFeature_OnSubsystemDestroy_m193E2C7CC911AD3F5232BFFA9ABB29A949EC4742,
	QrCodeTrackingFeature_OnHookMethods_mF1A8DCDBD8FB3C2792A2C0C98FC7006D80A0AFDB,
	QrCodeTrackingFeature_TryCreateMarkerTracker_mC3BCC1B1A96813C52C0776E6237F10CDD213FE12,
	QrCodeTrackingFeature_TryDestroyMarkerTracker_mA1E075863E3F92D14A80ECEEFE436F1FC2994FA5,
	QrCodeTrackingFeature_TryStartMarkerDetection_mF831B44E708F62644DF1A399E9C5CBE15A7634B2,
	QrCodeTrackingFeature_TryStopMarkerDetection_mCFCF3B16CABDDEE2433EC54718855B2DFB97AAD0,
	QrCodeTrackingFeature_TryAcquireMarkerUpdate_mF22F98C63A3232E4DAE8F9B35B9E73C2549E4A8B,
	QrCodeTrackingFeature_TryGetMarkerUpdateInfo_m8EE25E3BBCC69B900DB002C84821B4A63276C0C1,
	QrCodeTrackingFeature_TryReleaseMarkerUpdate_m9B51441F524584A01A14A117A4C493DC4F42EB26,
	QrCodeTrackingFeature_TryCreateMarkerSpace_mDAE482BF8D6013D93445B9CD49A6EA014A150CCC,
	QrCodeTrackingFeature_GetXrMarkerTrackingMode_m5B1B74EAC4D5CD4DF5D21172ED8878D94AAEA25F,
	QrCodeTrackingFeature_TryDestroyMarkerSpace_m3E67C327002397308932F4EE885B739423970C09,
	QrCodeTrackingFeature_TryGetQrCodeStringData_m9112F2AE93302EDCA86A75616FF1AF50148FBF72,
	QrCodeTrackingFeature_TryGetMarkerPoseAndTrackingState_m8469D93D4E485C2A8E1F737EA8E45998F8927E92,
	QrCodeTrackingFeature__ctor_m5F055F4AE3B2AE2E4E214841AE6F2CF766280D5B,
	QrCodeTrackingFeature__cctor_mEB7DD28C887D524234AE54D7E884EF858BA43362,
	EnumerateMarkerTypesQCOMDelegate__ctor_m12989DC6D923B9FC5175B3F6FB43A2E650F15576,
	EnumerateMarkerTypesQCOMDelegate_Invoke_m782A87B094DB02E53BBBFAAD0A1484468F695160,
	EnumerateMarkerTypesQCOMDelegate_BeginInvoke_m5CEF67EA85E30235BBCF64D1C3BD71403C4D9403,
	EnumerateMarkerTypesQCOMDelegate_EndInvoke_mDC2152D1FC31EC7CB4792AAD79B00F04EED7212F,
	EnumerateMarkerTrackingModesQCOMDelegate__ctor_mE62BD4FF2EF254D4419F767070A8472D6E88ABCF,
	EnumerateMarkerTrackingModesQCOMDelegate_Invoke_m9B340CF09E30174136E6B92A298C12D2108FED37,
	EnumerateMarkerTrackingModesQCOMDelegate_BeginInvoke_mD829652DC9171245E346038A8588276271170DB2,
	EnumerateMarkerTrackingModesQCOMDelegate_EndInvoke_m8CEDB0ADCB8DF63F3D5047BC06DAD1203E56BEA6,
	CreateMarkerTrackerQCOMDelegate__ctor_mD7A7614BB00F55789A7AC9111109946E36D50E03,
	CreateMarkerTrackerQCOMDelegate_Invoke_m1B07B6C5A5A655BDEA59F9C704FF4C35B1FD967E,
	CreateMarkerTrackerQCOMDelegate_BeginInvoke_m2C8BEC07D9D2F3D6434DDA76B38F09363EBBE69A,
	CreateMarkerTrackerQCOMDelegate_EndInvoke_mA99684C12B112EDD3ED0B79BC6CCB05B452162A5,
	DestroyMarkerTrackerQCOMDelegate__ctor_m0C1929CA659511A815962DEF0AA9C022E43BFEC6,
	DestroyMarkerTrackerQCOMDelegate_Invoke_m5CB65FDE20D78DA5B9AD116A1F2A1673FE26CAF5,
	DestroyMarkerTrackerQCOMDelegate_BeginInvoke_m98708B37CBB891ED08C84DB231F1E0E84EA04446,
	DestroyMarkerTrackerQCOMDelegate_EndInvoke_m694864D826CE5EB061D324E3DDBC8D7B722F6BC9,
	StartMarkerDetectionQCOMDelegate__ctor_m7FEEC075A11404A5E62D412281011EB5851590A8,
	StartMarkerDetectionQCOMDelegate_Invoke_m8DDAF05CBFA9D0B33A6B5704E67D047D585A74C7,
	StartMarkerDetectionQCOMDelegate_BeginInvoke_m4352A5856D43CE27B5C9142AE85773757D0A5F77,
	StartMarkerDetectionQCOMDelegate_EndInvoke_m67331F6AE17F744B22A6268E408328A62A3E41CB,
	StopMarkerDetectionQCOMDelegate__ctor_m048FEADB626E747B330023E9378D1E510AF4EE6B,
	StopMarkerDetectionQCOMDelegate_Invoke_mF85020950E3A7D379C5EF29450E5ED4A682CBA3E,
	StopMarkerDetectionQCOMDelegate_BeginInvoke_mB29B5B90465CE482AB619FD786E4A7F05384DCDB,
	StopMarkerDetectionQCOMDelegate_EndInvoke_mF65CA723F1F94FBD6F606EAF53EF7C1F65597AC2,
	GetMarkerSizeQCOMDelegate__ctor_m0B7CD5E7547341722193159F649E83E59F4F13DF,
	GetMarkerSizeQCOMDelegate_Invoke_m7D4A70A7159A4A720867EE54CAC8CFE63D9B46A6,
	GetMarkerSizeQCOMDelegate_BeginInvoke_m8B9465ACC90C001855923C0FD100042B23087D0E,
	GetMarkerSizeQCOMDelegate_EndInvoke_m79ED2D8B2C69A0CE6AF26C292A4473CDF646D819,
	GetMarkerTypeQCOMDelegate__ctor_m76930CF86F7CCAEE2851DBC3DCB1C6DAA4E9E8C5,
	GetMarkerTypeQCOMDelegate_Invoke_m67CA41ED6F3BC80E84C70DDDC2B0AAD4970905C5,
	GetMarkerTypeQCOMDelegate_BeginInvoke_m3D05CFD7238BC84D4C3C7D8E8C279349ECAE9A4E,
	GetMarkerTypeQCOMDelegate_EndInvoke_m897CB810820B4BFEDBB0B7D10839A6D027E835CD,
	GetQrCodeVersionQCOM__ctor_mA9E96F700CC6B35D99A48B45F19DF467D0A4D2F4,
	GetQrCodeVersionQCOM_Invoke_m7D4BFA32E0F8FA5EE9C3D3E6C664CFDC5CA929AA,
	GetQrCodeVersionQCOM_BeginInvoke_m205AD3FA51B877D078554FD2F3BC23658B3E2D8E,
	GetQrCodeVersionQCOM_EndInvoke_mB9F81D7217A7E7951D1FB14C5915C33ECEDD4986,
	GetQrCodeStringDataQCOMDelegate__ctor_mF6CA1F40518F99C5AF6242A2EF128656E004B14A,
	GetQrCodeStringDataQCOMDelegate_Invoke_m52BA55522558196E6073AD17E1B5E749395E9152,
	GetQrCodeStringDataQCOMDelegate_BeginInvoke_m803E0F10B045C3EB6A591A1A1F2892CE4EF252FF,
	GetQrCodeStringDataQCOMDelegate_EndInvoke_mFA5085A8E9CC07F581E7174232CC81A4F1591F3B,
	GetQrCodeRawDataQCOMDelegate__ctor_mC426F0C9FD22F4D6BC88E5F575EB99BD0C81A38C,
	GetQrCodeRawDataQCOMDelegate_Invoke_m62A24F75EC46108430BE2C4B6F7C605E1138BC04,
	GetQrCodeRawDataQCOMDelegate_BeginInvoke_m3CFFE3617542AF17E31085D10251F80C99398E07,
	GetQrCodeRawDataQCOMDelegate_EndInvoke_mD7558EB76EC21D5C34053527ECDA1A4C2675BAB6,
	CreateMarkerSpaceQCOMDelegate__ctor_mFE9F38698C20CC329D46738E21B03D9C1491CE31,
	CreateMarkerSpaceQCOMDelegate_Invoke_m437DFC04574805B22011129C1D4E5AA62FF34792,
	CreateMarkerSpaceQCOMDelegate_BeginInvoke_m5D2AFECE5A692000F51F7670D5FC88B3ADC54DDC,
	CreateMarkerSpaceQCOMDelegate_EndInvoke_m1979F5E2D945ABCFA17B40019FDF0896D3E31FDE,
	SetMarkerTrackingModeQCOMDelegate__ctor_m7F08952442DBED4310D412AB50C5AC3FBECDD63E,
	SetMarkerTrackingModeQCOMDelegate_Invoke_m81EFBED5E89CB0B1F54476256E6052B4D88B6E44,
	SetMarkerTrackingModeQCOMDelegate_BeginInvoke_m5679AA5A9CB3A57EB900E835EF197E5736EC037D,
	SetMarkerTrackingModeQCOMDelegate_EndInvoke_mC368D22CD5777E463BD5FFC675A890DFC68E064D,
	PollMarkerUpdateQCOMDelegate__ctor_m438325B413BB239C41A23E65799B1CAFFB2B4342,
	PollMarkerUpdateQCOMDelegate_Invoke_mD65FBF9DE98B81FF16BDE1612CFB836FB4CB9822,
	PollMarkerUpdateQCOMDelegate_BeginInvoke_m5794B16DE24D8957C74052857ADD2544477E51B2,
	PollMarkerUpdateQCOMDelegate_EndInvoke_mEB471989367D06F090C793FE02829B6CE3A7FEA8,
	GetMarkerUpdateInfoQCOMDelegate__ctor_mF996B4699C3095DF833696F6A20D5FADB713B829,
	GetMarkerUpdateInfoQCOMDelegate_Invoke_m28EFB1B76950E5F3837697D1BD5DEF5E426E5997,
	GetMarkerUpdateInfoQCOMDelegate_BeginInvoke_m578F7BBC4ABF7409227CB3B3828131115B862BD1,
	GetMarkerUpdateInfoQCOMDelegate_EndInvoke_mA73AA785E6CD06559C98D48925C997216D0F9AF1,
	ReleaseMarkerUpdateQCOMDelegate__ctor_m9AB1E51E6B8CAACD5B93B358C04CDA8017522CC1,
	ReleaseMarkerUpdateQCOMDelegate_Invoke_m81BE71F966856B4B14F6FF6216E135D8A2B830AF,
	ReleaseMarkerUpdateQCOMDelegate_BeginInvoke_mF0873A656BCDFA60C1C3BBE616E3C3E86C65DC2F,
	ReleaseMarkerUpdateQCOMDelegate_EndInvoke_m2BBE2345BBFE7093133D0097626A9D5CDA45292D,
	LocateSpaceDelegate__ctor_m1651232A134609E119DB1FABE5BC77BEA5A8EFFC,
	LocateSpaceDelegate_Invoke_mD1B6F693B1A3B87774CA7291F7E9860FF79F7885,
	LocateSpaceDelegate_BeginInvoke_m6298CE38560351F1E02B621A9304F33ACB5BD8DA,
	LocateSpaceDelegate_EndInvoke_mE990D62EC52CFD9F5A0010833F3B480008D2A260,
	DestroySpaceDelegate__ctor_mF59D96B6E0832B8FD26F19707727FA043F83295D,
	DestroySpaceDelegate_Invoke_m10F1B60B1DFEC5820A2B232D516A000771C9DCCB,
	DestroySpaceDelegate_BeginInvoke_mCB3A02EA52C5CE8B1AADFE819C2C898490BF8044,
	DestroySpaceDelegate_EndInvoke_m5A679D1B62605290F85FD9267EAFD5CD96B43F77,
	QrCodeTrackingSubsystem_RegisterDescriptor_mB3AF76447008213FAA576D874DDDE982B5938880,
	QrCodeTrackingSubsystem__ctor_mB4DCE13EEBF1555933AF83C24E93FA02AFA6486D,
	QrCodeTrackingProvider_Start_m75B1DD9BA4BEFA0E374EE25426664B7D3E0406D6,
	QrCodeTrackingProvider_Stop_m6CE044E36B15903A7670B8E88A657CE732ECF048,
	QrCodeTrackingProvider_Destroy_mD3C7B824C7473A5B0F12ECF5BBBCC6A108404E07,
	QrCodeTrackingProvider_GetChanges_mDCEDD7EBBA9CD342DD1AC58293E60C02653FEEE4,
	QrCodeTrackingProvider_SetMarkerDescriptor_m4649778F980424CD91609E2DC6D1FD4CD98B77B6,
	QrCodeTrackingProvider_TryGetMarkerData_m923DF41D1D6D5349B2240FC5D61DC674D72FEE44,
	QrCodeTrackingProvider_EnableMarkerTracking_m525A50BEC6F3369A725B32E3554A6C76AB0AEAE0,
	QrCodeTrackingProvider__ctor_m5F432B96102EFC12B58C374C3C900D466662E495,
	SpacesMarker__ctor_mC89B5CE6EB7669D170C5F200845D56A477AF2810,
	SpacesMarker_UpdatePoseAndTrackingState_m0811C38B01B8D1324AAC1677912A6584D86BC730,
	XRMarkerDescriptor_get_TrackingMode_mD80E7A2DFE0BB1FAAE9FDDDF022F9B811208C941,
	XRMarkerDescriptor_get_Size_m8413EA768805484DAFA6ABC04F8AA37B9F858DC5,
	XRMarkerDescriptor_get_QrCodeVersionRange_m95B6C333F887917DE81679CF8AA95BB76FE9C0F7,
	XRMarkerDescriptor__ctor_m5527F572D3A297CB5FA443715835F6C37DB54D41,
	XRMarkerDescriptor_get_DefaultDescriptor_m18D15CDA431C993F18A405BEB650DFAFF48B7822,
	XRMarkerDescriptor_Equals_m49C49CB847600ED9EAA595C48D099EF60EB1A43B,
	XRMarkerDescriptor_GetHashCode_m89653348B8764C2FC89570E8871EC6D325948A48,
	XRMarkerDescriptor__cctor_m91CA206B9B9B2833C1B0D00B578A24DE4D3EE12A,
	XRQrCodeTrackingSubsystem_GetChanges_mD60AC55BF7EC64500D9C6872965C5BEC3140E8C3,
	XRQrCodeTrackingSubsystem_SetMarkerDescriptor_m69A403D5FF7284A70C06A66656EFD46AB034B1B6,
	XRQrCodeTrackingSubsystem_TryGetMarkerData_m9D2D6E2A1A75F29F14679F49151CD0BE9006FD01,
	XRQrCodeTrackingSubsystem_EnableMarkerTracking_mEC7DC7B9177D936D2EC9F57C4257295CFCDFF702,
	XRQrCodeTrackingSubsystem__ctor_m37A83C006A7240E1F42CA493C288CA153A914A5B,
	NULL,
	NULL,
	NULL,
	NULL,
	Provider__ctor_mDA8CA8C38F7F9B3617142D533A2147E1B99D967A,
	XRQrCodeTrackingSubsystemDescriptor__ctor_mF5D0326303F91C06868B6E7A17053CD2C36DA546,
	XRQrCodeTrackingSubsystemDescriptor_Create_m2B1E6A3415956725068AD0C7DFD2427EDE403C84,
	Cinfo_get_id_mEB29904AA813FB8BD587DCD994F8AFC20C99B742,
	Cinfo_set_id_mE0C00BE3307B5E2BBE1E140CC20094040EE1B811,
	Cinfo_get_providerType_m5A559C875D7CD9A293A377E656786D28511F08AF,
	Cinfo_set_providerType_m3164B2DB59BAAF4BC8160BD6F4B8A466C5F5F7A6,
	Cinfo_get_subsystemTypeOverride_m719F057762950FBB9D4A5A858D7A17E00A5A5EDC,
	Cinfo_set_subsystemTypeOverride_m327467C849A070DE62F102D88CFD204B89B26BEF,
	Cinfo_GetHashCode_mDCB386966FE8B59139CDFA71F7F0FBE3FC8DD2FC,
	Cinfo_Equals_mE2F04B1A09111E4AA11D421E16C5789D222EC756,
	Cinfo_Equals_m987CEE46F6C2B0E1C63883BB4FA09AFEC38C172D,
	Cinfo_op_Equality_m41D9D625FD40323F89C8D639C1A4755EF364F81E,
	Cinfo_op_Inequality_m5F0D88D8702E816FBC07176D75A4F03121477B0C,
	XRTrackedMarker_get_trackableId_m2FA500913E2999A7E28AC1AE311EB83A326C4B70,
	XRTrackedMarker_get_pose_m7F044500815660018AC12237A0F1CC64CD31F88F,
	XRTrackedMarker_get_trackingState_m9A85DE39F1DF84FD53FFD8F0113CF169C56BEFFE,
	XRTrackedMarker_get_nativePtr_m016CB37BEC30269FE8D4D4FE1D7043C8EE195AEF,
	XRTrackedMarker__ctor_mB4EC0B012C7E2AA4D83DCBB711778354E51808AE,
	XRTrackedMarker_get_defaultValue_m9E7E54AC81848FF7425A1F0A93A5941679F8744D,
	XRTrackedMarker_GetHashCode_m7F8CB1B3555F5D10ADD12F61900A75816000D52B,
	XRTrackedMarker_Equals_mBF2E6DE4766E703265DDD427F0AC223D24855AEE,
	XRTrackedMarker_Equals_m4F74FFE82E700A097FDB1C1787B53180BF6422EA,
	XRTrackedMarker_op_Equality_m854EB73FF3C73AD86107BCC5ACC28127826E7D74,
	XRTrackedMarker_op_Inequality_m75E1E5BF3A9C263407857A19CE69CC9345BF3156,
	XRTrackedMarker__cctor_m7376CECE950C340551EC3A2E37849051D944BED0,
	SpacesAnchorStore_Awake_m21E040879F16C1478E6155F154E767DA1DAEBD4F,
	SpacesAnchorStore_OnEnable_m595445F25623F97E9D5DC16DD444C5D5908CE57A,
	SpacesAnchorStore_OnDisable_mFEF60FBC5569901F1B483E045402207F5E660604,
	SpacesAnchorStore_Update_m01E7D6F42B463722C1FADC24B9BACE93FAB80E96,
	SpacesAnchorStore_SaveAnchorsWorker_mED18C38FFED3140BDD3D847B6A92EBA24DA5C901,
	SpacesAnchorStore_LoadAnchorsWorker_m7964D88933310434F330FD011E3FD1A8BA23AE6F,
	SpacesAnchorStore_SaveAnchor_mA318437CDC8B7BF2B010BEB009BEB58DC99F2557,
	SpacesAnchorStore_SaveAnchor_mEB5CAF37BDDC441EDB7CC4A8705987174BBD2ACB,
	SpacesAnchorStore_SaveAnchorWithResult_m8D8C1FDC98A6BC8DD4E414EA0775FDE9714696BB,
	SpacesAnchorStore_SaveAnchorWithResult_m23A04C2D852B4FC7F5E3A89250306BBB77F7F3BB,
	SpacesAnchorStore_LoadSavedAnchor_m88B070A69A96D9B9A21EF992BC09C196C6FF4851,
	SpacesAnchorStore_LoadSavedAnchor_m5D981A8880C785E09E57A89CDA747A93849FDE1D,
	SpacesAnchorStore_LoadAllSavedAnchors_m06CACF8FDD130B02A9212FA75BCAA1DF594EC1CC,
	SpacesAnchorStore_DeleteSavedAnchor_m814A7ABD19B50A6B7946AC248C27EE428AC874C0,
	SpacesAnchorStore_ClearStore_m41494FDA4367B14DB783792FD8FA571E987B29BE,
	SpacesAnchorStore_GetSavedAnchorNames_mDF6A8348BC71BE9C4F60B6E3FFB24BDF7CE168F4,
	SpacesAnchorStore_GetSavedAnchorNameFromARAnchor_m004B1635DE15C4253A230E0F22E14A855699A054,
	SpacesAnchorStore__ctor_m0BFD7AF87D342C45A0F96FAA6BFF9174EF6C61F6,
	SaveAnchorData__ctor_mF16AF39431809253F45CECDD11FE45166BA80067,
	LoadAnchorData__ctor_mCA956DC558E2139C8D449DA3AE7E09568CA921F1,
	U3CU3Ec__DisplayClass21_0__ctor_mE2B8DA570B37672EF6E0A559A7CDAA71AB789DE3,
	U3CU3Ec__DisplayClass21_0_U3CSaveAnchorU3Eb__0_m8FB292C30096241A68AE0AD2D54DB2D68452EA5E,
	SpatialAnchor__ctor_m89B5BFA2F7EA61C84F97394772CBE0525C8EF8CB,
	SpatialAnchor_UpdateSubsystemAnchorPoseAndTrackingState_mDD00EA979ED3C6DAB1140CAD072BCD22BADA7C29,
	SpatialAnchorsFeature_get_IsRequiringBaseRuntimeFeature_mAF0EA26323F10AF6D2AAA0530A5BA1D21F0E21B4,
	SpatialAnchorsFeature_TryCreateSpatialAnchorHandle_m4685D97C85E540F32B9B33712A19B3C4543AD359,
	SpatialAnchorsFeature_TryCreateSpatialAnchorSpaceHandle_mED98DCE76E5721F42ACE282B5E59F6B190A7B5F6,
	SpatialAnchorsFeature_TryDestroySpatialAnchor_mB19090DFCA8F13FFD7710A751387F959BC0D5C67,
	SpatialAnchorsFeature_TryGetSpatialAnchorSpacePoseAndTrackingState_mD664863791E7F1A5813CF9EEA1E9D7868A9118B6,
	SpatialAnchorsFeature_TryCreateSpatialAnchorStoreConnection_m8B05FD91A3DAF31A4F95A89E125CDA8EC908BF32,
	SpatialAnchorsFeature_TryDestroySpatialAnchorStoreConnection_m4BEC6B257130BC4C2B76859D043C7AA10CE5F8C3,
	SpatialAnchorsFeature_TryClearSpatialAnchorStoreMSFT_mE2CB7E2C660A56E601C93EEBA9E86E3C6C34D656,
	SpatialAnchorsFeature_TryPersistSpatialAnchor_mC3405D6C3B2A2A8E534E48B70BA846C2A6F4AAC9,
	SpatialAnchorsFeature_TryUnpersistSpatialAnchor_m8D4D1AD7BF28030DC6193DBAC13C5DD5105967C5,
	SpatialAnchorsFeature_TryCreateSpatialAnchorFromPersistedNameMSFT_m564DFAA8B0802B284872BAC1B37B4C7AD060C73B,
	SpatialAnchorsFeature_TryEnumeratePersistedSpatialAnchorNames_m65120244410EBC374EAFE68DD7FC0BE692A44DD3,
	SpatialAnchorsFeature_GetXrLayersToLoad_m8604DA06AE0184EBE94C29E871C996E7D85F2C0B,
	SpatialAnchorsFeature_OnInstanceCreate_m1965A9C370CC7E834B7B470F50115328CA4E072A,
	SpatialAnchorsFeature_OnSubsystemCreate_m279245F1D49560839F61A53CD97A7C67C220FB7F,
	SpatialAnchorsFeature_OnSubsystemStop_m46BD0E199567738406AF385CC5B00E8259CB6239,
	SpatialAnchorsFeature_OnSubsystemDestroy_m6D0EBDB38708E3C11B5250D5E8FD9147C45B10AF,
	SpatialAnchorsFeature_OnHookMethods_m9A6036B77D73B75CFD691DD52CD72070CBCC5F45,
	SpatialAnchorsFeature__ctor_m5C5B37C43F1CFBCAB96944961B62806DB1478C64,
	SpatialAnchorsFeature__cctor_mEA98ED48B34FE6A2289DCDE8E4ADB6D4911DD35E,
	CreateSpatialAnchorMSFTDelegate__ctor_mF691172F5B23677D9490298005F408C139DA9ED7,
	CreateSpatialAnchorMSFTDelegate_Invoke_m360D2AD6BC6A9FCD2DBE1323F109BB0A392647B0,
	CreateSpatialAnchorMSFTDelegate_BeginInvoke_m1C7B3461BB6990C29739E57A9AE4692F035A3496,
	CreateSpatialAnchorMSFTDelegate_EndInvoke_m1C2A701467D24A8580907B36250536DB370F4670,
	CreateSpatialAnchorSpaceMSFTDelegate__ctor_m36F40E5A4543496C5D3481973CDC1878BB75305A,
	CreateSpatialAnchorSpaceMSFTDelegate_Invoke_m83C7DD3FD8A7B5167C7C927E20E644E323CCAF0B,
	CreateSpatialAnchorSpaceMSFTDelegate_BeginInvoke_m0E2845D2F498D296941D6101E20B4F6A89BE65D0,
	CreateSpatialAnchorSpaceMSFTDelegate_EndInvoke_m05E5BB2179A08ED8A2454F3571FFB7BED6AD154E,
	DestroySpatialAnchorMSFTDelegate__ctor_m439B1234EC35634A19BE7A7257B679DC73658BD9,
	DestroySpatialAnchorMSFTDelegate_Invoke_mD43C523B6B300A640A89403B0740723BF92F7579,
	DestroySpatialAnchorMSFTDelegate_BeginInvoke_mE6F9BF75F9DD2E9AA5EEA78B26560C5660F444D6,
	DestroySpatialAnchorMSFTDelegate_EndInvoke_m0448013075FE053F62D3785B285F7CAAA4C6FE46,
	CreateSpatialAnchorStoreConnectionMSFTDelegate__ctor_m9E39505763F871F9AC0AF22C21E715FD84FCB73B,
	CreateSpatialAnchorStoreConnectionMSFTDelegate_Invoke_m968E41F34BB96E29AFDA75A7F60C001C8768FB7A,
	CreateSpatialAnchorStoreConnectionMSFTDelegate_BeginInvoke_mA966E548D9865DA0AF8F745C12AC3A2DD2C1BB50,
	CreateSpatialAnchorStoreConnectionMSFTDelegate_EndInvoke_mAC419F701336F5A413E6A84BAB5A93E9464C70E8,
	DestroySpatialAnchorStoreConnectionMSFTDelegate__ctor_mE63BDD395A7C62C3CA5926C796D7F025E8F9313A,
	DestroySpatialAnchorStoreConnectionMSFTDelegate_Invoke_mEFB181C09A6237E3F36CB3887981360C060A54C0,
	DestroySpatialAnchorStoreConnectionMSFTDelegate_BeginInvoke_m88C2CFD7A6D411A927319CEF4187D341777AA6FA,
	DestroySpatialAnchorStoreConnectionMSFTDelegate_EndInvoke_m428E95F457E0810EF96800E304549C264483BED2,
	PersistSpatialAnchorMSFTDelegate__ctor_m76A5D24CCC511B93896DD6A263B61D7C90DD10F1,
	PersistSpatialAnchorMSFTDelegate_Invoke_m629801285D0E38414C2A1E7537960F98D5CB887B,
	PersistSpatialAnchorMSFTDelegate_BeginInvoke_m3AB30F69A6807F12BB5891259468C11294298174,
	PersistSpatialAnchorMSFTDelegate_EndInvoke_m8F4DD6950A6C2DB1F52AA391B2522356CDD37C0B,
	UnpersistSpatialAnchorMSFTDelegate__ctor_mE32740AEC3B347F059C4C52BB95953FA78A94694,
	UnpersistSpatialAnchorMSFTDelegate_Invoke_m574686A42D506A5CFF44232D1040363433728E28,
	UnpersistSpatialAnchorMSFTDelegate_BeginInvoke_m5E68648C9DAEC00E814CA1430405C5220A60FD38,
	UnpersistSpatialAnchorMSFTDelegate_EndInvoke_m16F28F6EA37DCD0715B79CF7A0C0105BCD48A883,
	EnumeratePersistedSpatialAnchorNamesMSFTDelegate__ctor_mAFAFD2711DF12FD7E0AFDDC6D67BF601E2861B32,
	EnumeratePersistedSpatialAnchorNamesMSFTDelegate_Invoke_m8D01B6BEA1F353BEF629E24877FF8020028F47CE,
	EnumeratePersistedSpatialAnchorNamesMSFTDelegate_BeginInvoke_mF20189A65702D6A04D9C95BA7F58A6D93FB173E3,
	EnumeratePersistedSpatialAnchorNamesMSFTDelegate_EndInvoke_m3767A6B38656AD3D3A049B5A956D9502AB0CA418,
	CreateSpatialAnchorFromPersistedNameMSFTDelegate__ctor_mC0C7549B313907B39BF54E087D5BBCE7172C05A8,
	CreateSpatialAnchorFromPersistedNameMSFTDelegate_Invoke_m8EE7A705BDFF61921963C9A9AD603B9A4DCD0F3C,
	CreateSpatialAnchorFromPersistedNameMSFTDelegate_BeginInvoke_m48433F3303F65A228720F2EBC11C52CC39C3D10D,
	CreateSpatialAnchorFromPersistedNameMSFTDelegate_EndInvoke_m7E349F63CB0710A23B99E39FF39B08BA043DF541,
	ClearSpatialAnchorStoreMSFTDelegate__ctor_m13D0AE7888E8B13D91AE3AB946240621F184719B,
	ClearSpatialAnchorStoreMSFTDelegate_Invoke_m1A2CD0184B4A8D1F231BE3F57EF07CBA20C62AA1,
	ClearSpatialAnchorStoreMSFTDelegate_BeginInvoke_m6CCF5D13AC2EDDD1A9BDA4B152B869E05134EED0,
	ClearSpatialAnchorStoreMSFTDelegate_EndInvoke_mA9F7719B9F48972F2A74970861F77F71DDBB9AAB,
	LocateSpaceDelegate__ctor_m2BF0040304888E95146FFB3E1E71F82C7C0DBAFB,
	LocateSpaceDelegate_Invoke_m09F2C746A247677A863DA20FE8FDDE4E5245F8C5,
	LocateSpaceDelegate_BeginInvoke_m813FD8C8E5C188A33A7F4B974E4EFD6C7243DCF6,
	LocateSpaceDelegate_EndInvoke_mAD80E9503E4D5E516D5F9638F7128059864E35DD,
	SpatialAnchorsSubsystem_RegisterDescriptor_mEFE16FC3EB00C57718F03116E4AD3B1543286349,
	SpatialAnchorsSubsystem__ctor_m3DA4F79A357AF845F3AF8E8B5AA6BE430FCDD82B,
	SpatialAnchorsProvider_Start_m315EF3EF6CFEF3FEF626DDFFA49F0F53F2847652,
	SpatialAnchorsProvider_Stop_m583C80431D1F330E1EEC7BA1C12DDBE5AF2E1D8F,
	SpatialAnchorsProvider_Destroy_m00F3AAB4418AFE6487E7753D86AA59DB42E04FD8,
	SpatialAnchorsProvider_AddAnchorsWorker_mEB4CBADFE8B1039B48FF946E475C59A6CFB63E9A,
	SpatialAnchorsProvider_GetChanges_m80C33A43A87FFE3EF1A5DBED4A9086DEBA5370C5,
	SpatialAnchorsProvider_TryAddAnchor_mB860D534301AD7AEDB3A994B044C4A70F74B7A95,
	SpatialAnchorsProvider_SetPersistentAnchorCandidate_m39F58BBE288998D8DCF8668964036C32CD729E2A,
	SpatialAnchorsProvider_TryRemoveAnchor_m57FD042A12277644B5136F9291219DB51323FF67,
	SpatialAnchorsProvider_TryAddAnchorFromPersistentName_m34B7DD94ACB60DC573DBC04CFE35204D91926F75,
	SpatialAnchorsProvider_TryGetSavedNameFromTrackableId_mF37BF9A04A5E321A4D8A91EF85D86EE5CEDCC875,
	SpatialAnchorsProvider_TryGetSpatialAnchorHandleFromTrackableId_mB6EE6A6D84FD6C7710F8684AA32EB240EEA05C82,
	SpatialAnchorsProvider_UpdateAnchorSavedName_m497685B1A1FC34271A9D1FCCAC011DEAD644A648,
	SpatialAnchorsProvider_ClearAllAnchorSavedNames_mD29F12637E25DE3B8F2A7E81C4DAAE3A3F716DB4,
	SpatialAnchorsProvider__ctor_mFD5C14D017F935CBF28DC2A6C5BE4B259DD4DBB5,
	SpatialAnchorsProvider_U3CGetChangesU3Eb__16_0_m3018377E0E8CDCA49628F57328C0B7F4E9E3D82C,
	AddAnchorData__ctor_m41F5A154755BC533FD63364040166BEE62A7CB0A,
	U3CU3Ec__DisplayClass19_0__ctor_m84553CC5488F55885316C4CC80820C3AC9A4A3F2,
	U3CU3Ec__DisplayClass19_0_U3CTryRemoveAnchorU3Eb__0_m499F84C3EC2E75636BE64CD24DC63AFCDBCFD0C4,
	SpacesARMeshManagerConfig_get_CalculateCpuNormals_m173C07259CC37DCF8ED8879889459232A020528B,
	SpacesARMeshManagerConfig_set_CalculateCpuNormals_mFE26E5FE895B48F186CE2A00CA5E0650AD387E2C,
	SpacesARMeshManagerConfig_get_UseSmoothedNormals_m59EA1784FE4DD817747244A2D47A34AAE8CAB1E1,
	SpacesARMeshManagerConfig_set_UseSmoothedNormals_m23E6E5F870D8B64C15944D560C822723354C3C76,
	SpacesARMeshManagerConfig_get_SmoothingEpsilon_m2917B8945305A25069A34332BA817FE7FCB5FF11,
	SpacesARMeshManagerConfig_set_SmoothingEpsilon_m53D760B5A5E5296FD04BE882530FEAD17FEAA2EB,
	SpacesARMeshManagerConfig_Awake_m022AD0A961200221E203E0ACB02DEED144676EF6,
	SpacesARMeshManagerConfig_OnEnable_m3104FDFDCB82C0EE35B1006284EC7AD6CA69DD92,
	SpacesARMeshManagerConfig_OnDisable_m63C83DD89B3607697973A06086947458EB77687D,
	SpacesARMeshManagerConfig_Internal_SetMeshVertexAttributesFlags_mF9DD96D8F1860CB2D092CF215733727B504BD9ED,
	SpacesARMeshManagerConfig_Internal_SetUseSmoothedNormals_m33CDBF9FEDE491A16022EDB17ED7CABBD48C8CFC,
	SpacesARMeshManagerConfig_UpdateMeshVertexAttributes_mD63EB54342EC033DD7AC7D9C42D8B0BBA81CB227,
	SpacesARMeshManagerConfig_Internal_FetchMeshLocations_mE4E4F772FC46E6138AA3E0DBA4239DA391B695C9,
	SpacesARMeshManagerConfig_UpdateMeshTransforms_m5760A560DD59692FEB3E762E61F62117624F4DAA,
	SpacesARMeshManagerConfig__ctor_mEDF57C58902085F21AAF398A1F556526888393A7,
	U3CU3Ec__cctor_m41F89D8F04CCDE897FFF048FD454B0AB996C6E8D,
	U3CU3Ec__ctor_m52D7D0C3E5A59D7900D0EE4367785F493338D1CC,
	U3CU3Ec_U3CUpdateMeshTransformsU3Eb__25_0_m56072C0A26A15F8E3067148ECD14418D9716B731,
	SpatialMeshingFeature_get_IsRequiringBaseRuntimeFeature_m35C9AF7CDCA8AA6E15B5EBE5156F577B4312AF24,
	SpatialMeshingFeature_GetXrLayersToLoad_mEA2654D185DB5DEDFF0AFDAA7C6AC5022415692C,
	SpatialMeshingFeature_HookGetInstanceProcAddr_mBEDA89FB8AE0C4F8436DB229A3A624597C0FEDAE,
	SpatialMeshingFeature_OnInstanceCreate_mBF2FEC09E00D968EB15B97A36C08EA913E3D19F5,
	SpatialMeshingFeature_OnSubsystemCreate_m27E5A22456F1153140FEF8D6539185088F341153,
	SpatialMeshingFeature_OnSubsystemStop_m10CB3845E2D47AC7A55B84BEB0DE02D559684865,
	SpatialMeshingFeature_OnSubsystemDestroy_m32E3C54AD2C8CC3B0D842E8446FCEECF3E75B26D,
	SpatialMeshingFeature_OnSessionCreate_m7A38F25F3413ABAA6472EAAFE7D2BD092C4FC1BD,
	SpatialMeshingFeature_OnAppSpaceChange_m52A405DF272B76C5258C297A0B474FD48CAE94BB,
	SpatialMeshingFeature_Internal_GetInterceptedInstanceProcAddr_m9688A072EEBA614133E53F5A7FD25E1AD397029F,
	SpatialMeshingFeature_Internal_RegisterMeshingLifecycleProvider_m5BD0977192D0C5323EA3A2BAD41BBE8DE4F18744,
	SpatialMeshingFeature_Internal_SetInstanceHandle_mAFCFE24FB40E7F7D108619005E2349A9EA80EFC1,
	SpatialMeshingFeature_Internal_SetSessionHandle_mA55D51A96361262F56449C9E27111C60F38F5022,
	SpatialMeshingFeature_Internal_SetSpaceHandle_mAB5E5F895CE3BBE833A2FBA2F256510DDDC197C0,
	SpatialMeshingFeature_Internal_RegisterProviderWithSceneObserver_m07FB89EE9FBB7844DA7A960A4C4E5FED8910AF36,
	SpatialMeshingFeature_Internal_UnregisterProviderWithSceneObserver_m8C2AE43EBC56404C2F205B999FB64C071A105096,
	SpatialMeshingFeature__ctor_m6715D10B5FDAC89A1B7D8A86107AE43D5436BAE1,
	SpatialMeshingFeature__cctor_mAE8142B52B445BF623DB4DFEDC63D7DC2D4BA5F1,
	CreateSceneObserverMSFTDelegate__ctor_m69AB72D4903A3BB6A6349AA1F65403C60D0ADE28,
	CreateSceneObserverMSFTDelegate_Invoke_m05CF0D8891D3F59D1372986C78BEFD46E321F390,
	CreateSceneObserverMSFTDelegate_BeginInvoke_mF7DBF17FFCE45B8E067A67DBD5983193C2EC4159,
	CreateSceneObserverMSFTDelegate_EndInvoke_m998EEC02E13DCBF505F51073A7B24698ABFF1A07,
	ComputeNewSceneMSFTDelegate__ctor_m436EED3867F77DD71F0BCC9CD4254411DB432FA6,
	ComputeNewSceneMSFTDelegate_Invoke_mF73F994CF781E9AE5E3CBE2D62D386B2FA74E016,
	ComputeNewSceneMSFTDelegate_BeginInvoke_m4A727DB4AB2F84128CF970C7A6AF847F72EB1F4C,
	ComputeNewSceneMSFTDelegate_EndInvoke_mE1822F7F48CB128A98BC47D83BA4C6CBE846DF85,
	GetSceneComputeStateMSFTDelegate__ctor_m224B9A87DCDF1B4780E88BA923EFC6F2073799AD,
	GetSceneComputeStateMSFTDelegate_Invoke_m54FAFDE8470FC944C05ED1774F967C7579BC36BE,
	GetSceneComputeStateMSFTDelegate_BeginInvoke_m6BCCA0E52260EEA8DCA74AB3F2ADD602A0F7FD71,
	GetSceneComputeStateMSFTDelegate_EndInvoke_m78E9DDDDE1AC7CAABCD0C259DDEF35E324C5157A,
	CreateSceneMSFTDelegate__ctor_mA4CB239F551DE61980347EC47E18194043B91E6F,
	CreateSceneMSFTDelegate_Invoke_m04B0655BEAB01AACC0AF3630D54704D37EA696EF,
	CreateSceneMSFTDelegate_BeginInvoke_m2D50A745EC2354B2D590C4ED359F546204FE504E,
	CreateSceneMSFTDelegate_EndInvoke_mE6F23C4F5A0F05AD085351F852FC2CE1D5FE5DFA,
	GetSceneComponentsMSFTDelegate__ctor_mF87106F1821844CEDA522EC90E404F22EC9AC4A1,
	GetSceneComponentsMSFTDelegate_Invoke_m585D6D6F72141455768BA4BA7CBAF310A3817F13,
	GetSceneComponentsMSFTDelegate_BeginInvoke_m9FC916EF9608A9B175E2B834298F8A6A1604F72C,
	GetSceneComponentsMSFTDelegate_EndInvoke_m52E57976567C5E40FF3B1AF2085A6894E0EE28EB,
	LocateSceneComponentsMSFTDelegate__ctor_m274DA0037673F612D02BA6920400D92261E8405A,
	LocateSceneComponentsMSFTDelegate_Invoke_mC0FE3A6E7BDE9E6A4B55556AEDC2F5643DB63A1F,
	LocateSceneComponentsMSFTDelegate_BeginInvoke_m563F08D3B1AC9160163E93DBA6284BE4E702D68E,
	LocateSceneComponentsMSFTDelegate_EndInvoke_m627E2124926722ED99898D35E6641DC2797048F5,
};
extern void ComponentVersion__ctor_m1398FA8526AA2FC1AA0C9C002AED93F555B3BC8B_AdjustorThunk (void);
extern void XrCameraActivationFrameConfigurationQCOM__ctor_mC01CC056B56188B07DCAF655B305D78BE7575D9E_AdjustorThunk (void);
extern void XrCameraActivationFrameConfigurationQCOM_ToString_mA33EBA223A3F907709277F3029DB734BAB05D069_AdjustorThunk (void);
extern void XrCameraActivationInfoQCOM__ctor_m95201ED05481B95C234EFFA791DB372BDA561B85_AdjustorThunk (void);
extern void XrCameraActivationInfoQCOM_ToString_m336B7CD08F1239F2F96F7A195AFE12D2FF72D101_AdjustorThunk (void);
extern void XrCameraFrameBufferQCOM__ctor_mB4B112F9286FE81D0A56B047D932AF79CC5D10F2_AdjustorThunk (void);
extern void XrCameraFrameBufferQCOM__ctor_mE785229ABB8C9A2FB5A1688229B1AB80B7165879_AdjustorThunk (void);
extern void XrCameraFrameBufferQCOM_get_BufferSize_m950D5D3907A1869EEA881A2317C0AD3D7912B7A2_AdjustorThunk (void);
extern void XrCameraFrameBufferQCOM_get_Buffer_mF7F72DFD8B5EC6CA89CAE6CA9A912A93794DFC55_AdjustorThunk (void);
extern void XrCameraFrameBufferQCOM_get_HardwareBuffer_m0AF3D153BED58C85694CE0952F9291D393882A88_AdjustorThunk (void);
extern void XrCameraFrameBufferQCOM_get_Offset_m2A9616A87A8D114D5C6F5B877614803B56612757_AdjustorThunk (void);
extern void XrCameraFrameBufferQCOM_get_PlaneCount_m068F55256645F2AFB1FF6D402ABDF7DF8B178A6B_AdjustorThunk (void);
extern void XrCameraFrameBufferQCOM_get_Planes_m30C5177D7A5E5D9CA49C2DDC3F43A5593D4A6E29_AdjustorThunk (void);
extern void XrCameraFrameBufferQCOM_ToString_m1640A05E0ED21CB8C11B2B3F53551D9A20C1505C_AdjustorThunk (void);
extern void XrCameraFrameBufferQCOM_get_PlanesArray_m781AA15B1A8B108136DC68546FBB869BAB0A69C0_AdjustorThunk (void);
extern void XrCameraFrameBuffersQCOM__ctor_mFAF9A5A88C5AF80F029B5F89BDF3E3536AA4BB9E_AdjustorThunk (void);
extern void XrCameraFrameBuffersQCOM_get_FrameBufferCount_mBB3219FC8C8263519C8D9C03EB5960AD0A7625FE_AdjustorThunk (void);
extern void XrCameraFrameBuffersQCOM_get_FrameBuffers_mEF5A67FA8F90E32A6D8FB70D97582CDA20B9BF81_AdjustorThunk (void);
extern void XrCameraFrameBuffersQCOM_ToString_m35FC790BEB29947A3C585A1910AEF0CC10DD6D16_AdjustorThunk (void);
extern void XrCameraFrameConfigurationQCOM__ctor_m3942FC71C58D9E6031DEFDB639F394D4932A86E0_AdjustorThunk (void);
extern void XrCameraFrameConfigurationQCOM_get_Format_m6BAA09753953C3355C176B7D28BA0264C58A9F55_AdjustorThunk (void);
extern void XrCameraFrameConfigurationQCOM_get_ResolutionName_mC9FAE99F606993BC7CB1DAC117B8787CD8B7E7DF_AdjustorThunk (void);
extern void XrCameraFrameConfigurationQCOM_get_Dimensions_mA4CCECC8E2328C0B5CF51C328C9E49B8CA29C389_AdjustorThunk (void);
extern void XrCameraFrameConfigurationQCOM_get_MinFPS_m94122334221AE771D207F98FE47F1FA1B1B92686_AdjustorThunk (void);
extern void XrCameraFrameConfigurationQCOM_get_MaxFPS_m98188152C09B0C2283C8ABB89BDF52AE641BA34F_AdjustorThunk (void);
extern void XrCameraFrameConfigurationQCOM_get_FrameBufferCount_mAEC0C4D48348E78839296BA0F17D16D1EDADCAD4_AdjustorThunk (void);
extern void XrCameraFrameConfigurationQCOM_get_FrameHardwareBufferCount_mAC6A3CE4A06E7BFA5F5DDCE5E1D7F1AE22F4A882_AdjustorThunk (void);
extern void XrCameraFrameConfigurationQCOM_ToString_m20BB1EE9D69A31994E54F082BF32C02DD73E8F33_AdjustorThunk (void);
extern void XrCameraFrameConfigurationQCOM_Equals_m79BAF59F2489C2FC04698EEA69660B3720E94C16_AdjustorThunk (void);
extern void XrCameraFrameConfigurationQCOM_Equals_m6C81D6EF68F4406E509277EDAC64592DF817FD32_AdjustorThunk (void);
extern void XrCameraFrameConfigurationQCOM_GetHashCode_m88D0A1632ECF24BE403724F6BE31D41C9BAF5D96_AdjustorThunk (void);
extern void XrCameraFrameDataQCOM__ctor_m21472D27A6AD1B4266106153194A8F2F1C95E11C_AdjustorThunk (void);
extern void XrCameraFrameDataQCOM_get_Type_mE5BBBBD0E4DFDE0D12419924BB91C99CFB730F58_AdjustorThunk (void);
extern void XrCameraFrameDataQCOM_get_Next_mCE6D17EB550A4FD2EE8A6EF399B18BD1600A0ACD_AdjustorThunk (void);
extern void XrCameraFrameDataQCOM_get_Handle_m26A480ABB370791A1258A3E102BD9FB3CC74DD93_AdjustorThunk (void);
extern void XrCameraFrameDataQCOM_get_Format_m5D209DAFD825E8967B0DE6001ADDCBC71762AA0A_AdjustorThunk (void);
extern void XrCameraFrameDataQCOM_get_FrameNumber_m8B4ED328D6E36ED8B61A2919C4C82A6C03F61B10_AdjustorThunk (void);
extern void XrCameraFrameDataQCOM_get_Timestamp_mBF90CF8A92D202ECE328A6C19955215D983C509C_AdjustorThunk (void);
extern void XrCameraFrameDataQCOM_ToString_mCA5D1225589871DC98C2959C1F2B4B7BF01895D8_AdjustorThunk (void);
extern void XrCameraFrameHardwareBufferQCOM__ctor_mB52C3509013DAC2D369350950A83731A07C429CB_AdjustorThunk (void);
extern void XrCameraFrameHardwareBufferQCOM_ToString_m6ED5F07B3836FC15534D01364A1CC9B226EA36D4_AdjustorThunk (void);
extern void XrCameraFramePlaneQCOM__ctor_m284891F8B4782C344844E1E3B769F8DC14A3F61D_AdjustorThunk (void);
extern void XrCameraFramePlaneQCOM_get_PlaneType_m056E8B6DCF863AF69C363C8C9BBC6A17AFAC0550_AdjustorThunk (void);
extern void XrCameraFramePlaneQCOM_get_Offset_m8AF8D2AF8562907454F27993C9C9853483FA114A_AdjustorThunk (void);
extern void XrCameraFramePlaneQCOM_get_Stride_m090D94F7651FE1AB5559A37739AB90178422290B_AdjustorThunk (void);
extern void XrCameraFramePlaneQCOM_ToString_m7F4DC8BA7B9EBEC86627F9DCF8AFD809E17A9D09_AdjustorThunk (void);
extern void XrCameraInfoQCOM__ctor_mAE436B7F665A8313BC9A5A24A7159C437775207A_AdjustorThunk (void);
extern void XrCameraInfoQCOM_get_CameraSet_mE444C2C329BA9C5F9A14A16310005465DCF958C1_AdjustorThunk (void);
extern void XrCameraInfoQCOM_get_CameraType_m4F5E8A91874162DD8331097F58D02F32296FB66D_AdjustorThunk (void);
extern void XrCameraInfoQCOM_get_SensorCount_m6AE604B7A97AE6C883978221F11CA564562E407D_AdjustorThunk (void);
extern void XrCameraInfoQCOM_ToString_mE8AB2E88F245A4DE1C185D918273C782647F29B2_AdjustorThunk (void);
extern void XrCameraSensorInfosQCOM__ctor_m5051C95DC0D6173C0045176AA82A987E2BD79CBE_AdjustorThunk (void);
extern void XrCameraSensorInfosQCOM_ToString_m4887306CA657989C0FD510390E019AAF3F383F45_AdjustorThunk (void);
extern void XrCameraSensorIntrinsicsQCOM__ctor_mF823C9405A4F3ADE635106439C22625FD54D39BE_AdjustorThunk (void);
extern void XrCameraSensorIntrinsicsQCOM_get_PrincipalPoint_mF4C53A48DF15DA4539C81ECB8ABC8331749D2E90_AdjustorThunk (void);
extern void XrCameraSensorIntrinsicsQCOM_get_FocalLength_m61659D7731F9C69E4D10B948F15C9F337F64C2D6_AdjustorThunk (void);
extern void XrCameraSensorIntrinsicsQCOM_get_RadialDistortion_mEDD0949CEB1ED6CB742D5DD4A5686FA40D38B42B_AdjustorThunk (void);
extern void XrCameraSensorIntrinsicsQCOM_get_TangentialDistortion_m649714F8CA4CCF1E0FDCDAFA70392E187E2D5992_AdjustorThunk (void);
extern void XrCameraSensorIntrinsicsQCOM_get_DistortionModel_m8E397728AFDA69997EADE3EFD106E384425C5938_AdjustorThunk (void);
extern void XrCameraSensorIntrinsicsQCOM_ToString_m1D9A21D03AE7EC2F530FAA9C1D7C80493B130733_AdjustorThunk (void);
extern void XrCameraSensorPropertiesQCOM__ctor_mEAFD9FC1E283431CA018AC2F7255C8B57C057AA6_AdjustorThunk (void);
extern void XrCameraSensorPropertiesQCOM_get_Intrinsics_m108ABACDFE8D9C6659ED98B336D80F337CBD376C_AdjustorThunk (void);
extern void XrCameraSensorPropertiesQCOM_get_Extrinsic_m87075C50E31AA53E33661B75F5347765FFD7AA19_AdjustorThunk (void);
extern void XrCameraSensorPropertiesQCOM_get_ImageOffset_mB40F948CF9DB970B4E7FA114ACBC1A90501CE9D4_AdjustorThunk (void);
extern void XrCameraSensorPropertiesQCOM_get_ImageDimensions_mA4FE4D7DC903DCE7F160EE064509A3B14209414F_AdjustorThunk (void);
extern void XrCameraSensorPropertiesQCOM_get_Facing_mF9019A5010372316F6EB0940630BA144310D778E_AdjustorThunk (void);
extern void XrCameraSensorPropertiesQCOM_get_RollingShutterLineTime_mA9A434624C43BC2B0D916E001267235101D41EAC_AdjustorThunk (void);
extern void XrCameraSensorPropertiesQCOM_ToString_m0DF439C02486E2353F88E5CE3F0074ADD0A28D91_AdjustorThunk (void);
extern void Timespec_get_Seconds_m2E1C0153CD3BEC4805B547A871DBD327ACA911B2_AdjustorThunk (void);
extern void Timespec_get_Nanoseconds_mF4FDF78B2ED03D58C3D591FCC414F0DAFDDEB594_AdjustorThunk (void);
extern void XrComponentVersionQCOM__ctor_mC929B10383F3D44EFB2300FF17EACE1B6B88C51C_AdjustorThunk (void);
extern void XrExtent2Df_get_Width_mA8D6682438E1FDE0E5BBB25F3786D19E79B2CE70_AdjustorThunk (void);
extern void XrExtent2Df_get_Height_m8F6D94CCA19BEDE6F61051882F0A6CC9A4CA981C_AdjustorThunk (void);
extern void XrExtent2Df__ctor_mA538CCEAF45364438C110B220C205691B6A953E6_AdjustorThunk (void);
extern void XrExtent2DfQCOM_get_Min_m94AECAF0A8B83DDDF1D0633F00601745139DAF8A_AdjustorThunk (void);
extern void XrExtent2DfQCOM_get_Max_m0A320B33A9969FF1F3CA9A3C5F4FDD1FE0B86B9C_AdjustorThunk (void);
extern void XrExtent2Di__ctor_mB29746B125742CB6AD747BA5FE9356AC685D73A0_AdjustorThunk (void);
extern void XrExtent2Di__ctor_m1D66F0C237CBCC874FE6E8A9523111C8FDBE6246_AdjustorThunk (void);
extern void XrExtent2Di_ToVector2Int_m16AB6984BE425191D978319A34B7A6605F7FF495_AdjustorThunk (void);
extern void XrExtent2Di_get_Width_m572E28B2072225975B15815FBD4C751999567319_AdjustorThunk (void);
extern void XrExtent2Di_get_Height_mA7B78D20A9F26DDDA1F0A9270350C5752193C5A5_AdjustorThunk (void);
extern void XrExtent2Di_ToString_m8BF3362EDF0DFBC8FCDF7F961F5A592EECA5C59D_AdjustorThunk (void);
extern void XrFrameState_get_PredictedDisplayTime_m6FBB46748AABE30FFE537C362C2D396B70CDC824_AdjustorThunk (void);
extern void XrOffset2Di__ctor_m526ED1D1AE4472C41EC084A8652E4E81CAAC3B75_AdjustorThunk (void);
extern void XrOffset2Di__ctor_m6F07B24B95F5D1A6FE46CAA42920C5009112F4AD_AdjustorThunk (void);
extern void XrOffset2Di_ToVector2Int_mB7CFEA8B38030163FFDA08995B60F4DF67CC1E63_AdjustorThunk (void);
extern void XrPosef__ctor_m3D4E4162D410903083F144466FE9B2DEBB0C21BD_AdjustorThunk (void);
extern void XrPosef__ctor_mE7394EBD4A40A09890877E8DECD26C8B6569209B_AdjustorThunk (void);
extern void XrPosef_ToPose_mDBAF38DC00B0EE86768DD9CA3031FF569FCB2BD6_AdjustorThunk (void);
extern void XrPosef_ToString_mC16B2E1F2C83C928C73B09A09F14CC2B371DCF27_AdjustorThunk (void);
extern void XrQuaternionf__ctor_m0D486D3121CBEE2FE399F14876D4FEF9031DBA94_AdjustorThunk (void);
extern void XrQuaternionf__ctor_mDA964313A2BBA72BED3617B247255478040C0E87_AdjustorThunk (void);
extern void XrQuaternionf_ToQuaternion_m62B17D280617BFDAC4F5F2AB88061F00CBCD5522_AdjustorThunk (void);
extern void XrQuaternionf_ToString_m870370BDABD005D8499212D09FA41F153D150061_AdjustorThunk (void);
extern void XrSpaceLocation_InitStructureType_m42DB29E751707B06F9BD6F9D572DF0F5B9EC5466_AdjustorThunk (void);
extern void XrSpaceLocation_GetPose_m45AE7E0E2ADECD3E177598478CDAD84EC9A6E2FA_AdjustorThunk (void);
extern void XrSpaceLocation_GetTrackingState_mC12F5FB6F53F113B8717D98F01ED775F6AA4D536_AdjustorThunk (void);
extern void XrSpaceLocation_ToString_mE8131ED4AC6143052739BB3ACAE76EBCA9A80734_AdjustorThunk (void);
extern void XrSystemGraphicsProperties_get_MaxSwapchainImageHeight_m043CAFF02E9975B3F693CCAE13D22CE7BEBC9FCD_AdjustorThunk (void);
extern void XrSystemGraphicsProperties_get_MaxSwapchainImageWidth_m4F8D3DEF2B72DDF06E14D4C0B17C57EAC01E2000_AdjustorThunk (void);
extern void XrSystemGraphicsProperties_get_MaxLayerCount_mE0298316C9D0D531C20B60C83803E03F624EB542_AdjustorThunk (void);
extern void XrSystemProperties__ctor_m9D2AC269F6F0BE29D1A4523057F1DC9DE9B9C7A9_AdjustorThunk (void);
extern void XrSystemProperties_GetGraphicsProperties_m9AD557E8D5670538BB5050A62B7A0F4210C4CC2C_AdjustorThunk (void);
extern void XrSystemProperties_GetTrackingProperties_m285EA2F0A78F1BC576B74AF66054F3EE3DF1C295_AdjustorThunk (void);
extern void XrSystemTrackingProperties_get_OrientationTracking_mE950098329F529540C759AD9F44456F48748141C_AdjustorThunk (void);
extern void XrSystemTrackingProperties_get_PositionTracking_m507982F3CE3D9ACA6597F6A4CD01F6C99D06466B_AdjustorThunk (void);
extern void XrVector2f__ctor_m64BBC3D64D3404C4B8292BB00FB2C9B1AF4B7DDB_AdjustorThunk (void);
extern void XrVector2f__ctor_m5FE06D12022775A9D701AAEB7572BAE5B66B8796_AdjustorThunk (void);
extern void XrVector2f_ToVector2_m5A4EC5EB1E0D1F204CEFA8AA9FB4BEB42BDD44E5_AdjustorThunk (void);
extern void XrVector3f__ctor_mD92D4AEDDCD261C251C659F451A7849DB6FF021B_AdjustorThunk (void);
extern void XrVector3f__ctor_mE6DAD65E298C88872C330C0A5EE282384FAE2D0C_AdjustorThunk (void);
extern void XrVector3f_ToVector3_m67309D501E9415F28F58B064E274AD69ED1B2455_AdjustorThunk (void);
extern void XrVector3f_ToString_m6D9D628E0DAD096FD0D3723CFDD7D9E6901A83F1_AdjustorThunk (void);
extern void XrHandGestureQCOM__ctor_m914EC3452AF5071A62AD3345CDE434285CF851B1_AdjustorThunk (void);
extern void XrHandGestureQCOM_get_Gesture_m4E999DAE36C6797A5493147F9F29E21C2BF9F0A5_AdjustorThunk (void);
extern void XrHandGestureQCOM_get_GestureRatio_m5E04D2CCC6BAC25F2E8560EDA3D471B7AE014556_AdjustorThunk (void);
extern void XrHandGestureQCOM_get_FlipRatio_m13546D6E39A468FBD1734435D08495951B4F2785_AdjustorThunk (void);
extern void XrHandJointLocationEXT_get_pose_mFA78A4F7BBD37CA6280AE3C951BD7879EE89DB41_AdjustorThunk (void);
extern void XrHandJointLocationsEXT__ctor_m0D12A0DCC179FDF9DBA8F067C8072E7B98AEC3F6_AdjustorThunk (void);
extern void XrHandJointLocationsEXT_get_IsActive_m0A68DDF8900B4CC714AC019BC540E77B6565A5D5_AdjustorThunk (void);
extern void XrHandJointLocationsEXT_get_JointCount_mC75E77B048DB6148FB577355FA1DFAF83D2A9267_AdjustorThunk (void);
extern void XrHandJointLocationsEXT_get_JointLocations_m56BA8627F5B7CA2BA9ADCDF72EFBC05C5897C3E1_AdjustorThunk (void);
extern void XrHandJointsLocateInfoEXT__ctor_mA49C108B1597D5FD8D6995A58B405FA704C2B46F_AdjustorThunk (void);
extern void XrHandTrackerCreateInfoEXT__ctor_m6F9563A07C20E6885F51F5A428A3BFFC87BEC97B_AdjustorThunk (void);
extern void XrRayCastCreateInfoQCOM__ctor_m68DE7FD4A34534AD378DF65AF8C3C41877EAEB2C_AdjustorThunk (void);
extern void XrRayCollisionQCOM_get_TargetId_mD6F97A47FD13AD014DFD78A060A76E54F1B3417E_AdjustorThunk (void);
extern void XrRayCollisionQCOM_get_Position_m3F35F66CE759A2D0DEA424B0D3110BCA73E4EADC_AdjustorThunk (void);
extern void XrRayCollisionQCOM_get_SurfaceNormal_m20D4DD727EF71A63BAA4D31361A656A5F3EC0CBA_AdjustorThunk (void);
extern void XrRayCollisionQCOM__ctor_m4F8A352EAED2EE7176296C9FD3757502B86353D7_AdjustorThunk (void);
extern void XrRayCollisionQCOM_ToString_m4488BA0A99E1DC859541D33B4B81114E538855D7_AdjustorThunk (void);
extern void XrRayCollisionsGetInfoQCOM__ctor_mE1F7A4E426D9DF4CA741BEFDF35259D0D6C37149_AdjustorThunk (void);
extern void XrRayCollisionsQCOM_get_Collisions_mE9E4205A7F22B2E28A60FB3BE42A0ACDE6FBAA7F_AdjustorThunk (void);
extern void XrRayCollisionsQCOM__ctor_m3F89E3FFDD2E276BCE00252958C795AB079AB960_AdjustorThunk (void);
extern void XrRayCollisionsQCOM__ctor_mCD6DC48F6634CA1C8ED3A63508573D2C0EA877E4_AdjustorThunk (void);
extern void XrImageTargetLocationQCOM_get_LocationFlags_m480BE04234F57D42B53BEFCDC02BD0C788D2854C_AdjustorThunk (void);
extern void XrImageTargetLocationQCOM_get_ImageTargetHandle_mF4D42C080A4D7CF533223F40CA8341BB67E6A07F_AdjustorThunk (void);
extern void XrImageTargetLocationQCOM_get_XrPose_mCF7B716AD6CEF043AFBAB4CB953EDFB4DF1BDD20_AdjustorThunk (void);
extern void XrImageTargetLocationsQCOM__ctor_mAD7AAE03A06E2ED5A1C422DD04FE6B27A0676C8D_AdjustorThunk (void);
extern void XrImageTargetLocationsQCOM_get_Count_m845436280D51DE2910F71B75077223B3D4655A8A_AdjustorThunk (void);
extern void XrImageTargetLocationsQCOM_get_ImageTargetLocationsPtr_mED494AF493F7E1A5965D0145B54F93C2819C5C78_AdjustorThunk (void);
extern void XrImageTargetsLocateInfoQCOM__ctor_mE60D4F7B12C7CEDD3BF35171287E3B5137D5B44E_AdjustorThunk (void);
extern void XrImageTargetsTrackingModeInfoQCOM__ctor_mF21A1217D975F220BAD61ED0454064ACDAE49768_AdjustorThunk (void);
extern void XrImageTrackerCreateInfoQCOM__ctor_m4EEBF84E77A1E1E4FFE770E46FF42B6CA1378295_AdjustorThunk (void);
extern void XrImageTrackerDataSetImageQCOM__ctor_m13420B9F18019E201E47AFEA44CC0C4E0680CD1E_AdjustorThunk (void);
extern void SceneUnderstandingMSFTPlane_get_VertexCount_mD2485F6B795486C4566D9AAC565ED9A3AB1E254A_AdjustorThunk (void);
extern void SceneUnderstandingMSFTPlane_get_IndexCount_mF0C611B76CADD2CCF2DADCB2330D9A2F14E1D2ED_AdjustorThunk (void);
extern void SceneUnderstandingMSFTPlane_get_Pose_m589F165643A3D65CB002F5395D1922CA863D2F0D_AdjustorThunk (void);
extern void SceneUnderstandingMSFTPlane_GetBoundedPlane_mEAC3E52128A7CFBE6A532673FC2E1E57BFFFCE8D_AdjustorThunk (void);
extern void SceneUnderstandingMSFTPlane_XrScenePlaneAlignmentTypeToPlaneAlignment_m180136E8B349E8D6BC1875951E36D52D9AED277E_AdjustorThunk (void);
extern void SceneUnderstandingMSFTPlane_GetTrackingState_mBF8451F916683716463C2EA604514E6659DC600C_AdjustorThunk (void);
extern void XrPlaneConvexHullBufferInfoQCOM__ctor_m6216FC2AD864CF71BFB322AD153B49D0BE188AE7_AdjustorThunk (void);
extern void XrPlaneConvexHullVertexBufferQCOM__ctor_m06D87B8A0B035C3D199FEC800009BE173F6CDB97_AdjustorThunk (void);
extern void XrPlaneConvexHullVertexBufferQCOM__ctor_m8F17950F6A9F35AFA3A8C8D5EECA6F682F99C212_AdjustorThunk (void);
extern void XrPlaneDetectionCreateInfoQCOM__ctor_m09D4C9EF97EE2904489A20E7E5150DA4972315EE_AdjustorThunk (void);
extern void XrPlaneExtentQCOM_get_ExtentX_mA43B232DF1EB85716DB7915E867FF9EEF779FF14_AdjustorThunk (void);
extern void XrPlaneExtentQCOM_get_ExtentY_m40E8D4402F3F490F0ECEC4A753F2AF888F1B352A_AdjustorThunk (void);
extern void XrPlaneLocationQCOM_get_ConvexHullId_mECBD3FEFFB2D85508052401E65354E90B98862A3_AdjustorThunk (void);
extern void XrPlaneLocationQCOM_GetBoundedPlane_mBFBE48AE22AF36F0CCAEBE6ACA62AC355CCB490B_AdjustorThunk (void);
extern void XrPlaneLocationQCOM_XrPlaneTypeToPlaneAlignment_mC084EB647B85BCD37B18C319F795F52243480165_AdjustorThunk (void);
extern void XrPlaneLocationsQCOM__ctor_m0732504CABB07CE7FB531E5DE2F3A0E1B6702BA9_AdjustorThunk (void);
extern void XrPlaneLocationsQCOM__ctor_mF62762AC161282C374C6990DC8677B01CC2D978C_AdjustorThunk (void);
extern void XrPlaneLocationsQCOM_get_PlaneCountOutput_mB291268D3ABA521FB809141A8D6E534397FC25B3_AdjustorThunk (void);
extern void XrPlaneLocationsQCOM_get_PlaneLocations_m9F42475FF18EF1BBA4B7F9D64C0ED19621C6E40E_AdjustorThunk (void);
extern void XrPlanesLocateInfoQCOM__ctor_m4F811F5724DA7A6A1E903B5C1B0981C0D4311FBF_AdjustorThunk (void);
extern void XrMarkerDetectionStartInfoQCOM__ctor_m4BE31B1DB8305A22473EBBB39C37C540DB57BBCF_AdjustorThunk (void);
extern void XrMarkerDetectionStartInfoQCOM__ctor_mCA884E7DE29D3C9B6369B613FD8822B9283F7C76_AdjustorThunk (void);
extern void XrMarkerSpaceCreateInfoQCOM__ctor_m50AB7485BB052A1E3266D4BEC03E50F774D1F935_AdjustorThunk (void);
extern void XrMarkerTrackingModeInfoQCOM__ctor_m2D680306B638728344DEB235C70995F87617D623_AdjustorThunk (void);
extern void XrMarkerUpdateInfoQCOM_get_Marker_mA80BD87CFDB08692508EA0594A5CC1B895139E6C_AdjustorThunk (void);
extern void XrMarkerUpdateInfoQCOM_get_IsDetected_mA1F9B8849E6EC7B8CF992E59EE8B71ACF45AAD11_AdjustorThunk (void);
extern void XrMarkerUpdateInfoQCOM_get_IsMarkerDataAvailable_m2F97E271C4DE68664F3438431623232E0245A48F_AdjustorThunk (void);
extern void XrMarkerUpdateInfoQCOM_ToString_m44B04704D107F400CF1C8CB036786C33628FF00C_AdjustorThunk (void);
extern void XrQrCodeVersionFilterQCOM__ctor_m6114340545DFA05ED9E671F6CDB0E7E9863E71EF_AdjustorThunk (void);
extern void XrQrCodeVersionRangeQCOM__ctor_mEE8968742EDBB3354417F01840242F4B8989C9DD_AdjustorThunk (void);
extern void XrUserDefinedMarkerSizeQCOM__ctor_m8CFE8D7BAB694A5A0068761AE3D3276216E63573_AdjustorThunk (void);
extern void XrSpatialAnchorCreateInfoMSFT__ctor_m30BEA3DC5CBAF77172B5CF78783DC2A0A4441DC6_AdjustorThunk (void);
extern void XrSpatialAnchorFromPersistedAnchorCreateInfoMSFT__ctor_m51AE46AEA9B4290FBC9288833FC4BE71C8C52B68_AdjustorThunk (void);
extern void XrSpatialAnchorPersistenceInfoMSFT__ctor_m597F42F50F6786918CF94BA946517678E9E2557B_AdjustorThunk (void);
extern void XrSpatialAnchorPersistenceNameMSFT__ctor_m79DF56CBF47872031AD00EFF1B94980060D679C8_AdjustorThunk (void);
extern void XrSpatialAnchorSpaceCreateInfoMSFT__ctor_m0E7262C30F18E0E1FE14CBC5E9F2DEDBE55EC70E_AdjustorThunk (void);
extern void Cinfo_get_id_mF3EBDAF7A344E71CBB5E82700C5B72F97FB69D5B_AdjustorThunk (void);
extern void Cinfo_set_id_mDC05FE6B201A56946C2C5EA912F8216DF9A09C92_AdjustorThunk (void);
extern void Cinfo_get_providerType_mA77251D3E258944822919C88EFD9B644664CF25A_AdjustorThunk (void);
extern void Cinfo_set_providerType_m6B9926F0E4230B88C381C91D2F5F42EFED493D6E_AdjustorThunk (void);
extern void Cinfo_get_subsystemTypeOverride_m46BA874D2F7FE887BC89D8FC6E5E63687BD0934D_AdjustorThunk (void);
extern void Cinfo_set_subsystemTypeOverride_mCF7CF4A13208D788BAAE060D96FAEF41723FA9B0_AdjustorThunk (void);
extern void Cinfo_GetHashCode_mD14AB2A03A219CED499DA7FF91AD90C29C13B117_AdjustorThunk (void);
extern void Cinfo_Equals_m374DC4526F1EC61A7F2A14FF9F7FF6051CA32FFD_AdjustorThunk (void);
extern void Cinfo_Equals_m7965F4DB417B97CB3972F630D2196E57284A4EA1_AdjustorThunk (void);
extern void XRTrackedHand_get_trackableId_m405042E716232C8EECA276E8639D53F5466E195D_AdjustorThunk (void);
extern void XRTrackedHand_get_nativePtr_mA695B53C1EE10B8B922A4E44F0FA37CFCE54E59F_AdjustorThunk (void);
extern void XRTrackedHand_get_pose_m45569784BDCB04B40CE717EF6043C8DCBF268BB9_AdjustorThunk (void);
extern void XRTrackedHand_set_pose_m4413C8A35AE5F773C992984623AEB3A6439C431E_AdjustorThunk (void);
extern void XRTrackedHand_get_trackingState_mE860352EC4FFF755F28F51A797A474E9088C20E9_AdjustorThunk (void);
extern void XRTrackedHand_set_trackingState_mB9D9B72BF2CA0EDD55B61B57A359D0088B653946_AdjustorThunk (void);
extern void XRTrackedHand__ctor_mB934D2C6BCF136E9648DCB30C91207C8A19BAF86_AdjustorThunk (void);
extern void XRTrackedHand_UpdatePoseAndTrackedState_m8D1B046B1613AF64B1688BCC8EBCC1501CF02E7D_AdjustorThunk (void);
extern void XRTrackedHand_GetHashCode_mD23C58F566A3F049FFA8A5185042B3D85BD6486E_AdjustorThunk (void);
extern void XRTrackedHand_Equals_mDB882CA9828980C6256982565930FAF1D4D0FC29_AdjustorThunk (void);
extern void XRTrackedHand_Equals_m2C2C8454E4B205407EE5AEB340B8BDE90F8D9CD2_AdjustorThunk (void);
extern void SpacesMarkersChangedEventArgs_get_added_mF8667E8CA2C87E2FE20F475940ADBC0722D97C9D_AdjustorThunk (void);
extern void SpacesMarkersChangedEventArgs_get_updated_mD1F9474086262DB5B1CED69A4F5AE73031151FF9_AdjustorThunk (void);
extern void SpacesMarkersChangedEventArgs_get_removed_m243A7EDEEFB7C0F520132232D090A991D5221A07_AdjustorThunk (void);
extern void SpacesMarkersChangedEventArgs__ctor_m2381BC40792947230BFA24DF519ACB5F0AE230BE_AdjustorThunk (void);
extern void SpacesMarkersChangedEventArgs_GetHashCode_m1D18494D3716BE3E5832F5FDDFDA0A336FF9FE63_AdjustorThunk (void);
extern void SpacesMarkersChangedEventArgs_Equals_mC9647B04CB0822A03BFDED271897C3CA02C04F58_AdjustorThunk (void);
extern void SpacesMarkersChangedEventArgs_Equals_mF42762FD75B2E3C660788392BB1A734F1EEEE5AA_AdjustorThunk (void);
extern void SpacesMarker__ctor_mC89B5CE6EB7669D170C5F200845D56A477AF2810_AdjustorThunk (void);
extern void SpacesMarker_UpdatePoseAndTrackingState_m0811C38B01B8D1324AAC1677912A6584D86BC730_AdjustorThunk (void);
extern void XRMarkerDescriptor_get_TrackingMode_mD80E7A2DFE0BB1FAAE9FDDDF022F9B811208C941_AdjustorThunk (void);
extern void XRMarkerDescriptor_get_Size_m8413EA768805484DAFA6ABC04F8AA37B9F858DC5_AdjustorThunk (void);
extern void XRMarkerDescriptor_get_QrCodeVersionRange_m95B6C333F887917DE81679CF8AA95BB76FE9C0F7_AdjustorThunk (void);
extern void XRMarkerDescriptor__ctor_m5527F572D3A297CB5FA443715835F6C37DB54D41_AdjustorThunk (void);
extern void XRMarkerDescriptor_Equals_m49C49CB847600ED9EAA595C48D099EF60EB1A43B_AdjustorThunk (void);
extern void XRMarkerDescriptor_GetHashCode_m89653348B8764C2FC89570E8871EC6D325948A48_AdjustorThunk (void);
extern void Cinfo_get_id_mEB29904AA813FB8BD587DCD994F8AFC20C99B742_AdjustorThunk (void);
extern void Cinfo_set_id_mE0C00BE3307B5E2BBE1E140CC20094040EE1B811_AdjustorThunk (void);
extern void Cinfo_get_providerType_m5A559C875D7CD9A293A377E656786D28511F08AF_AdjustorThunk (void);
extern void Cinfo_set_providerType_m3164B2DB59BAAF4BC8160BD6F4B8A466C5F5F7A6_AdjustorThunk (void);
extern void Cinfo_get_subsystemTypeOverride_m719F057762950FBB9D4A5A858D7A17E00A5A5EDC_AdjustorThunk (void);
extern void Cinfo_set_subsystemTypeOverride_m327467C849A070DE62F102D88CFD204B89B26BEF_AdjustorThunk (void);
extern void Cinfo_GetHashCode_mDCB386966FE8B59139CDFA71F7F0FBE3FC8DD2FC_AdjustorThunk (void);
extern void Cinfo_Equals_mE2F04B1A09111E4AA11D421E16C5789D222EC756_AdjustorThunk (void);
extern void Cinfo_Equals_m987CEE46F6C2B0E1C63883BB4FA09AFEC38C172D_AdjustorThunk (void);
extern void XRTrackedMarker_get_trackableId_m2FA500913E2999A7E28AC1AE311EB83A326C4B70_AdjustorThunk (void);
extern void XRTrackedMarker_get_pose_m7F044500815660018AC12237A0F1CC64CD31F88F_AdjustorThunk (void);
extern void XRTrackedMarker_get_trackingState_m9A85DE39F1DF84FD53FFD8F0113CF169C56BEFFE_AdjustorThunk (void);
extern void XRTrackedMarker_get_nativePtr_m016CB37BEC30269FE8D4D4FE1D7043C8EE195AEF_AdjustorThunk (void);
extern void XRTrackedMarker__ctor_mB4EC0B012C7E2AA4D83DCBB711778354E51808AE_AdjustorThunk (void);
extern void XRTrackedMarker_GetHashCode_m7F8CB1B3555F5D10ADD12F61900A75816000D52B_AdjustorThunk (void);
extern void XRTrackedMarker_Equals_mBF2E6DE4766E703265DDD427F0AC223D24855AEE_AdjustorThunk (void);
extern void XRTrackedMarker_Equals_m4F74FFE82E700A097FDB1C1787B53180BF6422EA_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[232] = 
{
	{ 0x060000E7, ComponentVersion__ctor_m1398FA8526AA2FC1AA0C9C002AED93F555B3BC8B_AdjustorThunk },
	{ 0x060000E8, XrCameraActivationFrameConfigurationQCOM__ctor_mC01CC056B56188B07DCAF655B305D78BE7575D9E_AdjustorThunk },
	{ 0x060000E9, XrCameraActivationFrameConfigurationQCOM_ToString_mA33EBA223A3F907709277F3029DB734BAB05D069_AdjustorThunk },
	{ 0x060000EA, XrCameraActivationInfoQCOM__ctor_m95201ED05481B95C234EFFA791DB372BDA561B85_AdjustorThunk },
	{ 0x060000EB, XrCameraActivationInfoQCOM_ToString_m336B7CD08F1239F2F96F7A195AFE12D2FF72D101_AdjustorThunk },
	{ 0x060000EC, XrCameraFrameBufferQCOM__ctor_mB4B112F9286FE81D0A56B047D932AF79CC5D10F2_AdjustorThunk },
	{ 0x060000ED, XrCameraFrameBufferQCOM__ctor_mE785229ABB8C9A2FB5A1688229B1AB80B7165879_AdjustorThunk },
	{ 0x060000EE, XrCameraFrameBufferQCOM_get_BufferSize_m950D5D3907A1869EEA881A2317C0AD3D7912B7A2_AdjustorThunk },
	{ 0x060000EF, XrCameraFrameBufferQCOM_get_Buffer_mF7F72DFD8B5EC6CA89CAE6CA9A912A93794DFC55_AdjustorThunk },
	{ 0x060000F0, XrCameraFrameBufferQCOM_get_HardwareBuffer_m0AF3D153BED58C85694CE0952F9291D393882A88_AdjustorThunk },
	{ 0x060000F1, XrCameraFrameBufferQCOM_get_Offset_m2A9616A87A8D114D5C6F5B877614803B56612757_AdjustorThunk },
	{ 0x060000F2, XrCameraFrameBufferQCOM_get_PlaneCount_m068F55256645F2AFB1FF6D402ABDF7DF8B178A6B_AdjustorThunk },
	{ 0x060000F3, XrCameraFrameBufferQCOM_get_Planes_m30C5177D7A5E5D9CA49C2DDC3F43A5593D4A6E29_AdjustorThunk },
	{ 0x060000F4, XrCameraFrameBufferQCOM_ToString_m1640A05E0ED21CB8C11B2B3F53551D9A20C1505C_AdjustorThunk },
	{ 0x060000F5, XrCameraFrameBufferQCOM_get_PlanesArray_m781AA15B1A8B108136DC68546FBB869BAB0A69C0_AdjustorThunk },
	{ 0x060000F6, XrCameraFrameBuffersQCOM__ctor_mFAF9A5A88C5AF80F029B5F89BDF3E3536AA4BB9E_AdjustorThunk },
	{ 0x060000F7, XrCameraFrameBuffersQCOM_get_FrameBufferCount_mBB3219FC8C8263519C8D9C03EB5960AD0A7625FE_AdjustorThunk },
	{ 0x060000F8, XrCameraFrameBuffersQCOM_get_FrameBuffers_mEF5A67FA8F90E32A6D8FB70D97582CDA20B9BF81_AdjustorThunk },
	{ 0x060000F9, XrCameraFrameBuffersQCOM_ToString_m35FC790BEB29947A3C585A1910AEF0CC10DD6D16_AdjustorThunk },
	{ 0x060000FA, XrCameraFrameConfigurationQCOM__ctor_m3942FC71C58D9E6031DEFDB639F394D4932A86E0_AdjustorThunk },
	{ 0x060000FB, XrCameraFrameConfigurationQCOM_get_Format_m6BAA09753953C3355C176B7D28BA0264C58A9F55_AdjustorThunk },
	{ 0x060000FC, XrCameraFrameConfigurationQCOM_get_ResolutionName_mC9FAE99F606993BC7CB1DAC117B8787CD8B7E7DF_AdjustorThunk },
	{ 0x060000FD, XrCameraFrameConfigurationQCOM_get_Dimensions_mA4CCECC8E2328C0B5CF51C328C9E49B8CA29C389_AdjustorThunk },
	{ 0x060000FE, XrCameraFrameConfigurationQCOM_get_MinFPS_m94122334221AE771D207F98FE47F1FA1B1B92686_AdjustorThunk },
	{ 0x060000FF, XrCameraFrameConfigurationQCOM_get_MaxFPS_m98188152C09B0C2283C8ABB89BDF52AE641BA34F_AdjustorThunk },
	{ 0x06000100, XrCameraFrameConfigurationQCOM_get_FrameBufferCount_mAEC0C4D48348E78839296BA0F17D16D1EDADCAD4_AdjustorThunk },
	{ 0x06000101, XrCameraFrameConfigurationQCOM_get_FrameHardwareBufferCount_mAC6A3CE4A06E7BFA5F5DDCE5E1D7F1AE22F4A882_AdjustorThunk },
	{ 0x06000102, XrCameraFrameConfigurationQCOM_ToString_m20BB1EE9D69A31994E54F082BF32C02DD73E8F33_AdjustorThunk },
	{ 0x06000103, XrCameraFrameConfigurationQCOM_Equals_m79BAF59F2489C2FC04698EEA69660B3720E94C16_AdjustorThunk },
	{ 0x06000104, XrCameraFrameConfigurationQCOM_Equals_m6C81D6EF68F4406E509277EDAC64592DF817FD32_AdjustorThunk },
	{ 0x06000105, XrCameraFrameConfigurationQCOM_GetHashCode_m88D0A1632ECF24BE403724F6BE31D41C9BAF5D96_AdjustorThunk },
	{ 0x06000106, XrCameraFrameDataQCOM__ctor_m21472D27A6AD1B4266106153194A8F2F1C95E11C_AdjustorThunk },
	{ 0x06000107, XrCameraFrameDataQCOM_get_Type_mE5BBBBD0E4DFDE0D12419924BB91C99CFB730F58_AdjustorThunk },
	{ 0x06000108, XrCameraFrameDataQCOM_get_Next_mCE6D17EB550A4FD2EE8A6EF399B18BD1600A0ACD_AdjustorThunk },
	{ 0x06000109, XrCameraFrameDataQCOM_get_Handle_m26A480ABB370791A1258A3E102BD9FB3CC74DD93_AdjustorThunk },
	{ 0x0600010A, XrCameraFrameDataQCOM_get_Format_m5D209DAFD825E8967B0DE6001ADDCBC71762AA0A_AdjustorThunk },
	{ 0x0600010B, XrCameraFrameDataQCOM_get_FrameNumber_m8B4ED328D6E36ED8B61A2919C4C82A6C03F61B10_AdjustorThunk },
	{ 0x0600010C, XrCameraFrameDataQCOM_get_Timestamp_mBF90CF8A92D202ECE328A6C19955215D983C509C_AdjustorThunk },
	{ 0x0600010D, XrCameraFrameDataQCOM_ToString_mCA5D1225589871DC98C2959C1F2B4B7BF01895D8_AdjustorThunk },
	{ 0x0600010E, XrCameraFrameHardwareBufferQCOM__ctor_mB52C3509013DAC2D369350950A83731A07C429CB_AdjustorThunk },
	{ 0x0600010F, XrCameraFrameHardwareBufferQCOM_ToString_m6ED5F07B3836FC15534D01364A1CC9B226EA36D4_AdjustorThunk },
	{ 0x06000110, XrCameraFramePlaneQCOM__ctor_m284891F8B4782C344844E1E3B769F8DC14A3F61D_AdjustorThunk },
	{ 0x06000111, XrCameraFramePlaneQCOM_get_PlaneType_m056E8B6DCF863AF69C363C8C9BBC6A17AFAC0550_AdjustorThunk },
	{ 0x06000112, XrCameraFramePlaneQCOM_get_Offset_m8AF8D2AF8562907454F27993C9C9853483FA114A_AdjustorThunk },
	{ 0x06000113, XrCameraFramePlaneQCOM_get_Stride_m090D94F7651FE1AB5559A37739AB90178422290B_AdjustorThunk },
	{ 0x06000114, XrCameraFramePlaneQCOM_ToString_m7F4DC8BA7B9EBEC86627F9DCF8AFD809E17A9D09_AdjustorThunk },
	{ 0x06000115, XrCameraInfoQCOM__ctor_mAE436B7F665A8313BC9A5A24A7159C437775207A_AdjustorThunk },
	{ 0x06000116, XrCameraInfoQCOM_get_CameraSet_mE444C2C329BA9C5F9A14A16310005465DCF958C1_AdjustorThunk },
	{ 0x06000117, XrCameraInfoQCOM_get_CameraType_m4F5E8A91874162DD8331097F58D02F32296FB66D_AdjustorThunk },
	{ 0x06000118, XrCameraInfoQCOM_get_SensorCount_m6AE604B7A97AE6C883978221F11CA564562E407D_AdjustorThunk },
	{ 0x06000119, XrCameraInfoQCOM_ToString_mE8AB2E88F245A4DE1C185D918273C782647F29B2_AdjustorThunk },
	{ 0x0600011A, XrCameraSensorInfosQCOM__ctor_m5051C95DC0D6173C0045176AA82A987E2BD79CBE_AdjustorThunk },
	{ 0x0600011B, XrCameraSensorInfosQCOM_ToString_m4887306CA657989C0FD510390E019AAF3F383F45_AdjustorThunk },
	{ 0x0600011C, XrCameraSensorIntrinsicsQCOM__ctor_mF823C9405A4F3ADE635106439C22625FD54D39BE_AdjustorThunk },
	{ 0x0600011D, XrCameraSensorIntrinsicsQCOM_get_PrincipalPoint_mF4C53A48DF15DA4539C81ECB8ABC8331749D2E90_AdjustorThunk },
	{ 0x0600011E, XrCameraSensorIntrinsicsQCOM_get_FocalLength_m61659D7731F9C69E4D10B948F15C9F337F64C2D6_AdjustorThunk },
	{ 0x0600011F, XrCameraSensorIntrinsicsQCOM_get_RadialDistortion_mEDD0949CEB1ED6CB742D5DD4A5686FA40D38B42B_AdjustorThunk },
	{ 0x06000120, XrCameraSensorIntrinsicsQCOM_get_TangentialDistortion_m649714F8CA4CCF1E0FDCDAFA70392E187E2D5992_AdjustorThunk },
	{ 0x06000121, XrCameraSensorIntrinsicsQCOM_get_DistortionModel_m8E397728AFDA69997EADE3EFD106E384425C5938_AdjustorThunk },
	{ 0x06000122, XrCameraSensorIntrinsicsQCOM_ToString_m1D9A21D03AE7EC2F530FAA9C1D7C80493B130733_AdjustorThunk },
	{ 0x06000123, XrCameraSensorPropertiesQCOM__ctor_mEAFD9FC1E283431CA018AC2F7255C8B57C057AA6_AdjustorThunk },
	{ 0x06000124, XrCameraSensorPropertiesQCOM_get_Intrinsics_m108ABACDFE8D9C6659ED98B336D80F337CBD376C_AdjustorThunk },
	{ 0x06000125, XrCameraSensorPropertiesQCOM_get_Extrinsic_m87075C50E31AA53E33661B75F5347765FFD7AA19_AdjustorThunk },
	{ 0x06000126, XrCameraSensorPropertiesQCOM_get_ImageOffset_mB40F948CF9DB970B4E7FA114ACBC1A90501CE9D4_AdjustorThunk },
	{ 0x06000127, XrCameraSensorPropertiesQCOM_get_ImageDimensions_mA4FE4D7DC903DCE7F160EE064509A3B14209414F_AdjustorThunk },
	{ 0x06000128, XrCameraSensorPropertiesQCOM_get_Facing_mF9019A5010372316F6EB0940630BA144310D778E_AdjustorThunk },
	{ 0x06000129, XrCameraSensorPropertiesQCOM_get_RollingShutterLineTime_mA9A434624C43BC2B0D916E001267235101D41EAC_AdjustorThunk },
	{ 0x0600012A, XrCameraSensorPropertiesQCOM_ToString_m0DF439C02486E2353F88E5CE3F0074ADD0A28D91_AdjustorThunk },
	{ 0x0600012B, Timespec_get_Seconds_m2E1C0153CD3BEC4805B547A871DBD327ACA911B2_AdjustorThunk },
	{ 0x0600012C, Timespec_get_Nanoseconds_mF4FDF78B2ED03D58C3D591FCC414F0DAFDDEB594_AdjustorThunk },
	{ 0x0600012D, XrComponentVersionQCOM__ctor_mC929B10383F3D44EFB2300FF17EACE1B6B88C51C_AdjustorThunk },
	{ 0x0600012E, XrExtent2Df_get_Width_mA8D6682438E1FDE0E5BBB25F3786D19E79B2CE70_AdjustorThunk },
	{ 0x0600012F, XrExtent2Df_get_Height_m8F6D94CCA19BEDE6F61051882F0A6CC9A4CA981C_AdjustorThunk },
	{ 0x06000130, XrExtent2Df__ctor_mA538CCEAF45364438C110B220C205691B6A953E6_AdjustorThunk },
	{ 0x06000131, XrExtent2DfQCOM_get_Min_m94AECAF0A8B83DDDF1D0633F00601745139DAF8A_AdjustorThunk },
	{ 0x06000132, XrExtent2DfQCOM_get_Max_m0A320B33A9969FF1F3CA9A3C5F4FDD1FE0B86B9C_AdjustorThunk },
	{ 0x06000133, XrExtent2Di__ctor_mB29746B125742CB6AD747BA5FE9356AC685D73A0_AdjustorThunk },
	{ 0x06000134, XrExtent2Di__ctor_m1D66F0C237CBCC874FE6E8A9523111C8FDBE6246_AdjustorThunk },
	{ 0x06000135, XrExtent2Di_ToVector2Int_m16AB6984BE425191D978319A34B7A6605F7FF495_AdjustorThunk },
	{ 0x06000136, XrExtent2Di_get_Width_m572E28B2072225975B15815FBD4C751999567319_AdjustorThunk },
	{ 0x06000137, XrExtent2Di_get_Height_mA7B78D20A9F26DDDA1F0A9270350C5752193C5A5_AdjustorThunk },
	{ 0x06000138, XrExtent2Di_ToString_m8BF3362EDF0DFBC8FCDF7F961F5A592EECA5C59D_AdjustorThunk },
	{ 0x06000139, XrFrameState_get_PredictedDisplayTime_m6FBB46748AABE30FFE537C362C2D396B70CDC824_AdjustorThunk },
	{ 0x0600013A, XrOffset2Di__ctor_m526ED1D1AE4472C41EC084A8652E4E81CAAC3B75_AdjustorThunk },
	{ 0x0600013B, XrOffset2Di__ctor_m6F07B24B95F5D1A6FE46CAA42920C5009112F4AD_AdjustorThunk },
	{ 0x0600013C, XrOffset2Di_ToVector2Int_mB7CFEA8B38030163FFDA08995B60F4DF67CC1E63_AdjustorThunk },
	{ 0x0600013D, XrPosef__ctor_m3D4E4162D410903083F144466FE9B2DEBB0C21BD_AdjustorThunk },
	{ 0x0600013E, XrPosef__ctor_mE7394EBD4A40A09890877E8DECD26C8B6569209B_AdjustorThunk },
	{ 0x0600013F, XrPosef_ToPose_mDBAF38DC00B0EE86768DD9CA3031FF569FCB2BD6_AdjustorThunk },
	{ 0x06000140, XrPosef_ToString_mC16B2E1F2C83C928C73B09A09F14CC2B371DCF27_AdjustorThunk },
	{ 0x06000141, XrQuaternionf__ctor_m0D486D3121CBEE2FE399F14876D4FEF9031DBA94_AdjustorThunk },
	{ 0x06000142, XrQuaternionf__ctor_mDA964313A2BBA72BED3617B247255478040C0E87_AdjustorThunk },
	{ 0x06000144, XrQuaternionf_ToQuaternion_m62B17D280617BFDAC4F5F2AB88061F00CBCD5522_AdjustorThunk },
	{ 0x06000145, XrQuaternionf_ToString_m870370BDABD005D8499212D09FA41F153D150061_AdjustorThunk },
	{ 0x06000146, XrSpaceLocation_InitStructureType_m42DB29E751707B06F9BD6F9D572DF0F5B9EC5466_AdjustorThunk },
	{ 0x06000147, XrSpaceLocation_GetPose_m45AE7E0E2ADECD3E177598478CDAD84EC9A6E2FA_AdjustorThunk },
	{ 0x06000148, XrSpaceLocation_GetTrackingState_mC12F5FB6F53F113B8717D98F01ED775F6AA4D536_AdjustorThunk },
	{ 0x06000149, XrSpaceLocation_ToString_mE8131ED4AC6143052739BB3ACAE76EBCA9A80734_AdjustorThunk },
	{ 0x0600014A, XrSystemGraphicsProperties_get_MaxSwapchainImageHeight_m043CAFF02E9975B3F693CCAE13D22CE7BEBC9FCD_AdjustorThunk },
	{ 0x0600014B, XrSystemGraphicsProperties_get_MaxSwapchainImageWidth_m4F8D3DEF2B72DDF06E14D4C0B17C57EAC01E2000_AdjustorThunk },
	{ 0x0600014C, XrSystemGraphicsProperties_get_MaxLayerCount_mE0298316C9D0D531C20B60C83803E03F624EB542_AdjustorThunk },
	{ 0x0600014D, XrSystemProperties__ctor_m9D2AC269F6F0BE29D1A4523057F1DC9DE9B9C7A9_AdjustorThunk },
	{ 0x0600014E, XrSystemProperties_GetGraphicsProperties_m9AD557E8D5670538BB5050A62B7A0F4210C4CC2C_AdjustorThunk },
	{ 0x0600014F, XrSystemProperties_GetTrackingProperties_m285EA2F0A78F1BC576B74AF66054F3EE3DF1C295_AdjustorThunk },
	{ 0x06000150, XrSystemTrackingProperties_get_OrientationTracking_mE950098329F529540C759AD9F44456F48748141C_AdjustorThunk },
	{ 0x06000151, XrSystemTrackingProperties_get_PositionTracking_m507982F3CE3D9ACA6597F6A4CD01F6C99D06466B_AdjustorThunk },
	{ 0x06000152, XrVector2f__ctor_m64BBC3D64D3404C4B8292BB00FB2C9B1AF4B7DDB_AdjustorThunk },
	{ 0x06000153, XrVector2f__ctor_m5FE06D12022775A9D701AAEB7572BAE5B66B8796_AdjustorThunk },
	{ 0x06000154, XrVector2f_ToVector2_m5A4EC5EB1E0D1F204CEFA8AA9FB4BEB42BDD44E5_AdjustorThunk },
	{ 0x06000155, XrVector3f__ctor_mD92D4AEDDCD261C251C659F451A7849DB6FF021B_AdjustorThunk },
	{ 0x06000156, XrVector3f__ctor_mE6DAD65E298C88872C330C0A5EE282384FAE2D0C_AdjustorThunk },
	{ 0x06000158, XrVector3f_ToVector3_m67309D501E9415F28F58B064E274AD69ED1B2455_AdjustorThunk },
	{ 0x06000159, XrVector3f_ToString_m6D9D628E0DAD096FD0D3723CFDD7D9E6901A83F1_AdjustorThunk },
	{ 0x0600015A, XrHandGestureQCOM__ctor_m914EC3452AF5071A62AD3345CDE434285CF851B1_AdjustorThunk },
	{ 0x0600015B, XrHandGestureQCOM_get_Gesture_m4E999DAE36C6797A5493147F9F29E21C2BF9F0A5_AdjustorThunk },
	{ 0x0600015C, XrHandGestureQCOM_get_GestureRatio_m5E04D2CCC6BAC25F2E8560EDA3D471B7AE014556_AdjustorThunk },
	{ 0x0600015D, XrHandGestureQCOM_get_FlipRatio_m13546D6E39A468FBD1734435D08495951B4F2785_AdjustorThunk },
	{ 0x0600015E, XrHandJointLocationEXT_get_pose_mFA78A4F7BBD37CA6280AE3C951BD7879EE89DB41_AdjustorThunk },
	{ 0x0600015F, XrHandJointLocationsEXT__ctor_m0D12A0DCC179FDF9DBA8F067C8072E7B98AEC3F6_AdjustorThunk },
	{ 0x06000160, XrHandJointLocationsEXT_get_IsActive_m0A68DDF8900B4CC714AC019BC540E77B6565A5D5_AdjustorThunk },
	{ 0x06000161, XrHandJointLocationsEXT_get_JointCount_mC75E77B048DB6148FB577355FA1DFAF83D2A9267_AdjustorThunk },
	{ 0x06000162, XrHandJointLocationsEXT_get_JointLocations_m56BA8627F5B7CA2BA9ADCDF72EFBC05C5897C3E1_AdjustorThunk },
	{ 0x06000163, XrHandJointsLocateInfoEXT__ctor_mA49C108B1597D5FD8D6995A58B405FA704C2B46F_AdjustorThunk },
	{ 0x06000164, XrHandTrackerCreateInfoEXT__ctor_m6F9563A07C20E6885F51F5A428A3BFFC87BEC97B_AdjustorThunk },
	{ 0x06000165, XrRayCastCreateInfoQCOM__ctor_m68DE7FD4A34534AD378DF65AF8C3C41877EAEB2C_AdjustorThunk },
	{ 0x06000166, XrRayCollisionQCOM_get_TargetId_mD6F97A47FD13AD014DFD78A060A76E54F1B3417E_AdjustorThunk },
	{ 0x06000167, XrRayCollisionQCOM_get_Position_m3F35F66CE759A2D0DEA424B0D3110BCA73E4EADC_AdjustorThunk },
	{ 0x06000168, XrRayCollisionQCOM_get_SurfaceNormal_m20D4DD727EF71A63BAA4D31361A656A5F3EC0CBA_AdjustorThunk },
	{ 0x06000169, XrRayCollisionQCOM__ctor_m4F8A352EAED2EE7176296C9FD3757502B86353D7_AdjustorThunk },
	{ 0x0600016A, XrRayCollisionQCOM_ToString_m4488BA0A99E1DC859541D33B4B81114E538855D7_AdjustorThunk },
	{ 0x0600016B, XrRayCollisionsGetInfoQCOM__ctor_mE1F7A4E426D9DF4CA741BEFDF35259D0D6C37149_AdjustorThunk },
	{ 0x0600016C, XrRayCollisionsQCOM_get_Collisions_mE9E4205A7F22B2E28A60FB3BE42A0ACDE6FBAA7F_AdjustorThunk },
	{ 0x0600016D, XrRayCollisionsQCOM__ctor_m3F89E3FFDD2E276BCE00252958C795AB079AB960_AdjustorThunk },
	{ 0x0600016E, XrRayCollisionsQCOM__ctor_mCD6DC48F6634CA1C8ED3A63508573D2C0EA877E4_AdjustorThunk },
	{ 0x0600016F, XrImageTargetLocationQCOM_get_LocationFlags_m480BE04234F57D42B53BEFCDC02BD0C788D2854C_AdjustorThunk },
	{ 0x06000170, XrImageTargetLocationQCOM_get_ImageTargetHandle_mF4D42C080A4D7CF533223F40CA8341BB67E6A07F_AdjustorThunk },
	{ 0x06000171, XrImageTargetLocationQCOM_get_XrPose_mCF7B716AD6CEF043AFBAB4CB953EDFB4DF1BDD20_AdjustorThunk },
	{ 0x06000172, XrImageTargetLocationsQCOM__ctor_mAD7AAE03A06E2ED5A1C422DD04FE6B27A0676C8D_AdjustorThunk },
	{ 0x06000173, XrImageTargetLocationsQCOM_get_Count_m845436280D51DE2910F71B75077223B3D4655A8A_AdjustorThunk },
	{ 0x06000174, XrImageTargetLocationsQCOM_get_ImageTargetLocationsPtr_mED494AF493F7E1A5965D0145B54F93C2819C5C78_AdjustorThunk },
	{ 0x06000175, XrImageTargetsLocateInfoQCOM__ctor_mE60D4F7B12C7CEDD3BF35171287E3B5137D5B44E_AdjustorThunk },
	{ 0x06000176, XrImageTargetsTrackingModeInfoQCOM__ctor_mF21A1217D975F220BAD61ED0454064ACDAE49768_AdjustorThunk },
	{ 0x06000177, XrImageTrackerCreateInfoQCOM__ctor_m4EEBF84E77A1E1E4FFE770E46FF42B6CA1378295_AdjustorThunk },
	{ 0x06000178, XrImageTrackerDataSetImageQCOM__ctor_m13420B9F18019E201E47AFEA44CC0C4E0680CD1E_AdjustorThunk },
	{ 0x06000179, SceneUnderstandingMSFTPlane_get_VertexCount_mD2485F6B795486C4566D9AAC565ED9A3AB1E254A_AdjustorThunk },
	{ 0x0600017A, SceneUnderstandingMSFTPlane_get_IndexCount_mF0C611B76CADD2CCF2DADCB2330D9A2F14E1D2ED_AdjustorThunk },
	{ 0x0600017B, SceneUnderstandingMSFTPlane_get_Pose_m589F165643A3D65CB002F5395D1922CA863D2F0D_AdjustorThunk },
	{ 0x0600017C, SceneUnderstandingMSFTPlane_GetBoundedPlane_mEAC3E52128A7CFBE6A532673FC2E1E57BFFFCE8D_AdjustorThunk },
	{ 0x0600017D, SceneUnderstandingMSFTPlane_XrScenePlaneAlignmentTypeToPlaneAlignment_m180136E8B349E8D6BC1875951E36D52D9AED277E_AdjustorThunk },
	{ 0x0600017E, SceneUnderstandingMSFTPlane_GetTrackingState_mBF8451F916683716463C2EA604514E6659DC600C_AdjustorThunk },
	{ 0x0600017F, XrPlaneConvexHullBufferInfoQCOM__ctor_m6216FC2AD864CF71BFB322AD153B49D0BE188AE7_AdjustorThunk },
	{ 0x06000180, XrPlaneConvexHullVertexBufferQCOM__ctor_m06D87B8A0B035C3D199FEC800009BE173F6CDB97_AdjustorThunk },
	{ 0x06000181, XrPlaneConvexHullVertexBufferQCOM__ctor_m8F17950F6A9F35AFA3A8C8D5EECA6F682F99C212_AdjustorThunk },
	{ 0x06000182, XrPlaneDetectionCreateInfoQCOM__ctor_m09D4C9EF97EE2904489A20E7E5150DA4972315EE_AdjustorThunk },
	{ 0x06000183, XrPlaneExtentQCOM_get_ExtentX_mA43B232DF1EB85716DB7915E867FF9EEF779FF14_AdjustorThunk },
	{ 0x06000184, XrPlaneExtentQCOM_get_ExtentY_m40E8D4402F3F490F0ECEC4A753F2AF888F1B352A_AdjustorThunk },
	{ 0x06000185, XrPlaneLocationQCOM_get_ConvexHullId_mECBD3FEFFB2D85508052401E65354E90B98862A3_AdjustorThunk },
	{ 0x06000186, XrPlaneLocationQCOM_GetBoundedPlane_mBFBE48AE22AF36F0CCAEBE6ACA62AC355CCB490B_AdjustorThunk },
	{ 0x06000187, XrPlaneLocationQCOM_XrPlaneTypeToPlaneAlignment_mC084EB647B85BCD37B18C319F795F52243480165_AdjustorThunk },
	{ 0x06000188, XrPlaneLocationsQCOM__ctor_m0732504CABB07CE7FB531E5DE2F3A0E1B6702BA9_AdjustorThunk },
	{ 0x06000189, XrPlaneLocationsQCOM__ctor_mF62762AC161282C374C6990DC8677B01CC2D978C_AdjustorThunk },
	{ 0x0600018A, XrPlaneLocationsQCOM_get_PlaneCountOutput_mB291268D3ABA521FB809141A8D6E534397FC25B3_AdjustorThunk },
	{ 0x0600018B, XrPlaneLocationsQCOM_get_PlaneLocations_m9F42475FF18EF1BBA4B7F9D64C0ED19621C6E40E_AdjustorThunk },
	{ 0x0600018C, XrPlanesLocateInfoQCOM__ctor_m4F811F5724DA7A6A1E903B5C1B0981C0D4311FBF_AdjustorThunk },
	{ 0x0600018D, XrMarkerDetectionStartInfoQCOM__ctor_m4BE31B1DB8305A22473EBBB39C37C540DB57BBCF_AdjustorThunk },
	{ 0x0600018E, XrMarkerDetectionStartInfoQCOM__ctor_mCA884E7DE29D3C9B6369B613FD8822B9283F7C76_AdjustorThunk },
	{ 0x0600018F, XrMarkerSpaceCreateInfoQCOM__ctor_m50AB7485BB052A1E3266D4BEC03E50F774D1F935_AdjustorThunk },
	{ 0x06000191, XrMarkerTrackingModeInfoQCOM__ctor_m2D680306B638728344DEB235C70995F87617D623_AdjustorThunk },
	{ 0x06000192, XrMarkerUpdateInfoQCOM_get_Marker_mA80BD87CFDB08692508EA0594A5CC1B895139E6C_AdjustorThunk },
	{ 0x06000193, XrMarkerUpdateInfoQCOM_get_IsDetected_mA1F9B8849E6EC7B8CF992E59EE8B71ACF45AAD11_AdjustorThunk },
	{ 0x06000194, XrMarkerUpdateInfoQCOM_get_IsMarkerDataAvailable_m2F97E271C4DE68664F3438431623232E0245A48F_AdjustorThunk },
	{ 0x06000195, XrMarkerUpdateInfoQCOM_ToString_m44B04704D107F400CF1C8CB036786C33628FF00C_AdjustorThunk },
	{ 0x06000196, XrQrCodeVersionFilterQCOM__ctor_m6114340545DFA05ED9E671F6CDB0E7E9863E71EF_AdjustorThunk },
	{ 0x06000197, XrQrCodeVersionRangeQCOM__ctor_mEE8968742EDBB3354417F01840242F4B8989C9DD_AdjustorThunk },
	{ 0x06000198, XrUserDefinedMarkerSizeQCOM__ctor_m8CFE8D7BAB694A5A0068761AE3D3276216E63573_AdjustorThunk },
	{ 0x0600019B, XrSpatialAnchorCreateInfoMSFT__ctor_m30BEA3DC5CBAF77172B5CF78783DC2A0A4441DC6_AdjustorThunk },
	{ 0x0600019C, XrSpatialAnchorFromPersistedAnchorCreateInfoMSFT__ctor_m51AE46AEA9B4290FBC9288833FC4BE71C8C52B68_AdjustorThunk },
	{ 0x0600019D, XrSpatialAnchorPersistenceInfoMSFT__ctor_m597F42F50F6786918CF94BA946517678E9E2557B_AdjustorThunk },
	{ 0x0600019E, XrSpatialAnchorPersistenceNameMSFT__ctor_m79DF56CBF47872031AD00EFF1B94980060D679C8_AdjustorThunk },
	{ 0x0600019F, XrSpatialAnchorSpaceCreateInfoMSFT__ctor_m0E7262C30F18E0E1FE14CBC5E9F2DEDBE55EC70E_AdjustorThunk },
	{ 0x0600022F, Cinfo_get_id_mF3EBDAF7A344E71CBB5E82700C5B72F97FB69D5B_AdjustorThunk },
	{ 0x06000230, Cinfo_set_id_mDC05FE6B201A56946C2C5EA912F8216DF9A09C92_AdjustorThunk },
	{ 0x06000231, Cinfo_get_providerType_mA77251D3E258944822919C88EFD9B644664CF25A_AdjustorThunk },
	{ 0x06000232, Cinfo_set_providerType_m6B9926F0E4230B88C381C91D2F5F42EFED493D6E_AdjustorThunk },
	{ 0x06000233, Cinfo_get_subsystemTypeOverride_m46BA874D2F7FE887BC89D8FC6E5E63687BD0934D_AdjustorThunk },
	{ 0x06000234, Cinfo_set_subsystemTypeOverride_mCF7CF4A13208D788BAAE060D96FAEF41723FA9B0_AdjustorThunk },
	{ 0x06000235, Cinfo_GetHashCode_mD14AB2A03A219CED499DA7FF91AD90C29C13B117_AdjustorThunk },
	{ 0x06000236, Cinfo_Equals_m374DC4526F1EC61A7F2A14FF9F7FF6051CA32FFD_AdjustorThunk },
	{ 0x06000237, Cinfo_Equals_m7965F4DB417B97CB3972F630D2196E57284A4EA1_AdjustorThunk },
	{ 0x06000246, XRTrackedHand_get_trackableId_m405042E716232C8EECA276E8639D53F5466E195D_AdjustorThunk },
	{ 0x06000247, XRTrackedHand_get_nativePtr_mA695B53C1EE10B8B922A4E44F0FA37CFCE54E59F_AdjustorThunk },
	{ 0x06000248, XRTrackedHand_get_pose_m45569784BDCB04B40CE717EF6043C8DCBF268BB9_AdjustorThunk },
	{ 0x06000249, XRTrackedHand_set_pose_m4413C8A35AE5F773C992984623AEB3A6439C431E_AdjustorThunk },
	{ 0x0600024A, XRTrackedHand_get_trackingState_mE860352EC4FFF755F28F51A797A474E9088C20E9_AdjustorThunk },
	{ 0x0600024B, XRTrackedHand_set_trackingState_mB9D9B72BF2CA0EDD55B61B57A359D0088B653946_AdjustorThunk },
	{ 0x0600024D, XRTrackedHand__ctor_mB934D2C6BCF136E9648DCB30C91207C8A19BAF86_AdjustorThunk },
	{ 0x0600024E, XRTrackedHand_UpdatePoseAndTrackedState_m8D1B046B1613AF64B1688BCC8EBCC1501CF02E7D_AdjustorThunk },
	{ 0x0600024F, XRTrackedHand_GetHashCode_mD23C58F566A3F049FFA8A5185042B3D85BD6486E_AdjustorThunk },
	{ 0x06000250, XRTrackedHand_Equals_mDB882CA9828980C6256982565930FAF1D4D0FC29_AdjustorThunk },
	{ 0x06000251, XRTrackedHand_Equals_m2C2C8454E4B205407EE5AEB340B8BDE90F8D9CD2_AdjustorThunk },
	{ 0x06000369, SpacesMarkersChangedEventArgs_get_added_mF8667E8CA2C87E2FE20F475940ADBC0722D97C9D_AdjustorThunk },
	{ 0x0600036A, SpacesMarkersChangedEventArgs_get_updated_mD1F9474086262DB5B1CED69A4F5AE73031151FF9_AdjustorThunk },
	{ 0x0600036B, SpacesMarkersChangedEventArgs_get_removed_m243A7EDEEFB7C0F520132232D090A991D5221A07_AdjustorThunk },
	{ 0x0600036C, SpacesMarkersChangedEventArgs__ctor_m2381BC40792947230BFA24DF519ACB5F0AE230BE_AdjustorThunk },
	{ 0x0600036D, SpacesMarkersChangedEventArgs_GetHashCode_m1D18494D3716BE3E5832F5FDDFDA0A336FF9FE63_AdjustorThunk },
	{ 0x0600036E, SpacesMarkersChangedEventArgs_Equals_mC9647B04CB0822A03BFDED271897C3CA02C04F58_AdjustorThunk },
	{ 0x0600036F, SpacesMarkersChangedEventArgs_Equals_mF42762FD75B2E3C660788392BB1A734F1EEEE5AA_AdjustorThunk },
	{ 0x060003E3, SpacesMarker__ctor_mC89B5CE6EB7669D170C5F200845D56A477AF2810_AdjustorThunk },
	{ 0x060003E4, SpacesMarker_UpdatePoseAndTrackingState_m0811C38B01B8D1324AAC1677912A6584D86BC730_AdjustorThunk },
	{ 0x060003E5, XRMarkerDescriptor_get_TrackingMode_mD80E7A2DFE0BB1FAAE9FDDDF022F9B811208C941_AdjustorThunk },
	{ 0x060003E6, XRMarkerDescriptor_get_Size_m8413EA768805484DAFA6ABC04F8AA37B9F858DC5_AdjustorThunk },
	{ 0x060003E7, XRMarkerDescriptor_get_QrCodeVersionRange_m95B6C333F887917DE81679CF8AA95BB76FE9C0F7_AdjustorThunk },
	{ 0x060003E8, XRMarkerDescriptor__ctor_m5527F572D3A297CB5FA443715835F6C37DB54D41_AdjustorThunk },
	{ 0x060003EA, XRMarkerDescriptor_Equals_m49C49CB847600ED9EAA595C48D099EF60EB1A43B_AdjustorThunk },
	{ 0x060003EB, XRMarkerDescriptor_GetHashCode_m89653348B8764C2FC89570E8871EC6D325948A48_AdjustorThunk },
	{ 0x060003F9, Cinfo_get_id_mEB29904AA813FB8BD587DCD994F8AFC20C99B742_AdjustorThunk },
	{ 0x060003FA, Cinfo_set_id_mE0C00BE3307B5E2BBE1E140CC20094040EE1B811_AdjustorThunk },
	{ 0x060003FB, Cinfo_get_providerType_m5A559C875D7CD9A293A377E656786D28511F08AF_AdjustorThunk },
	{ 0x060003FC, Cinfo_set_providerType_m3164B2DB59BAAF4BC8160BD6F4B8A466C5F5F7A6_AdjustorThunk },
	{ 0x060003FD, Cinfo_get_subsystemTypeOverride_m719F057762950FBB9D4A5A858D7A17E00A5A5EDC_AdjustorThunk },
	{ 0x060003FE, Cinfo_set_subsystemTypeOverride_m327467C849A070DE62F102D88CFD204B89B26BEF_AdjustorThunk },
	{ 0x060003FF, Cinfo_GetHashCode_mDCB386966FE8B59139CDFA71F7F0FBE3FC8DD2FC_AdjustorThunk },
	{ 0x06000400, Cinfo_Equals_mE2F04B1A09111E4AA11D421E16C5789D222EC756_AdjustorThunk },
	{ 0x06000401, Cinfo_Equals_m987CEE46F6C2B0E1C63883BB4FA09AFEC38C172D_AdjustorThunk },
	{ 0x06000404, XRTrackedMarker_get_trackableId_m2FA500913E2999A7E28AC1AE311EB83A326C4B70_AdjustorThunk },
	{ 0x06000405, XRTrackedMarker_get_pose_m7F044500815660018AC12237A0F1CC64CD31F88F_AdjustorThunk },
	{ 0x06000406, XRTrackedMarker_get_trackingState_m9A85DE39F1DF84FD53FFD8F0113CF169C56BEFFE_AdjustorThunk },
	{ 0x06000407, XRTrackedMarker_get_nativePtr_m016CB37BEC30269FE8D4D4FE1D7043C8EE195AEF_AdjustorThunk },
	{ 0x06000408, XRTrackedMarker__ctor_mB4EC0B012C7E2AA4D83DCBB711778354E51808AE_AdjustorThunk },
	{ 0x0600040A, XRTrackedMarker_GetHashCode_m7F8CB1B3555F5D10ADD12F61900A75816000D52B_AdjustorThunk },
	{ 0x0600040B, XRTrackedMarker_Equals_mBF2E6DE4766E703265DDD427F0AC223D24855AEE_AdjustorThunk },
	{ 0x0600040C, XRTrackedMarker_Equals_m4F74FFE82E700A097FDB1C1787B53180BF6422EA_AdjustorThunk },
};
static const int32_t s_InvokerIndices[1207] = 
{
	11528,
	10706,
	11528,
	3561,
	1767,
	1072,
	521,
	522,
	5033,
	6429,
	5062,
	2747,
	2762,
	1435,
	6417,
	5139,
	6325,
	6417,
	6325,
	6456,
	6325,
	6325,
	6339,
	6228,
	6325,
	3677,
	6429,
	6429,
	6429,
	6429,
	6228,
	4489,
	1597,
	3677,
	1143,
	6228,
	1734,
	1646,
	267,
	6429,
	5181,
	6429,
	11571,
	2759,
	795,
	194,
	1875,
	2759,
	380,
	128,
	1875,
	2759,
	1177,
	405,
	1159,
	2759,
	4284,
	1226,
	4234,
	2759,
	1177,
	405,
	1159,
	2759,
	4284,
	1226,
	4234,
	2759,
	1177,
	405,
	1159,
	2759,
	1182,
	410,
	1875,
	11571,
	6429,
	6325,
	6325,
	6228,
	6228,
	6418,
	6418,
	5140,
	6228,
	6228,
	4968,
	6418,
	6418,
	5140,
	6126,
	4868,
	6429,
	6429,
	6429,
	1540,
	3453,
	3453,
	1766,
	6228,
	3958,
	6429,
	6417,
	5139,
	6417,
	6429,
	6429,
	6429,
	4162,
	6429,
	6418,
	6295,
	6421,
	6294,
	6074,
	6074,
	6228,
	244,
	6429,
	2842,
	6429,
	6228,
	6325,
	6228,
	6325,
	3677,
	5140,
	5140,
	5140,
	6429,
	6429,
	6429,
	6429,
	5140,
	6429,
	6429,
	6325,
	5062,
	6429,
	3630,
	3453,
	4968,
	6228,
	6228,
	10694,
	11496,
	11496,
	10333,
	4424,
	7105,
	10716,
	10336,
	10716,
	8444,
	7290,
	7745,
	9363,
	9361,
	10716,
	11496,
	7744,
	11496,
	1247,
	5139,
	4425,
	5139,
	2854,
	2851,
	2855,
	2852,
	2846,
	6228,
	1019,
	6228,
	6295,
	10706,
	10719,
	10293,
	6429,
	10293,
	10293,
	10702,
	9263,
	6228,
	6228,
	6470,
	6429,
	6294,
	6429,
	11571,
	2759,
	1181,
	409,
	4234,
	2759,
	795,
	194,
	1875,
	2759,
	5140,
	1226,
	5062,
	2759,
	795,
	194,
	1875,
	2759,
	1992,
	835,
	1875,
	2759,
	1996,
	837,
	4234,
	2759,
	5187,
	1229,
	5062,
	2759,
	5033,
	1201,
	5062,
	2759,
	2514,
	808,
	5062,
	2759,
	1183,
	411,
	4234,
	486,
	921,
	6325,
	5062,
	6325,
	503,
	2880,
	6417,
	6296,
	6296,
	6465,
	6417,
	6325,
	6325,
	6325,
	1388,
	6417,
	6296,
	6325,
	139,
	6294,
	6325,
	6463,
	6417,
	6417,
	6417,
	6417,
	6325,
	3727,
	3589,
	6294,
	465,
	6294,
	6296,
	6418,
	6294,
	6417,
	6295,
	6325,
	5035,
	6325,
	1378,
	6294,
	6417,
	6417,
	6325,
	1426,
	6325,
	6294,
	6417,
	6325,
	1486,
	6325,
	505,
	6473,
	6473,
	6325,
	6325,
	6294,
	6325,
	247,
	6460,
	6466,
	6465,
	6463,
	6228,
	6418,
	6325,
	6295,
	6295,
	486,
	6372,
	6372,
	2823,
	6372,
	6372,
	5143,
	2512,
	6421,
	6417,
	6417,
	6325,
	6295,
	5143,
	2512,
	6421,
	5076,
	2892,
	6339,
	6325,
	5079,
	1011,
	11586,
	6345,
	6325,
	6429,
	6339,
	6294,
	6325,
	6417,
	6417,
	6417,
	5140,
	6469,
	6471,
	6228,
	6228,
	5142,
	2823,
	6420,
	5144,
	1471,
	11587,
	6422,
	6325,
	1374,
	6294,
	6372,
	6372,
	6466,
	2514,
	6228,
	6294,
	6296,
	2858,
	5033,
	1377,
	6418,
	6422,
	6422,
	243,
	6325,
	5140,
	6296,
	5035,
	1478,
	6418,
	6418,
	6466,
	2849,
	6294,
	6296,
	2858,
	1366,
	927,
	492,
	6417,
	6417,
	6339,
	3215,
	4211,
	6294,
	5140,
	5035,
	1478,
	2472,
	6462,
	6462,
	6418,
	3216,
	4211,
	5035,
	1478,
	6296,
	6296,
	2858,
	1478,
	2849,
	2731,
	6429,
	5033,
	6418,
	6228,
	6228,
	6325,
	2849,
	1350,
	5186,
	6429,
	6429,
	1466,
	2859,
	2777,
	5062,
	5140,
	10404,
	10404,
	10404,
	6420,
	5142,
	6345,
	5079,
	6422,
	5144,
	6417,
	5139,
	6429,
	6429,
	6429,
	6429,
	6429,
	6429,
	6429,
	11571,
	6429,
	6325,
	6418,
	6418,
	6294,
	6294,
	6418,
	5140,
	6294,
	6228,
	6228,
	6296,
	6272,
	6429,
	6429,
	6429,
	6325,
	3094,
	6429,
	6429,
	2877,
	5173,
	6429,
	6429,
	6418,
	5140,
	6418,
	5140,
	6418,
	5140,
	6418,
	5140,
	6228,
	4968,
	6294,
	5033,
	6228,
	3677,
	6429,
	0,
	5140,
	5140,
	5140,
	5140,
	2512,
	5140,
	5140,
	5140,
	4489,
	6325,
	10581,
	4424,
	6429,
	2759,
	1168,
	394,
	1875,
	11571,
	6429,
	3589,
	11571,
	6429,
	10715,
	11571,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	6325,
	6294,
	6372,
	6372,
	6339,
	6228,
	4968,
	6325,
	6325,
	5062,
	6429,
	2147,
	1374,
	2544,
	6429,
	6429,
	5062,
	6429,
	6429,
	6325,
	6325,
	6429,
	6429,
	5062,
	5062,
	6325,
	6325,
	5062,
	6325,
	2790,
	1440,
	4500,
	6429,
	1440,
	6325,
	6325,
	6325,
	5361,
	10725,
	6325,
	5062,
	6325,
	5062,
	6325,
	5062,
	6294,
	3894,
	3589,
	8823,
	8823,
	6449,
	6449,
	3180,
	2835,
	1012,
	6429,
	0,
	0,
	0,
	0,
	0,
	6429,
	6405,
	6296,
	6339,
	5076,
	6294,
	5033,
	11582,
	2836,
	2806,
	6294,
	3721,
	3589,
	8802,
	8802,
	11571,
	2878,
	6449,
	6228,
	2806,
	6418,
	6418,
	6228,
	6228,
	6228,
	4477,
	4477,
	6325,
	3677,
	6429,
	6429,
	6429,
	6429,
	6429,
	11571,
	2759,
	1177,
	405,
	1159,
	2759,
	4284,
	1226,
	4234,
	2759,
	1177,
	405,
	1159,
	2759,
	1182,
	410,
	1875,
	11571,
	6429,
	6429,
	6429,
	6429,
	6449,
	6449,
	1559,
	2835,
	1012,
	854,
	4500,
	6429,
	3453,
	3677,
	537,
	3677,
	1142,
	6325,
	3677,
	6429,
	6429,
	6429,
	6429,
	6429,
	11571,
	2759,
	1178,
	406,
	1875,
	2759,
	4284,
	1226,
	4234,
	2759,
	1992,
	835,
	1875,
	2759,
	382,
	130,
	4234,
	2759,
	1177,
	405,
	1159,
	2861,
	6418,
	2876,
	11571,
	6429,
	6429,
	6429,
	1558,
	1146,
	1124,
	5130,
	508,
	509,
	250,
	6429,
	3589,
	11571,
	6429,
	2018,
	2018,
	6429,
	3589,
	6228,
	1063,
	3677,
	1138,
	1140,
	1686,
	6325,
	3677,
	6429,
	6429,
	6429,
	6429,
	1137,
	6429,
	11571,
	2759,
	1177,
	405,
	1159,
	2759,
	4284,
	1226,
	4234,
	2759,
	1177,
	405,
	1159,
	2759,
	381,
	129,
	1159,
	2759,
	1998,
	838,
	4234,
	2759,
	4284,
	1226,
	4234,
	2759,
	1180,
	408,
	4234,
	2863,
	6418,
	11571,
	6429,
	5062,
	6294,
	5033,
	5062,
	6429,
	6429,
	6429,
	1560,
	4489,
	5062,
	2776,
	6429,
	6429,
	2759,
	6325,
	2144,
	4489,
	6429,
	3722,
	6429,
	2757,
	4234,
	3589,
	2774,
	6429,
	6429,
	6429,
	6325,
	6325,
	5062,
	6325,
	6325,
	6294,
	2757,
	4234,
	2757,
	6429,
	6429,
	11571,
	6429,
	4211,
	2762,
	6294,
	6325,
	5402,
	2285,
	6228,
	6418,
	6228,
	5062,
	5062,
	1248,
	3589,
	6228,
	3453,
	1753,
	6325,
	4424,
	3677,
	6429,
	6429,
	6429,
	5140,
	5140,
	6429,
	4211,
	6228,
	3453,
	3453,
	1753,
	1753,
	10333,
	9335,
	10706,
	10717,
	10717,
	10717,
	10173,
	11496,
	10161,
	8765,
	7872,
	6429,
	11571,
	2759,
	1177,
	405,
	1159,
	2759,
	4284,
	1226,
	4234,
	2759,
	1177,
	405,
	1159,
	2759,
	1992,
	835,
	1875,
	2759,
	1177,
	405,
	1159,
	11571,
	6429,
	6294,
	6294,
	5033,
	6429,
	6429,
	6429,
	1475,
	1556,
	6429,
	6429,
	6228,
	6228,
	4968,
	6429,
	6429,
	6228,
	4968,
	6325,
	5062,
	6429,
	6325,
	6325,
	6325,
	1440,
	6294,
	3589,
	3631,
	6325,
	5062,
	6228,
	4968,
	5062,
	5062,
	6429,
	6325,
	6325,
	2792,
	1440,
	6429,
	6429,
	6228,
	3677,
	6429,
	6429,
	6429,
	6429,
	3453,
	3677,
	1754,
	3677,
	1753,
	1137,
	3677,
	535,
	4211,
	3677,
	1753,
	1753,
	6429,
	11571,
	2759,
	795,
	194,
	1875,
	2759,
	795,
	194,
	1875,
	2759,
	1177,
	405,
	1159,
	2759,
	4284,
	1226,
	4234,
	2759,
	1992,
	835,
	1875,
	2759,
	4284,
	1226,
	4234,
	2759,
	1994,
	836,
	4234,
	2759,
	1994,
	836,
	4234,
	2759,
	1994,
	836,
	4234,
	2759,
	795,
	194,
	1875,
	2759,
	795,
	194,
	1875,
	2759,
	1177,
	405,
	1159,
	2759,
	1992,
	835,
	1875,
	2759,
	1992,
	835,
	1875,
	2759,
	1177,
	405,
	1159,
	2759,
	4284,
	1226,
	4234,
	2759,
	796,
	195,
	1875,
	2759,
	4284,
	1226,
	4234,
	11571,
	6429,
	6429,
	6429,
	6429,
	1561,
	5163,
	1735,
	4968,
	6429,
	2862,
	5062,
	6294,
	6420,
	6325,
	1379,
	11576,
	3708,
	6294,
	11571,
	3182,
	5163,
	1735,
	4968,
	6429,
	0,
	0,
	0,
	0,
	6429,
	5366,
	10728,
	6325,
	5062,
	6325,
	5062,
	6325,
	5062,
	6294,
	3899,
	3589,
	8827,
	8827,
	6405,
	6339,
	6294,
	6296,
	1013,
	11584,
	6294,
	3723,
	3589,
	8804,
	8804,
	11571,
	6429,
	6429,
	6429,
	6429,
	6429,
	6429,
	1440,
	2762,
	1440,
	2762,
	3589,
	2762,
	5062,
	5062,
	6429,
	6325,
	4489,
	6429,
	1440,
	2762,
	6429,
	5033,
	500,
	3589,
	6228,
	4655,
	4658,
	3677,
	4503,
	3453,
	3677,
	3677,
	536,
	1754,
	1139,
	1753,
	6325,
	3677,
	6429,
	6429,
	6429,
	6429,
	6429,
	11571,
	2759,
	1184,
	412,
	1875,
	2759,
	1185,
	413,
	1875,
	2759,
	4284,
	1226,
	4234,
	2759,
	1992,
	835,
	1875,
	2759,
	4284,
	1226,
	4234,
	2759,
	1992,
	835,
	1875,
	2759,
	1992,
	835,
	1875,
	2759,
	795,
	194,
	1875,
	2759,
	1177,
	405,
	1159,
	2759,
	4284,
	1226,
	4234,
	2759,
	796,
	195,
	1875,
	11571,
	6429,
	6429,
	6429,
	6429,
	6429,
	1557,
	1701,
	5062,
	3666,
	1754,
	4500,
	4656,
	2837,
	6429,
	6429,
	3589,
	2763,
	6429,
	3589,
	6228,
	4968,
	6228,
	4968,
	6372,
	5100,
	6429,
	6429,
	6429,
	10702,
	9254,
	6429,
	8697,
	4943,
	6429,
	11571,
	6429,
	4489,
	6228,
	6325,
	4424,
	3677,
	6429,
	6429,
	6429,
	5140,
	5140,
	10333,
	11571,
	10717,
	10717,
	10717,
	9335,
	10706,
	6429,
	11571,
	2759,
	1177,
	405,
	1159,
	2759,
	1992,
	835,
	1875,
	2759,
	1992,
	835,
	1875,
	2759,
	1179,
	407,
	1875,
	2759,
	1177,
	405,
	1159,
	2759,
	1177,
	405,
	1159,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[3] = 
{
	{ 0x060000B1, 1,  (void**)&BaseRuntimeFeature_OnFrameStateUpdate_m73151720BB25328366DD7B8665017ED420FC58B2_RuntimeMethod_var, 0 },
	{ 0x060000B6, 2,  (void**)&BaseRuntimeFeature_RunOnRenderThread_m2E8512A10A77075A41C26E119959C00AF6BD3B58_RuntimeMethod_var, 0 },
	{ 0x060000B7, 3,  (void**)&BaseRuntimeFeature_RunOnRenderThreadWithData_m02CE392AA2B19CCA6467DCA7CE3B166EDC6E1928_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[3] = 
{
	{ 0x0200009B, { 2, 4 } },
	{ 0x0200009C, { 6, 4 } },
	{ 0x060001DA, { 0, 2 } },
};
extern const uint32_t g_rgctx_TDelegate_tBB49CABF9A175602AB57707E3B479087EB98843D;
extern const uint32_t g_rgctx_TDelegate_tBB49CABF9A175602AB57707E3B479087EB98843D;
extern const uint32_t g_rgctx_Marshal_SizeOf_TisT_tB3827AD0B827CCBB26D280B5C71970FB100BF90E_m39B39CA600B96C119DA1A5FCA33AA5CF542C2A7C;
extern const uint32_t g_rgctx_ScopeArrayPtr_1_DisposeResources_m319C098417A4868D16DDC4A674E9C46CE9D6D0C9;
extern const uint32_t g_rgctx_Marshal_StructureToPtr_TisT_tB3827AD0B827CCBB26D280B5C71970FB100BF90E_mF06F0A51E2A5A763D29E82F59FD1E0151CC95F37;
extern const uint32_t g_rgctx_Marshal_PtrToStructure_TisT_tB3827AD0B827CCBB26D280B5C71970FB100BF90E_m1062458C10DED88F27E53DE0FDE583A992A4CCF2;
extern const uint32_t g_rgctx_Marshal_SizeOf_TisT_t61615F34FB3EBAA2FF1385548EA4A9ECC70DF859_mADF7DA817D5F0B8E3FFF005FC89EACB5497C4FEB;
extern const uint32_t g_rgctx_Marshal_StructureToPtr_TisT_t61615F34FB3EBAA2FF1385548EA4A9ECC70DF859_m142C02D3F8AE777BFE511649BB6A3C1CA684BF28;
extern const uint32_t g_rgctx_ScopePtr_1_DisposeResources_m8B9CC74F61A586FC7429C083060ED35B5A9F8D33;
extern const uint32_t g_rgctx_Marshal_PtrToStructure_TisT_t61615F34FB3EBAA2FF1385548EA4A9ECC70DF859_m653B55AEE64C506027C04EA5548025B2FD1AD597;
static const Il2CppRGCTXDefinition s_rgctxValues[10] = 
{
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TDelegate_tBB49CABF9A175602AB57707E3B479087EB98843D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TDelegate_tBB49CABF9A175602AB57707E3B479087EB98843D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Marshal_SizeOf_TisT_tB3827AD0B827CCBB26D280B5C71970FB100BF90E_m39B39CA600B96C119DA1A5FCA33AA5CF542C2A7C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ScopeArrayPtr_1_DisposeResources_m319C098417A4868D16DDC4A674E9C46CE9D6D0C9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Marshal_StructureToPtr_TisT_tB3827AD0B827CCBB26D280B5C71970FB100BF90E_mF06F0A51E2A5A763D29E82F59FD1E0151CC95F37 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Marshal_PtrToStructure_TisT_tB3827AD0B827CCBB26D280B5C71970FB100BF90E_m1062458C10DED88F27E53DE0FDE583A992A4CCF2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Marshal_SizeOf_TisT_t61615F34FB3EBAA2FF1385548EA4A9ECC70DF859_mADF7DA817D5F0B8E3FFF005FC89EACB5497C4FEB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Marshal_StructureToPtr_TisT_t61615F34FB3EBAA2FF1385548EA4A9ECC70DF859_m142C02D3F8AE777BFE511649BB6A3C1CA684BF28 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ScopePtr_1_DisposeResources_m8B9CC74F61A586FC7429C083060ED35B5A9F8D33 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Marshal_PtrToStructure_TisT_t61615F34FB3EBAA2FF1385548EA4A9ECC70DF859_m653B55AEE64C506027C04EA5548025B2FD1AD597 },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Snapdragon_Spaces_Runtime_CodeGenModule;
const Il2CppCodeGenModule g_Snapdragon_Spaces_Runtime_CodeGenModule = 
{
	"Snapdragon.Spaces.Runtime.dll",
	1207,
	s_methodPointers,
	232,
	s_adjustorThunks,
	s_InvokerIndices,
	3,
	s_reversePInvokeIndices,
	3,
	s_rgctxIndices,
	10,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
