// /******************************************************************************
//  * File: XRHandInteractableSnapPoseProviderEditor.cs
//  * Copyright (c) 2023 Qualcomm Technologies, Inc. and/or its subsidiaries. All rights reserved.
//  *
//  * Confidential and Proprietary - Qualcomm Technologies, Inc.
//  *
//  ******************************************************************************/

using QCHT.Interactions.Core;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor;
using UnityEditor.SceneManagement;
using QCHT.Interactions.Hands;

namespace QCHT.Interactions.Proximal.Editor
{
    [CustomEditor(typeof(XRHandInteractableSnapPoseProvider))]
    public sealed class XRHandInteractableSnapPoseProviderEditor : UnityEditor.Editor
    {
        private VisualElement _inspector;

        [SerializeField] private GameObject leftGhost;
        [SerializeField] private GameObject rightGhost;

        private XRHandInteractableSnapPoseProvider _provider;
        private HandGhost _handGhost;

        private float _scale = 1f;

        #region Editor

        private void OnEnable()
        {
            _provider = target as XRHandInteractableSnapPoseProvider;

            if (_provider != null)
            {
                InstantiateGhost(_provider.Handedness);
                UpdateGhost();
                
                var center = _provider.transform.position;
                var size = Vector3.one * 0.2f;
                var bounds = new Bounds(center, size);
                SceneView.lastActiveSceneView.Frame(bounds, false);
            }
        }

        private void OnDisable()
        {
            DestroyGhost();
        }

        public override void OnInspectorGUI()
        {
            const string kHandedness = "handedness";
            var handedness = (XrHandedness) serializedObject.FindProperty(kHandedness).intValue;

            EditorGUI.BeginChangeCheck();

            base.OnInspectorGUI();

            if (EditorGUI.EndChangeCheck())
            {
                var newHandedness = (XrHandedness) serializedObject.FindProperty(kHandedness).intValue;
                if (newHandedness != handedness)
                {
                    DestroyGhost();
                    InstantiateGhost(newHandedness);
                    UpdateGhost();
                }
            }

            EditorGUI.BeginChangeCheck();

            var posesProperty = serializedObject.FindProperty("poses");
            if (posesProperty.arraySize <= 0)
                return;

            XRHandInteractableSnapPose minPose = null;
            XRHandInteractableSnapPose maxPose = null;

            for (var i = 0; i < posesProperty.arraySize; i++)
            {
                var pose = posesProperty.GetArrayElementAtIndex(i).objectReferenceValue as XRHandInteractableSnapPose;
                if (pose == null) continue;
                if (minPose == null) minPose = pose;
                if (maxPose == null) maxPose = pose;
                if (minPose.Scale > pose.Scale) minPose = pose;
                if (maxPose.Scale < pose.Scale) maxPose = pose;
            }

            if (minPose != null && maxPose != null && Mathf.Abs(minPose.Scale - maxPose.Scale) > Mathf.Epsilon)
                _scale = EditorGUILayout.Slider("Scale", _scale, minPose.Scale, maxPose.Scale);

            if (EditorGUI.EndChangeCheck())
            {
                UpdateGhost();
                SceneView.RepaintAll();
            }
        }

        private void UpdateGhost()
        {
            var interHandPose = new HandData();
            var interRootPose = new Pose();

            if (_provider.TryGetInterpolatedHandPoseFromScale(ref interHandPose, ref interRootPose, _scale))
            {
                if (_handGhost != null)
                {
                    _handGhost.HandPose = interHandPose;
                    var transform = _provider.transform;
                    var ghostTransform = _handGhost.transform;
                    ghostTransform.rotation = transform.rotation * interRootPose.rotation;
                    ghostTransform.position = transform.TransformPoint(interRootPose.position);
                    ghostTransform.localScale = Vector3.one * _scale;
                }
            }
        }

        private void InstantiateGhost(XrHandedness handedness)
        {
            var ghostPrefab = handedness == XrHandedness.XR_HAND_LEFT ? leftGhost : rightGhost;
            var obj = Instantiate(ghostPrefab, null, true);
            obj.hideFlags = HideFlags.HideAndDontSave;
            StageUtility.PlaceGameObjectInCurrentStage(obj);
            _handGhost = obj.GetComponent<HandGhost>();
        }

        private void DestroyGhost()
        {
            if (_handGhost == null)
                return;

            DestroyImmediate(_handGhost.gameObject);
        }

        #endregion
    }
}