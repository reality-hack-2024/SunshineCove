// /******************************************************************************
//  * File: RobotHand.shader
//  * Copyright (c) 2023 Qualcomm Technologies, Inc. and/or its subsidiaries. All rights reserved.
//  *
//  * Confidential and Proprietary - Qualcomm Technologies, Inc.
//  *
//  ******************************************************************************/

Shader "Qualcomm/Hand/RobotHand" {
Properties {
    _MainTex ("Base (RGB)", 2D) = "white" {}
    _EmissiveTex ("Emissive (RGB)", 2D) = "white" {}
    _MapId ("MapID (RGB)", 2D) = "black" {}
    
    [HDR]_ColorThumb("Color Thumb", Color) = (1,1,1,1)
    [HDR]_ColorIndex("Color Index", Color) = (1,1,1,1)
    [HDR]_ColorMiddle("Color Middle", Color) = (1,1,1,1)
    [HDR]_ColorRing("Color Ring", Color) = (1,1,1,1)
    [HDR]_ColorPinky("Color Pinky", Color) = (1,1,1,1)
    [HDR]_ColorPalm("Color Palm", Color) = (1,1,1,1)
    
    _ColorThumbId("Id Thumb", Color) = (1,0,1,1)
    _ColorIndexId("Id Index", Color) = (1,1,0,1)
    _ColorMiddleId("Id Middle", Color) = (0,0,1,1)
    _ColorRingId("Id Ring", Color) = (0,1,0,1)
    _ColorPinkyId("Id Pinky", Color) = (1,0,0,1)
    _ColorPalmId("ID Palm", Color) = (0,1,1,1)

}
SubShader {
    Tags { "RenderType"="Opaque" }
    LOD 150

CGPROGRAM
#pragma surface surf Lambert noforwardadd

sampler2D _MainTex;
sampler2D _EmissiveTex;
sampler2D _MapId;

fixed4 _ColorThumb;
fixed4 _ColorIndex;
fixed4 _ColorMiddle;
fixed4 _ColorRing;
fixed4 _ColorPinky;
fixed4 _ColorPalm;

fixed4 _ColorThumbId;
fixed4 _ColorIndexId;
fixed4 _ColorMiddleId;
fixed4 _ColorRingId;
fixed4 _ColorPinkyId;
fixed4 _ColorPalmId;

struct Input {
    float2 uv_MainTex;
    float2 uv_EmissiveTex;
    float2 uv2_MapId;
};

bool equColor(fixed4 col1, fixed4 col2) {
    return col1.r == col2.r && col1.g == col2.g && col1.b == col2.b;
}

void surf (Input IN, inout SurfaceOutput o) {
    fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
    fixed4 e = tex2D(_EmissiveTex, IN.uv_EmissiveTex);
    fixed4 i = tex2D(_MapId, IN.uv2_MapId);
    
    o.Albedo = c.rgb;
    o.Alpha = c.a;
    
    fixed3 emiCol = fixed3(0,0,0);
    if (equColor(i, _ColorThumbId)) emiCol = _ColorThumb.xyz;
    else if (equColor(i, _ColorIndexId)) emiCol = _ColorIndex.xyz;
    else if (equColor(i, _ColorMiddleId)) emiCol = _ColorMiddle.xyz;
    else if (equColor(i, _ColorRingId)) emiCol = _ColorRing.xyz;
    else if (equColor(i, _ColorPinkyId)) emiCol = _ColorPinky.xyz;
    else if (equColor(i, _ColorPalmId)) emiCol = _ColorPalm.xyz;
    o.Emission = emiCol * e.xyz;
    
}

ENDCG
}

Fallback "Mobile/VertexLit"
}
