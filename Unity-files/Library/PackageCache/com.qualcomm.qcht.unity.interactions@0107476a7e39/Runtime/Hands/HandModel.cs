// /******************************************************************************
//  * File: HandModel.cs
//  * Copyright (c) 2023 Qualcomm Technologies, Inc. and/or its subsidiaries. All rights reserved.
//  *
//  * Confidential and Proprietary - Qualcomm Technologies, Inc.
//  *
//  ******************************************************************************/

using UnityEngine;
using QCHT.Interactions.Hands.VFF;
using UnityEngine.Serialization;

namespace QCHT.Interactions.Hands
{
    public class HandModel : MonoBehaviour, IHandSkinnable, IHideable, IHandPhysible
    {
        [FormerlySerializedAs("_handPhysible")] [SerializeField]
        private HandPhysible handPhysible;
        [FormerlySerializedAs("_mainHandVisualizer")] [SerializeField]
        private HandVisualizer mainHandVisualizer;
        [FormerlySerializedAs("_ghostHandVisualizer")] [SerializeField]
        private HandVisualizerGhost ghostHandVisualizer;

        #region IHandSkinnable

        public HandSkin HandSkin
        {
            get => mainHandVisualizer.HandSkin;
            set
            {
                mainHandVisualizer.HandSkin = value;
                ghostHandVisualizer.HandSkin = value;
            }
        }

        #endregion

        #region IHideable

        public void Show()
        {
            mainHandVisualizer.Show();
            ghostHandVisualizer.Show();
        }

        public void Hide()
        {
            mainHandVisualizer.Hide();
            ghostHandVisualizer.Hide();
        }

        #endregion

        #region IPhysible

        public bool IsPhysible
        {
            get => handPhysible != null && handPhysible.IsPhysible;
            set
            {
                if (handPhysible != null)
                {
                    handPhysible.IsPhysible = value;
                }
            }
        }

        #endregion
    }
}