// /******************************************************************************
//  * File: QCHTControlBoxHandlerRotate.cs
//  * Copyright (c) 2023 Qualcomm Technologies, Inc. and/or its subsidiaries. All rights reserved.
//  *
//  * Confidential and Proprietary - Qualcomm Technologies, Inc.
//  *
//  ******************************************************************************/

using QCHT.Interactions.Core;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace QCHT.Interactions.Distal.ControlBox
{
    public class QCHTControlBoxHandlerRotate : QCHTControlBoxHandler
    {
        [SerializeField] private Vector3 axis = Vector3.one;
        
        private Quaternion _originalGizmoRot;
       
        private Vector3 _pointerPressedPos;
        private float _pointerPressedDistance;

        public override event HandleEventHandler onHandleSelectEntered;
        public override event HandleEventHandler onHandleSelectExited;

        protected override void OnSelectEntered(SelectEnterEventArgs args)
        {
            base.OnSelectEntered(args);
            
            _interactor = args.interactorObject as XRRayInteractor;
            
            if (_interactor == null) return;
            
            _interactor.TryGetCurrentRaycast(out var raycastHit, out _, out _, out _, out _);

            if (raycastHit.HasValue)
            {
                _pointerPressedDistance = raycastHit.Value.distance;
                var t = _interactor.transform;
                _pointerPressedPos = t.position + t.forward.normalized * _pointerPressedDistance;
            }
            
            var handedness = XRInteractionGrid.GetInteractorHandedness(args.interactorObject);
            ref var select = ref handedness == XrHandedness.XR_HAND_LEFT ? ref _leftSelected : ref _rightSelected;
            select = true;

            _originalGizmoRot = _controlBoxTransform.rotation;

            onHandleSelectEntered?.Invoke(this);
        }
        
        protected override void OnSelectExited(SelectExitEventArgs args)
        {
            base.OnSelectExited(args);
            
            _interactor = null;
            var handedness = XRInteractionGrid.GetInteractorHandedness(args.interactorObject);
            ref var select = ref handedness == XrHandedness.XR_HAND_LEFT ? ref _leftSelected : ref _rightSelected;
            select = false;

            onHandleSelectExited?.Invoke(this);
        }

        public override void ProcessInteractable(XRInteractionUpdateOrder.UpdatePhase updatePhase)
        {
            base.ProcessInteractable(updatePhase);

            if (!IsSelected || _interactionGrid.IsControlBoxSelected)
                return;
            
            var initPoint = _pointerPressedPos;
            var t = _interactor.transform;
            var endPoint = t.position + t.forward.normalized * _pointerPressedDistance;

            var controlBoxPosition = _controlBoxTransform.position;
            var initDir = Vector3.ProjectOnPlane(initPoint - controlBoxPosition, axis).normalized;
            var currentDir = Vector3.ProjectOnPlane(endPoint - controlBoxPosition, axis).normalized;
            var goal = Quaternion.FromToRotation(initDir, currentDir) * _originalGizmoRot;
            _controlBoxTransform.rotation = goal;
        }
    }
}
