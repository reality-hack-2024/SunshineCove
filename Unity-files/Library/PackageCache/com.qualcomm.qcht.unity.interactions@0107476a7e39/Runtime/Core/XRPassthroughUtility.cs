// /******************************************************************************
// * File: XRPassthroughUtility.cs
// * Copyright (c) 2023 Qualcomm Technologies, Inc. and/or its subsidiaries. All rights reserved.
// *
// * Confidential and Proprietary - Qualcomm Technologies, Inc.
// *
// ******************************************************************************/

#if SPACES
using Qualcomm.Snapdragon.Spaces;
#endif
using UnityEngine;
using System.Runtime.InteropServices;
using UnityEngine.XR.OpenXR;

namespace QCHT.Interactions.Core
{
    public static class XRPassthroughUtility
    {
        private struct CameraState
        {
            public CameraClearFlags Flags;
            public Color Color;
        }

        private static CameraState s_cameraState;

        public static void SetPassthroughEnabled(bool enable)
        {
            var camera = XROriginUtility.GetOriginCamera();
            if (enable)
            {
                s_cameraState.Flags = camera.clearFlags;
                s_cameraState.Color = camera.backgroundColor;
                camera.clearFlags = CameraClearFlags.SolidColor;
                camera.backgroundColor =
                    new Color(s_cameraState.Color.r, s_cameraState.Color.g, s_cameraState.Color.b, 0f);
            }
            else
            {
                camera.clearFlags = s_cameraState.Flags;
                camera.backgroundColor = s_cameraState.Color;
            }
#if SPACES
            var baseRuntimeFeature = OpenXRSettings.Instance.GetFeature<BaseRuntimeFeature>();
            if (baseRuntimeFeature != null)
                baseRuntimeFeature.SetPassthroughEnabled(enable);
#endif
            SetPassthroughEnabledNative(enable);
        }

        public static bool GetPassthroughEnabled()
        {
#if SPACES
            var baseRuntimeFeature = OpenXRSettings.Instance.GetFeature<BaseRuntimeFeature>();
            if (baseRuntimeFeature != null)
                return baseRuntimeFeature.GetPassthroughEnabled();
#endif
            return GetPassthroughEnabledNative();
        }

        public static bool IsPassthroughSupported()
        {
#if SPACES
            var baseRuntimeFeature = OpenXRSettings.Instance.GetFeature<BaseRuntimeFeature>();
            if (baseRuntimeFeature != null)
                return baseRuntimeFeature.IsPassthroughSupported();
#endif
            return IsPassthroughSupportedNative();
        }

        #region Native calls

        private const string DllName = "QCHTOpenXRPlugin";

        [DllImport(DllName, EntryPoint = "SetPassthroughEnabled")]
        private static extern void SetPassthroughEnabledNative(bool enable);

        [DllImport(DllName, EntryPoint = "GetPassthroughEnabled")]
        private static extern bool GetPassthroughEnabledNative();

        [DllImport(DllName, EntryPoint = "IsPassthroughSupported")]
        private static extern bool IsPassthroughSupportedNative();

        #endregion
    }
}