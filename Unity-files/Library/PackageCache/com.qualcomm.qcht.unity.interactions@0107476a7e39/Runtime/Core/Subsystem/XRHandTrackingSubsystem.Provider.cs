// /******************************************************************************
//  * File: XRHandTrackingSubsystem.Provider.cs
//  * Copyright (c) 2023 Qualcomm Technologies, Inc. and/or its subsidiaries. All rights reserved.
//  *
//  * Confidential and Proprietary - Qualcomm Technologies, Inc.
//  *
//  ******************************************************************************/

using UnityEngine;
using UnityEngine.SubsystemsImplementation;

namespace QCHT.Interactions.Core
{
    public partial class XRHandTrackingSubsystem
    {
        public abstract class Provider : SubsystemProvider<XRHandTrackingSubsystem>
        {
            /// <summary>
            /// Gets Hand Tracking status from provider implementation.
            /// </summary>
            public HandTrackingStatus Status { get; protected set; }

            /// <summary>
            /// Gets the space which provider hand data are formatted. 
            /// </summary>
            public XrSpace Space { get; protected set; } = XrSpace.XR_HAND_WORLD;

            /// <summary>
            /// Tries to get hand data from provider. 
            /// </summary>
            /// <param name="handedness"> Handedness </param>
            /// <param name="isTracked"> Is Tracked? </param>
            /// <param name="rootPose"> Root pose in origin space. </param>
            /// <param name="joints"> Joints poses either in local, world or xr origin space. </param>
            /// <param name="scale"> Hand scale. Should be clamped between .5 and 1.6 </param>
            /// <param name="gesture"> Hand gesture. </param>
            /// <param name="gestureRatio"> Hand gesture ratio. </param>
            /// <param name="flipRatio"> Hand flip ratio. Should be clamped -1 and 1 </param>
            /// <returns></returns>
            public abstract bool TryUpdateHandData(XrHandedness handedness, ref bool isTracked, ref Pose rootPose, ref Pose[] joints, ref float scale, ref XrHandGesture gesture, ref float gestureRatio, ref float flipRatio);

            public override void Start() { }

            public override void Stop() { }

            public override void Destroy() { }
        }
    }
}