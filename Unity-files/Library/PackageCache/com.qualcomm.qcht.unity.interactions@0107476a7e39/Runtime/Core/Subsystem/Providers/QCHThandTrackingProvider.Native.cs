// /******************************************************************************
//  * File: QCHThandTrackingProvider.Native.cs
//  * Copyright (c) 2023 Qualcomm Technologies, Inc. and/or its subsidiaries. All rights reserved.
//  *
//  * Confidential and Proprietary - Qualcomm Technologies, Inc.
//  *
//  ******************************************************************************/

using System;
using System.Runtime.InteropServices;

namespace QCHT.Interactions.Core
{
    public partial class QCHTHandTrackingProvider
    {
        private const string DllName = "QCHTOpenXRPlugin";

        [DllImport(DllName, EntryPoint = "GetInterceptedInstanceProcAddr")]
        internal static extern IntPtr GetInterceptedInstanceProcAddr(IntPtr func);

        [DllImport(DllName, EntryPoint = "SetAppSpace")]
        internal static extern int SetAppSpace(ulong xrSpace);
        
        [DllImport(DllName, EntryPoint = "TryLocateHandJoints")]
        private static extern XrResult TryLocateHandJoints(XrHandSide handedness,
            ref bool isTracked,
            [MarshalAs(UnmanagedType.LPArray, SizeConst = (int) XrHandJoint.XR_HAND_JOINT_MAX)]
            XrPose[] handPoses,
            [MarshalAs(UnmanagedType.LPArray, SizeConst = (int) XrHandJoint.XR_HAND_JOINT_MAX)]
            float[] radius,
            ref int gesture,
            ref float gestureRatio,
            ref float flipRatio);

        [DllImport(DllName, EntryPoint = "GetSystemPropertyEnabled")]
        public static extern bool GetSystemPropertyEnabled(string propertyName);
    }
}