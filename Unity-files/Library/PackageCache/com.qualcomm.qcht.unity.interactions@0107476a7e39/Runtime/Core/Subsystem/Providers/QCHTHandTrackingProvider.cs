// /******************************************************************************
//  * File: QCHTHandTrackingProvider.cs
//  * Copyright (c) 2023 Qualcomm Technologies, Inc. and/or its subsidiaries. All rights reserved.
//  *
//  * Confidential and Proprietary - Qualcomm Technologies, Inc.
//  *
//  ******************************************************************************/

using Unity.Profiling;
using UnityEngine;

namespace QCHT.Interactions.Core
{
    public partial class QCHTHandTrackingProvider : XRHandTrackingSubsystem.Provider
    {
        public const string ID = "Qualcomm-HandTracking-Native";
        
        private XrPose[] _leftPoses = new XrPose[(int) XrHandJoint.XR_HAND_JOINT_MAX];
        private XrPose[] _rightPoses = new XrPose[(int) XrHandJoint.XR_HAND_JOINT_MAX];
        private float[] _radius = new float[(int) XrHandJoint.XR_HAND_JOINT_MAX];

        private static readonly ProfilerMarker TryUpdateHandDataMarker =
            new ProfilerMarker("[QCHT] QCHTHandTrackingProvider.TryUpdateHandData");
        
        private static readonly ProfilerMarker TryLocateHandJointsMarker =
            new ProfilerMarker("[QCHT] QCHTHandTrackingProvider.TryUpdateHandData.TryLocateHandJoints");

        public override void Start()
        {
            base.Start();
            Status = HandTrackingStatus.Running;
        }

        public override void Stop()
        {
            base.Stop();
            Status = HandTrackingStatus.Idle;
        }
        
        public override bool TryUpdateHandData(XrHandedness handedness, ref bool isTracked, ref Pose rootPose,
            ref Pose[] joints, ref float scale, ref XrHandGesture gesture, ref float gestureRatio, ref float flipRatio)
        {
            using (TryUpdateHandDataMarker.Auto())
            {
                var isLeft = handedness == XrHandedness.XR_HAND_LEFT;
                ref var poses = ref isLeft ? ref _leftPoses : ref _rightPoses;
                var handSide = isLeft ? XrHandSide.XR_HAND_LEFT : XrHandSide.XR_HAND_RIGHT;
                var intGesture = (int) gesture;
                
                XrResult result;

                using (TryLocateHandJointsMarker.Auto())
                {
                    result = TryLocateHandJoints(handSide, ref isTracked, poses, _radius, ref intGesture,
                        ref gestureRatio, ref flipRatio);
                }

                // In this case just return and wait for next frames
                if (result == XrResult.XR_ERROR_TIME_INVALID)
                {
                    Debug.Log("[QCHTHandTrackingProvider:TryUpdateHandData] OpenXR predicted display time");
                    return false;
                }

                if (result < XrResult.XR_SUCCESS)
                {
                    Status = HandTrackingStatus.Error;
                    return false;
                }

                Status = HandTrackingStatus.Running;

                for (var i = 0; i < (int) XrHandJoint.XR_HAND_JOINT_MAX; i++)
                {
                    joints[i] = poses[i].ToPose();
                }

                gesture = (XrHandGesture) intGesture;
                scale = _radius[(int) XrHandJoint.XR_HAND_JOINT_WRIST];
                rootPose = joints[(int) XrHandJoint.XR_HAND_JOINT_WRIST];
            }

            return true;
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        private static void RegisterDescriptor()
        {
            XRHandTrackingSubsystemDescriptor.Create(new XRHandTrackingSubsystemDescriptor.Cinfo
            {
                id = ID,
                providerType = typeof(QCHTHandTrackingProvider)
            });
        }
    }
}