// /******************************************************************************
//  * File: HandTrackingXRControllerDeviceImpl.cs
//  * Copyright (c) 2023 Qualcomm Technologies, Inc. and/or its subsidiaries. All rights reserved.
//  *
//  * Confidential and Proprietary - Qualcomm Technologies, Inc.
//  *
//  ******************************************************************************/

using System;
using QCHT.Interactions.Extensions;
using Unity.Profiling;
using UnityEngine;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.XR;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.XR;
using Hand = QCHT.Interactions.Core.XRHandTrackingSubsystem.Hand;
using CommonUsages = UnityEngine.InputSystem.CommonUsages;
using InputDevice = UnityEngine.InputSystem.InputDevice;

namespace QCHT.Interactions.Core
{
    public class HandTrackingXRControllerDeviceImpl : IHandTrackingInputDevice
    {
        private HandTrackingDevice _leftHandDevice;
        private HandTrackingDevice _rightHandDevice;

        private HandTrackingXRControllerInputState _leftState;
        private HandTrackingXRControllerInputState _rightState;

        /// <summary>
        /// Pinch activation sensitivity threshold.
        /// </summary>
        public float TriggerPinchThreshold = 0.1f;

        /// <summary>
        /// Pinch deactivation sensitivity threshold.
        /// </summary>
        public float ReleasePinchThreshold = 0.15f;

        private static readonly ProfilerMarker UpdateDeviceMarker =
            new ProfilerMarker("[QCHT] XRHandTrackingSubsystem.UpdateDevice");

        /// <summary>
        /// Updates Hand Tracking device in input system.
        /// </summary>
        private void UpdateDevice(Hand hand, ref HandTrackingXRControllerInputState state, InputDevice device)
        {
            if (device == null || !device.added) return;

            using (UpdateDeviceMarker.Auto())
            {
                state.isTracked = hand.IsTracked;
                state.trackingState = (int) (InputTrackingState.Position | InputTrackingState.Rotation);

                var eyePose = Pose.identity;

                if (InputSystem.GetDevice<XRHMD>() is var hmd && hmd != null)
                {
                    eyePose.position = hmd.centerEyePosition.ReadValue();
                    eyePose.rotation = hmd.centerEyeRotation.ReadValue();
                }

                var root = hand.Root;
                var palm = hand.GetHandJoint(XrHandJoint.XR_HAND_JOINT_PALM);
                var indexTip = hand.GetHandJoint(XrHandJoint.XR_HAND_JOINT_INDEX_TIP);
                var thumpTip = hand.GetHandJoint(XrHandJoint.XR_HAND_JOINT_THUMB_TIP);

                // Device pose
                state.devicePosition = root.position;
                state.deviceRotation = root.rotation;

                // Pointer pose
                var aimRay = hand.Handedness == XrHandedness.XR_HAND_LEFT
                    ? AimRay.AimRays[(int) XrHandedness.XR_HAND_LEFT]
                    : AimRay.AimRays[(int) XrHandedness.XR_HAND_RIGHT];

                var pointerPose = aimRay.GetPose(ref hand, eyePose);
                state.pointerPosition = pointerPose.position;
                state.pointerRotation = pointerPose.rotation;

                // Poke position
                state.pokePosition = indexTip.position;
                state.pokeRotation = indexTip.rotation;

                // Pinch position 
#if !UNITY_EDITOR
                state.pinchPosition = (indexTip.position + thumpTip.position) * .5f;
                state.pinchRotation = root.rotation;
#else
                state.pinchPosition = (indexTip.position + thumpTip.position) * .5f;
                state.pinchRotation = root.rotation;
#endif
                // Grasp position
                state.graspPosition = palm.position;
                state.graspRotation = palm.rotation;

                // Inputs
                var wasPinched = state.pinchPressed;
                var pinchStrength = hand.GetFingerPinching(XrFinger.XR_HAND_FINGER_INDEX);

                if (wasPinched)
                    state.pinchPressed = pinchStrength > 1f - ReleasePinchThreshold; // release threshold
                else
                    state.pinchPressed = pinchStrength > 1f - TriggerPinchThreshold; // trigger threshold

#if !UNITY_EDITOR
                state.pinch = state.pinchPressed ? 1f : 0f;
#else
                state.pinch = pinchStrength;
#endif

                state.graspPressed = hand.Gesture == XrHandGesture.XR_HAND_GRAB;
                state.grasp = hand.Gesture == XrHandGesture.XR_HAND_GRAB ? 1f : 0f;
                state.flipRatio = hand.FlipRatio;

                InputState.Change(device, state);
            }
        }

        #region IHandTrackingInputDeviceImpl

        public void AddDevices()
        {
            _leftHandDevice ??= AddDevice(true);
            _rightHandDevice ??= AddDevice(false);
        }

        public void RemoveDevices()
        {
            RemoveDevice(_leftHandDevice);
            _leftHandDevice = null;

            RemoveDevice(_rightHandDevice);
            _rightHandDevice = null;
        }

        public void UpdateDevices(ref Hand leftHand, ref Hand rightHand)
        {
            UpdateDevice(leftHand, ref _leftState, _leftHandDevice);
            UpdateDevice(rightHand, ref _rightState, _rightHandDevice);
        }

        private static HandTrackingDevice AddDevice(bool isLeft)
        {
            var usage = isLeft ? CommonUsages.LeftHand : CommonUsages.RightHand;
            var device =
                InputSystem.AddDevice<HandTrackingDevice>(
                    $"{nameof(HandTrackingDevice)} - {usage}");
            if (device != null) InputSystem.SetDeviceUsage(device, usage);
            return device;
        }

        private static void RemoveDevice(InputDevice device)
        {
            if (device == null) return;
            InputSystem.RemoveDevice(device);
        }

        #endregion

        private static readonly Vector3 s_leftShoulderOffset = new Vector3(-0.08f, -0.08f, 0.0f);
        private static readonly Vector3 s_rightShoulderOffset = new Vector3(0.08f, -0.08f, 0.0f);

        internal class AimRay
        {
            public static readonly AimRay[] AimRays =
            {
                new AimRay(true),
                new AimRay(false)
            };

            private OneEuroFilter3 _smooth = new OneEuroFilter3(0.0001f, 0.75f, 0.0001f, 0.75f, 0.0001f, 0.75f);

            private float _previousPinchRatio;
            private readonly Vector3 _shoulderOffset;

            public AimRay(bool isLeft)
            {
                _shoulderOffset = isLeft ? s_leftShoulderOffset : s_rightShoulderOffset;
            }

            public Pose GetPose(ref Hand hand, Pose eyePose, bool smooth = true)
            {
                var shoulder = eyePose.position + eyePose.rotation * _shoulderOffset;
                var rayStart = (hand.GetHandJoint(XrHandJoint.XR_HAND_JOINT_INDEX_PROXIMAL).position +
                                hand.GetHandJoint(XrHandJoint.XR_HAND_JOINT_THUMB_PROXIMAL).position) / 2f;

                var rayDir = (rayStart - shoulder).normalized;

                if (smooth)
                {
                    var smoothRayEnd = rayStart + 5.0f * rayDir;
                    if (hand.Gesture == XrHandGesture.XR_HAND_PINCH)
                    {
                        var pinchStrength = hand.GestureRatio;
                        if (_previousPinchRatio < pinchStrength)
                        {
                            var smoothRatio = .2f * Mathf.Abs(_previousPinchRatio - pinchStrength);
                            var previousEnd = _smooth.Current;
                            smoothRayEnd = (1.0f - smoothRatio) * smoothRayEnd + smoothRatio * previousEnd;
                        }
                    }
                    else
                    {
                        _previousPinchRatio = 0f;
                    }

                    _smooth.Filter(ref smoothRayEnd, Time.unscaledTime);
                    rayDir = (smoothRayEnd - rayStart).normalized;
                }

                var rayX = Vector3.Cross(hand.Root.rotation * Vector3.up, rayDir).normalized;
                var rayUp = Vector3.Cross(rayDir, rayX).normalized;
                return new Pose(rayStart, Quaternion.LookRotation(rayDir, rayUp));
            }

            internal struct OneEuroFilter3
            {
                private OneEuroFilter _xFilter, _yFilter, _zFilter;

                public Vector3 Current => new Vector3(_xFilter.Current, _yFilter.Current, _zFilter.Current);

                public OneEuroFilter3(in float xMinCutoff, in float xGain, in float yMinCutoff, in float yGain,
                    in float zMinCutoff, in float zGain)
                {
                    _xFilter = new OneEuroFilter(xMinCutoff, xMinCutoff, xGain, 1.0f);
                    _yFilter = new OneEuroFilter(yMinCutoff, yMinCutoff, yGain, 1.0f);
                    _zFilter = new OneEuroFilter(zMinCutoff, zMinCutoff, zGain, 1.0f);
                }

                public void Filter(ref Vector3 vector, float time)
                {
                    _xFilter.Filter(ref vector.x, time);
                    _yFilter.Filter(ref vector.y, time);
                    _zFilter.Filter(ref vector.z, time);
                }
            }

            internal struct OneEuroFilter
            {
                private float _freq, _minCutOff, _beta, _dCutoff;
                private float _lastTime;

                private LowPassFilter _xFilter;
                private LowPassFilter _dxFilter;

                public float Current => _xFilter.Current;

                public OneEuroFilter(float initFrequency = 30.0f, float minCutoffFrequency = 0.05f,
                    float gainCoeff = 5.0f, float derivativeCutoffFrequency = 5.0f)
                {
                    _freq = Mathf.Max(initFrequency, 0.0001f);
                    _minCutOff = minCutoffFrequency;
                    _beta = gainCoeff;
                    _dCutoff = derivativeCutoffFrequency;
                    _lastTime = -1;

                    _xFilter = new LowPassFilter();
                    _dxFilter = new LowPassFilter();
                }

                public float Filter(ref float x, in float time)
                {
                    if (Math.Abs(time - _lastTime) < Mathf.Epsilon)
                        return _xFilter.Current;

                    var dx = 0f;

                    if (_lastTime > 0 && time > 0)
                    {
                        _freq = 1f / (time - _lastTime);
                    }

                    _lastTime = time;

                    if (_xFilter.HadPrevious)
                    {
                        dx = (x - _xFilter.Current) * _freq;
                    }

                    var edx = _dxFilter.Filter(ref dx, Alpha(_dCutoff));
                    var cutoff = _minCutOff + _beta * Mathf.Abs(edx);
                    return _xFilter.Filter(ref x, Alpha(cutoff));
                }

                private float Alpha(float cutoff)
                {
                    var tau = 1.0f / (2.0f * Mathf.PI * cutoff);
                    var te = 1.0f / _freq;
                    return 1.0f / (1.0f + tau / te);
                }
            }

            internal struct LowPassFilter
            {
                private bool _hadPrevious;

                public bool HadPrevious => _hadPrevious;

                private float _xEstPrev;

                public float Current => _xEstPrev;

                public float Filter(ref float x, in float alpha)
                {
                    float xEst;

                    if (_hadPrevious)
                    {
                        xEst = alpha * x + (1.0f - alpha) * _xEstPrev;
                    }
                    else
                    {
                        xEst = x;
                        _hadPrevious = true;
                    }

                    _xEstPrev = xEst;
                    x = xEst;
                    return xEst;
                }
            }
        }
    }
}