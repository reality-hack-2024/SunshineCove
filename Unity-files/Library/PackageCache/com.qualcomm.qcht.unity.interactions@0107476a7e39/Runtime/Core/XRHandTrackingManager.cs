// /******************************************************************************
//  * File: XRHandTrackingManager.cs
//  * Copyright (c) 2023 Qualcomm Technologies, Inc. and/or its subsidiaries. All rights reserved.
//  *
//  * Confidential and Proprietary - Qualcomm Technologies, Inc.
//  *
//  ******************************************************************************/

using UnityEngine;
using QCHT.Interactions.Hands;
using QCHT.Interactions.Hands.VFF;
using UnityEngine.SubsystemsImplementation.Extensions;
using UnityEngine.XR.Interaction.Toolkit;

#if UNITY_EDITOR
using UnityEditor;
using System.Globalization;
#endif

namespace QCHT.Interactions.Core
{
    public partial class XRHandTrackingManager : MonoBehaviour
    {
        private const string kResourcesHandLeft = "QualcommHandLeft";
        private const string kResourcesHandRight = "QualcommHandRight";
        private const string kTrackableLeftName = "QC Hand Left";
        private const string kTrackableRightName = "QC Hand Right";

        [Space]
        [SerializeField] private GameObject leftHandPrefab;
        [SerializeField] private GameObject rightHandPrefab;

        protected XRHandTrackingSubsystem _subsystem;

        private GameObject _leftHand;
        private GameObject _rightHand;

        private bool _shouldLeftBeVisible = true;
        private bool _shouldRightBeVisible = true;

        /// <summary>
        /// Returns the current left hand game object instance.
        /// </summary>
        public GameObject LeftHand => _leftHand;

        /// <summary>
        /// Returns the current right hand game object instance.
        /// </summary>
        public GameObject RightHand => _rightHand;

        /// <summary>
        /// Left prefab object that will be instantiated.
        /// If the prefab changed after Instantiation time, it can be refreshed by calling RefreshLeftHand
        /// </summary>
        public GameObject LeftHandPrefab
        {
            get => leftHandPrefab;
            set => leftHandPrefab = value;
        }

        /// <summary>
        /// Right prefab object that will be instantiated.
        /// If the prefab changed after Instantiation time, it can be refreshed by calling RefreshRightHand.
        /// </summary>
        public GameObject RightHandPrefab
        {
            get => rightHandPrefab;
            set => rightHandPrefab = value;
        }

        /// <summary>
        /// Stores the left hand skin set.
        /// In case the skin provided can't be applied to the hand object, this field stores it to attempt applying it at next hand object's instantiation time.
        /// </summary>
        private HandSkin _leftHandSkin;

        public HandSkin LeftHandSkin
        {
            get => _leftHandSkin;
            set
            {
                _leftHandSkin = value;
                TrySetSkin(_leftHand, value);
            }
        }

        /// <summary>
        /// Stores the right hand skin set.
        /// In case the skin provided can't be applied to the hand object, this field stores it to attempt applying it at next hand object's instantiation time.
        /// </summary>
        private HandSkin _rightHandSkin;

        public HandSkin RightHandSkin
        {
            get => _rightHandSkin;
            set
            {
                _rightHandSkin = value;
                TrySetSkin(_rightHand, value);
            }
        }

        protected void OnEnable()
        {
            FindXRHandTrackingSubsystem();

            if (_subsystem != null)
            {
                UpdateHandVisibility(_subsystem.LeftHand);
                UpdateHandVisibility(_subsystem.RightHand);
            }
        }

        protected void OnDisable()
        {
            if (_subsystem != null)
            {
                _subsystem.OnHandTracked -= UpdateHandVisibility;
                _subsystem.OnHandUntracked -= UpdateHandVisibility;
                _subsystem.OnHandsUpdated -= OnHandsUpdated;
                _subsystem = null;
            }

            ToggleHand(XrHandedness.XR_HAND_LEFT, false);
            ToggleHand(XrHandedness.XR_HAND_RIGHT, false);
        }

        protected void Update()
        {
            FindXRHandTrackingSubsystem();

            if (_leftHand == null && leftHandPrefab != null)
                InitializeLeftHand();

            if (_rightHand == null && rightHandPrefab != null)
                InitializeRightHand();
        }

        protected void OnDestroy()
        {
            if (_leftHand != null)
                Destroy(_leftHand);

            if (_rightHand != null)
                Destroy(_rightHand);
        }

        /// <summary>
        /// Attempts to find the Hand tracking subsystem and registers to its callbacks
        /// </summary>
        private void FindXRHandTrackingSubsystem()
        {
            if (_subsystem != null)
                return;

            _subsystem = XRHandTrackingSubsystem.GetSubsystemInManager();

            if (_subsystem != null)
            {
                _subsystem.OnHandTracked += UpdateHandVisibility;
                _subsystem.OnHandUntracked += UpdateHandVisibility;
                _subsystem.OnHandsUpdated += OnHandsUpdated;
            }
        }

#if UNITY_EDITOR
        protected void OnGUI()
        {
            var simSubsystem = _subsystem?.GetProvider() as XRHandSimulationProvider;
            if (simSubsystem != null && simSubsystem.running)
            {
                GUILayout.BeginHorizontal("Box");
                GUILayout.Space(20);
                GUILayout.Label("Hand scale");
                GUILayout.Space(10);
                GUILayout.BeginVertical();
                GUILayout.FlexibleSpace();
                simSubsystem.HandScale =
                    GUILayout.HorizontalSlider(simSubsystem.HandScale, .1f, 2f, GUILayout.MinWidth(100));
                GUILayout.FlexibleSpace();
                GUILayout.EndVertical();
                GUILayout.Space(10);
                GUILayout.Label(simSubsystem.HandScale.ToString("#.##", CultureInfo.InvariantCulture),
                    GUILayout.MinWidth(30));
                GUILayout.Space(20);
                GUILayout.EndHorizontal();
            }
        }
#endif

        private void OnHandsUpdated(XRInteractionUpdateOrder.UpdatePhase updatePhase)
        {
            if (updatePhase != XRInteractionUpdateOrder.UpdatePhase.Dynamic)
                return;

            UpdateHand(_subsystem.LeftHand, _leftHand);
            UpdateHand(_subsystem.RightHand, _rightHand);
        }

        private static void UpdateHand(XRHandTrackingSubsystem.Hand hand, GameObject handObj)
        {
            if (handObj == null) return;

            handObj.transform.localPosition = hand.Root.position;
            handObj.transform.localRotation = hand.Root.rotation;
        }

        #region Hand Objects

        /// <summary>
        /// Forces regenerating the left hand trackable object.
        /// </summary>
        public void RefreshLeftHand()
        {
            RemoveLeftHand();
            InitializeLeftHand();
        }

        /// <summary>
        /// Forces regenerating the right hand trackable object.
        /// </summary>
        public void RefreshRightHand()
        {
            RemoveRightHand();
            InitializeRightHand();
        }

        /// <summary>
        /// Instantiates and inits left hand trackable object.
        /// </summary>
        private void InitializeLeftHand()
        {
            InstantiateHand(kTrackableLeftName, leftHandPrefab, ref _leftHand);

            if (_leftHandSkin != null)
                TrySetSkin(_leftHand, _leftHandSkin);
            else
                _leftHandSkin = TryGetComponentOnHand<IHandSkinnable>(_leftHand, out var skinnable)
                    ? skinnable.HandSkin
                    : null;

            if (_subsystem != null)
            {
                UpdateHandVisibility(_subsystem.LeftHand);
            }
        }

        /// <summary>
        /// Instantiates and inits right hand trackable object.
        /// </summary>
        private void InitializeRightHand()
        {
            InstantiateHand(kTrackableRightName, rightHandPrefab, ref _rightHand);

            if (_rightHandSkin != null)
                TrySetSkin(_rightHand, _rightHandSkin);
            else
                _rightHandSkin = TryGetComponentOnHand<IHandSkinnable>(_rightHand, out var skinnable)
                    ? skinnable.HandSkin
                    : null;

            if (_subsystem != null)
            {
                UpdateHandVisibility(_subsystem.RightHand);
            }
        }

        /// <summary>
        /// Instantiate a hand object.
        /// </summary>
        private void InstantiateHand(string objectName, GameObject prefab, ref GameObject hand)
        {
            if (hand) return;
            hand = InstantiateHandTrackable(objectName, prefab);
            SetParentTrackable(hand);
        }

        /// <summary>
        /// Destroys left hand trackable object.
        /// </summary>
        private void RemoveLeftHand() => RemoveHand(ref _leftHand);

        /// <summary>
        /// Destroys right hand trackable object.
        /// </summary>
        private void RemoveRightHand() => RemoveHand(ref _rightHand);

        /// <summary>
        /// Destroys a hand object.
        /// </summary>
        private static void RemoveHand(ref GameObject hand)
        {
            if (hand != null) Destroy(hand);
            hand = null;
        }

        private static bool TrySetSkin(GameObject hand, HandSkin skin)
        {
            if (TryGetComponentOnHand(hand, out IHandSkinnable skinnable))
            {
                skinnable.HandSkin = skin;
                return true;
            }

            Debug.Log("[XRHandTrackingManager:TrySetSkin] Hand object is not skinnable.");
            return false;
        }

        /// <summary>
        /// Toggles a hand game object.
        /// </summary>
        /// <param name="handedness"> Hand to toggle </param>
        /// <param name="visible"> Is the object visible? </param>
        public void ToggleHand(XrHandedness handedness, bool visible) =>
            ToggleHand(handedness == XrHandedness.XR_HAND_LEFT ? _leftHand : _rightHand, visible);

        /// <summary>
        /// Toggles the left hand visibility.
        /// Performs fading if the hand object is a IHandFadable. 
        /// </summary>
        /// <param name="visible"> Is the object visible? </param>
        public void ToggleLeftHand(bool visible)
        {
            _shouldLeftBeVisible = visible;

            if (_subsystem != null)
            {
                UpdateHandVisibility(_subsystem.LeftHand);
            }
        }

        /// <summary>
        /// Toggles the right hand visibility.
        /// </summary>
        /// <param name="visible"> Is the object visible? </param>
        public void ToggleRightHand(bool visible)
        {
            _shouldRightBeVisible = visible;

            if (_subsystem != null)
            {
                UpdateHandVisibility(_subsystem.RightHand);
            }
        }

        private void UpdateHandVisibility(XRHandTrackingSubsystem.Hand data)
        {
            ref var shouldBeVisible = ref data.Handedness == XrHandedness.XR_HAND_LEFT
                ? ref _shouldLeftBeVisible
                : ref _shouldRightBeVisible;

            ToggleHand(data.Handedness, shouldBeVisible && data.IsTracked);
        }

        /// <summary>
        /// Toggles a hand game object visibility.
        /// </summary>
        private static void ToggleHand(GameObject hand, bool visible)
        {
            if (hand == null)
                return;

            if (visible)
            {
                hand.gameObject.SetActive(true);
                TryShow(hand);
            }
            else
            {
                TryHide(hand);
                hand.gameObject.SetActive(false);
            }
        }

        private static bool TryShow(GameObject hand)
        {
            if (TryGetComponentOnHand(hand, out IHideable hideable))
            {
                hideable.Show();
                return true;
            }

            return false;
        }

        private static bool TryHide(GameObject hand)
        {
            if (TryGetComponentOnHand(hand, out IHideable hideable))
            {
                hideable.Hide();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Try to set virtual force feed back system state if available on right hand object.
        /// </summary>
        /// <param name="handedness"> Hand to set. </param>
        /// <param name="active"> Is Vff active? </param>
        /// <returns></returns>
        public bool TrySetVff(XrHandedness handedness, bool active) =>
            TrySetVff(handedness == XrHandedness.XR_HAND_LEFT ? _leftHand : _rightHand, active);

        /// <summary>
        /// Sets virtual force feed back system state if available on left hand object.
        /// </summary>
        /// <param name="active"> is vff active? </param>
        public void SetVffLeft(bool active) => TrySetVff(_leftHand, active);

        /// <summary>
        /// Sets virtual force feed back system state if available on right hand object.
        /// </summary>
        /// <param name="active"> is vff active? </param>
        public void SetVffRight(bool active) => TrySetVff(_rightHand, active);

        private static bool TrySetVff(GameObject hand, bool active)
        {
            if (TryGetComponentOnHand(hand, out IHandPhysible vffable))
            {
                vffable.IsPhysible = active;
                return true;
            }

            return false;
        }

        private static bool TryGetComponentOnHand<T>(GameObject hand, out T component)
        {
            if (hand == null)
            {
                component = default;
                return false;
            }

            return hand.TryGetComponent(out component);
        }

        private void SetParentTrackable(GameObject hand)
        {
            if (hand == null) return;
            var trackablesParent = XROriginUtility.GetTrackablesParent();
            trackablesParent = trackablesParent ? trackablesParent : transform;
            var handTransform = hand.transform;
            handTransform.SetParent(trackablesParent);
            handTransform.localPosition = Vector3.zero;
            handTransform.localRotation = Quaternion.identity;
        }

        #endregion

        #region static

        public static GameObject DefaultLeftHandPrefab => Resources.Load<GameObject>(kResourcesHandLeft);

        public static GameObject DefaultRightHandPrefab => Resources.Load<GameObject>(kResourcesHandRight);

        /// <summary>
        /// Instantiates a hand tracking manager with default hands prefabs.
        /// </summary>
        public static XRHandTrackingManager InstantiateHandTrackingManager() =>
            GetOrCreate(DefaultLeftHandPrefab, DefaultRightHandPrefab);

        /// <summary>
        /// Gets existing or creates a Hand Tracking Manager if it doesn't exist.
        /// If an XRHandTrackingManager instance already exists, Set Left/Right prefabs fields on exiting instance then use RefreshHandLeft/Right functions to update hand models.
        /// </summary>
        /// <param name="leftPrefab"> Left prefab reference in case of XRHandTrackingManager instance is created. </param>
        /// <param name="rightPrefab"> Right prefab reference in case of XRHandTrackingManager instance is created. </param>
        /// <returns> New or existing hand tracking manager instance. </returns>
        public static XRHandTrackingManager GetOrCreate(GameObject leftPrefab = null, GameObject rightPrefab = null)
        {
            var manager = FindObjectOfType<XRHandTrackingManager>(true);
            if (manager != null)
            {
#if UNITY_EDITOR
                Selection.activeGameObject = manager.gameObject;
#endif
                return manager;
            }

            GameObject go = null;
#if UNITY_EDITOR
            go = Selection.activeGameObject;
#endif
            if (!go)
            {
                go = new GameObject("XR Hand Tracking Manager");
#if UNITY_EDITOR
                Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
#endif
            }

            manager = go.AddComponent<XRHandTrackingManager>();

            manager.leftHandPrefab = leftPrefab;
            manager.rightHandPrefab = rightPrefab;

            if (Application.isPlaying)
            {
                manager.RefreshLeftHand();
                manager.RefreshRightHand();
            }

            return manager;
        }

        /// <summary>
        /// Destroys Hand Tracking Manager instance if it does exist.
        /// </summary>
        public static void Destroy(XRHandTrackingManager manager = null)
        {
            manager = manager ? manager : FindObjectOfType<XRHandTrackingManager>();
            if (manager == null) return;
            GameObject.Destroy(manager);
        }

        private static GameObject InstantiateHandTrackable(string handName, GameObject prefab)
        {
            if (prefab == null)
            {
                return null;
            }

            GameObject instance;
            if (prefab.scene.rootCount == 0)
            {
                instance = Instantiate(prefab);
                instance.name = handName;
            }
            else
            {
                instance = prefab;
            }

            return instance;
        }

        #endregion
    }
}