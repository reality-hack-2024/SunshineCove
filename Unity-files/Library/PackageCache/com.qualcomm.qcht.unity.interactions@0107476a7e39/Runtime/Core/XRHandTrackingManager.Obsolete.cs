// /******************************************************************************
//  * File: XRHandTrackingManager.Obsolete.cs
//  * Copyright (c) 2023 Qualcomm Technologies, Inc. and/or its subsidiaries. All rights reserved.
//  *
//  *
//  ******************************************************************************/

using System;
using QCHT.Interactions.Hands;

namespace QCHT.Interactions.Core
{
    public partial class XRHandTrackingManager
    {
        #region Subsystem

        [Obsolete(
            "Consider getting the subsystem with `XRHandTrackingSubsystem.GetSubsystemInManager()` in your script then call subsystem.Status instead.")]
        public HandTrackingStatus HandTrackingStatus => _subsystem?.Status ?? HandTrackingStatus.Idle;

        [Obsolete(
            "Consider getting the subsystem with `XRHandTrackingSubsystem.GetSubsystemInManager()` in your script then call subsystem.RightHand.Scale property instead.")]
        public float RightHandScale => _subsystem?.RightHand.Scale ?? 1f;

        [Obsolete(
            "Consider getting the subsystem with `XRHandTrackingSubsystem.GetSubsystemInManager()` in your script then call subsystem.LeftHand.Scale property instead.")]
        public float RightLeftScale => _subsystem?.LeftHand.Scale ?? 1f;

        [Obsolete(
            "Consider getting the subsystem with `XRHandTrackingSubsystem.GetSubsystemInManager()` in your script then call subsystem.Start() instead.")]
        public void StartHandTracking() => _subsystem?.Start();

        [Obsolete(
            "Consider getting the subsystem with `XRHandTrackingSubsystem.GetSubsystemInManager()` in your script then call subsystem.Stop() instead.")]
        public void StopHandTracking() => _subsystem?.Stop();

        #endregion

        #region HandSkin

        /// <summary>
        /// Try to set hand skin.
        /// Do nothing if the hand object is not ISkinnable.
        /// </summary>
        /// <param name="handedness"> Hand to skin </param>
        /// <param name="skin"> Skin to set. </param>
        [Obsolete(
            "Use the LeftHandSkin and RightHandSkin properties instead. This function will ever return true even if the skin has failed to be set.")]
        public bool SetHandSkin(XrHandedness handedness, HandSkin skin)
        {
            if (handedness == XrHandedness.XR_HAND_LEFT)
                LeftHandSkin = skin;
            else
                RightHandSkin = skin;

            return true;
        }

        /// <summary>
        /// Sets the left hand skin.
        /// Do nothing if the hand object is not ISkinnable.
        /// </summary>
        /// <param name="skin"> Skin to set. </param>
        [Obsolete("Use the LeftHandSkin property instead.")]
        public void SetLeftHandSkin(HandSkin skin) => LeftHandSkin = skin;

        /// <summary>
        /// Sets the right hand skin.
        /// Do nothing if the hand object is not ISkinnable.
        /// </summary>
        /// <param name="skin"> Skin to set. </param>
        [Obsolete("Use the RightHandSkin property instead.")]
        public void SetRightHandSkin(HandSkin skin) => RightHandSkin = skin;

        #endregion
    }
}