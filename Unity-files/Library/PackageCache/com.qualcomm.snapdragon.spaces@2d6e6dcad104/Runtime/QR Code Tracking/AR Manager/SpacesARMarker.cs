/******************************************************************************
 * File: SpacesARMarker.cs
 * Copyright (c) 2023 Qualcomm Technologies, Inc. and/or its subsidiaries. All rights reserved.
 *
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 ******************************************************************************/

using UnityEngine;
using UnityEngine.XR.ARFoundation;

namespace Qualcomm.Snapdragon.Spaces
{
    [DisallowMultipleComponent]
    public sealed class SpacesARMarker : ARTrackable<XRTrackedMarker, SpacesARMarker>
    {
        public bool IsMarkerDataAvailable { get; private set; }
        public string Data => IsMarkerDataAvailable ? _data : string.Empty;

        internal void TryGetMarkerData(XRQrCodeTrackingSubsystem subsystem)
        {
            IsMarkerDataAvailable = subsystem.TryGetMarkerData(sessionRelativeData.trackableId, out _data);
        }

        private string _data;
    }
}
